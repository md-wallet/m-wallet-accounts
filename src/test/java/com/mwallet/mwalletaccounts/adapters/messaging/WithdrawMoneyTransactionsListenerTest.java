package com.mwallet.mwalletaccounts.adapters.messaging;

import com.mwallet.mwalletaccounts.domain.Payout.PayoutRequest;
import com.mwallet.mwalletcommons.utils.AMQPUtils;
import com.mwallet.mwalletaccounts.adapters.persistence.accounts.AccountEntityJpaRepository;
import com.mwallet.mwalletaccounts.adapters.persistence.transactions.jpa.TransactionEntityJpaRepository;
import com.mwallet.mwalletaccounts.application.ports.in.transactions.WithdrawMoneyUseCase;
import com.mwallet.mwalletaccounts.application.ports.out.accounts.SaveAccountOutputPort;
import com.mwallet.mwalletaccounts.application.ports.out.payments.PaymentsOutputPort;
import com.mwallet.mwalletaccounts.application.ports.out.transactions.PublishTransactionOutputPort;
import com.mwallet.mwalletaccounts.application.ports.out.transactions.SaveTransactionOutputPort;
import com.mwallet.mwalletaccounts.config.RedisExtension;
import com.mwallet.mwalletaccounts.domain.Account;
import com.mwallet.mwalletaccounts.domain.Payout;
import com.mwallet.mwalletaccounts.domain.Transaction;
import com.mwallet.mwalletaccounts.infrastructure.stream.TransactionSink;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ActiveProfiles;

import java.math.BigDecimal;
import java.util.List;

import static com.mwallet.mwalletaccounts.domain.Account.AccountStatus.ACTIVE;
import static com.mwallet.mwalletaccounts.domain.Account.AccountType.TRUST_ACCOUNT;
import static com.mwallet.mwalletaccounts.domain.Account.AccountType.USER_ACCOUNT;
import static com.mwallet.mwalletaccounts.domain.Account.builder;
import static com.mwallet.mwalletaccounts.domain.Payout.PayoutRequest.PayoutRequestStatus.*;
import static com.mwallet.mwalletaccounts.domain.Transaction.ConfirmationStatus.PROCEED;
import static com.mwallet.mwalletaccounts.domain.Transaction.ConfirmationStatus.ROLLBACK;
import static com.mwallet.mwalletaccounts.domain.Transaction.PaymentMode.MPESA;
import static com.mwallet.mwalletaccounts.domain.Transaction.TransactionStatus.*;
import static com.mwallet.mwalletaccounts.domain.Transaction.TransactionType.WITHDRAW;
import static java.util.Comparator.comparing;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.groups.Tuple.tuple;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.*;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(RedisExtension.class)
class WithdrawMoneyTransactionsListenerTest {
    private Account srcAccount;
    private Account destAccount;
    private Transaction transaction;

    @Captor
    private ArgumentCaptor<?> captor;

    @Autowired
    private TransactionSink transactionSink;

    @SpyBean
    private SaveAccountOutputPort saveAccountOutputPort;
    @SpyBean
    private WithdrawMoneyUseCase withdrawMoneyUseCase;
    @SpyBean
    private SaveTransactionOutputPort saveTransactionOutputPort;
    @MockBean
    private PaymentsOutputPort paymentsOutputPort;
    @SpyBean
    private PublishTransactionOutputPort publishTransactionOutputPort;

    @Autowired
    private TransactionEntityJpaRepository transactionEntityJpaRepository;
    @Autowired
    private AccountEntityJpaRepository accountEntityJpaRepository;

    @BeforeEach
    void setUp() {
        srcAccount = builder()
                .linkId("TEST_LINK_ID_1")
                .accountId("ID_1")
                .accountStatus(ACTIVE)
                .accountType(USER_ACCOUNT)
                .availableBalance(new BigDecimal("1000"))
                .actualBalance(new BigDecimal("2000"))
                .build();
        destAccount = srcAccount.toBuilder()
                .accountId("ID_2")
                .accountType(TRUST_ACCOUNT).linkId("TEST_LINK_ID_2").build();

        srcAccount = saveAccountOutputPort.createAccount(srcAccount);
        destAccount = saveAccountOutputPort.createAccount(destAccount);

        transaction = Transaction.builder()
                .transactionRef("TEST_TRANSACTION_REF")
                .sourceAccount(srcAccount)
                .destinationAccount(destAccount)
                .amount(new BigDecimal("100"))
                .transactionCost(new BigDecimal("20"))
                .transactionType(WITHDRAW)
                .transactionStatus(Transaction.TransactionStatus.ACCEPTED)
                .paymentMode(MPESA)
                .billingAddress("TEST_BILLING_ADDRESS")
                .transactionDescription("TEST_TRANSACTION_DESC")
                .transactionDomainType("TEST_DOMAIN_TYPE")
                .confirmationType(Transaction.ConfirmationType.INTERNAL)
                .confirmationStatus(Transaction.ConfirmationStatus.NONE)
                .build();

        transaction = saveTransactionOutputPort.createTransaction(transaction);
    }

    @AfterEach
    void tearDown() {
        accountEntityJpaRepository.deleteAll();
        transactionEntityJpaRepository.deleteAll();
    }

    @Test
    @DisplayName("WITHDRAW: ACCEPTED --> PREPARED")
    void receiveTransaction__prepareWithdrawMoney(){
        //GIVEN
        Transaction givenTransaction = transaction;

        //WHEN
        transactionSink.withdrawMoneyTransactionsInput().send(AMQPUtils.buildMessageFrom(givenTransaction));

        //THEN
        then(withdrawMoneyUseCase).should().prepareWithdrawMoney(any(Transaction.class));
        then(saveTransactionOutputPort).should().updateTransaction((Transaction) captor.capture());
        Transaction updatedTransaction = (Transaction) captor.getValue();

        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(updatedTransaction)
                .isNotNull()
                .hasNoNullFieldsOrProperties()
                .extracting(Transaction::getVersion, Transaction::getTransactionStatus, Transaction::getAmount, Transaction::getTransactionCost)
                .containsExactly(0L, PREPARED, new BigDecimal("100.00"), new BigDecimal("20.00"));

        softly.assertThat(updatedTransaction)
                .extracting(Transaction::getSourceAccount, Transaction::getDestinationAccount)
                .flatExtracting("availableBalance", "actualBalance")
                .containsExactly(new BigDecimal("880.00"), new BigDecimal("2000.00"), new BigDecimal("1000.00"), new BigDecimal("2100.00"));
        softly.assertAll();
    }

    @Test
    @DisplayName("DEPOSIT: PREPARED --> PAYOUT_REQUESTED  (PAYOUT_REQUEST_ACCEPTED)")
    void receiveTransaction__requestWithdrawMoneyPayout_ACCEPTED(){
        //GIVEN
        Transaction givenTransaction = transaction.toBuilder().transactionStatus(PREPARED).build();
        saveTransactionOutputPort.updateTransaction(givenTransaction);
        reset(saveTransactionOutputPort);

        Payout payoutRequestResult = PayoutRequest.builder()
                .narration(PAYOUT_REQUEST_ACCEPTED.name() + ":" + "TEST_NARRATION")
                .payoutRequestStatus(PAYOUT_REQUEST_ACCEPTED)
                .amount(givenTransaction.getAmount())
                .transactionRef(givenTransaction.getTransactionRef())
                .billingAddress(givenTransaction.getBillingAddress())
                .build();

        given(paymentsOutputPort.requestPayout(any(Transaction.class))).willReturn((PayoutRequest) payoutRequestResult);

        //WHEN
        transactionSink.withdrawMoneyTransactionsInput().send(AMQPUtils.buildMessageFrom(givenTransaction));

        //THEN
        then(withdrawMoneyUseCase).should().requestWithdrawMoneyPayout(any(Transaction.class));
        then(saveTransactionOutputPort).should().updateTransaction((Transaction) captor.capture());
        Transaction updatedTransaction = (Transaction) captor.getValue();

        assertThat(updatedTransaction)
                .isNotNull()
                .hasNoNullFieldsOrProperties()
                .extracting(Transaction::getTransactionStatus)
                .isEqualTo(PAYOUT_REQUESTED);
    }

    @Test
    @DisplayName("DEPOSIT: PREPARED --> PAYOUT_REQUESTED  (PAYOUT_REQUEST_REJECTED)")
    void receiveTransaction__requestWithdrawMoneyPayout_REJECTED(){
        //GIVEN
        Transaction givenTransaction = transaction.toBuilder().transactionStatus(PREPARED).build();
        saveTransactionOutputPort.updateTransaction(givenTransaction);
        reset(saveTransactionOutputPort);

        Payout payoutRequestResult = PayoutRequest.builder()
                .narration(PAYOUT_REQUEST_REJECTED.name() + ":" + "TEST_NARRATION")
                .payoutRequestStatus(PAYOUT_REQUEST_REJECTED)
                .amount(givenTransaction.getAmount())
                .transactionRef(givenTransaction.getTransactionRef())
                .billingAddress(givenTransaction.getBillingAddress())
                .build();
        given(paymentsOutputPort.requestPayout(any(Transaction.class))).willReturn((PayoutRequest) payoutRequestResult);

        //WHEN
        transactionSink.withdrawMoneyTransactionsInput().send(AMQPUtils.buildMessageFrom(givenTransaction));

        //THEN
        then(withdrawMoneyUseCase).should().requestWithdrawMoneyPayout(any(Transaction.class));
        then(saveTransactionOutputPort).should(times(2)).updateTransaction((Transaction) captor.capture());

        Transaction updatedTransactionOnPayoutRequest = (Transaction) captor.getAllValues().get(0);
        Transaction updatedTransactionOnRollback = (Transaction) captor.getAllValues().get(1);

        assertThat(List.of(updatedTransactionOnPayoutRequest,updatedTransactionOnRollback))
                .extracting(Transaction::getTransactionStatus,Transaction::getConfirmationStatus)
                .containsExactly(tuple(PAYOUT_REQUESTED, Transaction.ConfirmationStatus.NONE), tuple(FAILED, ROLLBACK));
    }

    @Test
    @DisplayName("DEPOSIT: PREPARED --> PAYOUT_REQUESTED  (PAYOUT_REQUEST_FAILED)")
    void receiveTransaction__requestWithdrawMoneyPayout_FAILED(){
        //GIVEN
        Transaction givenTransaction = transaction.toBuilder().transactionStatus(PREPARED).build();
        saveTransactionOutputPort.updateTransaction(givenTransaction);
        reset(saveTransactionOutputPort);

        Payout payoutRequestResult = PayoutRequest.builder()
                .narration(PAYOUT_REQUEST_FAILED.name() + ":" + "TEST_NARRATION")
                .payoutRequestStatus(PAYOUT_REQUEST_FAILED)
                .amount(givenTransaction.getAmount())
                .transactionRef(givenTransaction.getTransactionRef())
                .billingAddress(givenTransaction.getBillingAddress())
                .build();

        given(paymentsOutputPort.requestPayout(any(Transaction.class))).willReturn((PayoutRequest) payoutRequestResult);
        doNothing().when(publishTransactionOutputPort).publishDelayedTransaction(any(Transaction.class),anyLong(),anyLong());

        //WHEN
        transactionSink.withdrawMoneyTransactionsInput().send(AMQPUtils.buildMessageFrom(givenTransaction));

        //THEN
        then(withdrawMoneyUseCase).should().requestWithdrawMoneyPayout(any(Transaction.class));
        then(saveTransactionOutputPort).should(times(2)).updateTransaction((Transaction) captor.capture());
        then(publishTransactionOutputPort).should().publishDelayedTransaction((Transaction)captor.capture(),(Long)captor.capture(),(Long) captor.capture());

        Transaction updatedTransactionOnPaymentRequest = (Transaction) captor.getAllValues().get(0);
        Transaction updatedTransactionOnFail = (Transaction) captor.getAllValues().get(1);
        Transaction republishedTransaction = (Transaction) captor.getAllValues().get(2);
        Long exponentialDelay = (Long) captor.getAllValues().get(3);
        Long retryCount = (Long) captor.getAllValues().get(4);

        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(List.of(updatedTransactionOnPaymentRequest,updatedTransactionOnFail,republishedTransaction))
                .extracting(Transaction::getTransactionStatus,Transaction::getConfirmationStatus)
                .containsExactly(tuple(PAYOUT_REQUESTED, Transaction.ConfirmationStatus.NONE), tuple(PREPARED, Transaction.ConfirmationStatus.NONE),tuple(PREPARED, Transaction.ConfirmationStatus.NONE));

        softly.assertThat(exponentialDelay).isNotNull().isEqualTo(0);
        softly.assertThat(retryCount).isNotNull().isEqualTo(1);

        softly.assertAll();
    }

    @Test
    @DisplayName("WITHDRAW: PAYOUT_VERIFIED --> CONFIRMED (PROCEED)")
    void receiveTransaction__confirmWithdrawMoney_PROCEED(){
        //GIVEN
        Transaction givenTransaction = transaction.toBuilder().transactionStatus(PAYOUT_VERIFIED).confirmationStatus(PROCEED).build();
        saveTransactionOutputPort.updateTransaction(givenTransaction);
        reset(saveTransactionOutputPort);

        //WHEN
        transactionSink.withdrawMoneyTransactionsInput().send(AMQPUtils.buildMessageFrom(givenTransaction));

        //THEN
        then(saveTransactionOutputPort).should().updateTransaction((Transaction) captor.capture());

        Transaction updatedTransaction = (Transaction) captor.getValue();

        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(updatedTransaction)
                .isNotNull()
                .extracting(Transaction::getTransactionStatus, Transaction::getConfirmationStatus)
                .containsExactly(CONFIRMED, PROCEED);

        softly.assertThat(updatedTransaction)
                .extracting(Transaction::getSourceAccount, Transaction::getDestinationAccount)
                .flatExtracting("availableBalance", "actualBalance")
                .usingElementComparator(comparing(o -> ((BigDecimal) o)))
                .containsExactly(BigDecimal.valueOf(1000), BigDecimal.valueOf(1880), BigDecimal.valueOf(1100), BigDecimal.valueOf(2000));

        softly.assertAll();
    }

    @Test
    @DisplayName("WITHDRAW: PAYOUT_VERIFIED --> CONFIRMED (ROLLBACK)")
    void receiveTransaction__confirmWithdrawMoney_ROLLBACK(){
        Transaction givenTransaction = transaction.toBuilder().transactionStatus(PAYOUT_VERIFIED).confirmationStatus(ROLLBACK).build();
        saveTransactionOutputPort.updateTransaction(givenTransaction);
        reset(saveTransactionOutputPort);

        //WHEN
        transactionSink.withdrawMoneyTransactionsInput().send(AMQPUtils.buildMessageFrom(givenTransaction));

        //THEN
        then(saveTransactionOutputPort).should().updateTransaction((Transaction) captor.capture());

        Transaction updatedTransaction = (Transaction) captor.getValue();
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(updatedTransaction)
                .isNotNull()
                .extracting(Transaction::getTransactionStatus, Transaction::getConfirmationStatus)
                .containsExactly(FAILED, ROLLBACK);

        softly.assertThat(updatedTransaction)
                .extracting(Transaction::getSourceAccount, Transaction::getDestinationAccount)
                .flatExtracting("availableBalance", "actualBalance")
                .usingElementComparator(comparing(o -> ((BigDecimal) o)))
                .containsExactly(BigDecimal.valueOf(1120), BigDecimal.valueOf(2000), BigDecimal.valueOf(1000), BigDecimal.valueOf(1900));

        softly.assertAll();
    }

    @Test
    @DisplayName("WITHDRAW: CONFIRMED --> COMPLETED")
    void receiveTransaction__completeWithdrawMoney(){
        Transaction givenTransaction = transaction.toBuilder().transactionStatus(CONFIRMED).build();
        saveTransactionOutputPort.updateTransaction(givenTransaction);
        reset(saveTransactionOutputPort);

        //WHEN
        transactionSink.withdrawMoneyTransactionsInput().send(AMQPUtils.buildMessageFrom(givenTransaction));

        //THEN
        then(withdrawMoneyUseCase).should().completeWithdrawMoney(any(Transaction.class));
        then(saveTransactionOutputPort).should().updateTransaction((Transaction)captor.capture());
        Transaction updatedTransaction = (Transaction) captor.getValue();

        assertThat(updatedTransaction)
                .isNotNull()
                .hasNoNullFieldsOrProperties()
                .extracting(Transaction::getTransactionStatus)
                .isEqualTo(COMPLETED);
    }
}
