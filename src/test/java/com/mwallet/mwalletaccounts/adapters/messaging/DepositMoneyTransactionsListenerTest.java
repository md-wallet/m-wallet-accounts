package com.mwallet.mwalletaccounts.adapters.messaging;

import com.mwallet.mwalletaccounts.domain.Payment.PaymentRequest;
import com.mwallet.mwalletcommons.utils.AMQPUtils;
import com.mwallet.mwalletaccounts.adapters.persistence.accounts.AccountEntityJpaRepository;
import com.mwallet.mwalletaccounts.adapters.persistence.transactions.jpa.TransactionEntityJpaRepository;
import com.mwallet.mwalletaccounts.application.ports.in.transactions.DepositMoneyUseCase;
import com.mwallet.mwalletaccounts.application.ports.out.accounts.SaveAccountOutputPort;
import com.mwallet.mwalletaccounts.application.ports.out.payments.PaymentsOutputPort;
import com.mwallet.mwalletaccounts.application.ports.out.transactions.PublishTransactionOutputPort;
import com.mwallet.mwalletaccounts.application.ports.out.transactions.SaveTransactionOutputPort;
import com.mwallet.mwalletaccounts.config.RedisExtension;
import com.mwallet.mwalletaccounts.domain.Account;
import com.mwallet.mwalletaccounts.domain.Payment;
import com.mwallet.mwalletaccounts.domain.Transaction;
import com.mwallet.mwalletaccounts.domain.Transaction.ConfirmationStatus;
import com.mwallet.mwalletaccounts.infrastructure.stream.TransactionSink;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ActiveProfiles;

import java.math.BigDecimal;
import java.util.List;

import static com.mwallet.mwalletaccounts.domain.Account.AccountStatus.ACTIVE;
import static com.mwallet.mwalletaccounts.domain.Account.AccountType.TRUST_ACCOUNT;
import static com.mwallet.mwalletaccounts.domain.Account.AccountType.USER_ACCOUNT;
import static com.mwallet.mwalletaccounts.domain.Account.builder;
import static com.mwallet.mwalletaccounts.domain.Payment.PaymentRequest.PaymentRequestStatus.PAYMENT_REQUEST_FAILED;
import static com.mwallet.mwalletaccounts.domain.Payment.PaymentRequest.PaymentRequestStatus.PAYMENT_REQUEST_REJECTED;
import static com.mwallet.mwalletaccounts.domain.Transaction.ConfirmationStatus.PROCEED;
import static com.mwallet.mwalletaccounts.domain.Transaction.ConfirmationStatus.ROLLBACK;
import static com.mwallet.mwalletaccounts.domain.Transaction.PaymentMode.MPESA;
import static com.mwallet.mwalletaccounts.domain.Transaction.TransactionStatus.*;
import static com.mwallet.mwalletaccounts.domain.Transaction.TransactionType.DEPOSIT;
import static java.util.Comparator.comparing;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.groups.Tuple.tuple;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.*;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(RedisExtension.class)
class DepositMoneyTransactionsListenerTest {

    private Account srcAccount;
    private Account destAccount;
    private Transaction transaction;

    @Captor
    private ArgumentCaptor<?> captor;

    @Autowired
    private TransactionSink transactionSink;

    @SpyBean
    private SaveAccountOutputPort saveAccountOutputPort;
    @SpyBean
    private DepositMoneyUseCase depositMoneyUseCase;
    @SpyBean
    private SaveTransactionOutputPort saveTransactionOutputPort;
    @MockBean
    private PaymentsOutputPort paymentsOutputPort;
    @SpyBean
    private PublishTransactionOutputPort publishTransactionOutputPort;

    @Autowired
    private TransactionEntityJpaRepository transactionEntityJpaRepository;
    @Autowired
    private AccountEntityJpaRepository accountEntityJpaRepository;

    @BeforeEach
    void setUp() {
        srcAccount = builder()
                .linkId("TEST_LINK_ID_1")
                .accountId("ID_1")
                .accountStatus(ACTIVE)
                .accountType(TRUST_ACCOUNT)
                .availableBalance(new BigDecimal("1000"))
                .actualBalance(new BigDecimal("2000"))
                .build();
        destAccount = srcAccount.toBuilder().accountId("ID_2").accountType(USER_ACCOUNT).linkId("TEST_LINK_ID_2").build();

        srcAccount = saveAccountOutputPort.createAccount(srcAccount);
        destAccount = saveAccountOutputPort.createAccount(destAccount);

        transaction = Transaction.builder()
                .transactionRef("TEST_TRANSACTION_REF")
                .sourceAccount(srcAccount)
                .destinationAccount(destAccount)
                .amount(new BigDecimal("100"))
                .transactionCost(BigDecimal.ZERO)
                .transactionType(DEPOSIT)
                .transactionStatus(Transaction.TransactionStatus.ACCEPTED)
                .paymentMode(MPESA)
                .billingAddress("TEST_BILLING_ADDRESS")
                .transactionDescription("TEST_TRANSACTION_DESC")
                .transactionDomainType("TEST_DOMAIN_TYPE")
                .confirmationType(Transaction.ConfirmationType.INTERNAL)
                .confirmationStatus(ConfirmationStatus.NONE)
                .build();

        transaction = saveTransactionOutputPort.createTransaction(transaction);
    }

    @AfterEach
    void tearDown() {
        accountEntityJpaRepository.deleteAll();
        transactionEntityJpaRepository.deleteAll();
    }

    @Test
    @DisplayName("DEPOSIT: ACCEPTED --> PREPARED")
    void receiveTransaction__prepareDepositMoney() {
        //GIVEN
        Transaction givenTransaction = transaction;

        //WHEN
        transactionSink.depositMoneyTransactionsInput().send(AMQPUtils.buildMessageFrom(givenTransaction));

        //THEN
        then(depositMoneyUseCase).should().prepareDepositMoney(any(Transaction.class));
        then(saveTransactionOutputPort).should().updateTransaction((Transaction) captor.capture());
        Transaction updatedTransaction = (Transaction) captor.getValue();

        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(updatedTransaction)
                .isNotNull()
                .hasNoNullFieldsOrProperties()
                .extracting(Transaction::getVersion, Transaction::getTransactionStatus, Transaction::getAmount, Transaction::getTransactionCost)
                .usingComparatorForType(comparing(o -> ((BigDecimal) o)),BigDecimal.class)
                .containsExactly(0L, PREPARED, new BigDecimal("100.00"), BigDecimal.ZERO);

        softly.assertThat(updatedTransaction)
                .extracting(Transaction::getSourceAccount, Transaction::getDestinationAccount)
                .flatExtracting("availableBalance", "actualBalance")
                .containsExactly(new BigDecimal("900.00"), new BigDecimal("2000.00"), new BigDecimal("1000.00"), new BigDecimal("2100.00"));
        softly.assertAll();
    }

    @Test
    @DisplayName("DEPOSIT: PREPARED --> PAYMENT_REQUESTED  (PAYMENT_REQUEST_ACCEPTED)")
    void receiveTransaction__requestDepositMoneyPayment_ACCEPTED() {
        //GIVEN
        Transaction givenTransaction = transaction.toBuilder().transactionStatus(PREPARED).build();
        saveTransactionOutputPort.updateTransaction(givenTransaction);
        reset(saveTransactionOutputPort);

        Payment paymentResponse = PaymentRequest.builder()
                .narration(PaymentRequest.PaymentRequestStatus.PAYMENT_REQUEST_ACCEPTED.name() + ":" + "TEST_NARRATION")
                .paymentRequestStatus(PaymentRequest.PaymentRequestStatus.PAYMENT_REQUEST_ACCEPTED)
                .amount(givenTransaction.getAmount())
                .transactionRef(givenTransaction.getTransactionRef())
                .billingAddress(givenTransaction.getBillingAddress())
                .build();
        given(paymentsOutputPort.requestPayment(any(Transaction.class))).willReturn((PaymentRequest) paymentResponse);

        //WHEN
        transactionSink.depositMoneyTransactionsInput().send(AMQPUtils.buildMessageFrom(givenTransaction));

        //THEN
        then(depositMoneyUseCase).should().requestDepositMoneyPayment(any(Transaction.class));
        then(saveTransactionOutputPort).should().updateTransaction((Transaction) captor.capture());
        Transaction updatedTransaction = (Transaction) captor.getValue();

        assertThat(updatedTransaction)
                .isNotNull()
                .hasNoNullFieldsOrProperties()
                .extracting(Transaction::getTransactionStatus)
                .isEqualTo(PAYMENT_REQUESTED);
    }

    @Test
    @DisplayName("DEPOSIT: PREPARED --> PAYMENT_REQUESTED --> FAILED  (PAYMENT_REQUEST_REJECTED)")
    void receiveTransaction__requestDepositMoneyPayment_REJECTED() {
        //GIVEN
        Transaction givenTransaction = transaction.toBuilder().transactionStatus(PREPARED).build();
        saveTransactionOutputPort.updateTransaction(givenTransaction);
        reset(saveTransactionOutputPort);

        Payment paymentResponse = PaymentRequest.builder()
                .narration(PAYMENT_REQUEST_REJECTED.name() + ":" + "TEST_NARRATION")
                .paymentRequestStatus(PAYMENT_REQUEST_REJECTED)
                .amount(givenTransaction.getAmount())
                .transactionRef(givenTransaction.getTransactionRef())
                .billingAddress(givenTransaction.getBillingAddress())
                .build();
        given(paymentsOutputPort.requestPayment(any(Transaction.class))).willReturn((PaymentRequest) paymentResponse);


        //WHEN
        transactionSink.depositMoneyTransactionsInput().send(AMQPUtils.buildMessageFrom(givenTransaction));

        //THEN
        then(depositMoneyUseCase).should().requestDepositMoneyPayment(any(Transaction.class));
        then(saveTransactionOutputPort).should(times(2)).updateTransaction((Transaction) captor.capture());

        Transaction updatedTransactionOnPaymentRequest = (Transaction) captor.getAllValues().get(0);
        Transaction updatedTransactionOnRollback = (Transaction) captor.getAllValues().get(1);

        assertThat(List.of(updatedTransactionOnPaymentRequest,updatedTransactionOnRollback))
                .extracting(Transaction::getTransactionStatus,Transaction::getConfirmationStatus)
                .containsExactly(tuple(PAYMENT_REQUESTED,ConfirmationStatus.NONE), tuple(FAILED, ROLLBACK));

    }

    @Test
    @DisplayName("DEPOSIT: PREPARED --> PAYMENT_REQUESTED --> PREPARED  (PAYMENT_REQUEST_FAILED)")
    void receiveTransaction__requestDepositMoneyPayment_FAILED() {
        //GIVEN
        Transaction givenTransaction = transaction.toBuilder().transactionStatus(PREPARED).build();
        saveTransactionOutputPort.updateTransaction(givenTransaction);
        reset(saveTransactionOutputPort);

        PaymentRequest paymentResponse = PaymentRequest.builder()
                .narration(PAYMENT_REQUEST_FAILED.name() + ":" + "TEST_NARRATION")
                .paymentRequestStatus(PAYMENT_REQUEST_FAILED)
                .amount(givenTransaction.getAmount())
                .transactionRef(givenTransaction.getTransactionRef())
                .billingAddress(givenTransaction.getBillingAddress())
                .build();

        given(paymentsOutputPort.requestPayment(any(Transaction.class))).willReturn(paymentResponse);
        doNothing().when(publishTransactionOutputPort).publishDelayedTransaction(any(Transaction.class),anyLong(),anyLong());

        //WHEN
        transactionSink.depositMoneyTransactionsInput().send(AMQPUtils.buildMessageFrom(givenTransaction));

        //THEN
        then(depositMoneyUseCase).should().requestDepositMoneyPayment(any(Transaction.class));
        then(saveTransactionOutputPort).should(times(2)).updateTransaction((Transaction) captor.capture());
        then(publishTransactionOutputPort).should().publishDelayedTransaction((Transaction)captor.capture(),(Long)captor.capture(),(Long) captor.capture());

        Transaction updatedTransactionOnPaymentRequest = (Transaction) captor.getAllValues().get(0);
        Transaction updatedTransactionOnFail = (Transaction) captor.getAllValues().get(1);
        Transaction republishedTransaction = (Transaction) captor.getAllValues().get(2);
        Long exponentialDelay = (Long) captor.getAllValues().get(3);
        Long retryCount = (Long) captor.getAllValues().get(4);

        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(List.of(updatedTransactionOnPaymentRequest,updatedTransactionOnFail,republishedTransaction))
                .extracting(Transaction::getTransactionStatus,Transaction::getConfirmationStatus)
                .containsExactly(tuple(PAYMENT_REQUESTED, ConfirmationStatus.NONE), tuple(PREPARED, ConfirmationStatus.NONE),tuple(PREPARED, ConfirmationStatus.NONE));

        softly.assertThat(exponentialDelay).isNotNull().isEqualTo(0);
        softly.assertThat(retryCount).isNotNull().isEqualTo(1);

        softly.assertAll();
    }

    @Test
    @DisplayName("DEPOSIT: PAYMENT_VERIFIED --> CONFIRMED (PROCEED)")
    void receiveTransaction__confirmDepositMoney_PROCEED(){
        //GIVEN
        Transaction givenTransaction = transaction.toBuilder().transactionStatus(PAYMENT_VERIFIED).confirmationStatus(PROCEED).build();
        saveTransactionOutputPort.updateTransaction(givenTransaction);
        reset(saveTransactionOutputPort);

        //WHEN
        transactionSink.depositMoneyTransactionsInput().send(AMQPUtils.buildMessageFrom(givenTransaction));

        //THEN
        then(saveTransactionOutputPort).should().updateTransaction((Transaction) captor.capture());

        Transaction updatedTransaction = (Transaction) captor.getValue();

        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(updatedTransaction)
                .isNotNull()
                .extracting(Transaction::getTransactionStatus, Transaction::getConfirmationStatus)
                .containsExactly(CONFIRMED, PROCEED);

        softly.assertThat(updatedTransaction)
                .extracting(Transaction::getSourceAccount, Transaction::getDestinationAccount)
                .flatExtracting("availableBalance", "actualBalance")
                .usingElementComparator(comparing(o -> ((BigDecimal) o)))
                .containsExactly(BigDecimal.valueOf(1000), BigDecimal.valueOf(1900), BigDecimal.valueOf(1100), BigDecimal.valueOf(2000));

        softly.assertAll();
    }

    @Test
    @DisplayName("DEPOSIT: PAYMENT_VERIFIED --> CONFIRMED (ROLLBACK)")
    void receiveTransaction__confirmDepositMoney_ROLLBACK(){
        Transaction givenTransaction = transaction.toBuilder().transactionStatus(PAYMENT_VERIFIED).confirmationStatus(ROLLBACK).build();
        saveTransactionOutputPort.updateTransaction(givenTransaction);
        reset(saveTransactionOutputPort);

        //WHEN
        transactionSink.depositMoneyTransactionsInput().send(AMQPUtils.buildMessageFrom(givenTransaction));

        //THEN
        then(saveTransactionOutputPort).should().updateTransaction((Transaction) captor.capture());

        Transaction updatedTransaction = (Transaction) captor.getValue();

        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(updatedTransaction)
                .isNotNull()
                .extracting(Transaction::getTransactionStatus, Transaction::getConfirmationStatus)
                .containsExactly(FAILED, ROLLBACK);

        softly.assertThat(updatedTransaction)
                .extracting(Transaction::getSourceAccount, Transaction::getDestinationAccount)
                .flatExtracting("availableBalance", "actualBalance")
                .usingElementComparator(comparing(o -> ((BigDecimal) o)))
                .containsExactly(BigDecimal.valueOf(1100), BigDecimal.valueOf(2000), BigDecimal.valueOf(1000), BigDecimal.valueOf(1900));

        softly.assertAll();
    }

    @Test
    @DisplayName("DEPOSIT: CONFIRMED --> COMPLETED")
    void receiveTransaction__completeDepositMoney(){
        Transaction givenTransaction = transaction.toBuilder().transactionStatus(CONFIRMED).build();
        saveTransactionOutputPort.updateTransaction(givenTransaction);
        reset(saveTransactionOutputPort);

        //WHEN
        transactionSink.depositMoneyTransactionsInput().send(AMQPUtils.buildMessageFrom(givenTransaction));

        //THEN
        then(depositMoneyUseCase).should().completeDepositMoney(any(Transaction.class));
        then(saveTransactionOutputPort).should().updateTransaction((Transaction)captor.capture());
        Transaction updatedTransaction = (Transaction) captor.getValue();

        assertThat(updatedTransaction)
                .isNotNull()
                .hasNoNullFieldsOrProperties()
                .extracting(Transaction::getTransactionStatus)
                .isEqualTo(COMPLETED);
    }


}
