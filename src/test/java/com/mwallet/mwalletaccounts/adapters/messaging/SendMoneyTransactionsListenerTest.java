package com.mwallet.mwalletaccounts.adapters.messaging;

import com.mwallet.mwalletcommons.utils.AMQPUtils;
import com.mwallet.mwalletaccounts.adapters.persistence.accounts.AccountEntityJpaRepository;
import com.mwallet.mwalletaccounts.adapters.persistence.transactions.jpa.TransactionEntityJpaRepository;
import com.mwallet.mwalletaccounts.application.ports.in.transactions.SendMoneyUseCase;
import com.mwallet.mwalletaccounts.application.ports.out.accounts.SaveAccountOutputPort;
import com.mwallet.mwalletaccounts.application.ports.out.transactions.SaveTransactionOutputPort;
import com.mwallet.mwalletaccounts.config.RedisExtension;
import com.mwallet.mwalletaccounts.domain.Account;
import com.mwallet.mwalletaccounts.domain.Transaction;
import com.mwallet.mwalletaccounts.infrastructure.stream.TransactionSink;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ActiveProfiles;

import java.math.BigDecimal;

import static com.mwallet.mwalletaccounts.domain.Account.AccountStatus.ACTIVE;
import static com.mwallet.mwalletaccounts.domain.Account.AccountType.JOINT_ACCOUNT;
import static com.mwallet.mwalletaccounts.domain.Account.AccountType.USER_ACCOUNT;
import static com.mwallet.mwalletaccounts.domain.Account.builder;
import static com.mwallet.mwalletaccounts.domain.Transaction.PaymentMode.WALLET;
import static com.mwallet.mwalletaccounts.domain.Transaction.TransactionStatus.*;
import static com.mwallet.mwalletaccounts.domain.Transaction.TransactionType.SEND_MONEY;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.reset;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(RedisExtension.class)
class SendMoneyTransactionsListenerTest {
    private Account srcAccount;
    private Account destAccount;
    private Transaction transaction;

    @Captor
    private ArgumentCaptor<?> captor;

    @Autowired
    private TransactionSink transactionSink;

    @SpyBean
    private SaveAccountOutputPort saveAccountOutputPort;
    @SpyBean
    private SendMoneyUseCase sendMoneyUseCase;
    @SpyBean
    private SaveTransactionOutputPort saveTransactionOutputPort;
    @Autowired
    private TransactionEntityJpaRepository transactionEntityJpaRepository;
    @Autowired
    private AccountEntityJpaRepository accountEntityJpaRepository;


    @BeforeEach
    void setUp() {
        srcAccount = builder()
                .linkId("1")
                .accountId("ID_1")
                .accountStatus(ACTIVE)
                .accountType(USER_ACCOUNT)
                .availableBalance(new BigDecimal("1000"))
                .actualBalance(new BigDecimal("2000"))
                .build();
        destAccount = srcAccount.toBuilder().accountId("ID_2").linkId("20").accountType(JOINT_ACCOUNT).build();

        srcAccount = saveAccountOutputPort.createAccount(srcAccount);
        destAccount = saveAccountOutputPort.createAccount(destAccount);

        transaction = Transaction.builder()
                .transactionRef("FSNFSJDNFSK")
                .sourceAccount(srcAccount)
                .destinationAccount(destAccount)
                .amount(new BigDecimal("100"))
                .transactionCost(new BigDecimal("20"))
                .transactionType(SEND_MONEY)
                .transactionStatus(Transaction.TransactionStatus.ACCEPTED)
                .paymentMode(WALLET)
                .billingAddress("TEST_BILLING_ADDRESS")
                .transactionDescription("TEST_TRANSACTION_DESC")
                .transactionDomainType("TEST_DOMAIN_TYPE")
                .confirmationType(Transaction.ConfirmationType.INTERNAL)
                .confirmationStatus(Transaction.ConfirmationStatus.NONE)
                .build();

        transaction = saveTransactionOutputPort.createTransaction(transaction);
    }

    @AfterEach
    void tearDown() {
        accountEntityJpaRepository.deleteAll();
        transactionEntityJpaRepository.deleteAll();
    }


    @Test
    @DisplayName("SEND_MONEY: ACCEPTED --> PREPARED")
    void receiveTransaction__prepareSendMoney() {
        //GIVEN
        Transaction givenTransaction = transaction;

        //WHEN
        transactionSink.sendMoneyTransactionsInput().send(AMQPUtils.buildMessageFrom(givenTransaction));

        //THEN
        then(sendMoneyUseCase).should().prepareSendMoney(any(Transaction.class));
        then(saveTransactionOutputPort).should().updateTransaction((Transaction) captor.capture());
        Transaction updatedTransaction = (Transaction) captor.getValue();

        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(updatedTransaction)
                .isNotNull()
                .hasNoNullFieldsOrProperties()
                .extracting(Transaction::getVersion,Transaction::getTransactionStatus,Transaction::getAmount,Transaction::getTransactionCost)
                .containsExactly(0L, PREPARED,new BigDecimal("100.00"),new BigDecimal("20.00"));

        softly.assertThat(updatedTransaction)
                .extracting(Transaction::getSourceAccount,Transaction::getDestinationAccount)
                .flatExtracting("availableBalance","actualBalance")
                .containsExactly(new BigDecimal("880.00"),new BigDecimal("2000.00"),new BigDecimal("1000.00"),new BigDecimal("2100.00"));
        softly.assertAll();
    }

    @Test
    @DisplayName("SEND_MONEY: PREPARED --> CONFIRMED")
    void receiveTransaction__confirmSendMoney(){
        //GIVEN
        srcAccount = srcAccount.toBuilder().availableBalance(new BigDecimal("880.00")).build();
        destAccount = destAccount.toBuilder().actualBalance(new BigDecimal("2100.00")).build();
        saveAccountOutputPort.updateAccount(srcAccount);
        saveAccountOutputPort.updateAccount(destAccount);

        Transaction givenTransaction = transaction.toBuilder().transactionStatus(PREPARED).build();
        saveTransactionOutputPort.updateTransaction(givenTransaction);
        reset(saveTransactionOutputPort);

        //WHEN
        transactionSink.sendMoneyTransactionsInput().send(AMQPUtils.buildMessageFrom(givenTransaction));

        //THEN
        then(sendMoneyUseCase).should().confirmSendMoney(anyString());
        then(saveTransactionOutputPort).should().updateTransaction((Transaction)captor.capture());
        Transaction updatedTransaction = (Transaction) captor.getValue();

        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(updatedTransaction)
                .isNotNull()
                .hasNoNullFieldsOrProperties()
                .extracting(Transaction::getVersion,Transaction::getTransactionStatus,Transaction::getAmount,Transaction::getTransactionCost)
                .containsExactly(1L, CONFIRMED,new BigDecimal("100.00"),new BigDecimal("20.00"));

        softly.assertThat(updatedTransaction)
                .extracting(Transaction::getSourceAccount,Transaction::getDestinationAccount)
                .flatExtracting("availableBalance","actualBalance")
                .containsExactly(new BigDecimal("880.00"),new BigDecimal("1880.00"),new BigDecimal("1100.00"),new BigDecimal("2100.00"));
        softly.assertAll();
    }

    @Test
    @DisplayName("SEND_MONEY: CONFIRMED --> COMPLETED")
    void receiveTransaction__completeSendMoney(){
        //GIVEN
        Transaction givenTransaction = transaction.toBuilder().transactionStatus(CONFIRMED).build();
        saveTransactionOutputPort.updateTransaction(givenTransaction);
        reset(saveTransactionOutputPort);

        //WHEN
        transactionSink.sendMoneyTransactionsInput().send(AMQPUtils.buildMessageFrom(givenTransaction));

        //THEN
        then(sendMoneyUseCase).should().completeSendMoney(any(Transaction.class));
        then(saveTransactionOutputPort).should().updateTransaction((Transaction)captor.capture());
        Transaction updatedTransaction = (Transaction) captor.getValue();

        assertThat(updatedTransaction)
                .isNotNull()
                .hasNoNullFieldsOrProperties()
                .extracting(Transaction::getTransactionStatus)
                .isEqualTo(COMPLETED);

    }
}
