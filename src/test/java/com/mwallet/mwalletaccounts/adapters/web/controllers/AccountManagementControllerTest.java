package com.mwallet.mwalletaccounts.adapters.web.controllers;

import com.mwallet.mwalletaccounts.adapters.web.dtos.accounts.CreateAccountDTO;
import com.mwallet.mwalletaccounts.application.ports.in.accounts.CreateAccountUseCase;
import com.mwallet.mwalletaccounts.application.ports.in.accounts.GetAccountUseCase;
import com.mwallet.mwalletaccounts.application.ports.in.accounts.UpdateAccountStatusUseCase;
import com.mwallet.mwalletaccounts.config.RedisExtension;
import com.mwallet.mwalletaccounts.domain.Account;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.List;

import static com.mwallet.mwalletcommons.utils.ObjectUtils.writeJson;
import static com.mwallet.mwalletaccounts.domain.Account.builder;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.jwt;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static org.hamcrest.Matchers.is;


@ActiveProfiles("test")
@AutoConfigureMockMvc
@SpringBootTest
@ExtendWith(RedisExtension.class)
class AccountManagementControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean(extraInterfaces = {CreateAccountUseCase.class, UpdateAccountStatusUseCase.class})
    private GetAccountUseCase getAccountUseCase;
    @MockBean(extraInterfaces = {GetAccountUseCase.class, UpdateAccountStatusUseCase.class})
    private CreateAccountUseCase createAccountUseCase;

    private Account account;
    private final List<GrantedAuthority> authorities = List.of(new SimpleGrantedAuthority("SCOPE_m-wallet-accounts:read"), new SimpleGrantedAuthority("SCOPE_m-wallet-accounts:write"));

    @BeforeEach
    void setUp() {
        account = builder()
                .accountId("TEST_ACCOUNT_ID")
                .linkId("1")
                .accountType(Account.AccountType.USER_ACCOUNT)
                .actualBalance(new BigDecimal("45.56"))
                .availableBalance(new BigDecimal("100.01")).build();
    }

    @Test
    @SneakyThrows
    void getAccountInfo() {
        //GIVEN
        Account givenAccount = account;
        BDDMockito.given(getAccountUseCase.getAccountById(givenAccount.getAccountId())).willReturn(givenAccount);

        //WHEN + THEN
        mockMvc.perform(
                get("/v1/management/TEST_ACCOUNT_ID")
                        .contentType(MediaType.APPLICATION_JSON)
                        .with(jwt().authorities(authorities)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.accountId", is(givenAccount.getAccountId())))
                .andExpect(jsonPath("$.linkId", is(givenAccount.getLinkId())))
                .andExpect(jsonPath("$.accountStatus", is(givenAccount.getAccountStatus().name())))
                .andExpect(jsonPath("$.accountType", is(givenAccount.getAccountType().name())))
                .andExpect(jsonPath("$.availableBalance", is(givenAccount.getAvailableBalance().toString())))
                .andExpect(jsonPath("$.actualBalance", is(givenAccount.getActualBalance().toString())));
    }

    @Test
    @SneakyThrows
    void createAccount() {
        //GIVEN
        Account givenAccount = account;
        CreateAccountDTO givenCreateAccountDTO = CreateAccountDTO.builder()
                .linkId(givenAccount.getLinkId())
                .accountType(CreateAccountDTO.PublicAccountType.USER_ACCOUNT)
                .build();
        BDDMockito.given(createAccountUseCase.createAccount(any())).willReturn(givenAccount);

        //WHEN + THEN
        mockMvc.perform(
                post("/v1/management/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(writeJson(givenCreateAccountDTO))
                        .with(jwt().authorities(authorities)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.accountId", is(givenAccount.getAccountId())))
                .andExpect(jsonPath("$.linkId", is(givenAccount.getLinkId())))
                .andExpect(jsonPath("$.accountStatus", is(givenAccount.getAccountStatus().name())))
                .andExpect(jsonPath("$.accountType", is(givenAccount.getAccountType().name())))
                .andExpect(jsonPath("$.availableBalance", is(givenAccount.getAvailableBalance().toString())))
                .andExpect(jsonPath("$.actualBalance", is(givenAccount.getActualBalance().toString())));
    }
}