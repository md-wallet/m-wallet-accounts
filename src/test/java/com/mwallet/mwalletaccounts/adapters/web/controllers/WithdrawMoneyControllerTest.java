package com.mwallet.mwalletaccounts.adapters.web.controllers;

import com.mwallet.mwalletaccounts.adapters.web.dtos.transactions.WithdrawMoneyDTOs;
import com.mwallet.mwalletaccounts.application.ports.in.accounts.CreateAccountUseCase;
import com.mwallet.mwalletaccounts.application.ports.in.accounts.GetAccountUseCase;
import com.mwallet.mwalletaccounts.application.ports.in.accounts.UpdateAccountStatusUseCase;
import com.mwallet.mwalletaccounts.application.ports.in.transactions.WithdrawMoneyUseCase;
import com.mwallet.mwalletaccounts.config.RedisExtension;
import com.mwallet.mwalletaccounts.domain.Account;
import com.mwallet.mwalletaccounts.domain.Transaction;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.util.Pair;
import org.springframework.http.MediaType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.List;

import static com.mwallet.mwalletcommons.utils.ObjectUtils.writeJson;
import static com.mwallet.mwalletaccounts.domain.Account.AccountStatus.ACTIVE;
import static com.mwallet.mwalletaccounts.domain.Account.AccountType.TRUST_ACCOUNT;
import static com.mwallet.mwalletaccounts.domain.Account.AccountType.USER_ACCOUNT;
import static com.mwallet.mwalletaccounts.domain.Account.builder;
import static com.mwallet.mwalletaccounts.domain.Transaction.TransactionStatus.ACCEPTED;
import static com.mwallet.mwalletaccounts.domain.Transaction.TransactionStatus.VALIDATED;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.jwt;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@AutoConfigureMockMvc
@SpringBootTest
@ExtendWith(RedisExtension.class)
class WithdrawMoneyControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private WithdrawMoneyUseCase withdrawMoneyUseCase;

    @MockBean(extraInterfaces = {CreateAccountUseCase.class, UpdateAccountStatusUseCase.class})
    private GetAccountUseCase getAccountUseCase;

    private Account srcAccount;
    private Account destAccount;

    private final List<GrantedAuthority> authorities = List.of(new SimpleGrantedAuthority("SCOPE_m-wallet-accounts:read"), new SimpleGrantedAuthority("SCOPE_m-wallet-accounts:write"));

    @BeforeEach
    void setUp() {
        destAccount = builder()
                .accountId("TRUST_ACCOUNT")
                .linkId("TRUST_ACCOUNT")
                .accountStatus(ACTIVE)
                .accountType(TRUST_ACCOUNT)
                .build();

        srcAccount = destAccount.toBuilder()
                .accountId("TEST_ACCOUNT_ID_2")
                .linkId("TEST_LINK_ID_2")
                .accountType(USER_ACCOUNT)
                .availableBalance(new BigDecimal("1000"))
                .actualBalance(new BigDecimal("2000"))
                .build();
    }

    @Test
    @SneakyThrows
    void validateWithdrawMoney() {
        WithdrawMoneyDTOs.ValidationRequestDTO validationRequestDTO = WithdrawMoneyDTOs.ValidationRequestDTO.builder()
                .amount(BigDecimal.valueOf(200))
                .sourceAccountId(srcAccount.getAccountId())
                .billingAddress("TEST_BILLING_ADDRESS")
                .transactionCost(BigDecimal.ZERO)
                .transactionDescription("TEST_DESC")
                .paymentMode(Transaction.PaymentMode.MPESA)
                .transactionDomainType("TEST_DOMAIN_TYPE")
                .build();

        given(getAccountUseCase.getAccountById(srcAccount.getAccountId())).willReturn(srcAccount);
        given(getAccountUseCase.getAccountById(destAccount.getAccountId())).willReturn(destAccount);
        given(withdrawMoneyUseCase.validateWithdrawMoney(any(Transaction.class))).willAnswer(invocationOnMock -> {
            Transaction transaction = invocationOnMock.getArgument(0);
            transaction.setTransactionStatus(VALIDATED);

            return Pair.of("TEST_TRANSACTION_HASH", transaction);
        });


        //WHEN+THEN
        mockMvc.perform(
                post("/v1/withdrawmoney/validate/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(writeJson(validationRequestDTO))
                        .with(jwt().authorities(authorities)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.transactionHash", is("TEST_TRANSACTION_HASH")))
                .andExpect(jsonPath("$.transactionStatus", is(VALIDATED.name())));
    }

    @Test
    @SneakyThrows
    void requestWithdrawMoney() {
        //GIVEN
        String givenTransactionHash = "TEST_TRANSACTION_HASH";
        String givenTransactionRef = "TEST_TRANSACTION_REF";

        given(withdrawMoneyUseCase.requestWithdrawMoney(givenTransactionHash)).willAnswer(invocationOnMock -> Transaction.builder()
                .transactionRef(givenTransactionRef)
                .transactionStatus(ACCEPTED)
                .build());

        //WHEN+THEN
        mockMvc.perform(
                post("/v1/withdrawmoney/request/" + givenTransactionHash)
                        .contentType(MediaType.APPLICATION_JSON)
                        .with(jwt().authorities(authorities)))
                .andExpect(status().isAccepted())
                .andExpect(jsonPath("$.transactionRef", is("TEST_TRANSACTION_REF")))
                .andExpect(jsonPath("$.transactionStatus", is(ACCEPTED.name())));
    }
}
