package com.mwallet.mwalletaccounts.adapters.web.controllers;

import com.mwallet.mwalletaccounts.adapters.web.dtos.transactions.SendMoneyDTOs;
import com.mwallet.mwalletaccounts.application.ports.in.accounts.CreateAccountUseCase;
import com.mwallet.mwalletaccounts.application.ports.in.accounts.GetAccountUseCase;
import com.mwallet.mwalletaccounts.application.ports.in.accounts.UpdateAccountStatusUseCase;
import com.mwallet.mwalletaccounts.application.ports.in.transactions.SendMoneyUseCase;
import com.mwallet.mwalletaccounts.config.RedisExtension;
import com.mwallet.mwalletaccounts.domain.Account;
import com.mwallet.mwalletaccounts.domain.Transaction;
import com.mwallet.mwalletaccounts.domain.Transaction.ConfirmationStatus;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.util.Pair;
import org.springframework.http.MediaType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.List;

import static com.mwallet.mwalletcommons.utils.ObjectUtils.writeJson;
import static com.mwallet.mwalletaccounts.adapters.web.dtos.transactions.SendMoneyDTOs.ValidationRequestDTO;
import static com.mwallet.mwalletaccounts.domain.Account.AccountStatus.ACTIVE;
import static com.mwallet.mwalletaccounts.domain.Account.AccountType.USER_ACCOUNT;
import static com.mwallet.mwalletaccounts.domain.Account.builder;
import static com.mwallet.mwalletaccounts.domain.Transaction.ConfirmationType;
import static com.mwallet.mwalletaccounts.domain.Transaction.TransactionStatus;
import static com.mwallet.mwalletaccounts.domain.Transaction.TransactionStatus.ACCEPTED;
import static com.mwallet.mwalletaccounts.domain.Transaction.TransactionStatus.VALIDATED;
import static java.util.Comparator.comparing;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.jwt;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@AutoConfigureMockMvc
@SpringBootTest
@ExtendWith(RedisExtension.class)
class SendMoneyControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Captor
    private ArgumentCaptor<?> captor;

    @MockBean
    private SendMoneyUseCase sendMoneyUseCase;
    @MockBean(extraInterfaces = {CreateAccountUseCase.class, UpdateAccountStatusUseCase.class})
    private GetAccountUseCase getAccountUseCase;

    private Account srcAccount;
    private Account destAccount;

    private final List<GrantedAuthority> authorities = List.of(new SimpleGrantedAuthority("SCOPE_m-wallet-accounts:read"), new SimpleGrantedAuthority("SCOPE_m-wallet-accounts:write"));

    @BeforeEach
    void setUp() {
        srcAccount = builder()
                .accountId("TEST_ACCOUNT_ID_1")
                .linkId("TEST_LINK_ID_1")
                .accountStatus(ACTIVE)
                .accountType(USER_ACCOUNT)
                .availableBalance(new BigDecimal("1000"))
                .actualBalance(new BigDecimal("2000"))
                .build();
        destAccount = srcAccount.toBuilder()
                .accountId("TEST_ACCOUNT_ID_2")
                .linkId("TEST_LINK_ID_2")
                .build();
    }

    @Test
    @SneakyThrows
    void validateSendMoney() {
        //GIVEN
        ValidationRequestDTO sendMoneyValidationRequestDTO = ValidationRequestDTO.builder()
                .amount(new BigDecimal("200"))
                .transactionCost(new BigDecimal("10"))
                .confirmationType(ConfirmationType.INTERNAL)
                .sourceAccountId(srcAccount.getAccountId())
                .destinationAccountId(destAccount.getAccountId())
                .transactionDescription("TEST_DESC")
                .transactionDomainType("TEST_DOMAIN_TYPE")
                .build();

        given(getAccountUseCase.getAccountById(srcAccount.getAccountId())).willReturn(srcAccount);
        given(getAccountUseCase.getAccountById(destAccount.getAccountId())).willReturn(destAccount);
        given(sendMoneyUseCase.validateSendMoney(any(Transaction.class))).willAnswer(invocationOnMock -> {
            Transaction transaction = invocationOnMock.getArgument(0);
            transaction.setTransactionStatus(VALIDATED);

            return Pair.of("TEST_TRANSACTION_HASH", transaction);
        });

        //WHEN+THEN
        mockMvc.perform(
                post("/v1/sendmoney/validate/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(writeJson(sendMoneyValidationRequestDTO))
                        .with(jwt().authorities(authorities)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.transactionHash", is("TEST_TRANSACTION_HASH")))
                .andExpect(jsonPath("$.transactionStatus", is(VALIDATED.name())));

        then(sendMoneyUseCase).should().validateSendMoney((Transaction) captor.capture());

        Transaction transaction = (Transaction) captor.getValue();
        assertThat(transaction)
                .isNotNull()
                .hasNoNullFieldsOrPropertiesExcept("id", "version", "createdOn", "updatedOn", "transactionRef")
                .extracting(Transaction::getAmount, Transaction::getTransactionCost, Transaction::getConfirmationType)
                .usingComparatorForType(comparing(o -> ((BigDecimal) o)), BigDecimal.class)
                .containsExactly(
                        sendMoneyValidationRequestDTO.getAmount(),
                        sendMoneyValidationRequestDTO.getTransactionCost(),
                        sendMoneyValidationRequestDTO.getConfirmationType()
                );
    }

    @Test
    @SneakyThrows
    void requestSendMoney() {
        //GIVEN
        String givenTransactionHash = "TEST_TRANSACTION_HASH";
        String givenTransactionRef = "TEST_TRANSACTION_REF";

        given(sendMoneyUseCase.requestSendMoney(givenTransactionHash)).willAnswer(invocationOnMock -> Transaction.builder()
                .transactionRef(givenTransactionRef)
                .transactionStatus(ACCEPTED)
                .build());

        //WHEN+THEN
        mockMvc.perform(
                post("/v1/sendmoney/request/" + givenTransactionHash)
                        .contentType(MediaType.APPLICATION_JSON)
                        .with(jwt().authorities(authorities)))
                .andExpect(status().isAccepted())
                .andExpect(jsonPath("$.transactionRef", is("TEST_TRANSACTION_REF")))
                .andExpect(jsonPath("$.transactionStatus", is(ACCEPTED.name())));


    }

    @ParameterizedTest
    @CsvSource({
            "PROCEED,P2P Loan Offer Accepted,CONFIRMED",
            "ROLLBACK,P2P Loan Offer Rejected,FAILED"
    })
    @SneakyThrows
    void confirmSendMoney(ConfirmationStatus givenConfirmationStatus, String givenNarration, TransactionStatus expectedTransactionStatus) {
        //GIVEN
        SendMoneyDTOs.ConfirmationRequestDTO confirmationRequestDTO = SendMoneyDTOs.ConfirmationRequestDTO.builder()
                .transactionRef("TEST_TRANSACTION_REF")
                .confirmationStatus(givenConfirmationStatus)
                .narration(givenNarration)
                .build();

        given(sendMoneyUseCase.confirmSendMoney(confirmationRequestDTO.getTransactionRef(), confirmationRequestDTO.getConfirmationStatus(), confirmationRequestDTO.getNarration()))
                .willAnswer(invocationOnMock -> Transaction.builder()
                        .transactionRef("TEST_TRANSACTION_REF")
                        .transactionStatus(expectedTransactionStatus)
                        .build());

        //WHEN+THEN
        mockMvc.perform(
                post("/v1/sendmoney/confirm/")
                        .content(writeJson(confirmationRequestDTO))
                        .contentType(MediaType.APPLICATION_JSON)
                        .with(jwt().authorities(authorities)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.transactionRef", is("TEST_TRANSACTION_REF")))
                .andExpect(jsonPath("$.transactionStatus", is(expectedTransactionStatus.name())));

    }
}
