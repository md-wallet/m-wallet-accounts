package com.mwallet.mwalletaccounts.adapters.external;

import com.mwallet.mwalletaccounts.config.RedisExtension;
import com.mwallet.mwalletaccounts.domain.Account;
import com.mwallet.mwalletaccounts.domain.Payment;
import com.mwallet.mwalletaccounts.domain.Transaction;
import com.mwallet.mwalletaccounts.infrastructure.feign.PaymentsFeignClient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ActiveProfiles;

import java.math.BigDecimal;

import static com.mwallet.mwalletaccounts.domain.Payment.PaymentRequest.PaymentRequestStatus.PAYMENT_REQUEST_ACCEPTED;
import static com.mwallet.mwalletaccounts.domain.Payout.PayoutRequest;
import static com.mwallet.mwalletaccounts.domain.Payout.PayoutRequest.PayoutRequestStatus.PAYOUT_REQUEST_ACCEPTED;
import static com.mwallet.mwalletaccounts.domain.Transaction.PaymentMode.MPESA;
import static com.mwallet.mwalletaccounts.domain.Transaction.TransactionType.DEPOSIT;
import static com.mwallet.mwalletaccounts.domain.Transaction.TransactionType.WITHDRAW;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doReturn;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(RedisExtension.class)
class PaymentsExternalAdapterTest {

    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private PaymentsExternalAdapter paymentsExternalAdapter;

    @MockBean
    private PaymentsFeignClient paymentsFeignClient;

    private Transaction transaction;

    @BeforeEach
    void setUp() {
        transaction = Transaction.builder()
                .sourceAccount(new Account())
                .destinationAccount(new Account())
                .transactionRef("TEST_TRANSACTION_REF")
                .amount(new BigDecimal("500"))
                .transactionCost(new BigDecimal("20"))
                .transactionType(DEPOSIT)
                .paymentMode(MPESA)
                .billingAddress("TEST_BILLING_ADDRESS")
                .transactionDescription("TEST_TRANSACTION_DESC")
                .transactionDomainType("TEST_DOMAIN_TYPE")
                .transactionStatus(Transaction.TransactionStatus.NONE)
                .confirmationType(Transaction.ConfirmationType.INTERNAL)
                .confirmationStatus(Transaction.ConfirmationStatus.NONE)
                .build();
    }

    @Test
    void requestPayment() {
        //GIVEN
        Transaction givenTransaction = transaction;


        given(paymentsFeignClient.requestPayment(any(PaymentsFeignClient.Models.PaymentRequestDTO.class))).willAnswer(invocationOnMock -> {
            PaymentsFeignClient.Models.PaymentRequestDTO paymentRequestDTO = invocationOnMock.getArgument(0);
            Payment.PaymentRequest paymentRequest = modelMapper.map(paymentRequestDTO, Payment.PaymentRequest.class);
            return paymentRequest.toBuilder().paymentRequestStatus(PAYMENT_REQUEST_ACCEPTED).build();
        });


        //WHEN
        Payment.PaymentRequest paymentResult = paymentsExternalAdapter.requestPayment(givenTransaction);

        //THEN
        assertThat(paymentResult)
                .isNotNull()
                .hasNoNullFieldsOrPropertiesExcept("id","version","createdOn","updatedOn")
                .extracting(Payment.PaymentRequest::getPaymentRequestStatus)
                .isEqualTo(PAYMENT_REQUEST_ACCEPTED);
    }

    @Test
    void requestPayout() {
        //GIVEN
        Transaction givenTransaction = transaction.toBuilder().transactionType(WITHDRAW).build();
        given(paymentsFeignClient.requestPayout(any(PaymentsFeignClient.Models.PayoutRequestDTO.class))).willAnswer(invocationOnMock -> {
            PaymentsFeignClient.Models.PayoutRequestDTO payoutRequestDTO = invocationOnMock.getArgument(0);
            PayoutRequest payoutRequest = modelMapper.map(payoutRequestDTO, PayoutRequest.class);
            return payoutRequest.toBuilder().payoutRequestStatus(PAYOUT_REQUEST_ACCEPTED).build();
        });

        //WHEN
        PayoutRequest payoutResult = paymentsExternalAdapter.requestPayout(givenTransaction);

        //THEN
        assertThat(payoutResult)
                .isNotNull()
                .hasNoNullFieldsOrPropertiesExcept("id","version","createdOn","updatedOn")
                .extracting(PayoutRequest::getPayoutRequestStatus)
                .isEqualTo(PAYOUT_REQUEST_ACCEPTED);
    }
}
