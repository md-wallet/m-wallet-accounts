package com.mwallet.mwalletaccounts.adapters.persistence.accounts;

import com.mwallet.mwalletaccounts.application.ports.out.accounts.SaveAccountOutputPort;
import com.mwallet.mwalletaccounts.config.RedisExtension;
import com.mwallet.mwalletaccounts.domain.Account;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.util.Pair;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;

import static com.mwallet.mwalletaccounts.domain.Account.AccountType.USER_ACCOUNT;
import static com.mwallet.mwalletaccounts.domain.Account.builder;
import static java.util.Comparator.comparing;

@Slf4j
@ActiveProfiles("test")
@SpringBootTest
@ExtendWith(RedisExtension.class)
class FundsTransferAdapterTest {

    @Autowired
    private SaveAccountOutputPort saveAccountOutputPort;
    @Autowired
    private AccountEntityJpaRepository accountEntityJpaRepository;
    @Autowired
    private FundsTransferAdapter fundsTransferAdapter;

    private Account srcAccount;
    private Account destAccount;

    @BeforeEach
    void setUp() {
        srcAccount = builder()
                .linkId("1")
                .accountId("ID_1")
                .accountType(USER_ACCOUNT)
                .availableBalance(new BigDecimal("1000"))
                .actualBalance(new BigDecimal("2000"))
                .build();
        destAccount = srcAccount.toBuilder().accountId("ID_2").linkId("20").build();

        srcAccount = saveAccountOutputPort.createAccount(srcAccount);
        destAccount = saveAccountOutputPort.createAccount(destAccount);
    }

    @AfterEach
    void tearDown() {
        accountEntityJpaRepository.deleteAll();
    }

    @Test
    void prepareTransfer() {
        //GIVEN
        String srcAccountId = srcAccount.getAccountId();
        String destAccountId = destAccount.getAccountId();
        BigDecimal amount = new BigDecimal("300");
        BigDecimal transactionCost = new BigDecimal("20");

        //WHEN
        Pair<Account,Account> accounts = fundsTransferAdapter.prepareTransfer(srcAccountId,destAccountId,amount,transactionCost);

        //THEN
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(accounts.getFirst())
                .isNotNull()
                .usingComparatorForType(comparing(o -> ((BigDecimal) o)), BigDecimal.class)
                .isEqualToIgnoringGivenFields(srcAccount,"availableBalance","updatedOn","version")
                .extracting(Account::getAvailableBalance)
                .usingComparator(BigDecimal::compareTo)
                .isEqualTo(srcAccount.getAvailableBalance().subtract(amount.add(transactionCost)));
        softly.assertThat(accounts.getSecond())
                .isNotNull()
                .usingComparatorForType(comparing(o -> ((BigDecimal) o)), BigDecimal.class)
                .isEqualToIgnoringGivenFields(destAccount,"actualBalance","updatedOn","version")
                .extracting(Account::getActualBalance)
                .usingComparator(BigDecimal::compareTo)
                .isEqualTo(destAccount.getActualBalance().add(amount));

        softly.assertAll();
    }

    @Test
    void completeTransfer() {
        //GIVEN
        String srcAccountId = srcAccount.getAccountId();
        String destAccountId = destAccount.getAccountId();
        BigDecimal amount = new BigDecimal("300");
        BigDecimal transactionCost = new BigDecimal("20");

        //WHEN
        Pair<Account,Account> accounts = fundsTransferAdapter.confirmTransfer(srcAccountId,destAccountId,amount,transactionCost);

        //THEN
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(accounts.getFirst())
                .isNotNull()
                .usingComparatorForType(comparing(o -> ((BigDecimal) o)), BigDecimal.class)
                .isEqualToIgnoringGivenFields(srcAccount,"actualBalance","updatedOn","version")
                .extracting(Account::getActualBalance)
                .usingComparator(BigDecimal::compareTo)
                .isEqualTo(srcAccount.getActualBalance().subtract(amount.add(transactionCost)));
        softly.assertThat(accounts.getSecond())
                .isNotNull()
                .usingComparatorForType(comparing(o -> ((BigDecimal) o)), BigDecimal.class)
                .isEqualToIgnoringGivenFields(destAccount,"availableBalance","updatedOn","version")
                .extracting(Account::getAvailableBalance)
                .usingComparator(BigDecimal::compareTo)
                .isEqualTo(destAccount.getAvailableBalance().add(amount));

        softly.assertAll();
    }

    @Test
    void rollBackTransfer(){
        //GIVEN
        String srcAccountId = srcAccount.getAccountId();
        String destAccountId = destAccount.getAccountId();
        BigDecimal amount = new BigDecimal("300");
        BigDecimal transactionCost = new BigDecimal("20");

        //WHEN
        Pair<Account,Account> accounts = fundsTransferAdapter.rollbackTransfer(srcAccountId,destAccountId,amount,transactionCost);

        //THEN
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(accounts.getFirst())
                .isNotNull()
                .usingComparatorForType(comparing(o -> ((BigDecimal) o)), BigDecimal.class)
                .isEqualToIgnoringGivenFields(srcAccount,"availableBalance","updatedOn","version")
                .extracting(Account::getAvailableBalance)
                .usingComparator(BigDecimal::compareTo)
                .isEqualTo(srcAccount.getAvailableBalance().add(amount.add(transactionCost)));
        softly.assertThat(accounts.getSecond())
                .isNotNull()
                .usingComparatorForType(comparing(o -> ((BigDecimal) o)), BigDecimal.class)
                .isEqualToIgnoringGivenFields(destAccount,"actualBalance","updatedOn","version")
                .extracting(Account::getActualBalance)
                .usingComparator(BigDecimal::compareTo)
                .isEqualTo(destAccount.getActualBalance().subtract(amount));

        softly.assertAll();
    }
}
