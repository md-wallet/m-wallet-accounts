package com.mwallet.mwalletaccounts.adapters.persistence.transactionlogs;

import com.mwallet.mwalletaccounts.adapters.persistence.accounts.AccountEntityJpaRepository;
import com.mwallet.mwalletaccounts.adapters.persistence.transactions.jpa.TransactionEntityJpaRepository;
import com.mwallet.mwalletaccounts.application.ports.out.accounts.SaveAccountOutputPort;
import com.mwallet.mwalletaccounts.application.ports.out.transactions.SaveTransactionOutputPort;
import com.mwallet.mwalletaccounts.config.RedisExtension;
import com.mwallet.mwalletaccounts.domain.Account;
import com.mwallet.mwalletaccounts.domain.Transaction;
import com.mwallet.mwalletaccounts.domain.TransactionLog;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.math.BigDecimal;
import java.util.List;

import static com.mwallet.mwalletaccounts.domain.Account.AccountStatus.ACTIVE;
import static com.mwallet.mwalletaccounts.domain.Account.AccountType.USER_ACCOUNT;
import static com.mwallet.mwalletaccounts.domain.Account.builder;
import static com.mwallet.mwalletaccounts.domain.Transaction.PaymentMode.MPESA;
import static com.mwallet.mwalletaccounts.domain.Transaction.TransactionType.SEND_MONEY;
import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(RedisExtension.class)
class TransactionLogEntityPersistenceAdapterTest {

    private Account srcAccount;
    private Account destAccount;
    private Transaction transaction;

    private TransactionLog transactionLog;

    @Autowired
    private SaveAccountOutputPort saveAccountOutputPort;
    @Autowired
    private SaveTransactionOutputPort saveTransactionOutputPort;

    @Autowired
    private AccountEntityJpaRepository accountEntityJpaRepository;
    @Autowired
    private TransactionEntityJpaRepository transactionEntityJpaRepository;
    @Autowired
    private TransactionLogEntityJpaRepository transactionLogEntityJpaRepository;

    @Autowired
    private TransactionLogEntityPersistenceAdapter transactionLogEntityPersistenceAdapter;

    @BeforeEach
    void setUp() {
        srcAccount = builder()
                .linkId("1")
                .accountId("ID_1")
                .accountStatus(ACTIVE)
                .accountType(USER_ACCOUNT)
                .availableBalance(new BigDecimal("1000"))
                .actualBalance(new BigDecimal("2000"))
                .build();
        destAccount = srcAccount.toBuilder().accountId("ID_2").linkId("20").build();

        srcAccount = saveAccountOutputPort.createAccount(srcAccount);
        destAccount = saveAccountOutputPort.createAccount(destAccount);

        transaction = Transaction.builder()
                .transactionRef("FSNFSJDNFSK")
                .sourceAccount(srcAccount)
                .destinationAccount(destAccount)
                .amount(new BigDecimal("1000.00"))
                .transactionCost(new BigDecimal("20.00"))
                .transactionType(SEND_MONEY)
                .paymentMode(MPESA)
                .billingAddress("TEST_BILLING_ADDRESS")
                .transactionDescription("TEST_TRANSACTION_DESC")
                .transactionDomainType("TEST_DOMAIN_TYPE")
                .confirmationType(Transaction.ConfirmationType.INTERNAL)
                .transactionStatus(Transaction.TransactionStatus.NONE)
                .confirmationStatus(Transaction.ConfirmationStatus.NONE)
                .build();

        transaction = saveTransactionOutputPort.createTransaction(transaction);


    }

    @AfterEach
    void tearDown() {
        accountEntityJpaRepository.deleteAll();
        transactionEntityJpaRepository.deleteAll();
        transactionLogEntityJpaRepository.deleteAll();
    }

    @Test
    @DisplayName("transaction -> transactionLog")
    void logTransaction() {
        //GIVEN
        Transaction givenTransaction = transaction;
        //WHEN
        TransactionLog createdTransactionLog = transactionLogEntityPersistenceAdapter.logTransaction(givenTransaction, "SUCCESS");
        //THEN
        assertThat(createdTransactionLog)
                .isNotNull()
                .hasNoNullFieldsOrProperties()
                .extracting(TransactionLog::getTransaction)
                .usingRecursiveComparison()
                .isEqualTo(givenTransaction);
    }

    @Test
    @DisplayName("ref -> transactionLogs")
    void loadTransactionLogsByTransactionRef() {
        //GIVEN
        TransactionLog givenTransactionLog = transactionLogEntityPersistenceAdapter.logTransaction(transaction, "SUCCESS");
        //WHEN
        List<TransactionLog> transactionLogList = transactionLogEntityPersistenceAdapter
                .loadTransactionLogsByTransactionRef(givenTransactionLog.getTransaction().getTransactionRef());
        //THEN
        assertThat(transactionLogList)
                .isNotEmpty()
                .hasSize(1);
    }
}
