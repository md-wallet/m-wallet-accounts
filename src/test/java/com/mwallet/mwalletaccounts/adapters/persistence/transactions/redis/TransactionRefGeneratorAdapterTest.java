package com.mwallet.mwalletaccounts.adapters.persistence.transactions.redis;

import com.mwallet.mwalletaccounts.application.ports.out.transactions.GenerateTransactionRefOutputPort;
import com.mwallet.mwalletaccounts.config.RedisExtension;
import com.mwallet.mwalletaccounts.domain.Transaction;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.mwallet.mwalletaccounts.domain.Transaction.TransactionType.DEPOSIT;
import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(RedisExtension.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class TransactionRefGeneratorAdapterTest {

    @Autowired
    private GenerateTransactionRefOutputPort generateTransactionRefOutputPort;
    private Transaction transaction;

    @BeforeEach
    void setUp() {
        transaction = Transaction.builder().transactionType(DEPOSIT).build();
    }

    @Test
    @SneakyThrows
    void generateTransactionRef_HasNoDuplicates_And_IsThreadSafe(){
        //GIVEN
        Transaction givenTransaction = transaction;
        List<String> transactionRefs = Collections.synchronizedList(new ArrayList<>());

        int thread_count = 2;
        int thread_iterations = 300;
        List<Thread> threads = new ArrayList<>();

        //WHEN
        for(int i=0;i<thread_count;i++)
            threads.add(new Thread(() -> {
                long iterations = thread_iterations;
                while (iterations>0){
                    String transactionRef = generateTransactionRefOutputPort.generateTransactionRef(givenTransaction);
                    transactionRefs.add(transactionRef);
                    iterations--;
                }
            }));
        threads.forEach(Thread::start);
        for (Thread thread : threads) thread.join();

        //THEN
        log.debug("TEST THREAD_COUNT: {}",thread_count);
        log.debug("TEST TRANSACTION_COUNT: {}",transactionRefs.size());

        assertThat(transactionRefs)
                .hasSize(thread_count*thread_iterations)
                .doesNotHaveDuplicates();

    }
}
