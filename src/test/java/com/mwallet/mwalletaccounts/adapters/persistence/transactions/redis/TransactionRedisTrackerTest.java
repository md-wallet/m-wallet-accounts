package com.mwallet.mwalletaccounts.adapters.persistence.transactions.redis;

import com.mwallet.mwalletaccounts.config.RedisExtension;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(RedisExtension.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class TransactionRedisTrackerTest {

    @Autowired
    private TransactionRedisTracker transactionRedisTracker;

    @BeforeEach
    void setUp() {
        transactionRedisTracker.getTransactionCounter().set(1);
    }


    @Test
    @SneakyThrows
    @DisplayName("getTransactionCount -> threadSafe")
    void getAndIncrementTransactionCount_IsThreadSafe() {
        //GIVEN
        long thread_iterations = 300;
        long thread_count = 3;
        List<Thread> threads = new ArrayList<>();
        transactionRedisTracker.setResetIntervalSeconds(24 * 60 * 60 * 1000L);//1 day

        //WHEN
        for (int i = 0; i < thread_count; i++)
            threads.add(new Thread(() -> {
                long iterations = thread_iterations;
                while (iterations > 0) {
                    transactionRedisTracker.getAndIncrementTransactionCounter();
                    iterations--;
                }
            }));
        threads.forEach(Thread::start);
        for (Thread thread : threads) thread.join();

        //THEN
        assertThat(transactionRedisTracker.getTransactionCounter().get()).isEqualTo((thread_iterations * thread_count) + 1);
    }

    @Test
    @DisplayName("getTransactionCount -> staleCounter -> reset")
    void getAndIncrementTransactionCount_WillResetOnStaleCounter() {
        //GIVEN
        long startTransactionCounterTimestamp = LocalDateTime.now()
                .minusSeconds(1)
                .atZone(ZoneId.of("Africa/Nairobi"))
                .toInstant()
                .getEpochSecond();

        transactionRedisTracker.getStartTransactionCounterTimestamp().set(startTransactionCounterTimestamp);
        transactionRedisTracker.getTransactionCounter().getAndSet(2000);
        transactionRedisTracker.setResetIntervalSeconds(1L);

        //WHEN
        transactionRedisTracker.getAndIncrementTransactionCounter();

        //ASSERT
        long transactionCount = transactionRedisTracker.getTransactionCounter().get();
        assertThat(transactionCount).isEqualTo(2L); //value was incremented because of getAndIncrement
    }

}
