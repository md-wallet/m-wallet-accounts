package com.mwallet.mwalletaccounts.adapters.persistence.accounts;


import com.mwallet.mwalletaccounts.application.ports.out.accounts.LoadAccountOutputPort;
import com.mwallet.mwalletaccounts.application.ports.out.accounts.SaveAccountOutputPort;
import com.mwallet.mwalletaccounts.application.ports.out.accounts.SaveAccountOutputPort.NonUniqueAccountException;
import com.mwallet.mwalletaccounts.config.RedisExtension;
import com.mwallet.mwalletaccounts.domain.Account;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.math.BigDecimal;

import static com.mwallet.mwalletaccounts.domain.Account.AccountStatus.ACTIVE;
import static com.mwallet.mwalletaccounts.domain.Account.AccountStatus.INACTIVE;
import static com.mwallet.mwalletaccounts.domain.Account.AccountType;
import static com.mwallet.mwalletaccounts.domain.Account.AccountType.USER_ACCOUNT;
import static com.mwallet.mwalletaccounts.domain.Account.builder;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@Slf4j
@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(RedisExtension.class)
class AccountEntityPersistenceAdapterTest {

    @Autowired
    private SaveAccountOutputPort saveAccountOutputPort;
    @Autowired
    private LoadAccountOutputPort loadAccountOutputPort;

    @Autowired
    private AccountEntityJpaRepository accountEntityJpaRepository;

    private Account account;
    private Account existingAccount;

    @BeforeEach
    void setUp() {
        account = builder()
                .linkId("ABCD")
                .accountId("ACCOUNT_ID")
                .accountStatus(ACTIVE)
                .accountType(USER_ACCOUNT)
                .availableBalance(new BigDecimal("0.00"))
                .actualBalance(new BigDecimal("0.00"))
                .build();
        existingAccount = account.toBuilder().accountId("DIFF_ACCOUNT_ID").linkId("ABCE").build();
        existingAccount = saveAccountOutputPort.createAccount(existingAccount);
    }

    @AfterEach
    void tearDown() {
        accountEntityJpaRepository.deleteAll();
    }

    @Test
    @DisplayName("createAccount -> persist")
    void createAccount_WillCreateNewAccount() {
        //GIVEN
        Account givenAccount = account;
        //WHEN
        Account createdAccount = saveAccountOutputPort.createAccount(givenAccount);
        account = createdAccount;
        //THEN
        assertThat(createdAccount)
                .isNotNull()
                .hasNoNullFieldsOrProperties()
                .isEqualToIgnoringNullFields(givenAccount);
    }

    @Test
    @DisplayName("createAccount -> existingAccount -> NonUniqueAccountException")
    void createAccount_WillThrow_NonUniqueAccountException() {
        //GIVEN
        Account givenAccount = existingAccount;
        //WHEN+THEN
        assertThatThrownBy(() -> saveAccountOutputPort.createAccount(givenAccount))
                .isInstanceOf(NonUniqueAccountException.class)
                .hasMessageContaining("Duplicate account can not be created");
    }

    @Test
    @DisplayName("updateAccount -> updateSelf")
    void updateAccount_WillUpdateSelf() {
        //GIVEN
        BigDecimal newAvailableBalance = new BigDecimal("16000.25");
        Account givenAccount = existingAccount.toBuilder()
                .accountStatus(INACTIVE)
                .availableBalance(newAvailableBalance)
                .build();
        //WHEN
        Account updatedAccount = saveAccountOutputPort.updateAccount(givenAccount);
        //THEN
        assertThat(updatedAccount)
                .isNotNull()
                .hasNoNullFieldsOrProperties()
                .isEqualToIgnoringGivenFields(existingAccount, "accountStatus", "availableBalance", "version", "updatedOn")
                .extracting(Account::getAccountStatus, Account::getAvailableBalance)
                .containsExactly(INACTIVE, newAvailableBalance);
    }

    @ParameterizedTest
    @DisplayName("accountExists -> (linkId,AccountType)")
    @CsvSource({
            "ABCE,USER_ACCOUNT,true",
            "XYZ,JOINT_ACCOUNT,false"
    })
    void existsByLinkIdAndAccountType(String givenLinkId, AccountType givenAccountType, boolean expectedResult) {
        boolean actualResult = loadAccountOutputPort.existsByLinkIdAndAccountType(givenLinkId, givenAccountType);
        assertThat(actualResult).isEqualTo(expectedResult);
    }
}