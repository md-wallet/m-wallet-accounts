package com.mwallet.mwalletaccounts.adapters.persistence.transactions.redis;

import com.mwallet.mwalletaccounts.adapters.persistence.accounts.AccountEntityJpaRepository;
import com.mwallet.mwalletaccounts.application.ports.out.accounts.SaveAccountOutputPort;
import com.mwallet.mwalletaccounts.application.ports.out.transactions.TransactionCacheManagementOutputPort.DuplicateTransactionException;
import com.mwallet.mwalletaccounts.config.RedisExtension;
import com.mwallet.mwalletaccounts.domain.Account;
import com.mwallet.mwalletaccounts.domain.Transaction;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.math.BigDecimal;

import static com.mwallet.mwalletaccounts.application.ports.out.transactions.LoadTransactionOutputPort.TransactionNotFoundException;
import static com.mwallet.mwalletaccounts.domain.Account.AccountStatus.ACTIVE;
import static com.mwallet.mwalletaccounts.domain.Account.AccountType.USER_ACCOUNT;
import static com.mwallet.mwalletaccounts.domain.Account.builder;
import static com.mwallet.mwalletaccounts.domain.Transaction.PaymentMode.MPESA;
import static com.mwallet.mwalletaccounts.domain.Transaction.TransactionType.SEND_MONEY;
import static com.mwallet.mwalletaccounts.domain.Transaction.TransactionType.WITHDRAW;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@Slf4j
@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(RedisExtension.class)
class TransactionCacheManagementAdapterTest {

    private Account srcAccount;
    private Account destAccount;
    private Transaction transaction;
    private Transaction existingTransaction;
    private String existingTransactionHash;

    @Autowired
    private SaveAccountOutputPort saveAccountOutputPort;
    @Autowired
    private AccountEntityJpaRepository accountEntityJpaRepository;
    @Autowired
    private TransactionCacheManagementAdapter transactionCacheManagementAdapter;
    @Autowired
    private TransactionRedisEntityRepository transactionRedisEntityRepository;

    @BeforeEach
    void setUp() {
        srcAccount = builder()
                .accountId("TEST_ACCOUNT_ID_1")
                .linkId("1")
                .accountStatus(ACTIVE)
                .accountType(USER_ACCOUNT)
                .availableBalance(new BigDecimal("1000"))
                .actualBalance(new BigDecimal("2000"))
                .build();
        destAccount = srcAccount.toBuilder().accountId("TEST_ACCOUNT_ID_2").linkId("20").build();

        srcAccount = saveAccountOutputPort.createAccount(srcAccount);
        destAccount = saveAccountOutputPort.createAccount(destAccount);

        transaction = Transaction.builder()
                .sourceAccount(srcAccount)
                .destinationAccount(destAccount)
                .amount(new BigDecimal("1000.00"))
                .transactionCost(new BigDecimal("20.00"))
                .transactionType(SEND_MONEY)
                .billingAddress("TEST_BILLING_ADDRESS")
                .paymentMode(MPESA)
                .transactionDescription("TEST_TRANSACTION_DESC")
                .transactionDomainType("TEST_DOMAIN_TYPE")
                .confirmationType(Transaction.ConfirmationType.INTERNAL)
                .confirmationStatus(Transaction.ConfirmationStatus.NONE)
                .transactionStatus(Transaction.TransactionStatus.NONE)
                .build();

        existingTransaction = transaction.toBuilder().transactionType(WITHDRAW).build();
        existingTransactionHash = transactionCacheManagementAdapter.saveTransaction(existingTransaction);
    }

    @AfterEach
    void tearDown() {
        accountEntityJpaRepository.deleteAll();
        transactionRedisEntityRepository.deleteAll();
    }

    @Test
    @DisplayName("saveTransaction -> OK")
    void saveTransaction() {
        //GIVEN
        Transaction givenTransaction = transaction;
        //WHEN
        String transactionHash = transactionCacheManagementAdapter.saveTransaction(transaction);
        //THEN
        assertThat(transactionHash).isNotBlank();
    }

    @Test
    @DisplayName("saveTransaction -> DuplicateTransactionException")
    void saveTransaction_WillThrow_DuplicateTransactionException() {
        //GIVEN
        Transaction givenTransaction = existingTransaction;
        //WHEN+THEN
        assertThatThrownBy(() -> transactionCacheManagementAdapter.saveTransaction(givenTransaction))
                .isInstanceOf(DuplicateTransactionException.class)
                .hasMessageContaining("Duplicate Transaction: " + existingTransactionHash);

    }

    @Test
    @DisplayName("existsByHash -> TRUE")
    void existsByHash() {
        //GIVEN
        String transactionHash = transactionCacheManagementAdapter.saveTransaction(transaction);
        //WHEN
        boolean found = transactionCacheManagementAdapter.existsByHash(transactionHash);
        //THEN
        assertThat(found).isTrue();
    }

    @Test
    @DisplayName("getTransactionByHash -> OK")
    void getTransactionByHash() {
        //GIVEN
        String givenTransactionHash = existingTransactionHash;
        //WHEN
        Transaction foundTransaction = transactionCacheManagementAdapter.getTransactionByHash(givenTransactionHash);
        //THEN
        assertThat(foundTransaction)
                .isNotNull()
                .hasNoNullFieldsOrPropertiesExcept("transactionRef", "id", "version", "createdOn", "updatedOn");
    }

    @Test
    @DisplayName("getTransactionByHash -> TransactionNotFoundException")
    void getTransactionByHash_WillThrow_TransactionNotFoundException() {
        //GIVEN
        String givenHash = "HASH_NON_EXISTENT";
        //WHEN+THEN
        assertThatThrownBy(() -> transactionCacheManagementAdapter.getTransactionByHash(givenHash))
                .isInstanceOf(TransactionNotFoundException.class)
                .hasMessageContaining("Transaction Not Found: " + givenHash);
    }

    @Test
    void deleteByHash() {
        transactionCacheManagementAdapter.deleteByHash(existingTransactionHash);
        //WHEN+THEN
        assertThatThrownBy(() -> transactionCacheManagementAdapter.getTransactionByHash(existingTransactionHash))
                .isInstanceOf(TransactionNotFoundException.class)
                .hasMessageContaining("Transaction Not Found: " + existingTransactionHash);
    }
}