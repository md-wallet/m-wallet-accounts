package com.mwallet.mwalletaccounts.adapters.persistence.transactions.jpa;

import com.mwallet.mwalletcommons.utils.DateUtils;
import com.mwallet.mwalletaccounts.adapters.persistence.accounts.AccountEntityJpaRepository;
import com.mwallet.mwalletaccounts.application.ports.out.accounts.SaveAccountOutputPort;
import com.mwallet.mwalletaccounts.application.ports.out.transactions.LoadTransactionOutputPort;
import com.mwallet.mwalletaccounts.application.ports.out.transactions.SaveTransactionOutputPort;
import com.mwallet.mwalletaccounts.config.RedisExtension;
import com.mwallet.mwalletaccounts.domain.Account;
import com.mwallet.mwalletaccounts.domain.Transaction;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.test.context.ActiveProfiles;

import java.math.BigDecimal;

import static com.mwallet.mwalletaccounts.domain.Account.AccountStatus.ACTIVE;
import static com.mwallet.mwalletaccounts.domain.Account.AccountType.USER_ACCOUNT;
import static com.mwallet.mwalletaccounts.domain.Account.builder;
import static com.mwallet.mwalletaccounts.domain.Transaction.PaymentMode.MPESA;
import static com.mwallet.mwalletaccounts.domain.Transaction.TransactionType.SEND_MONEY;
import static java.util.Comparator.comparing;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@Slf4j
@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(RedisExtension.class)
class TransactionEntityPersistenceAdapterTest {

    @Autowired
    private SaveAccountOutputPort saveAccountOutputPort;
    @Autowired
    private SaveTransactionOutputPort saveTransactionOutputPort;
    @Autowired
    private LoadTransactionOutputPort loadTransactionOutputPort;
    @Autowired
    private AccountEntityJpaRepository accountEntityJpaRepository;
    @Autowired
    private TransactionEntityJpaRepository transactionEntityJpaRepository;

    private Account srcAccount;
    private Account destAccount;
    private Transaction transaction;
    private Transaction existingTransaction_1;
    private Transaction existingTransaction_2;

    @BeforeEach
    void setUp() {
        srcAccount = builder()
                .accountId("TEST_ACCOUNT_ID_1")
                .linkId("1")
                .accountStatus(ACTIVE)
                .accountType(USER_ACCOUNT)
                .availableBalance(new BigDecimal("1000"))
                .actualBalance(new BigDecimal("2000"))
                .build();
        destAccount = srcAccount.toBuilder().accountId("TEST_ACCOUNT_ID_2").linkId("20").build();

        srcAccount = saveAccountOutputPort.createAccount(srcAccount);
        destAccount = saveAccountOutputPort.createAccount(destAccount);

        transaction = Transaction.builder()
                .transactionRef("FSNFSJDNFSK")
                .sourceAccount(srcAccount)
                .destinationAccount(destAccount)
                .amount(new BigDecimal("1000.00"))
                .transactionCost(new BigDecimal("20.00"))
                .transactionType(SEND_MONEY)
                .paymentMode(MPESA)
                .billingAddress("TEST_BILLING_ADDRESS")
                .transactionDescription("TEST_TRANSACTION_DESC")
                .transactionDomainType("TEST_DOMAIN_TYPE")
                .confirmationType(Transaction.ConfirmationType.INTERNAL)
                .transactionStatus(Transaction.TransactionStatus.NONE)
                .confirmationStatus(Transaction.ConfirmationStatus.NONE)
                .build();
        existingTransaction_1 = transaction.toBuilder().transactionRef("TEST_TRANSACTION_REF_1").build();
        existingTransaction_2 = transaction.toBuilder().transactionRef("TEST_TRANSACTION_REF_2").build();

        existingTransaction_1 = saveTransactionOutputPort.createTransaction(existingTransaction_1);
        existingTransaction_2 = saveTransactionOutputPort.createTransaction(existingTransaction_2);
    }

    @AfterEach
    void tearDown() {
        accountEntityJpaRepository.deleteAll();
        transactionEntityJpaRepository.deleteAll();
    }

    @Test
    @DisplayName("createTransaction -> persist")
    void createTransaction_WillCreateNewTransaction() {
        //GIVEN
        Transaction givenTransaction = transaction;
        //WHEN
        Transaction createdTransaction = saveTransactionOutputPort.createTransaction(givenTransaction);
        //THEN
        assertThat(createdTransaction)
                .isNotNull()
                .hasNoNullFieldsOrProperties();
    }

    @Test
    @DisplayName("createTransaction -> existingTransaction -> NonUniqueTransactionException")
    void createTransaction_WillThrow_DuplicateTransactionException() {
        //GIVEN
        Transaction givenTransaction = existingTransaction_1;
        //WHEN+THEN
        assertThatThrownBy(() -> saveTransactionOutputPort.createTransaction(givenTransaction))
                .isInstanceOf(SaveTransactionOutputPort.NonUniqueTransactionException.class)
                .hasMessageContaining("NonUniqueTransaction, TransactionRef: " + givenTransaction.getTransactionRef());
    }

    @Test
    @DisplayName("updateTransaction -> updateSelf -> !!!DOES NOT!!! updateLinkedAccounts")
    void updateTransaction_WillUpdateSelf_But_NOT_LinkedAccountsState() {
        //GIVEN
        BigDecimal prevSrcAccountAvailableBalance = srcAccount.getAvailableBalance();
        BigDecimal newSrcAccountAvailableBalance = new BigDecimal("16000.25");

        BigDecimal prevDestAccountActualBalance = destAccount.getActualBalance();
        BigDecimal newDestAccountActualBalance = new BigDecimal("45000.97");

        Transaction givenTransaction = existingTransaction_1.toBuilder()
                .transactionStatus(Transaction.TransactionStatus.COMPLETED)
                .sourceAccount(srcAccount.toBuilder().availableBalance(newSrcAccountAvailableBalance).build())
                .destinationAccount(destAccount.toBuilder().actualBalance(newDestAccountActualBalance).build())
                .build();

        //WHEN
        Transaction updatedTransaction = saveTransactionOutputPort.updateTransaction(givenTransaction);
        //THEN
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(updatedTransaction)
                .isNotNull()
                .hasNoNullFieldsOrProperties()
                .isEqualToIgnoringGivenFields(givenTransaction, "updatedOn", "version", "sourceAccount", "destinationAccount")
                .extracting(Transaction::getTransactionStatus, Transaction::getVersion)
                .containsExactly(Transaction.TransactionStatus.COMPLETED, 1L);

        softly.assertThat(updatedTransaction)
                .extracting(Transaction::getSourceAccount, Transaction::getDestinationAccount)
                .flatExtracting("availableBalance", "actualBalance")
                .usingComparatorForType(comparing(o -> ((BigDecimal) o)), BigDecimal.class)
                .containsExactly(prevSrcAccountAvailableBalance, srcAccount.getActualBalance(), destAccount.getAvailableBalance(), prevDestAccountActualBalance);

        softly.assertAll();
    }

    @Test
    @DisplayName("getTransactions --> sourceAccount || destinationAccount")
    void getTransactionsByAccountId() {
        //GIVEN
        Account givenAccount = existingTransaction_1.getSourceAccount();

        //WHEN
        Page<Transaction> transactions = loadTransactionOutputPort.loadTransactionsByAccountIdFilterByTime(
                givenAccount.getAccountId(),
                DateUtils.getZonedDateTimeAtHourOfDay("Africa/Nairobi", 0).toLocalDateTime(),
                DateUtils.getZonedDateTimeAtHourOfDay("Africa/Nairobi", 23).toLocalDateTime(),
                0,
                10
        );

        //THEN
        assertThat(transactions.toList())
                .isNotEmpty()
                .hasSize(2);
    }

}
