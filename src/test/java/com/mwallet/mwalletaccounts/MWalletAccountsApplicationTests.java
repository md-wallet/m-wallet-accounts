package com.mwallet.mwalletaccounts;

import com.mwallet.mwalletaccounts.config.RedisExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(RedisExtension.class)
class MWalletAccountsApplicationTests {

    @Test
    void contextLoads() {
    }

}
