package com.mwallet.mwalletaccounts.infrastructure.feign;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.http.Fault;
import com.github.tomakehurst.wiremock.http.HttpHeader;
import com.mwallet.mwalletcommons.utils.WireMockTestUtils;
import com.mwallet.mwalletaccounts.config.RedisExtension;
import com.mwallet.mwalletaccounts.domain.Payment.PaymentRequest;
import com.mwallet.mwalletaccounts.domain.Payout;
import com.mwallet.mwalletaccounts.domain.Payout.PayoutRequest;
import com.mwallet.mwalletaccounts.domain.Transaction;
import com.mwallet.mwalletaccounts.infrastructure.feign.PaymentsFeignClient.Models.PaymentRequestDTO;
import com.mwallet.mwalletaccounts.infrastructure.feign.PaymentsFeignClient.Models.PayoutRequestDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpHeaders;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.bind.annotation.RequestMethod;

import java.math.BigDecimal;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.mwallet.mwalletcommons.utils.ObjectUtils.writeJson;
import static com.mwallet.mwalletaccounts.domain.Payment.PaymentRequest.PaymentRequestStatus.PAYMENT_REQUEST_ACCEPTED;
import static com.mwallet.mwalletaccounts.domain.Payment.PaymentRequest.PaymentRequestStatus.PAYMENT_REQUEST_FAILED;
import static com.mwallet.mwalletaccounts.domain.Payout.PayoutRequest.PayoutRequestStatus.PAYOUT_REQUEST_ACCEPTED;
import static com.mwallet.mwalletaccounts.domain.Payout.PayoutRequest.PayoutRequestStatus.PAYOUT_REQUEST_FAILED;
import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(RedisExtension.class)
class PaymentsFeignClientTest {

    private String paymentsBaseURL;
    private WireMockServer wireMockServer;
    private PaymentRequestDTO paymentRequestDTO;
    private PayoutRequestDTO payoutRequestDTO;

    @Autowired
    private PaymentsFeignClient paymentsFeignClient;


    @BeforeEach
    void setUp() {
        paymentRequestDTO = PaymentRequestDTO.builder()
                .transactionRef("TEST_TRANSACTION_REF")
                .paymentMode(Transaction.PaymentMode.MPESA)
                .billingAddress("TEST_BILLING_ADDRESS")
                .amount(BigDecimal.valueOf(200))
                .transactionDescription("TEST_DESC")
                .build();

        payoutRequestDTO = PayoutRequestDTO.builder()
                .transactionRef("TEST_TRANSACTION_REF")
                .paymentMode(Transaction.PaymentMode.MPESA)
                .billingAddress("TEST_BILLING_ADDRESS")
                .amount(BigDecimal.valueOf(200))
                .transactionDescription("TEST_DESC")
                .build();

        paymentsBaseURL = "/payments/v1/";
        wireMockServer = WireMockTestUtils.configureServer(8091, true);
        wireMockServer.start();
    }

    @AfterEach
    void tearDown() {
        wireMockServer.stop();
    }

    @Test
    @DisplayName("requestPayment -> OK")
    void requestPayment_OK() {
        //GIVEN
        PaymentRequestDTO givenPaymentRequestDTO = paymentRequestDTO;
        String givenUrl = paymentsBaseURL + "payments/request";
        log.debug("paymentRequest: {}", writeJson(paymentRequestDTO));

        WireMockTestUtils.createStub(
                wireMockServer,
                RequestMethod.POST,
                givenUrl,
                HttpStatus.OK,
                "PaymentsFeignClientTest/PaymentRequest_Response_OK.json",
                true,
                new HttpHeader("Content-Type", "application/json")

        );

        //WHEN
        PaymentRequest paymentRequestResult = paymentsFeignClient.requestPayment(givenPaymentRequestDTO);
        log.debug("paymentRequestResult: {}", writeJson(paymentRequestResult));

        //THEN
        //-->check request
        wireMockServer.verify(postRequestedFor(urlPathMatching(givenUrl))
                .withHeader("Content-Type", matching("application/json"))
                .withRequestBody(equalToJson(writeJson(givenPaymentRequestDTO)))
        );

        //-->check response
        assertThat(paymentRequestResult)
                .isNotNull()
                .extracting(PaymentRequest::getPaymentRequestStatus, PaymentRequest::getNarration)
                .containsExactly(PAYMENT_REQUEST_ACCEPTED, "STK_REQUEST SUCCESSFUL");
    }


    @ParameterizedTest
    @DisplayName("requestPayment -> FAULT -> Fallback")
    @EnumSource(Fault.class)
    void requestPayment_FAULT(Fault fault) {
        //GIVEN
        PaymentRequestDTO givenPaymentRequestDTO = paymentRequestDTO;
        String givenUrl = paymentsBaseURL + "payments/request";
        log.debug("paymentRequest: {}", writeJson(paymentRequestDTO));

        WireMockTestUtils.createFaultStub(wireMockServer, RequestMethod.POST, givenUrl, fault);

        //WHEN
        PaymentRequest paymentRequestResult = paymentsFeignClient.requestPayment(givenPaymentRequestDTO);
        log.debug("paymentRequestResult: {}", writeJson(paymentRequestResult));

        //THEN
        //-->check request
        wireMockServer.verify(postRequestedFor(urlPathMatching(givenUrl))
                .withHeader("Content-Type", matching("application/json"))
                .withRequestBody(equalToJson(writeJson(givenPaymentRequestDTO)))
        );

        //-->check response
        assertThat(paymentRequestResult)
                .isNotNull()
                .extracting(PaymentRequest::getPaymentRequestStatus)
                .isEqualTo(PAYMENT_REQUEST_FAILED);

    }

    @Test
    @DisplayName("requestPayout -> OK")
    void requestPayout_OK() {
        //GIVEN
        PayoutRequestDTO givenPayoutRequestDTO = payoutRequestDTO;
        String givenUrl = paymentsBaseURL + "payouts/request";
        log.debug("payoutRequestDTO: {}", writeJson(givenPayoutRequestDTO));

        WireMockTestUtils.createStub(
                wireMockServer,
                RequestMethod.POST,
                givenUrl,
                HttpStatus.OK,
                "PaymentsFeignClientTest/PayoutRequest_Response_OK.json",
                true,
                new HttpHeader("Content-Type", "application/json")
        );

        //WHEN
        PayoutRequest payoutRequestResult = paymentsFeignClient.requestPayout(givenPayoutRequestDTO);

        //THEN
        //-->check request
        wireMockServer.verify(postRequestedFor(urlPathMatching(givenUrl))
                .withHeader(HttpHeaders.CONTENT_TYPE, matching(MediaType.APPLICATION_JSON_VALUE))
                .withRequestBody(equalToJson(writeJson(givenPayoutRequestDTO))));

        //-->check response
        assertThat(payoutRequestResult)
                .isNotNull()
                .extracting(PayoutRequest::getPayoutRequestStatus, Payout::getNarration)
                .containsExactly(PAYOUT_REQUEST_ACCEPTED, "PAYOUT SUCCESSFUL");

    }

    @ParameterizedTest
    @DisplayName("requestPayout -> FAULT -> Fallback")
    @EnumSource(Fault.class)
    void requestPayout_FAULT(Fault fault) {
        //GIVEN
        //GIVEN
        PayoutRequestDTO givenPayoutRequestDTO = payoutRequestDTO;
        String givenUrl = paymentsBaseURL + "payouts/request";
        log.debug("payoutRequestDTO: {}", writeJson(givenPayoutRequestDTO));

        WireMockTestUtils.createFaultStub(wireMockServer, RequestMethod.POST, givenUrl, fault);

        //WHEN
        PayoutRequest payoutRequestResult = paymentsFeignClient.requestPayout(givenPayoutRequestDTO);

        //THEN
        //-->check request
        wireMockServer.verify(postRequestedFor(urlPathMatching(givenUrl))
                .withHeader("Content-Type", matching("application/json"))
                .withRequestBody(equalToJson(writeJson(givenPayoutRequestDTO)))
        );

        //-->check response
        assertThat(payoutRequestResult)
                .isNotNull()
                .extracting(PayoutRequest::getPayoutRequestStatus)
                .isEqualTo(PAYOUT_REQUEST_FAILED);
    }
}
