package com.mwallet.mwalletaccounts.infrastructure.feign;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.http.HttpHeader;
import com.mwallet.mwalletaccounts.config.RedisExtension;
import com.mwallet.mwalletcommons.utils.WireMockTestUtils;
import com.mwallet.mwalletaccounts.domain.PaymentCard;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.bind.annotation.RequestMethod;

import static org.assertj.core.api.Assertions.assertThat;


@Slf4j
@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(RedisExtension.class)
class PaymentCardsFeignClientTest {

    private String authBaseURL;
    private WireMockServer wireMockServer;

    @Autowired
    private PaymentCardsFeignClient paymentCardsFeignClient;

    @BeforeEach
    void setUp() {
        authBaseURL = "/auth/v1/integrations/card/";
        wireMockServer = WireMockTestUtils.configureServer(8079, true);
        wireMockServer.start();
    }

    @AfterEach
    void tearDown() {
        wireMockServer.stop();
    }

    @Test
    @DisplayName("getCard -> OK")
    void getCard_OK() {
        //GIVEN
        String givenToken = "TOKEN";
        String givenTokenUrl = authBaseURL + givenToken;

        WireMockTestUtils.createStub(
                wireMockServer,
                RequestMethod.GET,
                givenTokenUrl,
                HttpStatus.OK,
                "PaymentCardFeignClientTest/GetPaymentCard_OK.json",
                true,
                new HttpHeader("Content-Type", "application/json"));

        //WHEN
        PaymentCard foundCard = paymentCardsFeignClient.getCard(givenToken);


        //THEN
        assertThat(foundCard)
                .isNotNull()
                .hasNoNullFieldsOrProperties();
    }


    @Test
    @DisplayName("getCard -> NotFound -> Fallback")
    void getCard_NotFound() {
        //GIVEN
        String givenToken = "TOKEN";
        String givenTokenUrl = authBaseURL + givenToken;

        WireMockTestUtils.createStub(
                wireMockServer,
                RequestMethod.GET,
                givenTokenUrl,
                HttpStatus.NOT_FOUND,
                "PaymentCardFeignClientTest/GetPaymentCard_NotFound.json",
                true,
                new HttpHeader("Content-Type", "application/json"));

        //WHEN
        PaymentCard foundCard = paymentCardsFeignClient.getCard(givenToken);

        //THEN
        assertThat(foundCard).isNull();
    }
}