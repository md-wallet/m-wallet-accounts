package com.mwallet.mwalletaccounts.infrastructure.mapping;

import com.github.rozidan.springboot.modelmapper.WithModelMapper;
import com.mwallet.mwalletaccounts.adapters.web.dtos.transactions.TransactionLogDTO;
import com.mwallet.mwalletaccounts.domain.Transaction;
import com.mwallet.mwalletaccounts.domain.TransactionLog;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Comparator;

import static com.mwallet.mwalletcommons.utils.ObjectUtils.writeJson;
import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
@SpringBootTest
@ActiveProfiles("test")
@ContextConfiguration
class TransactionLogMappingConfigTest {

    @Autowired
    private ModelMapper modelMapper;

    private TransactionLog transactionLog;

    @BeforeEach
    void setUp() {
        transactionLog = TransactionLog.builder()
                .transaction(
                        Transaction.builder()
                                .transactionRef("TEST_TRANSACTION_REF")
                                .amount(BigDecimal.valueOf(2000))
                                .transactionCost(BigDecimal.valueOf(20))
                                .build()
                )
                .confirmationStatus(Transaction.ConfirmationStatus.PROCEED)
                .destActualBalance(BigDecimal.valueOf(2000))
                .destAvailableBalance(BigDecimal.valueOf(2000))
                .message("TEST_MESSAGE")
                .srcActualBalance(BigDecimal.valueOf(2000))
                .srcAvailableBalance(BigDecimal.valueOf(2000))
                .transactionStatus(Transaction.TransactionStatus.COMPLETED)
                .createdOn(LocalDateTime.now())
                .build();
    }

    @Test
    @DisplayName("transactionLog -> transactionLogDTO")
    void transactionLogModelToTransactionLogDTOTest(){
        //GIVEN
        TransactionLog givenTransactionLog = transactionLog;
        log.debug("givenTransactionLog: {}",writeJson(givenTransactionLog));
        //WHEN
        TransactionLogDTO mappedTransactionLogDTO = modelMapper.map(givenTransactionLog,TransactionLogDTO.class);
        log.debug("mappedTransactionLogDTO: {}",writeJson(mappedTransactionLogDTO));
        //THEN
        assertThat(mappedTransactionLogDTO)
                .isNotNull()
                .hasNoNullFieldsOrProperties()
                .isEqualToComparingOnlyGivenFields(givenTransactionLog.getTransaction(),"transactionRef")
                .extracting(TransactionLogDTO::getAmount,TransactionLogDTO::getTransactionCost)
                .usingElementComparator((Comparator.comparing(num -> ((BigDecimal) num))))
                .containsExactly(givenTransactionLog.getTransaction().getAmount(),givenTransactionLog.getTransaction().getTransactionCost());
    }


    @Configuration
    @WithModelMapper(basePackages = "com.mwallet")
    static class ModelMapperConfig{

    }
}
