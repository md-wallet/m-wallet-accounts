package com.mwallet.mwalletaccounts.infrastructure.mapping;

import com.github.rozidan.springboot.modelmapper.WithModelMapper;
import com.mwallet.mwalletaccounts.adapters.web.dtos.transactions.TransactionInfoDTO;
import com.mwallet.mwalletaccounts.domain.Account;
import com.mwallet.mwalletaccounts.domain.Transaction;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static com.mwallet.mwalletcommons.utils.ObjectUtils.writeJson;
import static com.mwallet.mwalletaccounts.domain.Account.AccountStatus.ACTIVE;
import static com.mwallet.mwalletaccounts.domain.Account.AccountType.USER_ACCOUNT;
import static com.mwallet.mwalletaccounts.domain.Account.builder;
import static com.mwallet.mwalletaccounts.domain.Transaction.PaymentMode.MPESA;
import static com.mwallet.mwalletaccounts.domain.Transaction.TransactionType.SEND_MONEY;
import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
@SpringBootTest
@ActiveProfiles("test")
@ContextConfiguration
class TransactionMappingConfigTest {

    @Autowired
    private ModelMapper modelMapper;

    private Account srcAccount;
    private Account destAccount;
    private Transaction transaction;

    @BeforeEach
    void setUp() {

        srcAccount = builder()
                .accountId("TEST_ACCOUNT_ID_1")
                .linkId("1")
                .accountStatus(ACTIVE)
                .accountType(USER_ACCOUNT)
                .availableBalance(new BigDecimal("1000"))
                .actualBalance(new BigDecimal("2000"))
                .build();
        destAccount = srcAccount.toBuilder().accountId("TEST_ACCOUNT_ID_2").linkId("20").build();

        transaction = Transaction.builder()
                .transactionRef("TEST_TRANSACTION_REF")
                .sourceAccount(srcAccount)
                .destinationAccount(destAccount)
                .amount(new BigDecimal("1000.00"))
                .transactionCost(new BigDecimal("20.00"))
                .transactionType(SEND_MONEY)
                .paymentMode(MPESA)
                .createdOn(LocalDateTime.now())
                .updatedOn(LocalDateTime.now())
                .transactionDescription("TEST_DESC")
                .transactionDomainType("TEST_DOMAIN_TYPE")
                .confirmationType(Transaction.ConfirmationType.INTERNAL)
                .confirmationStatus(Transaction.ConfirmationStatus.NONE)
                .transactionStatus(Transaction.TransactionStatus.NONE)
                .build();
    }

    @Test
    @DisplayName("transaction -> transactionInfoDTO")
    void transactionToTransactionInfoDTO(){
        //GIVEN
        Transaction givenTransaction = transaction;
        log.debug("givenTransaction: {}",writeJson(givenTransaction));

        //WHEN
        TransactionInfoDTO transactionInfoDTO = modelMapper.map(transaction,TransactionInfoDTO.class);
        log.debug("transactionInfoDTO: {}",writeJson(transactionInfoDTO));

        //THEN
        assertThat(transactionInfoDTO)
                .isNotNull()
                .hasNoNullFieldsOrProperties()
                .isEqualToIgnoringGivenFields(givenTransaction,"sourceAccountId","destinationAccountId","createdOn","updatedOn")
                .extracting(TransactionInfoDTO::getSourceAccountId,TransactionInfoDTO::getDestinationAccountId)
                .containsExactly("TEST_ACCOUNT_ID_1","TEST_ACCOUNT_ID_2");

    }

    @Configuration
    @WithModelMapper(basePackages = "com.mwallet")
    static class ModelMapperConfig{

    }
}
