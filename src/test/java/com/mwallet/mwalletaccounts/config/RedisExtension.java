package com.mwallet.mwalletaccounts.config;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import redis.embedded.RedisServer;

/**
 * @author lawrence
 * created 22/01/2020 at 15:19
 **/
@Slf4j
public class RedisExtension implements BeforeAllCallback, AfterAllCallback {

    private RedisServer redisServer;

    @Override
    public void beforeAll(ExtensionContext extensionContext) throws Exception {
        log.info("\n================================================================ " +
                "\n<-------------------->STARTING REDIS SERVER<------------------->" +
                "\n================================================================");
        redisServer = RedisServer.builder()
                .port(6379)
                .setting("maxmemory 128M")
                .build();

        redisServer.start();
    }


    @Override
    public void afterAll(ExtensionContext extensionContext) throws Exception {
        log.info("\n================================================================ " +
                "\n<-------------------->STOPPING REDIS SERVER<------------------->" +
                "\n================================================================");
        redisServer.stop();
    }
}
