package com.mwallet.mwalletaccounts.domain;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.util.Pair;
import org.springframework.test.context.ActiveProfiles;

import java.math.BigDecimal;
import java.util.List;

import static com.mwallet.mwalletaccounts.domain.Account.AccountStatus.ACTIVE;
import static com.mwallet.mwalletaccounts.domain.Account.AccountStatus.INACTIVE;
import static com.mwallet.mwalletaccounts.domain.Account.AccountType.JOINT_ACCOUNT;
import static com.mwallet.mwalletaccounts.domain.Account.AccountType.USER_ACCOUNT;
import static com.mwallet.mwalletaccounts.domain.Account.builder;
import static com.mwallet.mwalletaccounts.domain.Transaction.PaymentMode;
import static com.mwallet.mwalletaccounts.domain.Transaction.PaymentMode.MPESA;
import static com.mwallet.mwalletaccounts.domain.Transaction.PaymentMode.WALLET;
import static com.mwallet.mwalletaccounts.domain.Transaction.TransactionStatus;
import static com.mwallet.mwalletaccounts.domain.Transaction.TransactionStatus.*;
import static com.mwallet.mwalletaccounts.domain.Transaction.TransactionType.DEPOSIT;
import static com.mwallet.mwalletaccounts.domain.Transaction.TransactionType.SEND_MONEY;
import static com.mwallet.mwalletaccounts.domain.ValidateTransaction.InvalidTransactionException;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@Slf4j
@ActiveProfiles("test")
@ExtendWith(MockitoExtension.class)
class ValidateTransactionTest {

    private static Account srcAccount;
    private static Account destAccount;
    private static Transaction transaction;
    ValidateTransaction validateTransaction = Mockito.mock(ValidateTransaction.class, Mockito.CALLS_REAL_METHODS);


    @BeforeEach
    void setUp() {
        srcAccount = builder()
                .accountId("TEST_ACCOUNT_ID_1")
                .linkId("1")
                .accountStatus(ACTIVE)
                .accountType(USER_ACCOUNT)
                .availableBalance(new BigDecimal("1000"))
                .actualBalance(new BigDecimal("2000"))
                .build();
        destAccount = srcAccount.toBuilder().accountId("TEST_ACCOUNT_ID_2").linkId("20").build();

        transaction = Transaction.builder()
                .sourceAccount(srcAccount)
                .destinationAccount(destAccount)
                .amount(new BigDecimal("1000"))
                .transactionCost(new BigDecimal("20"))
                .transactionType(SEND_MONEY)
                .paymentMode(MPESA)
                .confirmationType(Transaction.ConfirmationType.INTERNAL)
                .confirmationStatus(Transaction.ConfirmationStatus.NONE)
                .build();
    }

    /*#################################################################################################################*/
    @Test
    @DisplayName("checkCyclicTransaction -> InvalidTransactionException")
    void checkCyclicTransaction() {
        Transaction cyclicTransaction = transaction.toBuilder().sourceAccount(srcAccount).destinationAccount(srcAccount).build();
        assertThatThrownBy(() -> validateTransaction.checkCyclicTransaction(cyclicTransaction))
                .isInstanceOf(InvalidTransactionException.class)
                .hasMessageContaining("SAME Source and Destination Account");
    }

    @Test
    @DisplayName("checkCyclicTransaction -> ✅(DoesNotThrow")
    void checkCyclicTransaction_DoesNotThrow() {
        Transaction nonCyclicTransaction = transaction;
        assertThatCode(() -> validateTransaction.checkCyclicTransaction(nonCyclicTransaction)).doesNotThrowAnyException();
    }

    /*#################################################################################################################*/
    @Test
    @DisplayName("checkAccountStatus -> InvalidTransactionException")
    void checkAccountsStatus() {
        Transaction inactiveSourceAccount = transaction.toBuilder().sourceAccount(srcAccount.toBuilder().accountStatus(INACTIVE).build()).build();
        assertThatThrownBy(() -> validateTransaction.checkAccountsStatus(inactiveSourceAccount))
                .isInstanceOf(InvalidTransactionException.class)
                .hasMessageContaining("INACTIVE Source Account");
    }

    @Test
    @DisplayName("checkAccountsStatus -> ✅(DoesNotThrow)")
    void checkAccountsStatus_DoesNotThrow() {
        Transaction activeSourceAccount = transaction;
        assertThatCode(() -> validateTransaction.checkAccountsStatus(activeSourceAccount)).doesNotThrowAnyException();
    }

    /*#################################################################################################################*/
    @ParameterizedTest
    @CsvSource({"5", "20000"})
    @DisplayName("checkTransactionAmountInRange -> InvalidTransactionException")
    void checkTransactionAmountInRange(BigDecimal amount) {
        Transaction givenTransaction = transaction.toBuilder().amount(amount).build();
        assertThatThrownBy(() -> validateTransaction.checkTransactionAmountInRange(givenTransaction, BigDecimal.valueOf(10), BigDecimal.valueOf(10000)))
                .isInstanceOf(InvalidTransactionException.class)
                .hasMessageContaining("Transaction Amount must be within the range: 10-10000");
    }

    @ParameterizedTest
    @CsvSource({"10", "200", "400", "10000"})
    @DisplayName("checkTransactionAmountInRange -> ✅(DoesNotThrow)")
    void checkTransactionAmountInRange_DoesNotThrow(BigDecimal amount) {
        Transaction givenTransaction = transaction.toBuilder().amount(amount).build();
        assertThatCode(() -> validateTransaction.checkTransactionAmountInRange(givenTransaction, BigDecimal.valueOf(10), BigDecimal.valueOf(10000))).doesNotThrowAnyException();
    }

    /*#################################################################################################################*/
    @Test
    @DisplayName("checkFundsAvailability -> InvalidTransactionException")
    void checkFundsAvailability() {
        Transaction insufficientBalance = transaction.toBuilder().sourceAccount(srcAccount.toBuilder().availableBalance(new BigDecimal("200")).build()).build();
        assertThatThrownBy(() -> validateTransaction.checkFundsAvailability(insufficientBalance))
                .isInstanceOf(InvalidTransactionException.class)
                .hasMessageContaining("Insufficient Available Balance: 200");
    }

    @Test
    @DisplayName("checkFundsAvailability -> ✅(DoesNotThrow)")
    void checkFundsAvailability_DoesNotThrow() {
        Transaction sufficientBalance = transaction.toBuilder().sourceAccount(srcAccount.toBuilder().availableBalance(new BigDecimal("5000")).build()).build();
        assertThatCode(() -> validateTransaction.checkFundsAvailability(sufficientBalance)).doesNotThrowAnyException();
    }

    /*#################################################################################################################*/
    @Test
    @DisplayName("checkTransactionType -> InvalidTransactionException")
    void checkTransactionType() {
        Transaction givenTransaction = transaction.toBuilder().transactionType(DEPOSIT).build();
        assertThatThrownBy(() -> validateTransaction.checkTransactionType(givenTransaction, SEND_MONEY))
                .isInstanceOf(InvalidTransactionException.class)
                .hasMessageContaining("Invalid Transaction Type: " + givenTransaction.getTransactionType());
    }

    @Test
    @DisplayName("checkTransactionType -> ✅(DoesNotThrow)")
    void checkTransactionType_DoesNotThrow() {
        Transaction givenTransaction = transaction.toBuilder().transactionType(DEPOSIT).build();
        assertThatCode(() -> validateTransaction.checkTransactionType(givenTransaction, DEPOSIT)).doesNotThrowAnyException();
    }

    /*#################################################################################################################*/
    @Test
    @DisplayName("checkSupportedPaymentModes -> InvalidTransactionException")
    void checkSupportedPaymentModes() {
        Transaction givenTransaction = transaction;
        List<PaymentMode> supportedPaymentModes = List.of(WALLET);
        assertThatThrownBy(() -> validateTransaction.checkSupportedPaymentModes(givenTransaction, supportedPaymentModes))
                .isInstanceOf(InvalidTransactionException.class)
                .hasMessageContaining("Unsupported Payment Mode: " + givenTransaction.getPaymentMode());
    }

    @Test
    @DisplayName("checkSupportedPaymentModes -> ✅(DoesNotThrow)")
    void checkSupportedPaymentModes_DoesNotThrow() {
        Transaction givenTransaction = transaction;
        List<PaymentMode> supportedPaymentModes = List.of(MPESA);
        assertThatCode(() -> validateTransaction.checkSupportedPaymentModes(givenTransaction, supportedPaymentModes)).doesNotThrowAnyException();
    }

    /*#################################################################################################################*/
    @Test
    @DisplayName("checkTransactionStatus -> InvalidTransactionException")
    void checkTransactionStatus() {
        Transaction givenTransaction = transaction.toBuilder().transactionStatus(NONE).build();
        List<TransactionStatus> permissibleTransactionStatus = List.of(CONFIRMED, COMPLETED);
        assertThatThrownBy(() -> validateTransaction.checkTransactionStatus(givenTransaction, permissibleTransactionStatus))
                .isInstanceOf(InvalidTransactionException.class)
                .hasMessageContaining("Unexpected Transaction Status: " + givenTransaction.getTransactionStatus());
    }

    @Test
    @DisplayName("checkTransactionStatus ->  ✅(DoesNotThrow)")
    void checkTransactionStatus_DoesNotThrow() {
        Transaction givenTransaction = transaction.toBuilder().transactionStatus(NONE).build();
        List<TransactionStatus> permissibleTransactionStatus = List.of(NONE);
        assertThatCode(() -> validateTransaction.checkTransactionStatus(givenTransaction, permissibleTransactionStatus)).doesNotThrowAnyException();
    }
    /*#################################################################################################################*/

    @Test
    @DisplayName("checkTransactionAccountTypes -> InvalidTransaction")
    void checkTransactionAccountTypes() {
        Transaction givenTransaction = transaction.toBuilder()
                .sourceAccount(srcAccount.toBuilder().accountType(USER_ACCOUNT).build())
                .destinationAccount(destAccount.toBuilder().accountType(USER_ACCOUNT).build())
                .build();
        List<Pair<Account.AccountType, Account.AccountType>> supportedSrcDestAccountTypes = List.of(Pair.of(USER_ACCOUNT, JOINT_ACCOUNT));
        assertThatThrownBy(() -> validateTransaction.checkSupportedAccountTypes(givenTransaction, supportedSrcDestAccountTypes))
                .isInstanceOf(InvalidTransactionException.class)
                .hasMessageContaining("Invalid Source, Destination Account Types: <USER_ACCOUNT,USER_ACCOUNT>");
    }

    @Test
    @DisplayName("checkTransactionAccountTypes ->  ✅(DoesNotThrow)")
    void checkTransactionAccountTypes_DoesNotThrow() {
        Transaction givenTransaction = transaction.toBuilder()
                .sourceAccount(srcAccount.toBuilder().accountType(USER_ACCOUNT).build())
                .destinationAccount(destAccount.toBuilder().accountType(USER_ACCOUNT).build())
                .build();
        List<Pair<Account.AccountType, Account.AccountType>> supportedSrcDestAccountTypes = List.of(Pair.of(USER_ACCOUNT, USER_ACCOUNT));
        assertThatCode(() -> validateTransaction.checkSupportedAccountTypes(givenTransaction, supportedSrcDestAccountTypes)).doesNotThrowAnyException();
    }

    /*#################################################################################################################*/

    @Test
    @DisplayName("checkPaymentResponse ->  InvalidTransaction")
    void checkPaymentResponse() {
        Transaction givenTransaction = transaction.toBuilder()
                .paymentMode(MPESA)
                .transactionRef("TEST_TRANSACTION_REF")
                .amount(BigDecimal.valueOf(200))
                .build();

        Payment.PaymentResponse paymentResponse = Payment.PaymentResponse.builder()
                .paymentMode(MPESA)
                .transactionRef("TEST_TRANSACTION_REF")
                .amount(BigDecimal.valueOf(300))
                .build();
        assertThatThrownBy(() -> validateTransaction.checkPaymentResponse(paymentResponse, givenTransaction))
                .isInstanceOf(InvalidTransactionException.class)
                .hasMessageContaining("Invalid PaymentResponse for transaction: TEST_TRANSACTION_REF");
    }

    @Test
    @DisplayName("checkPaymentResponse ->  ✅(DoesNotThrow)")
    void checkPaymentResponse_DoesNotThrow() {
        Transaction givenTransaction = transaction.toBuilder()
                .paymentMode(MPESA)
                .transactionRef("TEST_TRANSACTION_REF")
                .amount(BigDecimal.valueOf(200))
                .transactionCost(BigDecimal.ZERO)
                .build();

        Payment.PaymentResponse paymentResponse = Payment.PaymentResponse.builder()
                .paymentMode(MPESA)
                .transactionRef("TEST_TRANSACTION_REF")
                .billingAddress("TEST_BILLING_ADDRESS")
                .amount(BigDecimal.valueOf(200))
                .build();

        assertThatCode(() -> validateTransaction.checkPaymentResponse(paymentResponse, givenTransaction)).doesNotThrowAnyException();
    }

    /*#################################################################################################################*/

    @Test
    @DisplayName("checkPayoutResponse ->  InvalidTransaction")
    void checkPayoutResponse() {
        Transaction givenTransaction = transaction.toBuilder()
                .paymentMode(MPESA)
                .transactionRef("TEST_TRANSACTION_REF")
                .amount(BigDecimal.valueOf(200))
                .build();

        Payout.PayoutResponse payoutResponse = Payout.PayoutResponse.builder()
                .paymentMode(MPESA)
                .transactionRef("TEST_TRANSACTION_REF")
                .amount(BigDecimal.valueOf(300))
                .build();

        assertThatThrownBy(() -> validateTransaction.checkPayoutResponse(payoutResponse, givenTransaction))
                .isInstanceOf(InvalidTransactionException.class)
                .hasMessageContaining("Invalid PayoutResponse for transaction: TEST_TRANSACTION_REF");
    }

    @Test
    @DisplayName("checkPaymentResponse ->  ✅(DoesNotThrow)")
    void checkPayoutResponse_DoesNotThrow() {
        Transaction givenTransaction = transaction.toBuilder()
                .paymentMode(MPESA)
                .transactionRef("TEST_TRANSACTION_REF")
                .amount(BigDecimal.valueOf(200))
                .build();

        Payout.PayoutResponse payoutResponse = Payout.PayoutResponse.builder()
                .paymentMode(MPESA)
                .transactionRef("TEST_TRANSACTION_REF")
                .amount(BigDecimal.valueOf(200))
                .build();

        assertThatCode(() -> validateTransaction.checkPayoutResponse(payoutResponse, givenTransaction)).doesNotThrowAnyException();
    }

    /*#################################################################################################################*/

    @Test
    @DisplayName("checkPaymentCard --> InvalidTransaction")
    void checkPaymentCard() {
        PaymentCard givenCard = null;
        assertThatThrownBy(() -> validateTransaction.checkPaymentCard(givenCard))
                .isInstanceOf(InvalidTransactionException.class)
                .hasMessageContaining("Invalid Card");
    }

    @Test
    @DisplayName("checkPaymentCard ->  ✅(DoesNotThrow)")
    void checkPaymentCard_DoesNotThrow(){
        PaymentCard givenCard = PaymentCard.builder().cardStatus(PaymentCard.CardStatus.ACTIVE).build();
        assertThatCode(()->validateTransaction.checkPaymentCard(givenCard)).doesNotThrowAnyException();
    }


}
