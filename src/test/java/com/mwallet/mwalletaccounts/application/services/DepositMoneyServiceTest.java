package com.mwallet.mwalletaccounts.application.services;

import com.mwallet.mwalletcommons.notifications.NotificationType;
import com.mwallet.mwalletaccounts.application.ports.out.accounts.LoadAccountOutputPort;
import com.mwallet.mwalletaccounts.application.ports.out.accounts.TransferFundsOutputPort;
import com.mwallet.mwalletaccounts.application.ports.out.payments.PaymentsOutputPort;
import com.mwallet.mwalletaccounts.application.ports.out.transactionlogs.LogTransactionOutputPort;
import com.mwallet.mwalletaccounts.application.ports.out.transactions.*;
import com.mwallet.mwalletaccounts.domain.Account;
import com.mwallet.mwalletaccounts.domain.Payment;
import com.mwallet.mwalletaccounts.domain.Transaction;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.util.Pair;
import org.springframework.test.context.ActiveProfiles;

import java.math.BigDecimal;

import static com.mwallet.mwalletcommons.utils.ObjectUtils.readJson;
import static com.mwallet.mwalletcommons.utils.ObjectUtils.writeJson;
import static com.mwallet.mwalletaccounts.domain.Account.AccountStatus.ACTIVE;
import static com.mwallet.mwalletaccounts.domain.Account.AccountType.JOINT_ACCOUNT;
import static com.mwallet.mwalletaccounts.domain.Account.builder;
import static com.mwallet.mwalletaccounts.domain.Payment.PaymentRequest.PaymentRequestStatus.PAYMENT_REQUEST_FAILED;
import static com.mwallet.mwalletaccounts.domain.Payment.PaymentRequest.PaymentRequestStatus.PAYMENT_REQUEST_REJECTED;
import static com.mwallet.mwalletaccounts.domain.Transaction.ConfirmationStatus.PROCEED;
import static com.mwallet.mwalletaccounts.domain.Transaction.ConfirmationStatus.ROLLBACK;
import static com.mwallet.mwalletaccounts.domain.Transaction.PaymentMode.MPESA;
import static com.mwallet.mwalletaccounts.domain.Transaction.TransactionStatus.*;
import static com.mwallet.mwalletaccounts.domain.Transaction.TransactionType.DEPOSIT;
import static java.util.Comparator.comparing;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.*;

@Slf4j
@ActiveProfiles("test")
@ExtendWith(MockitoExtension.class)
class DepositMoneyServiceTest {

    private Account srcAccount;
    private Account destAccount;
    private Transaction transaction;

    @Captor
    private ArgumentCaptor<?> argumentCaptor;

    @Mock
    private SaveTransactionOutputPort saveTransactionOutputPort;
    @Mock
    private LogTransactionOutputPort logTransactionOutputPort;
    @Mock
    private TransferFundsOutputPort transferFundsOutputPort;
    @Mock
    private GenerateTransactionRefOutputPort transactionRefOutputPort;
    @Mock
    private TransactionCacheManagementOutputPort transactionCacheManagementOutputPort;
    @Mock
    private LoadTransactionOutputPort loadTransactionOutputPort;
    @Mock
    private LoadAccountOutputPort loadAccountOutputPort;
    @Mock
    private PublishTransactionOutputPort publishTransactionOutputPort;
    @Mock
    private PaymentsOutputPort paymentsOutputPort;
    @Mock
    private SendTransactionNotificationOutputPort sendTransactionNotificationOutputPort;

    @InjectMocks
    private DepositMoneyService depositMoneyService;

    @BeforeEach
    void setUp() {
        srcAccount = builder()
                .accountId("TEST_ACCOUNT_ID_1")
                .linkId(Account.AccountType.TRUST_ACCOUNT.name())
                .accountStatus(ACTIVE)
                .accountType(Account.AccountType.TRUST_ACCOUNT)
                .availableBalance(new BigDecimal("1000"))
                .actualBalance(new BigDecimal("2000"))
                .build();
        destAccount = srcAccount.toBuilder().accountId("TEST_ACCOUNT_ID_2").linkId("20").accountType(JOINT_ACCOUNT).build();

        transaction = Transaction.builder()
                .sourceAccount(srcAccount)
                .destinationAccount(destAccount)
                .amount(new BigDecimal("500"))
                .transactionCost(BigDecimal.ZERO)
                .transactionType(DEPOSIT)
                .confirmationStatus(Transaction.ConfirmationStatus.NONE)
                .confirmationType(Transaction.ConfirmationType.INTERNAL)
                .transactionStatus(NONE)
                .paymentMode(MPESA)
                .build();
    }

    @Test
    void validateDepositMoney() {
        //GIVEN
        Transaction givenTransaction = transaction;
        String transactionHash = "TEST_TRANSACTION_HASH";
        given(transactionCacheManagementOutputPort.saveTransaction(any(Transaction.class))).willReturn(transactionHash);

        //WHEN
        Pair<String, Transaction> validationResult = depositMoneyService.validateDepositMoney(givenTransaction);
        //THEN
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(validationResult)
                .isNotNull()
                .isEqualToIgnoringNullFields(Pair.of(transactionHash, givenTransaction));
        softly.assertThat(validationResult.getSecond())
                .extracting(Transaction::getTransactionStatus)
                .isEqualTo(VALIDATED);

        softly.assertAll();
    }

    @Test
    void requestDepositMoney() {
        //GIVEN
        Transaction givenTransaction = transaction.toBuilder().transactionStatus(VALIDATED).build();
        String transactionHash = "TEST_TRANSACTION_HASH";
        String transactionRef = "TEST_TRANSACTION_REF";

        given(transactionCacheManagementOutputPort.getTransactionByHash(anyString())).willReturn(givenTransaction);
        given(loadAccountOutputPort.loadAccountById(srcAccount.getAccountId())).willReturn(srcAccount);
        given(loadAccountOutputPort.loadAccountById(destAccount.getAccountId())).willReturn(destAccount);
        given(transactionRefOutputPort.generateTransactionRef(any(Transaction.class))).willReturn(transactionRef);
        given(saveTransactionOutputPort.createTransaction(any(Transaction.class))).willAnswer(returnsFirstArg());

        //WHEN
        depositMoneyService.requestDepositMoney(transactionHash);

        //THEN
        then(saveTransactionOutputPort).should().createTransaction((Transaction) argumentCaptor.capture());
        then(logTransactionOutputPort).should().logTransaction(any(Transaction.class), (String) argumentCaptor.capture());

        Transaction generatedTransaction = (Transaction) argumentCaptor.getAllValues().get(0);
        String message = (String) argumentCaptor.getAllValues().get(1);

        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(generatedTransaction)
                .isNotNull()
                .extracting(Transaction::getTransactionRef, Transaction::getTransactionStatus)
                .containsExactly(transactionRef, ACCEPTED);

        softly.assertThat(message)
                .isNotNull()
                .isEqualTo("STARTED TXN: TEST_TRANSACTION_REF");

        softly.assertAll();
    }

    @Test
    void prepareDepositMoney() {
        //GIVEN
        Transaction givenTransaction = transaction.toBuilder().transactionStatus(ACCEPTED).build();

        given(saveTransactionOutputPort.updateTransaction(givenTransaction)).willAnswer(returnsFirstArg());
        given(loadTransactionOutputPort.loadTransactionByTransactionRef(givenTransaction.getTransactionRef())).willReturn(givenTransaction);
        given(transferFundsOutputPort.prepareTransfer(any(String.class), any(String.class), any(BigDecimal.class), any(BigDecimal.class)))
                .willAnswer(invocationOnMock -> {
                    BigDecimal amount = invocationOnMock.getArgument(2);
                    BigDecimal transactionCost = invocationOnMock.getArgument(3);
                    Account sourceAccount = givenTransaction.getSourceAccount().toBuilder().availableBalance(srcAccount.getAvailableBalance().subtract(amount.add(transactionCost))).build();
                    Account destinationAccount = givenTransaction.getDestinationAccount().toBuilder().actualBalance(destAccount.getActualBalance().add(amount)).build();
                    return Pair.of(sourceAccount, destinationAccount);
                });

        //WHEN
        depositMoneyService.prepareDepositMoney(givenTransaction);
        //THEN
        then(saveTransactionOutputPort).should().updateTransaction((Transaction) argumentCaptor.capture());
        then(logTransactionOutputPort).should().logTransaction(any(Transaction.class), (String) argumentCaptor.capture());

        Transaction updatedTransaction = (Transaction) argumentCaptor.getAllValues().get(0);
        String message = (String) argumentCaptor.getAllValues().get(1);

        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(updatedTransaction)
                .isNotNull()
                .extracting(Transaction::getTransactionStatus)
                .isEqualTo(PREPARED);
        softly.assertThat(updatedTransaction)
                .extracting(Transaction::getSourceAccount, Transaction::getDestinationAccount)
                .flatExtracting("availableBalance", "actualBalance")
                .usingElementComparator(comparing(o -> ((BigDecimal) o)))
                .containsExactly(BigDecimal.valueOf(500.00000), BigDecimal.valueOf(2000), BigDecimal.valueOf(1000), BigDecimal.valueOf(2500));

        softly.assertThat(message)
                .isNotNull()
                .isEqualTo("ACCOUNTS: -SRC_AVAIL(500), +DST_ACTUAL(500)");
        softly.assertAll();

        verifyNoMoreInteractions(transferFundsOutputPort, saveTransactionOutputPort, logTransactionOutputPort);
    }

    @Test
    @DisplayName("requestDepositMoneyPayment --> PAYMENT_REQUEST_ACCEPTED")
    void requestDepositMoneyPayment_PAYMENT_REQUEST_ACCEPTED() {
        //GIVEN
        Transaction givenTransaction = transaction.toBuilder().transactionStatus(PREPARED).build();
        given(saveTransactionOutputPort.updateTransaction(any(Transaction.class))).willAnswer(returnsFirstArg());
        given(loadTransactionOutputPort.loadTransactionByTransactionRef(givenTransaction.getTransactionRef())).willReturn(givenTransaction);
        given(paymentsOutputPort.requestPayment(any(Transaction.class)))
                .willAnswer(invocationOnMock -> {
                    Transaction transaction = invocationOnMock.getArgument(0);
                    return Payment.PaymentRequest.builder()
                            .narration(Payment.PaymentRequest.PaymentRequestStatus.PAYMENT_REQUEST_ACCEPTED.name() + ":" + "STK_REQUEST SUCCESSFUL")
                            .paymentRequestStatus(Payment.PaymentRequest.PaymentRequestStatus.PAYMENT_REQUEST_ACCEPTED)
                            .amount(transaction.getAmount())
                            .transactionRef(transaction.getTransactionRef())
                            .billingAddress(transaction.getBillingAddress())
                            .build();
                });

        //WHEN
        boolean depositRequestSuccessful = depositMoneyService.requestDepositMoneyPayment(givenTransaction);

        //THEN
        then(saveTransactionOutputPort).should().updateTransaction((Transaction) argumentCaptor.capture());
        then(logTransactionOutputPort).should().logTransaction(any(Transaction.class), (String) argumentCaptor.capture());

        Transaction updatedTransaction = (Transaction) argumentCaptor.getAllValues().get(0);
        String message = (String) argumentCaptor.getAllValues().get(1);

        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(depositRequestSuccessful).isTrue();

        softly.assertThat(updatedTransaction)
                .isNotNull()
                .extracting(Transaction::getTransactionStatus)
                .isEqualTo(PAYMENT_REQUESTED);

        softly.assertThat(message)
                .isNotNull()
                .isEqualTo("PAYMENT_REQUEST_ACCEPTED:STK_REQUEST SUCCESSFUL");
        softly.assertAll();

        verifyNoMoreInteractions(saveTransactionOutputPort, logTransactionOutputPort, paymentsOutputPort);

    }

    @Test
    @DisplayName("requestDepositMoneyPayment --> PAYMENT_REQUEST_REJECTED")
    void requestDepositMoneyPayment_PAYMENT_REQUEST_REJECTED() {
        //GIVEN
        Transaction givenTransaction = transaction.toBuilder().transactionStatus(PREPARED).build();
        given(saveTransactionOutputPort.updateTransaction(any(Transaction.class)))
                .willAnswer(invocationOnMock -> {
                    Transaction transaction = invocationOnMock.getArgument(0);
                    return readJson(writeJson(transaction), Transaction.class);
                });
        given(loadTransactionOutputPort.loadTransactionByTransactionRef(givenTransaction.getTransactionRef())).willReturn(givenTransaction);
        given(paymentsOutputPort.requestPayment(any(Transaction.class)))
                .willAnswer(invocationOnMock -> {
                    Transaction transaction = invocationOnMock.getArgument(0);
                    return Payment.PaymentRequest.builder()
                            .narration(PAYMENT_REQUEST_REJECTED.name() + ":" + "INVALID MSISDN")
                            .paymentRequestStatus(PAYMENT_REQUEST_REJECTED)
                            .amount(transaction.getAmount())
                            .transactionRef(transaction.getTransactionRef())
                            .billingAddress(transaction.getBillingAddress())
                            .build();
                });
        given(transferFundsOutputPort.rollbackTransfer(any(String.class), any(String.class), any(BigDecimal.class), any(BigDecimal.class)))
                .willAnswer(invocationOnMock -> {
                    BigDecimal amount = invocationOnMock.getArgument(2);
                    BigDecimal transactionCost = invocationOnMock.getArgument(3);
                    Account sourceAccount = givenTransaction.getSourceAccount().toBuilder().availableBalance(srcAccount.getAvailableBalance().add(amount.add(transactionCost))).build();
                    Account destinationAccount = givenTransaction.getDestinationAccount().toBuilder().actualBalance(destAccount.getActualBalance().subtract(amount)).build();
                    return Pair.of(sourceAccount, destinationAccount);
                });

        //WHEN
        boolean depositRequestSuccessful = depositMoneyService.requestDepositMoneyPayment(givenTransaction);

        //THEN
        then(saveTransactionOutputPort).should(times(2)).updateTransaction((Transaction) argumentCaptor.capture());
        then(logTransactionOutputPort).should(times(2)).logTransaction(any(Transaction.class), (String) argumentCaptor.capture());

        Transaction updatedTransactionOnPaymentRequest = (Transaction) argumentCaptor.getAllValues().get(0);
        Transaction updatedTransactionOnRollback = (Transaction) argumentCaptor.getAllValues().get(1);

        String messageOnPaymentRequest = (String) argumentCaptor.getAllValues().get(2);
        String messageOnRollback = (String) argumentCaptor.getAllValues().get(3);

        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(depositRequestSuccessful).isTrue();

        softly.assertThat(updatedTransactionOnPaymentRequest)
                .isNotNull()
                .extracting(Transaction::getTransactionStatus)
                .isEqualTo(PAYMENT_REQUESTED);

        softly.assertThat(updatedTransactionOnRollback)
                .isNotNull()
                .extracting(Transaction::getTransactionStatus)
                .isEqualTo(FAILED);

        softly.assertThat(updatedTransactionOnRollback)
                .extracting(Transaction::getSourceAccount, Transaction::getDestinationAccount)
                .flatExtracting("availableBalance", "actualBalance")
                .usingElementComparator(comparing(o -> ((BigDecimal) o)))
                .containsExactly(BigDecimal.valueOf(1500), BigDecimal.valueOf(2000), BigDecimal.valueOf(1000), BigDecimal.valueOf(1500));

        softly.assertThat(messageOnPaymentRequest)
                .isNotNull()
                .isEqualTo("PAYMENT_REQUEST_REJECTED:INVALID MSISDN");

        softly.assertThat(messageOnRollback)
                .isNotNull()
                .isEqualTo("ACCOUNTS: +SRC_AVAIL(500), -DST_ACTUAL(500), ROLLBACK_NARRATION: PAYMENT_REQUEST_REJECTED:INVALID MSISDN");

        softly.assertAll();

        verifyNoMoreInteractions(saveTransactionOutputPort, logTransactionOutputPort, paymentsOutputPort, transferFundsOutputPort);
    }

    @Test
    @DisplayName("requestDepositMoneyPayment --> PAYMENT_REQUEST_FAILED")
    void requestDepositMoneyPayment_PAYMENT_REQUEST_FAILED() {
        //GIVEN
        Transaction givenTransaction = transaction.toBuilder().transactionStatus(PREPARED).build();
        given(saveTransactionOutputPort.updateTransaction(any(Transaction.class)))
                .willAnswer(invocationOnMock -> {
                    Transaction transaction = invocationOnMock.getArgument(0);
                    return readJson(writeJson(transaction), Transaction.class);
                });
        given(loadTransactionOutputPort.loadTransactionByTransactionRef(givenTransaction.getTransactionRef())).willReturn(givenTransaction);
        given(paymentsOutputPort.requestPayment(any(Transaction.class)))
                .willAnswer(invocationOnMock -> {
                    Transaction transaction = invocationOnMock.getArgument(0);
                    return Payment.PaymentRequest.builder()
                            .narration(PAYMENT_REQUEST_FAILED.name() + ":" + "SERVICE_UNAVAILABLE")
                            .paymentRequestStatus(PAYMENT_REQUEST_FAILED)
                            .amount(transaction.getAmount())
                            .transactionRef(transaction.getTransactionRef())
                            .billingAddress(transaction.getBillingAddress())
                            .build();
                });

        //WHEN
        boolean depositRequestSuccessful = depositMoneyService.requestDepositMoneyPayment(givenTransaction);

        //THEN
        then(saveTransactionOutputPort).should(times(2)).updateTransaction((Transaction) argumentCaptor.capture());
        then(logTransactionOutputPort).should(times(2)).logTransaction(any(Transaction.class), (String) argumentCaptor.capture());

        Transaction updatedTransactionOnPaymentRequest = (Transaction) argumentCaptor.getAllValues().get(0);
        Transaction updatedTransactionOnReset = (Transaction) argumentCaptor.getAllValues().get(1);

        String messageOnPaymentRequest = (String) argumentCaptor.getAllValues().get(2);
        String messageOnReset = (String) argumentCaptor.getAllValues().get(3);

        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(depositRequestSuccessful).isFalse();

        softly.assertThat(updatedTransactionOnPaymentRequest)
                .isNotNull()
                .extracting(Transaction::getTransactionStatus)
                .isEqualTo(PAYMENT_REQUESTED);

        softly.assertThat(updatedTransactionOnReset)
                .isNotNull()
                .extracting(Transaction::getTransactionStatus)
                .isEqualTo(PREPARED);

        softly.assertThat(messageOnPaymentRequest)
                .isNotNull()
                .isEqualTo("PAYMENT_REQUEST_FAILED:SERVICE_UNAVAILABLE");

        softly.assertThat(messageOnReset)
                .isNotNull()
                .isEqualTo("RESET_TRANSACTION TO PREPARED STATE & RETRY PAYMENT_REQUEST");

        softly.assertAll();

        verifyNoMoreInteractions(saveTransactionOutputPort, logTransactionOutputPort, paymentsOutputPort);
    }

    @ParameterizedTest
    @DisplayName("verifyDepositMoneyPayment)")
    @CsvSource({
            "PAYMENT_RESPONSE_VERIFIED,PROCEED",
            "PAYMENT_RESPONSE_FAILED,ROLLBACK",
    })
    void verifyDepositMoneyPayment(Payment.PaymentResponse.PaymentResponseStatus givenPaymentResponseStatus, Transaction.ConfirmationStatus expectedConfirmationStatus) {
        //GIVEN
        Transaction givenTransaction = transaction.toBuilder()
                .transactionRef("TEST_TRANSACTION_REF")
                .billingAddress("TEST_BILLING_ADDRESS")
                .paymentMode(MPESA)
                .transactionStatus(PAYMENT_REQUESTED)
                .build();
        Payment.PaymentResponse paymentResponse = Payment.PaymentResponse.builder()
                .narration(givenPaymentResponseStatus.name() + ":" + "TEST_NARRATION")
                .paymentResponseStatus(givenPaymentResponseStatus)
                .amount(givenTransaction.getAmount())
                .transactionRef(givenTransaction.getTransactionRef())
                .billingAddress(givenTransaction.getBillingAddress())
                .paymentMode(givenTransaction.getPaymentMode())
                .build();

        given(saveTransactionOutputPort.updateTransaction(any(Transaction.class))).willAnswer(returnsFirstArg());
        given(loadTransactionOutputPort.loadTransactionByTransactionRef(givenTransaction.getTransactionRef())).willReturn(givenTransaction);
        doNothing().when(publishTransactionOutputPort).publishTransaction(any(Transaction.class));

        //WHEN
        depositMoneyService.verifyDepositMoneyPayment(paymentResponse);

        //THEN
        then(saveTransactionOutputPort).should().updateTransaction((Transaction) argumentCaptor.capture());
        then(logTransactionOutputPort).should().logTransaction(any(Transaction.class), (String) argumentCaptor.capture());
        then(publishTransactionOutputPort).should().publishTransaction(any(Transaction.class));

        Transaction updatedTransaction = (Transaction) argumentCaptor.getAllValues().get(0);
        String message = (String) argumentCaptor.getAllValues().get(1);

        SoftAssertions softly = new SoftAssertions();

        softly.assertThat(updatedTransaction)
                .isNotNull()
                .extracting(Transaction::getTransactionStatus, Transaction::getConfirmationStatus)
                .containsExactly(PAYMENT_VERIFIED, expectedConfirmationStatus);

        softly.assertThat(message)
                .isNotNull()
                .isEqualTo(givenPaymentResponseStatus.name() + ":TEST_NARRATION");

        softly.assertAll();

        verifyNoMoreInteractions(saveTransactionOutputPort, logTransactionOutputPort, publishTransactionOutputPort);
    }

    @Test
    @DisplayName("confirmDepositMoney --> PROCEED")
    void confirmDepositMoney_PROCEED() {
        //GIVEN
        Transaction givenTransaction = transaction.toBuilder().transactionStatus(PAYMENT_VERIFIED).confirmationStatus(PROCEED).build();

        given(loadTransactionOutputPort.loadTransactionByTransactionRef(givenTransaction.getTransactionRef())).willReturn(givenTransaction);
        given(saveTransactionOutputPort.updateTransaction(givenTransaction)).willAnswer(returnsFirstArg());
        given(transferFundsOutputPort.confirmTransfer(anyString(), anyString(), any(BigDecimal.class), any(BigDecimal.class)))
                .willAnswer(invocationOnMock -> {
                    BigDecimal amount = invocationOnMock.getArgument(2);
                    BigDecimal transactionCost = invocationOnMock.getArgument(3);
                    Account sourceAccount = givenTransaction.getSourceAccount().toBuilder().actualBalance(srcAccount.getActualBalance().subtract(amount.add(transactionCost))).build();
                    Account destinationAccount = givenTransaction.getDestinationAccount().toBuilder().availableBalance(destAccount.getAvailableBalance().add(amount)).build();
                    return Pair.of(sourceAccount, destinationAccount);
                });


        //WHEN
        depositMoneyService.confirmDepositMoney(givenTransaction);

        //THEN
        then(saveTransactionOutputPort).should().updateTransaction((Transaction) argumentCaptor.capture());
        then(logTransactionOutputPort).should().logTransaction(any(Transaction.class), (String) argumentCaptor.capture());

        Transaction updatedTransaction = (Transaction) argumentCaptor.getAllValues().get(0);
        String message = (String) argumentCaptor.getAllValues().get(1);

        SoftAssertions softly = new SoftAssertions();

        softly.assertThat(updatedTransaction)
                .isNotNull()
                .extracting(Transaction::getTransactionStatus, Transaction::getConfirmationStatus)
                .containsExactly(CONFIRMED, PROCEED);

        softly.assertThat(updatedTransaction)
                .extracting(Transaction::getSourceAccount, Transaction::getDestinationAccount)
                .flatExtracting("availableBalance", "actualBalance")
                .usingElementComparator(comparing(o -> ((BigDecimal) o)))
                .containsExactly(BigDecimal.valueOf(1000), BigDecimal.valueOf(1500), BigDecimal.valueOf(1500), BigDecimal.valueOf(2000));

        softly.assertThat(message)
                .isNotNull()
                .isEqualTo("ACCOUNTS: -SRC_ACTUAL(500), +DST_AVAIL(500), CONFIRMATION_NARRATION: IMPLICIT_PROCEED");

        softly.assertAll();
    }

    @Test
    @DisplayName("confirmDepositMoney --> ROLLBACK")
    void confirmDepositMoney_ROLLBACK() {
        //GIVEN
        Transaction givenTransaction = transaction.toBuilder().transactionStatus(PAYMENT_VERIFIED).confirmationStatus(ROLLBACK).build();

        given(loadTransactionOutputPort.loadTransactionByTransactionRef(givenTransaction.getTransactionRef())).willReturn(givenTransaction);
        given(saveTransactionOutputPort.updateTransaction(givenTransaction)).willAnswer(returnsFirstArg());
        given(transferFundsOutputPort.rollbackTransfer(any(String.class), any(String.class), any(BigDecimal.class), any(BigDecimal.class)))
                .willAnswer(invocationOnMock -> {
                    BigDecimal amount = invocationOnMock.getArgument(2);
                    BigDecimal transactionCost = invocationOnMock.getArgument(3);
                    Account sourceAccount = givenTransaction.getSourceAccount().toBuilder().availableBalance(srcAccount.getAvailableBalance().add(amount.add(transactionCost))).build();
                    Account destinationAccount = givenTransaction.getDestinationAccount().toBuilder().actualBalance(destAccount.getActualBalance().subtract(amount)).build();
                    return Pair.of(sourceAccount, destinationAccount);
                });
        //WHEN
        depositMoneyService.confirmDepositMoney(givenTransaction);

        //THEN
        then(saveTransactionOutputPort).should().updateTransaction((Transaction) argumentCaptor.capture());
        then(logTransactionOutputPort).should().logTransaction(any(Transaction.class), (String) argumentCaptor.capture());
        then(publishTransactionOutputPort).should().publishTransactionCallback(any(Transaction.class));
        then(sendTransactionNotificationOutputPort).should().send(any(Transaction.class), (NotificationType) argumentCaptor.capture());

        Transaction updatedTransaction = (Transaction) argumentCaptor.getAllValues().get(0);
        String message = (String) argumentCaptor.getAllValues().get(1);
        NotificationType notificationType = (NotificationType) argumentCaptor.getAllValues().get(2);

        SoftAssertions softly = new SoftAssertions();

        softly.assertThat(updatedTransaction)
                .isNotNull()
                .extracting(Transaction::getTransactionStatus,Transaction::getConfirmationStatus)
                .containsExactly(FAILED,ROLLBACK);

        softly.assertThat(updatedTransaction)
                .extracting(Transaction::getSourceAccount, Transaction::getDestinationAccount)
                .flatExtracting("availableBalance", "actualBalance")
                .usingElementComparator(comparing(o -> ((BigDecimal) o)))
                .containsExactly(BigDecimal.valueOf(1500), BigDecimal.valueOf(2000), BigDecimal.valueOf(1000), BigDecimal.valueOf(1500));

        softly.assertThat(message)
                .isNotNull()
                .isEqualTo("ACCOUNTS: +SRC_AVAIL(500), -DST_ACTUAL(500), ROLLBACK_NARRATION: IMPLICIT_ROLLBACK");

        softly.assertThat(notificationType)
                .isNotNull()
                .isEqualTo(NotificationType.TRANSACTION_FAIL);

        softly.assertAll();
    }

    @Test
    void completeDepositMoney(){
        //GIVEN
        Transaction givenTransaction = transaction.toBuilder().transactionRef("TEST_TRANSACTION_REF").transactionStatus(CONFIRMED).build();

        given(loadTransactionOutputPort.loadTransactionByTransactionRef(givenTransaction.getTransactionRef())).willReturn(givenTransaction);
        given(saveTransactionOutputPort.updateTransaction(givenTransaction)).willAnswer(returnsFirstArg());

        //WHEN
        depositMoneyService.completeDepositMoney(givenTransaction);

        //THEN
        then(saveTransactionOutputPort).should().updateTransaction((Transaction) argumentCaptor.capture());
        then(logTransactionOutputPort).should().logTransaction(any(Transaction.class), (String) argumentCaptor.capture());
        then(publishTransactionOutputPort).should().publishTransactionCallback(any(Transaction.class));
        then(sendTransactionNotificationOutputPort).should().send(any(Transaction.class), (NotificationType) argumentCaptor.capture());

        Transaction updatedTransaction = (Transaction) argumentCaptor.getAllValues().get(0);
        String message = (String) argumentCaptor.getAllValues().get(1);
        NotificationType notificationType = (NotificationType) argumentCaptor.getAllValues().get(2);

        SoftAssertions softly = new SoftAssertions();

        softly.assertThat(updatedTransaction)
                .isNotNull()
                .extracting(Transaction::getTransactionStatus)
                .isEqualTo(COMPLETED);
        softly.assertThat(message)
                .isNotNull()
                .isEqualTo("COMPLETED TXN: TEST_TRANSACTION_REF");
        softly.assertThat(notificationType)
                .isNotNull()
                .isEqualTo(NotificationType.TRANSACTION_SUCCESS);
        softly.assertAll();
    }
}
