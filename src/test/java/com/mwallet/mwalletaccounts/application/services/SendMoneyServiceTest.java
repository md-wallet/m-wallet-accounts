package com.mwallet.mwalletaccounts.application.services;

import com.mwallet.mwalletcommons.notifications.NotificationType;
import com.mwallet.mwalletaccounts.application.ports.out.accounts.LoadAccountOutputPort;
import com.mwallet.mwalletaccounts.application.ports.out.accounts.TransferFundsOutputPort;
import com.mwallet.mwalletaccounts.application.ports.out.transactionlogs.LogTransactionOutputPort;
import com.mwallet.mwalletaccounts.application.ports.out.transactions.*;
import com.mwallet.mwalletaccounts.domain.Account;
import com.mwallet.mwalletaccounts.domain.Transaction;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.SoftAssertions;
import org.assertj.core.util.BigDecimalComparator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.util.Pair;
import org.springframework.test.context.ActiveProfiles;

import java.math.BigDecimal;
import java.util.Comparator;

import static com.mwallet.mwalletaccounts.domain.Account.AccountStatus.ACTIVE;
import static com.mwallet.mwalletaccounts.domain.Account.AccountType.JOINT_ACCOUNT;
import static com.mwallet.mwalletaccounts.domain.Account.AccountType.USER_ACCOUNT;
import static com.mwallet.mwalletaccounts.domain.Account.builder;
import static com.mwallet.mwalletaccounts.domain.Transaction.ConfirmationStatus.PROCEED;
import static com.mwallet.mwalletaccounts.domain.Transaction.ConfirmationStatus.ROLLBACK;
import static com.mwallet.mwalletaccounts.domain.Transaction.PaymentMode.WALLET;
import static com.mwallet.mwalletaccounts.domain.Transaction.TransactionStatus.*;
import static com.mwallet.mwalletaccounts.domain.Transaction.TransactionType.SEND_MONEY;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@Slf4j
@ActiveProfiles("test")
@ExtendWith(MockitoExtension.class)
class SendMoneyServiceTest {

    private Account srcAccount;
    private Account destAccount;
    private Transaction transaction;

    @Captor
    private ArgumentCaptor<?> argumentCaptor;

    @Mock
    private SaveTransactionOutputPort saveTransactionOutputPort;
    @Mock
    private LogTransactionOutputPort logTransactionOutputPort;
    @Mock
    private TransferFundsOutputPort transferFundsOutputPort;
    @Mock
    private GenerateTransactionRefOutputPort transactionRefOutputPort;
    @Mock
    private TransactionCacheManagementOutputPort transactionCacheManagementOutputPort;
    @Mock
    private LoadTransactionOutputPort loadTransactionOutputPort;
    @Mock
    private LoadAccountOutputPort loadAccountOutputPort;
    @Mock
    private PublishTransactionOutputPort publishTransactionOutputPort;
    @Mock
    private SendTransactionNotificationOutputPort sendTransactionNotificationOutputPort;

    @InjectMocks
    private SendMoneyService sendMoneyService;

    @BeforeEach
    void setUp() {
        srcAccount = builder()
                .accountId("TEST_ACCOUNT_ID_1")
                .linkId("1")
                .accountStatus(ACTIVE)
                .accountType(USER_ACCOUNT)
                .availableBalance(new BigDecimal("1000"))
                .actualBalance(new BigDecimal("2000"))
                .build();
        destAccount = srcAccount.toBuilder().accountId("TEST_ACCOUNT_ID_2").linkId("20").accountType(JOINT_ACCOUNT).build();

        transaction = Transaction.builder()
                .sourceAccount(srcAccount)
                .destinationAccount(destAccount)
                .amount(new BigDecimal("500"))
                .transactionCost(new BigDecimal("20"))
                .transactionType(SEND_MONEY)
                .confirmationStatus(Transaction.ConfirmationStatus.NONE)
                .confirmationType(Transaction.ConfirmationType.INTERNAL)
                .transactionStatus(NONE)
                .paymentMode(WALLET)
                .build();
    }

    @Test
    void validateSendMoney() {
        //GIVEN
        Transaction givenTransaction = transaction;
        String transactionHash = "TEST_TRANSACTION_HASH";

        given(transactionCacheManagementOutputPort.saveTransaction(any(Transaction.class))).willReturn(transactionHash);
        //WHEN
        Pair<String, Transaction> validationResult = sendMoneyService.validateSendMoney(givenTransaction);
        //THEN
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(validationResult)
                .isNotNull()
                .isEqualToIgnoringNullFields(Pair.of(transactionHash, givenTransaction));
        softly.assertThat(validationResult.getSecond())
                .extracting(Transaction::getTransactionStatus)
                .isEqualTo(VALIDATED);

        softly.assertAll();
    }

    @Test
    void requestSendMoney() {
        //GIVEN
        Transaction givenTransaction = transaction.toBuilder().transactionStatus(VALIDATED).build();
        String transactionHash = "TEST_TRANSACTION_HASH";
        String transactionRef = "TEST_TRANSACTION_REF";

        given(transactionCacheManagementOutputPort.getTransactionByHash(anyString())).willReturn(givenTransaction);
        given(loadAccountOutputPort.loadAccountById(srcAccount.getAccountId())).willReturn(srcAccount);
        given(loadAccountOutputPort.loadAccountById(destAccount.getAccountId())).willReturn(destAccount);
        given(transactionRefOutputPort.generateTransactionRef(any(Transaction.class))).willReturn(transactionRef);
        given(saveTransactionOutputPort.createTransaction(any(Transaction.class))).willAnswer(returnsFirstArg());

        //WHEN
        sendMoneyService.requestSendMoney(transactionHash);

        //THEN
        then(saveTransactionOutputPort).should().createTransaction((Transaction) argumentCaptor.capture());
        then(logTransactionOutputPort).should().logTransaction(any(Transaction.class), (String) argumentCaptor.capture());
        then(transactionCacheManagementOutputPort).should().deleteByHash(anyString());

        Transaction generatedTransaction = (Transaction) argumentCaptor.getAllValues().get(0);
        String message = (String) argumentCaptor.getAllValues().get(1);

        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(generatedTransaction)
                .isNotNull()
                .extracting(Transaction::getTransactionRef, Transaction::getTransactionStatus)
                .containsExactly(transactionRef, ACCEPTED);

        softly.assertThat(message)
                .isNotNull()
                .isEqualTo("STARTED TXN: TEST_TRANSACTION_REF");

        softly.assertAll();

        verifyNoMoreInteractions(transactionCacheManagementOutputPort, loadAccountOutputPort, transactionRefOutputPort, saveTransactionOutputPort, logTransactionOutputPort);
    }

    @Test
    void prepareSendMoney() {
        //GIVEN
        Transaction givenTransaction = transaction.toBuilder().transactionStatus(ACCEPTED).build();

        given(saveTransactionOutputPort.updateTransaction(givenTransaction)).willAnswer(returnsFirstArg());
        given(loadTransactionOutputPort.loadTransactionByTransactionRef(givenTransaction.getTransactionRef())).willReturn(givenTransaction);
        given(transferFundsOutputPort.prepareTransfer(any(String.class), any(String.class), any(BigDecimal.class), any(BigDecimal.class)))
                .willAnswer(invocationOnMock -> {
                    BigDecimal amount = invocationOnMock.getArgument(2);
                    BigDecimal transactionCost = invocationOnMock.getArgument(3);
                    Account sourceAccount = givenTransaction.getSourceAccount().toBuilder().availableBalance(srcAccount.getAvailableBalance().subtract(amount.add(transactionCost))).build();
                    Account destinationAccount = givenTransaction.getDestinationAccount().toBuilder().actualBalance(destAccount.getActualBalance().add(amount)).build();
                    return Pair.of(sourceAccount, destinationAccount);
                });

        //WHEN
        sendMoneyService.prepareSendMoney(givenTransaction);
        //THEN
        then(saveTransactionOutputPort).should().updateTransaction((Transaction) argumentCaptor.capture());
        then(logTransactionOutputPort).should().logTransaction(any(Transaction.class), (String) argumentCaptor.capture());

        Transaction updatedTransaction = (Transaction) argumentCaptor.getAllValues().get(0);
        String message = (String) argumentCaptor.getAllValues().get(1);

        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(updatedTransaction)
                .isNotNull()
                .extracting(Transaction::getTransactionStatus)
                .isEqualTo(PREPARED);
        softly.assertThat(updatedTransaction)
                .extracting(Transaction::getSourceAccount, Transaction::getDestinationAccount)
                .flatExtracting("availableBalance", "actualBalance")
                .usingElementComparator((Comparator) new BigDecimalComparator())
                .containsExactly(BigDecimal.valueOf(480.00000), BigDecimal.valueOf(2000), BigDecimal.valueOf(1000), BigDecimal.valueOf(2500));

        softly.assertThat(message)
                .isNotNull()
                .isEqualTo("ACCOUNTS: -SRC_AVAIL(520), +DST_ACTUAL(500)");

        softly.assertAll();

        verifyNoMoreInteractions(transferFundsOutputPort, saveTransactionOutputPort, logTransactionOutputPort);
    }

    @Test
    @DisplayName("confirmSendMoney --> PROCEED")
    void confirmSendMoney_proceed() {
        //GIVEN
        Transaction givenTransaction = transaction.toBuilder().transactionStatus(PREPARED).build();

        given(loadTransactionOutputPort.loadTransactionByTransactionRef(givenTransaction.getTransactionRef())).willReturn(givenTransaction);
        given(saveTransactionOutputPort.updateTransaction(givenTransaction)).willAnswer(returnsFirstArg());
        given(transferFundsOutputPort.confirmTransfer(anyString(), anyString(), any(BigDecimal.class), any(BigDecimal.class)))
                .willAnswer(invocationOnMock -> {
                    BigDecimal amount = invocationOnMock.getArgument(2);
                    BigDecimal transactionCost = invocationOnMock.getArgument(3);
                    Account sourceAccount = givenTransaction.getSourceAccount().toBuilder().actualBalance(srcAccount.getActualBalance().subtract(amount.add(transactionCost))).build();
                    Account destinationAccount = givenTransaction.getDestinationAccount().toBuilder().availableBalance(destAccount.getAvailableBalance().add(amount)).build();
                    return Pair.of(sourceAccount, destinationAccount);
                });


        //WHEN
        sendMoneyService.confirmSendMoney(givenTransaction.getTransactionRef(), PROCEED, "TEST_NARRATION");
        //THEN
        then(saveTransactionOutputPort).should().updateTransaction((Transaction) argumentCaptor.capture());
        then(logTransactionOutputPort).should().logTransaction(any(Transaction.class), (String) argumentCaptor.capture());

        Transaction updatedTransaction = (Transaction) argumentCaptor.getAllValues().get(0);
        String message = (String) argumentCaptor.getAllValues().get(1);

        SoftAssertions softly = new SoftAssertions();

        softly.assertThat(updatedTransaction)
                .isNotNull()
                .extracting(Transaction::getTransactionStatus, Transaction::getConfirmationStatus)
                .containsExactly(CONFIRMED, PROCEED);

        softly.assertThat(updatedTransaction)
                .extracting(Transaction::getSourceAccount, Transaction::getDestinationAccount)
                .flatExtracting("availableBalance", "actualBalance")
                .usingElementComparator((Comparator) new BigDecimalComparator())
                .containsExactly(BigDecimal.valueOf(1000), BigDecimal.valueOf(1480), BigDecimal.valueOf(1500), BigDecimal.valueOf(2000));

        softly.assertThat(message)
                .isNotNull()
                .isEqualTo("ACCOUNTS: -SRC_ACTUAL(520), +DST_AVAIL(500), CONFIRMATION_NARRATION: TEST_NARRATION");

        softly.assertAll();
    }

    @Test
    @DisplayName("confirmSendMoney --> ROLLBACK")
    void confirmSendMoney_Rollback() {
        //GIVEN
        Transaction givenTransaction = transaction.toBuilder().transactionStatus(PREPARED).build();

        given(loadTransactionOutputPort.loadTransactionByTransactionRef(givenTransaction.getTransactionRef())).willReturn(givenTransaction);
        given(saveTransactionOutputPort.updateTransaction(givenTransaction)).willAnswer(returnsFirstArg());
        given(transferFundsOutputPort.rollbackTransfer(any(String.class), any(String.class), any(BigDecimal.class), any(BigDecimal.class)))
                .willAnswer(invocationOnMock -> {
                    BigDecimal amount = invocationOnMock.getArgument(2);
                    BigDecimal transactionCost = invocationOnMock.getArgument(3);
                    Account sourceAccount = givenTransaction.getSourceAccount().toBuilder().availableBalance(srcAccount.getAvailableBalance().add(amount.add(transactionCost))).build();
                    Account destinationAccount = givenTransaction.getDestinationAccount().toBuilder().actualBalance(destAccount.getActualBalance().subtract(amount)).build();
                    return Pair.of(sourceAccount, destinationAccount);
                });
        //WHEN
        sendMoneyService.confirmSendMoney(givenTransaction.getTransactionRef(), ROLLBACK, "TEST_NARRATION");

        //THEN
        then(saveTransactionOutputPort).should().updateTransaction((Transaction) argumentCaptor.capture());
        then(logTransactionOutputPort).should().logTransaction(any(Transaction.class), (String) argumentCaptor.capture());
        then(publishTransactionOutputPort).should().publishTransactionCallback(any(Transaction.class));
        then(sendTransactionNotificationOutputPort).should().send(any(Transaction.class), (NotificationType) argumentCaptor.capture());

        Transaction updatedTransaction = (Transaction) argumentCaptor.getAllValues().get(0);
        String message = (String) argumentCaptor.getAllValues().get(1);
        NotificationType notificationType = (NotificationType) argumentCaptor.getAllValues().get(2);

        SoftAssertions softly = new SoftAssertions();

        softly.assertThat(updatedTransaction)
                .isNotNull()
                .extracting(Transaction::getTransactionStatus, Transaction::getConfirmationStatus)
                .containsExactly(FAILED, ROLLBACK);

        softly.assertThat(updatedTransaction)
                .extracting(Transaction::getSourceAccount, Transaction::getDestinationAccount)
                .flatExtracting("availableBalance", "actualBalance")
                .usingElementComparator((Comparator) new BigDecimalComparator())
                .containsExactly(BigDecimal.valueOf(1520), BigDecimal.valueOf(2000), BigDecimal.valueOf(1000), BigDecimal.valueOf(1500));

        softly.assertThat(message)
                .isNotNull()
                .isEqualTo("ACCOUNTS: +SRC_AVAIL(520), -DST_ACTUAL(500), ROLLBACK_NARRATION: TEST_NARRATION");

        softly.assertThat(notificationType)
                .isNotNull()
                .isEqualTo(NotificationType.TRANSACTION_FAIL);

        softly.assertAll();
    }

    @Test
    void completeSendMoney() {
        //GIVEN
        Transaction givenTransaction = transaction.toBuilder().transactionRef("TEST_TRANSACTION_REF").transactionStatus(CONFIRMED).build();

        given(loadTransactionOutputPort.loadTransactionByTransactionRef(givenTransaction.getTransactionRef())).willReturn(givenTransaction);
        given(saveTransactionOutputPort.updateTransaction(givenTransaction)).willAnswer(returnsFirstArg());

        //WHEN
        sendMoneyService.completeSendMoney(givenTransaction);

        //THEN
        then(saveTransactionOutputPort).should().updateTransaction((Transaction) argumentCaptor.capture());
        then(logTransactionOutputPort).should().logTransaction(any(Transaction.class), (String) argumentCaptor.capture());
        then(publishTransactionOutputPort).should().publishTransactionCallback(any(Transaction.class));
        then(sendTransactionNotificationOutputPort).should().send(any(Transaction.class), (NotificationType) argumentCaptor.capture());

        Transaction updatedTransaction = (Transaction) argumentCaptor.getAllValues().get(0);
        String message = (String) argumentCaptor.getAllValues().get(1);
        NotificationType notificationType = (NotificationType) argumentCaptor.getAllValues().get(2);

        SoftAssertions softly = new SoftAssertions();

        softly.assertThat(updatedTransaction)
                .isNotNull()
                .extracting(Transaction::getTransactionStatus)
                .isEqualTo(COMPLETED);

        softly.assertThat(message)
                .isNotNull()
                .isEqualTo("COMPLETED TXN: TEST_TRANSACTION_REF");

        softly.assertThat(notificationType)
                .isNotNull()
                .isEqualTo(NotificationType.TRANSACTION_SUCCESS);

        softly.assertAll();

    }
}
