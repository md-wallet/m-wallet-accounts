package com.mwallet.mwalletaccounts.application.services;

import com.mwallet.mwalletaccounts.application.ports.in.accounts.CreateAccountUseCase;
import com.mwallet.mwalletaccounts.application.ports.out.accounts.LoadAccountOutputPort;
import com.mwallet.mwalletaccounts.application.ports.out.accounts.SaveAccountOutputPort;
import com.mwallet.mwalletaccounts.domain.Account;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.ActiveProfiles;

import static com.mwallet.mwalletaccounts.domain.Account.*;
import static com.mwallet.mwalletaccounts.domain.Account.AccountStatus.ACTIVE;
import static com.mwallet.mwalletaccounts.domain.Account.AccountStatus.INACTIVE;
import static com.mwallet.mwalletaccounts.domain.Account.AccountType.USER_ACCOUNT;
import static com.mwallet.mwalletaccounts.domain.Account.builder;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@Slf4j
@ActiveProfiles("test")
@ExtendWith(MockitoExtension.class)
class AccountManagementServiceTest {

    @Mock
    private LoadAccountOutputPort loadAccountOutputPort;
    @Mock
    private SaveAccountOutputPort saveAccountOutputPort;

    @InjectMocks
    private AccountManagementService accountManagementService;

    private Account account;

    @BeforeEach
    void setUp() {
        account = builder().id(1L).accountId("TEST_ACCOUNT_ID_1").linkId("1").accountStatus(ACTIVE).accountType(USER_ACCOUNT).build();
    }

    @Test
    @DisplayName("createAccount -> OK")
    void createAccount() {
        //GIVEN
        Account givenAccount = account;
        given(loadAccountOutputPort.existsByLinkIdAndAccountType(givenAccount.getLinkId(), givenAccount.getAccountType())).willReturn(false);
        given(saveAccountOutputPort.createAccount(givenAccount)).willReturn(givenAccount);
        given(saveAccountOutputPort.updateAccount(any(Account.class))).willAnswer(returnsFirstArg());

        //WHEN
        Account createdAccount = accountManagementService.createAccount(givenAccount);

        //THEN
        assertThat(createdAccount)
                .isNotNull()
                .extracting(Account::getLinkId, Account::getAccountType)
                .containsExactly(givenAccount.getLinkId(), givenAccount.getAccountType());

    }

    @Test
    @DisplayName("createAccount -> AccountAlreadyExistsException")
    void createAccount_WillThrowAccountAlreadyExistsException() {
        //GIVEN
        Account givenAccount = account;
        given(loadAccountOutputPort.existsByLinkIdAndAccountType(givenAccount.getLinkId(), givenAccount.getAccountType())).willReturn(true);

        //WHEN + THEN
        assertThatThrownBy(() -> accountManagementService.createAccount(givenAccount))
                .isInstanceOf(CreateAccountUseCase.AccountAlreadyExistsException.class)
                .hasMessageContaining("Account already registered");
    }

    @Test
    @DisplayName("updateAccountStatus -> OK")
    void updateAccountStatus() {
        //GIVEN
        Account givenAccount = account;
        AccountStatus givenAccountStatus = INACTIVE;
        given(loadAccountOutputPort.loadAccountById(givenAccount.getAccountId())).willReturn(givenAccount);
        given(saveAccountOutputPort.updateAccount(any(Account.class))).willAnswer(returnsFirstArg());

        //WHEN
        Account updatedAccount = accountManagementService.updateAccountStatus(givenAccount.getAccountId(), givenAccountStatus);

        //THEN
        assertThat(updatedAccount)
                .isNotNull()
                .isEqualToIgnoringGivenFields(givenAccount, "accountStatus")
                .extracting(Account::getAccountStatus)
                .isEqualTo(givenAccountStatus);
    }
}