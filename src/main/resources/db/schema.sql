CREATE DATABASE IF NOT EXISTS m_wallet_accounts;
USE m_wallet_accounts;

########################### ACCOUNTS_TABLE ##############################
CREATE TABLE IF NOT EXISTS accounts
(
    id                BIGINT         NOT NULL PRIMARY KEY AUTO_INCREMENT,
    account_id        VARCHAR(32) UNIQUE,
    link_id           VARCHAR(32)    NOT NULL,
    version           BIGINT    DEFAULT 0,
    created_on        TIMESTAMP DEFAULT NOW(),
    updated_on        TIMESTAMP DEFAULT NOW(),
    account_type      VARCHAR(64)    NOT NULL,
    account_status    VARCHAR(64)    NOT NULL,
    actual_balance    DECIMAL(13, 2) NOT NULL,
    available_balance DECIMAL(13, 2) NOT NULL
);

########################### TRANSACTIONS_TABLE ##############################
CREATE TABLE IF NOT EXISTS transactions
(
    id                      BIGINT         NOT NULL PRIMARY KEY AUTO_INCREMENT,
    src_account_id          VARCHAR(32)    NOT NULL,
    dest_account_id         VARCHAR(32)    NOT NULL,
    version                 BIGINT    DEFAULT 0,
    created_on              TIMESTAMP DEFAULT NOW(),
    updated_on              TIMESTAMP DEFAULT NOW(),
    amount                  DECIMAL(13, 2),
    transaction_type        VARCHAR(64)    NOT NULL,
    transaction_cost        DECIMAL(13, 2) NOT NULL,
    transaction_state       VARCHAR(64)    NOT NULL,
    transaction_ref         VARCHAR(64)    NOT NULL,
    payment_mode            VARCHAR(64)    NOT NULL,
    billing_address         VARCHAR(64)    NOT NULL,
    confirmation_type       VARCHAR(64)    NOT NULL,
    confirmation_status     VARCHAR(64)    NOT NULL,
    transaction_desc        VARCHAR(1000)  NOT NULL,
    transaction_domain_type VARCHAR(500)   NOT NULL,
    FOREIGN KEY fk_src_account_id (src_account_id) REFERENCES accounts (account_id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY fk_dest_account_id (dest_account_id) REFERENCES accounts (account_id) ON UPDATE CASCADE ON DELETE CASCADE
);

########################### TRANSACTIONS_LOGS_TABLE ##############################
CREATE TABLE IF NOT EXISTS transaction_logs
(
    id                    BIGINT         NOT NULL PRIMARY KEY AUTO_INCREMENT,
    version               BIGINT,
    created_on            TIMESTAMP DEFAULT NOW(),
    updated_on            TIMESTAMP DEFAULT NOW(),
    transaction_id        BIGINT         NOT NULL,
    message               VARCHAR(1000)  NOT NULL,
    transaction_state     VARCHAR(64)    NOT NULL,
    confirmation_status   VARCHAR(64)    NOT NULL,
    src_available_balance DECIMAL(13, 2) NOT NULL,
    src_actual_balance    DECIMAL(13, 2) NOT NULL,
    dst_available_balance DECIMAL(13, 2) NOT NULL,
    dst_actual_balance    DECIMAL(13, 2) NOT NULL,
    FOREIGN KEY fk_transaction_id (transaction_id) REFERENCES transactions (id) ON UPDATE CASCADE ON DELETE CASCADE
);
