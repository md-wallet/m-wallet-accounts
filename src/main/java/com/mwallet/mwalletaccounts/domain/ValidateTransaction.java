package com.mwallet.mwalletaccounts.domain;


import com.mwallet.mwalletcommons.exceptions.GenericHttpException;
import com.mwallet.mwalletcommons.utils.ComparableUtils;
import com.sun.istack.Nullable;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.util.Pair;
import org.springframework.http.HttpStatus;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

import static com.mwallet.mwalletaccounts.domain.Account.AccountStatus.INACTIVE;
import static com.mwallet.mwalletaccounts.domain.Transaction.*;


/**
 * @author lawrence
 * created 17/01/2020 at 09:49
 **/
@Slf4j
public abstract class ValidateTransaction {

    /**
     * Ensure a transaction traverses two unique accounts
     *
     * @param transaction transaction to validate
     */
    protected void checkCyclicTransaction(Transaction transaction) {
        log.info("checkCyclicTransaction: [src:{},dest:{}]", transaction.getSourceAccount().getAccountId(), transaction.getDestinationAccount().getAccountId());
        if (transaction.getSourceAccount().getAccountId().equals(transaction.getDestinationAccount().getAccountId()))
            throw new InvalidTransactionException("SAME Source and Destination Account", HttpStatus.UNPROCESSABLE_ENTITY);
    }

    /**
     * Ensure both source and destination accounts are ACTIVE for transactions
     *
     * @param transaction transaction to validate
     */
    protected void checkAccountsStatus(Transaction transaction) {
        log.info("checkAccountsStatus: [src:{},dest:{}]", transaction.getSourceAccount().getAccountStatus(), transaction.getDestinationAccount().getAccountStatus());
        if (transaction.getSourceAccount().getAccountStatus() == INACTIVE)
            throw new InvalidTransactionException("INACTIVE Source Account", HttpStatus.UNPROCESSABLE_ENTITY);
        if (transaction.getDestinationAccount().getAccountStatus() == INACTIVE)
            throw new InvalidTransactionException("INACTIVE Destination Account", HttpStatus.UNPROCESSABLE_ENTITY);
    }

    /**
     * Ensure the transaction amount is not less than the required amount for
     * this type of transaction
     *
     * @param transaction   transaction to validate
     * @param minimumAmount minimum allowed amount
     * @param maximumAmount maximum allowed amount
     */
    protected void checkTransactionAmountInRange(Transaction transaction, BigDecimal minimumAmount, BigDecimal maximumAmount) {
        log.info("checkTransactionAmountInRange: [amnt:{},range:({},{})]", transaction.getAmount(), minimumAmount, maximumAmount);
        if (!ComparableUtils.isBetween(transaction.getAmount(), minimumAmount, maximumAmount))
            throw new InvalidTransactionException("Transaction Amount must be within the range: " + minimumAmount + "-" + maximumAmount, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    /**
     * Ensure the source account has enough available balance to cover the transaction
     * amount and any transaction costs
     *
     * @param transaction transaction to validate
     */
    protected void checkFundsAvailability(Transaction transaction) {

        if (transaction.getSourceAccount().getAccountType() == Account.AccountType.TRUST_ACCOUNT)
            return;

        BigDecimal availableBalance = transaction.getSourceAccount().getAvailableBalance();
        BigDecimal totalAmount = transaction.getAmount().add(transaction.getTransactionCost());

        log.info("checkFundsAvailability: [total:{},avail:{}]", totalAmount, availableBalance);

        if (ComparableUtils.isLessThan(availableBalance, totalAmount))
            throw new InvalidTransactionException("Insufficient Available Balance: " + availableBalance, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    /**
     * Ensure the transaction has the expected transactionType
     *
     * @param transaction     transaction to check
     * @param transactionType transaction type to check against
     */
    protected void checkTransactionType(Transaction transaction, TransactionType transactionType) {
        log.info("checkTransactionType: [actual:{},expected:{}]", transaction.getTransactionType(), transactionType);
        if (transaction.getTransactionType() != transactionType)
            throw new InvalidTransactionException("Invalid Transaction Type: " + transaction.getTransactionType(), HttpStatus.CONFLICT);
    }

    /**
     * Ensure the transaction has the supported payment mode for this type of transaction
     *
     * @param transaction           transaction to check
     * @param supportedPaymentModes list of supported payment modes
     */
    protected void checkSupportedPaymentModes(Transaction transaction, List<PaymentMode> supportedPaymentModes) {
        log.info("checkSupportedPaymentModes: [trans:{},list:{}]", transaction.getPaymentMode(), supportedPaymentModes);
        if (!supportedPaymentModes.contains(transaction.getPaymentMode()))
            throw new InvalidTransactionException("Unsupported Payment Mode: " + transaction.getPaymentMode(), HttpStatus.UNPROCESSABLE_ENTITY);
    }

    /**
     * Ensure the transaction is in the correct state before processing
     *
     * @param transaction                  transaction to check
     * @param permissibleTransactionStatus list of permissible transaction status
     */
    protected void checkTransactionStatus(Transaction transaction, List<TransactionStatus> permissibleTransactionStatus) {
        log.info("checkTransactionStatus: [trans:{},list:{}]", transaction.getTransactionStatus(), permissibleTransactionStatus);
        if (!permissibleTransactionStatus.contains(transaction.getTransactionStatus()))
            throw new InvalidTransactionException("Unexpected Transaction Status: " + transaction.getTransactionStatus(), HttpStatus.CONFLICT);
    }

    /**
     * @param transaction       transaction to check
     * @param transactionStatus permissible transaction status
     * @see #checkTransactionStatus(Transaction, List)
     * <br>
     * Validate when the permissible status is just one
     */
    protected void checkTransactionStatus(Transaction transaction, TransactionStatus transactionStatus) {
        checkTransactionStatus(transaction, List.of(transactionStatus));
    }

    /**
     * Ensure the source and destination account types are supported for this transaction type
     *
     * @param transaction                    transaction to check
     * @param permissibleSrcDestAccountTypes list of supported (src,dest) account types
     */
    protected void checkSupportedAccountTypes(Transaction transaction, List<Pair<Account.AccountType, Account.AccountType>> permissibleSrcDestAccountTypes) {
        log.info("checkSupportedAccountTypes: [src_dest:({},{}) | list:{}]", transaction.getSourceAccount().getAccountType(), transaction.getDestinationAccount().getAccountType(), permissibleSrcDestAccountTypes);
        Account.AccountType srcAccountType = transaction.getSourceAccount().getAccountType();
        Account.AccountType destAccountType = transaction.getDestinationAccount().getAccountType();

        if (!permissibleSrcDestAccountTypes.contains(Pair.of(srcAccountType, destAccountType)))
            throw new InvalidTransactionException("Invalid Source, Destination Account Types: <" + srcAccountType + "," + destAccountType + ">", HttpStatus.UNPROCESSABLE_ENTITY);

    }

    /**
     * Ensure the paymentResponse matches the target transaction
     *
     * @param paymentResponse received payment response
     * @param transaction     target transaction
     */
    protected void checkPaymentResponse(Payment.PaymentResponse paymentResponse, Transaction transaction) {
        boolean isValidPaymentResponse =
                paymentResponse.getTransactionRef().equals(transaction.getTransactionRef()) &&
                        paymentResponse.getPaymentMode().equals(transaction.getPaymentMode()) &&
                        ComparableUtils.isEqualTo(paymentResponse.getAmount(), transaction.getAmount());

        if (!isValidPaymentResponse)
            throw new InvalidTransactionException("Invalid PaymentResponse for transaction: " + transaction.getTransactionRef(), HttpStatus.UNPROCESSABLE_ENTITY);
    }

    /**
     * Ensure the payoutResponse matches the target transaction
     *
     * @param payoutResponse received payout response
     * @param transaction    target transaction
     */
    protected void checkPayoutResponse(Payout.PayoutResponse payoutResponse, Transaction transaction) {
        boolean isValidPayoutResponse =
                payoutResponse.getTransactionRef().equals(transaction.getTransactionRef()) &&
                        payoutResponse.getPaymentMode().equals(transaction.getPaymentMode()) &&
                        ComparableUtils.isEqualTo(payoutResponse.getAmount(), transaction.getAmount());

        if (!isValidPayoutResponse)
            throw new InvalidTransactionException("Invalid PayoutResponse for transaction: " + transaction.getTransactionRef(), HttpStatus.UNPROCESSABLE_ENTITY);
    }

    /**
     * Ensure credit card token is valid
     *
     * @param paymentCard Payment Card to check
     */
    protected void checkPaymentCard(@Nullable PaymentCard paymentCard) {
        boolean isValidCard = Objects.nonNull(paymentCard) && paymentCard.getCardStatus() == PaymentCard.CardStatus.ACTIVE;
        if (!isValidCard)
            throw new InvalidTransactionException("Invalid Card", HttpStatus.UNPROCESSABLE_ENTITY);
    }

    public static class InvalidTransactionException extends GenericHttpException {
        public InvalidTransactionException(String message, HttpStatus httpStatus) {
            super(message, httpStatus);
        }
    }
}
