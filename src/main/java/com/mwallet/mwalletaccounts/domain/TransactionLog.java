package com.mwallet.mwalletaccounts.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import com.mwallet.mwalletcommons.data.models.BaseDomain;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

import static com.mwallet.mwalletaccounts.domain.Transaction.*;


/**
 * @author lawrence
 * created 22/01/2020 at 11:59
 **/
@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties({"version", "updatedOn", "createdOn"})
public class TransactionLog extends BaseDomain {

    @NotNull
    private TransactionStatus transactionStatus;

    @NotNull
    private ConfirmationStatus confirmationStatus;

    @NotNull
    @JsonIgnoreProperties({
            "sourceAccount", "destinationAccount",
            "paymentMode", "confirmationType",
            "id", "version", "updatedOn", "createdOn"
    })
    private Transaction transaction;

    @NotNull
    @DecimalMin(value = "0.0")
    private BigDecimal srcAvailableBalance;

    @NotNull
    @DecimalMin(value = "0.0")
    private BigDecimal srcActualBalance;

    @NotNull
    @DecimalMin(value = "0.0")
    private BigDecimal destAvailableBalance;

    @NotNull
    @DecimalMin(value = "0.0")
    private BigDecimal destActualBalance;

    @NotNull
    private String message;
}
