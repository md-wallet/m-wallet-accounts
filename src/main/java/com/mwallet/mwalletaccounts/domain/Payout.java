package com.mwallet.mwalletaccounts.domain;


import com.mwallet.mwalletcommons.data.models.BaseDomain;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * @author lawrence
 * created 11/02/2020 at 11:22
 **/
@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public abstract class Payout extends BaseDomain {
    @NotBlank
    private String transactionRef;

    @NotNull
    @DecimalMin(value = "0.0")
    private BigDecimal amount;

    @NotNull
    private Transaction.PaymentMode paymentMode;

    @NotBlank
    private String billingAddress;

    @NotBlank
    @Builder.Default
    private String narration = "NONE";

    @NotBlank
    @Builder.Default
    private String externalRefId = "NONE";

    @Getter
@Setter
    @SuperBuilder(toBuilder = true)
    @NoArgsConstructor
    @AllArgsConstructor
    @EqualsAndHashCode(callSuper = true)
    public static class PayoutRequest extends Payout {

        @NotBlank
        private String transactionDescription;

        @NotNull
        @Builder.Default
        private PayoutRequest.PayoutRequestStatus payoutRequestStatus = PayoutRequestStatus.NONE;

        @Getter
        @AllArgsConstructor
        public enum PayoutRequestStatus {
            NONE,
            PAYOUT_REQUEST_ACCEPTED,
            PAYOUT_REQUEST_FAILED,
            PAYOUT_REQUEST_REJECTED;
        }
    }


    @Getter
@Setter
    @SuperBuilder(toBuilder = true)
    @NoArgsConstructor
    @AllArgsConstructor
    @EqualsAndHashCode(callSuper = true)
    public static class PayoutResponse extends Payout {

        @Override
        public @NotBlank String getTransactionRef() {
            return Objects.isNull(super.getTransactionRef()) ? "NONE" : super.getTransactionRef();
        }

        @Override
        public @NotBlank String getBillingAddress() {
            return Objects.isNull(super.getBillingAddress()) ? "NONE" : super.getBillingAddress();
        }

        @Override
        public @NotNull @DecimalMin(value = "0.0") BigDecimal getAmount() {
            return Objects.isNull(super.getAmount()) ? BigDecimal.ZERO : super.getAmount();
        }

        @NotNull
        @Builder.Default
        private boolean orphan = true;

        @NotNull
        @Builder.Default
        private String receiptNumber = "NONE";

        @NotNull
        private PayoutResponseStatus payoutResponseStatus;

        @NotNull
        @Builder.Default
        private PayoutNotificationStatus payoutNotificationStatus = PayoutNotificationStatus.NONE;

        @Getter
        @AllArgsConstructor
        public enum PayoutResponseStatus {
            PAYOUT_RESPONSE_VERIFIED,
            PAYOUT_RESPONSE_FAILED;
        }

        public enum PayoutNotificationStatus {
            NONE,
            SENT,
            FAILED
        }
    }
}
