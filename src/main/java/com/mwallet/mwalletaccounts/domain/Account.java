package com.mwallet.mwalletaccounts.domain;



import com.mwallet.mwalletcommons.data.models.BaseDomain;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author lawrence
 * created 17/12/2019 at 10:29
 **/
@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class Account extends BaseDomain {

    private String accountId;

    @NotBlank
    private String linkId;

    @NotNull
    @Builder.Default
    private AccountStatus accountStatus = AccountStatus.ACTIVE;

    @NotNull
    private AccountType accountType;

    @NotNull
    @Builder.Default
    private BigDecimal availableBalance = new BigDecimal("0.00");

    @Builder.Default
    @NotNull
    private BigDecimal actualBalance = new BigDecimal("0.00");

    /**
     * @author lawrence
     * created 17/12/2019 at 10:32
     **/
    public enum AccountStatus {
        ACTIVE,
        INACTIVE
    }

    /**
     * @author lawrence
     * created 17/12/2019 at 10:31
     **/
    public enum AccountType {
        USER_ACCOUNT,
        JOINT_ACCOUNT,
        TRUST_ACCOUNT
    }
}
