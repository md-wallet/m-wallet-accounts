package com.mwallet.mwalletaccounts.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import com.mwallet.mwalletcommons.data.models.BaseDomain;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;



/**
 * @author lawrence
 * created 17/12/2019 at 15:17
 **/
@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties({
        "id", "version",
        "updatedOn", "createdOn"
})
public class Transaction extends BaseDomain {

    private String transactionRef;

    @NotNull
    @JsonIgnoreProperties({
            "linkId", "accountStatus",
            "id", "version",
            "updatedOn", "createdOn"
    })
    private Account sourceAccount;

    @NotNull
    @JsonIgnoreProperties({
            "linkId", "accountStatus",
            "id", "version",
            "updatedOn", "createdOn"
    })
    private Account destinationAccount;

    @NotNull
    @DecimalMin(value = "0.0")
    private BigDecimal amount;

    @NotNull
    @DecimalMin(value = "0.0")
    private BigDecimal transactionCost;

    @NotNull
    private TransactionType transactionType;

    @NotNull
    private Transaction.TransactionStatus transactionStatus;

    @NotNull
    private PaymentMode paymentMode;

    @NotBlank
    private String billingAddress;

    @NotNull
    private ConfirmationType confirmationType;

    @NotNull
    private ConfirmationStatus confirmationStatus;

    @NotBlank
    private String transactionDescription;

    @NotBlank
    private String transactionDomainType;

    public enum TransactionType {
        DEPOSIT,
        WITHDRAW,
        SEND_MONEY
    }

    public enum TransactionStatus {
        NONE,
        VALIDATED,
        ACCEPTED,
        PREPARED,
        PAYMENT_REQUESTED,
        PAYMENT_VERIFIED,
        PAYOUT_REQUESTED,
        PAYOUT_VERIFIED,
        CONFIRMED,
        COMPLETED,
        FAILED
    }

    public enum PaymentMode {
        WALLET,
        MPESA,
        CARD,
        BANK
    }

    public enum ConfirmationType {
        INTERNAL,
        EXTERNAL,
    }


    public enum ConfirmationStatus {
        NONE,
        PROCEED,
        ROLLBACK;
    }

}
