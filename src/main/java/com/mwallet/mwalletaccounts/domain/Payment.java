package com.mwallet.mwalletaccounts.domain;



import com.mwallet.mwalletcommons.data.models.BaseDomain;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Objects;

import static com.mwallet.mwalletaccounts.domain.Transaction.*;

/**
 * @author lawrence
 * created 01/02/2020 at 21:13
 **/
@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public abstract class Payment extends BaseDomain {

    @NotBlank
    private String transactionRef;

    @NotNull
    @DecimalMin(value = "0.0")
    private BigDecimal amount;

    @NotNull
    private PaymentMode paymentMode;

    @NotBlank
    private String billingAddress;

    @NotBlank
    @Builder.Default
    private String narration = "NONE";

    @NotBlank
    @Builder.Default
    private String externalRefId = "NONE";

    @Getter
@Setter
    @SuperBuilder(toBuilder = true)
    @NoArgsConstructor
    @AllArgsConstructor
    @EqualsAndHashCode(callSuper = true)
    public static class PaymentRequest extends Payment {

        @NotBlank
        private String transactionDescription;

        @NotNull
        @Builder.Default
        private PaymentRequestStatus paymentRequestStatus = PaymentRequestStatus.NONE;

        @Getter
        @AllArgsConstructor
        public enum PaymentRequestStatus {
            NONE,
            PAYMENT_REQUEST_ACCEPTED,
            PAYMENT_REQUEST_FAILED,
            PAYMENT_REQUEST_REJECTED;
        }
    }

    @Getter
@Setter
    @SuperBuilder(toBuilder = true)
    @NoArgsConstructor
    @AllArgsConstructor
    @EqualsAndHashCode(callSuper = true)
    public static class PaymentResponse extends Payment {


        @Override
        public @NotBlank String getTransactionRef() {
            return Objects.isNull(super.getTransactionRef()) ? "NONE" : super.getTransactionRef();
        }

        @Override
        public @NotBlank String getBillingAddress() {
            return Objects.isNull(super.getBillingAddress()) ? "NONE" : super.getBillingAddress();
        }

        @Override
        public @NotNull @DecimalMin(value = "0.0") BigDecimal getAmount() {
            return Objects.isNull(super.getAmount()) ? BigDecimal.ZERO : super.getAmount();
        }

        @NotNull
        @Builder.Default
        private boolean orphan = true;

        @NotNull
        @Builder.Default
        private String receiptNumber = "NONE";

        @NotNull
        private PaymentResponseStatus paymentResponseStatus;

        @NotNull
        @Builder.Default
        private PaymentNotificationStatus paymentNotificationStatus = PaymentNotificationStatus.NONE;

        @Getter
        @AllArgsConstructor
        public enum PaymentResponseStatus {
            PAYMENT_RESPONSE_VERIFIED,
            PAYMENT_RESPONSE_FAILED;
        }

        public enum PaymentNotificationStatus {
            NONE,
            SENT,
            FAILED
        }
    }
}
