package com.mwallet.mwalletaccounts.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author lawrencemwaniki
 * created 14/05/2020 at 16:37
 **/

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class PaymentCard {

    @NotBlank
    private String maskedPan;

    @NotNull
    private CardType cardType;

    @NotBlank
    private String token;

    @NotBlank
    private String name;

    @NotNull
    private CardStatus cardStatus;

    @NotNull
    private User user;

    public enum CardType {
        VISA, MASTERCARD, AMEX;

        public int resolve() {
            return this.ordinal() + 1;
        }
    }

    public enum CardStatus {
        ACTIVE, INACTIVE
    }
}
