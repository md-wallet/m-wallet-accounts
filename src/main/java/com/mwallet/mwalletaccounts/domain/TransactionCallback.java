package com.mwallet.mwalletaccounts.domain;


import com.mwallet.mwalletaccounts.domain.Transaction.TransactionStatus;
import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author lawrencemwaniki
 * created 02/04/2020 at 10:18
 **/
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TransactionCallback {

    @NotBlank
    private String transactionRef;

    @NotNull
    private TransactionStatus transactionStatus;

    @NotNull
    private String transactionDomainType;

}
