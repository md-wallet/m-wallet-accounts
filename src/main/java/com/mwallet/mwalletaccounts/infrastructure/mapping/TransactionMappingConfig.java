package com.mwallet.mwalletaccounts.infrastructure.mapping;

import com.github.rozidan.springboot.modelmapper.TypeMapConfigurer;
import com.mwallet.mwalletaccounts.adapters.web.dtos.transactions.TransactionInfoDTO;
import com.mwallet.mwalletaccounts.domain.Transaction;
import com.mwallet.mwalletcommons.annotations.TypeMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.TypeMap;
import org.springframework.stereotype.Component;

/**
 * @author lawrence
 * created 26/01/2020 at 23:11
 **/
@Slf4j
@Component
class TransactionMappingConfig {

    @TypeMapper
    @RequiredArgsConstructor
    static class TransactionModelToTransactionInfoDTO extends TypeMapConfigurer<Transaction, TransactionInfoDTO> {


        @Override
        public void configure(TypeMap<Transaction, TransactionInfoDTO> typeMap) {
            typeMap.addMappings(mapper->{
                mapper.map(src->src.getSourceAccount().getAccountId(),TransactionInfoDTO::setSourceAccountId);
                mapper.map(src->src.getDestinationAccount().getAccountId(),TransactionInfoDTO::setDestinationAccountId);
            });
        }
    }
}
