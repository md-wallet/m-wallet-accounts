package com.mwallet.mwalletaccounts.infrastructure.mapping;

import com.github.rozidan.springboot.modelmapper.TypeMapConfigurer;
import com.mwallet.mwalletaccounts.adapters.web.dtos.transactions.TransactionLogDTO;
import com.mwallet.mwalletaccounts.domain.TransactionLog;
import com.mwallet.mwalletcommons.annotations.TypeMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.TypeMap;
import org.springframework.stereotype.Component;

/**
 * @author lawrence
 * created 26/01/2020 at 21:23
 **/
@Slf4j
@Component
class TransactionLogMappingConfig {

    @TypeMapper
    @RequiredArgsConstructor
    static class TransactionLogModelToTransactionLogDTOTypeMap extends TypeMapConfigurer<TransactionLog, TransactionLogDTO>{



        @Override
        public void configure(TypeMap<TransactionLog, TransactionLogDTO> typeMap) {
            typeMap.addMappings(mapper->{
                mapper.map(src->src.getTransaction().getTransactionRef(),TransactionLogDTO::setTransactionRef);
                mapper.map(src->src.getTransaction().getAmount(),TransactionLogDTO::setAmount);
                mapper.map(src->src.getTransaction().getTransactionCost(),TransactionLogDTO::setTransactionCost);
            });
        }
    }
}
