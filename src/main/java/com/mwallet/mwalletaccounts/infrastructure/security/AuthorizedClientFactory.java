package com.mwallet.mwalletaccounts.infrastructure.security;

import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.oauth2.client.AuthorizedClientServiceOAuth2AuthorizedClientManager;
import org.springframework.security.oauth2.client.OAuth2AuthorizeRequest;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.stereotype.Component;

/**
 * @author lawrence
 * created 28/01/2020 at 11:33
 **/
@Component
public class AuthorizedClientFactory {

    @Autowired
    private AuthorizedClientServiceOAuth2AuthorizedClientManager authorizedClientManager;

    public OAuth2AuthorizedClient getAuthorizedClient(String registeredClient) {
        val authorizedRequest = OAuth2AuthorizeRequest.withClientRegistrationId(registeredClient)
                .principal(new AnonymousAuthenticationToken(
                        registeredClient,registeredClient, AuthorityUtils.createAuthorityList("ANONYMOUS")))
                .build();
        return authorizedClientManager.authorize(authorizedRequest);
    }
}
