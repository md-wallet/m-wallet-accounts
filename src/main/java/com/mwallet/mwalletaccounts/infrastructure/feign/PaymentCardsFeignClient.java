package com.mwallet.mwalletaccounts.infrastructure.feign;


import com.mwallet.mwalletaccounts.domain.PaymentCard;
import com.mwallet.mwalletaccounts.infrastructure.security.AuthorizedClientFactory;
import com.mwallet.mwalletcommons.exceptions.GenericHttpException;
import com.netflix.hystrix.exception.HystrixBadRequestException;
import feign.RequestInterceptor;
import feign.RetryableException;
import feign.Retryer;
import feign.codec.ErrorDecoder;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Collections;
import java.util.List;


import static com.mwallet.mwalletaccounts.infrastructure.feign.PaymentCardsFeignClient.*;
import static com.mwallet.mwalletcommons.exceptions.DefaultServiceErrorMessage.extractEmbeddedErrorMessage;

/**
 * @author lawrencemwaniki
 * created 14/05/2020 at 16:56
 **/
@FeignClient(
        value = "payment-card-management",
        url = "${integrations.endpoints.auth-service}",
        fallbackFactory = PaymentCardsFeignClientFallbackFactory.class,
        configuration = PaymentsFeignClient.Configuration.class
)
public interface PaymentCardsFeignClient {

    @GetMapping(value = "/v1/integrations/card/{token}")
    PaymentCard getCard(@PathVariable String token);

    @Slf4j
    @Component
    class PaymentCardsFeignClientFallbackFactory implements FallbackFactory<PaymentCardsFeignClient> {

        @Override
        public PaymentCardsFeignClient create(Throwable throwable) {
            log.error("Executing PaymentCardsFeignClient Fallback");
            log.error("cause: {}", throwable.toString());

            return token -> null;
        }
    }

    @Slf4j
    class Configuration {

        private static final String AUTHORIZATION_HEADER = "Authorization";
        private static final String BEARER_TOKEN_TYPE = "Bearer";
        private static final String CLIENT_AUTH = "client-auth";
        private static final List<HttpStatus> RETRYABLE_STATUS_CODES = Collections.unmodifiableList(List.of(HttpStatus.SERVICE_UNAVAILABLE, HttpStatus.BAD_GATEWAY));
        private static final List<HttpStatus> PROPAGATABLE_STATUS_CODES = Collections.unmodifiableList(List.of(HttpStatus.UNPROCESSABLE_ENTITY, HttpStatus.CONFLICT));


        @Bean
        @Profile("!test")
        public RequestInterceptor oauthRequestInterceptor(final AuthorizedClientFactory authorizedClientFactory) {

            return requestTemplate -> {
                OAuth2AuthorizedClient authorizedClient = authorizedClientFactory.getAuthorizedClient(CLIENT_AUTH);
                //Get token
                final OAuth2AccessToken accessToken = authorizedClient.getAccessToken();

                String token = accessToken.getTokenValue();
                if (requestTemplate.headers().containsKey(AUTHORIZATION_HEADER))
                    log.warn("Authorization header has already been set");
                else if (token == null)
                    log.error("Can not get existing token for request, ignore if resource is not secured");
                else
                    requestTemplate.header(AUTHORIZATION_HEADER, String.format("%s %s", BEARER_TOKEN_TYPE, token));

            };
        }

        @Bean
        public Retryer retryer() {
            return new Retryer.Default();
        }

        @Bean
        public ErrorDecoder errorDecoder() {
            return (methodKey, response) -> {
                log.error("http status: {}, message: {}", response.status(), response.reason());

                if (RETRYABLE_STATUS_CODES.contains(HttpStatus.valueOf(response.status())))
                    throw new RetryableException(response.status(), response.toString(), response.request().httpMethod(), null, response.request());
                else if (PROPAGATABLE_STATUS_CODES.contains(HttpStatus.valueOf(response.status())))
                    throw new HystrixBadRequestException(
                            response.reason(),
                            new GenericHttpException(
                                    extractEmbeddedErrorMessage(response),
                                    HttpStatus.valueOf(response.status())));

                return new ErrorDecoder.Default().decode(methodKey, response);
            };
        }
    }
}
