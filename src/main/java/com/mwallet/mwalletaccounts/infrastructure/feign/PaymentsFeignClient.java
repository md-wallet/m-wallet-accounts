package com.mwallet.mwalletaccounts.infrastructure.feign;

import com.mwallet.mwalletaccounts.domain.Payment.PaymentRequest;
import com.mwallet.mwalletaccounts.domain.Payout.PayoutRequest;
import com.mwallet.mwalletaccounts.domain.Transaction;
import com.mwallet.mwalletaccounts.infrastructure.feign.PaymentsFeignClient.Models.PaymentRequestDTO;
import com.mwallet.mwalletaccounts.infrastructure.feign.PaymentsFeignClient.Models.PayoutRequestDTO;
import com.mwallet.mwalletaccounts.infrastructure.security.AuthorizedClientFactory;
import feign.RequestInterceptor;
import feign.RetryableException;
import feign.Retryer;
import feign.codec.ErrorDecoder;
import feign.hystrix.FallbackFactory;
import lombok.*;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.mwallet.mwalletaccounts.domain.Payment.PaymentRequest.PaymentRequestStatus.PAYMENT_REQUEST_FAILED;
import static com.mwallet.mwalletaccounts.domain.Payout.PayoutRequest.PayoutRequestStatus.PAYOUT_REQUEST_FAILED;


/**
 * @author lawrence
 * created 02/02/2020 at 11:35
 **/
@FeignClient(
        value = "payments-service",
        url = "${integrations.endpoints.payments-service}",
        fallbackFactory = PaymentsFeignClient.PaymentsFeignClientFallback.class,
        configuration = PaymentsFeignClient.Configuration.class
)
public interface PaymentsFeignClient {

    @PostMapping(value = "/v1/payments/request")
    PaymentRequest requestPayment(@RequestBody PaymentRequestDTO paymentRequestDTO);

    @PostMapping(value = "/v1/payouts/request")
    PayoutRequest requestPayout(@RequestBody PayoutRequestDTO payoutRequestDTO);

    class Models {
        @Getter
        @Setter
        @SuperBuilder(toBuilder = true)
        @NoArgsConstructor
        @AllArgsConstructor
        public static class PaymentRequestDTO {
            @NotBlank
            private String transactionRef;

            @NotNull
            @DecimalMin(value = "0.0")
            private BigDecimal amount;

            @NotNull
            private Transaction.PaymentMode paymentMode;

            @NotBlank
            private String billingAddress;

            @NotBlank
            @Builder.Default
            private String narration = "NONE";

            @NotBlank
            private String transactionDescription;
        }

        @Getter
        @Setter
        @SuperBuilder(toBuilder = true)
        @NoArgsConstructor
        @AllArgsConstructor
        public static class PayoutRequestDTO {
            @NotBlank
            private String transactionRef;

            @NotNull
            @DecimalMin(value = "0.0")
            private BigDecimal amount;

            @NotNull
            private Transaction.PaymentMode paymentMode;

            @NotBlank
            private String billingAddress;

            @NotBlank
            @Builder.Default
            private String narration = "NONE";

            @NotBlank
            private String transactionDescription;
        }
    }

    @Slf4j
    @Component
    class PaymentsFeignClientFallback implements FallbackFactory<PaymentsFeignClient> {

        @Override
        public PaymentsFeignClient create(Throwable throwable) {
            log.error("Executing PaymentsFeignClient Fallback");
            log.error("cause: {}", throwable.toString());

            return new PaymentsFeignClient() {
                @Override
                public PaymentRequest requestPayment(PaymentRequestDTO paymentRequestDTO) {
                    //attempt to get the actual error message if none exists then lets use the default one
                    String narration = PAYMENT_REQUEST_FAILED.name() + ":" + Optional.ofNullable(throwable.toString()).orElse("UNKNOWN");
                    return PaymentRequest.builder()
                            .transactionRef(paymentRequestDTO.transactionRef)
                            .amount(paymentRequestDTO.amount)
                            .paymentMode(paymentRequestDTO.paymentMode)
                            .billingAddress(paymentRequestDTO.billingAddress)
                            .transactionDescription(paymentRequestDTO.transactionDescription)
                            .paymentRequestStatus(PAYMENT_REQUEST_FAILED)
                            .narration(narration)
                            .build();
                }

                @Override
                public PayoutRequest requestPayout(PayoutRequestDTO payoutRequestDTO) {
                    String narration = PAYMENT_REQUEST_FAILED.name() + ":" + Optional.ofNullable(throwable.toString()).orElse("UNKNOWN");
                    return PayoutRequest.builder()
                            .transactionRef(payoutRequestDTO.transactionRef)
                            .amount(payoutRequestDTO.amount)
                            .paymentMode(payoutRequestDTO.paymentMode)
                            .billingAddress(payoutRequestDTO.billingAddress)
                            .transactionDescription(payoutRequestDTO.transactionDescription)
                            .payoutRequestStatus(PAYOUT_REQUEST_FAILED)
                            .narration(narration)
                            .build();
                }


            };
        }
    }

    @Slf4j
    class Configuration {

        private static final String AUTHORIZATION_HEADER = "Authorization";
        private static final String BEARER_TOKEN_TYPE = "Bearer";
        private static final String CLIENT_PAYMENTS = "client-payments";
        private static final List<HttpStatus> RETRYABLE_STATUS_CODES = Collections.unmodifiableList(List.of(HttpStatus.SERVICE_UNAVAILABLE, HttpStatus.BAD_GATEWAY));

        @Bean
        @Profile("!test")
        public RequestInterceptor oauthRequestInterceptor(final AuthorizedClientFactory authorizedClientFactory) {

            return requestTemplate -> {

                OAuth2AuthorizedClient authorizedClient = authorizedClientFactory.getAuthorizedClient(CLIENT_PAYMENTS);
                //Get token
                final OAuth2AccessToken accessToken = authorizedClient.getAccessToken();

                String token = accessToken.getTokenValue();
                if (requestTemplate.headers().containsKey(AUTHORIZATION_HEADER))
                    log.warn("Authorization header has already been set");
                else if (token == null)
                    log.error("Can not get existing token for request, ignore if resource is not secured");
                else
                    requestTemplate.header(AUTHORIZATION_HEADER, String.format("%s %s", BEARER_TOKEN_TYPE, token));

            };
        }

        @Bean
        public Retryer retryer() {
            return new Retryer.Default();
        }

        @Bean
        public ErrorDecoder errorDecoder() {
            return (methodKey, response) -> {
                log.error("http status: {}, message: {}", response.status(), response.reason());

                if (RETRYABLE_STATUS_CODES.contains(HttpStatus.valueOf(response.status())))
                    throw new RetryableException(response.status(), response.toString(), response.request().httpMethod(), null, response.request());

                return new ErrorDecoder.Default().decode(methodKey, response);
            };
        }
    }
}
