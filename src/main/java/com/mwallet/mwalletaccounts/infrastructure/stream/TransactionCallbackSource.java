package com.mwallet.mwalletaccounts.infrastructure.stream;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

/**
 * @author lawrencemwaniki
 * created 02/04/2020 at 12:54
 **/
public interface TransactionCallbackSource {

    String TRANSACTIONS_CALLBACK_OUTPUT = "transactionsCallbackChannelOutput";

    @Output(TransactionCallbackSource.TRANSACTIONS_CALLBACK_OUTPUT)
    MessageChannel sendTransactionCallback();
}
