package com.mwallet.mwalletaccounts.infrastructure.stream;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

/**
 * @author lawrence
 * created 18/02/2020 at 11:02
 **/
public interface PaymentSink {
    String PAYMENT_RESPONSE_INPUT = "paymentsResponseChannelInput";
    String PAYOUT_RESPONSE_INPUT = "payoutsResponseChannelInput";

    @Input(PaymentSink.PAYMENT_RESPONSE_INPUT)
    SubscribableChannel paymentsResponseChannelInput();

    @Input(PaymentSink.PAYOUT_RESPONSE_INPUT)
    SubscribableChannel payoutsResponseChannelInput();


}
