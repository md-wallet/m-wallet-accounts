package com.mwallet.mwalletaccounts.infrastructure.stream;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

/**
 * @author lawrence
 * created 20/01/2020 at 02:50
 **/
public interface TransactionSink {
    String SEND_MONEY_INPUT = "sendMoneyTransactionsChannelInput";
    String DEPOSIT_MONEY_INPUT = "depositMoneyTransactionsChannelInput";
    String WITHDRAW_MONEY_INPUT = "withdrawMoneyTransactionsChannelInput";

    @Input(TransactionSink.SEND_MONEY_INPUT)
    SubscribableChannel sendMoneyTransactionsInput();

    @Input(TransactionSink.DEPOSIT_MONEY_INPUT)
    SubscribableChannel depositMoneyTransactionsInput();

    @Input(TransactionSink.WITHDRAW_MONEY_INPUT)
    SubscribableChannel withdrawMoneyTransactionsInput();

}
