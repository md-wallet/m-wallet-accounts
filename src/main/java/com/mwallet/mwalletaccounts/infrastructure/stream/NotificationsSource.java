package com.mwallet.mwalletaccounts.infrastructure.stream;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

/**
 * @author lawrence
 * created 06/12/2019 at 09:18
 **/
public interface NotificationsSource {

    String OUTPUT = "notificationsChannelOutput";

    @Output(NotificationsSource.OUTPUT)
    MessageChannel notificationsOutput();
}