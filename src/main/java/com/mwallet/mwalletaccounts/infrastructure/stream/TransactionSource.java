package com.mwallet.mwalletaccounts.infrastructure.stream;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

/**
 * @author lawrence
 * created 20/01/2020 at 02:34
 **/
public interface TransactionSource {
    String SEND_MONEY_OUTPUT = "sendMoneyTransactionsChannelOutput";
    String DEPOSIT_MONEY_OUTPUT="depositMoneyTransactionsChannelOutput";
    String WITHDRAW_MONEY_OUTPUT="withdrawMoneyTransactionsChannelOutput";

    @Output(TransactionSource.SEND_MONEY_OUTPUT)
    MessageChannel sendMoneyTransactionsOutput();

    @Output(TransactionSource.DEPOSIT_MONEY_OUTPUT)
    MessageChannel depositMoneyTransactionsOutput();

    @Output(TransactionSource.WITHDRAW_MONEY_OUTPUT)
    MessageChannel withdrawMoneyTransactionsOutput();
}
