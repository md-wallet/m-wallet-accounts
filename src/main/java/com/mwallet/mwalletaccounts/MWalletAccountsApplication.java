package com.mwallet.mwalletaccounts;

import com.mwallet.mwalletaccounts.infrastructure.stream.*;
import com.mwallet.mwalletcommons.annotations.EnableMWalletCommons;
import com.mwallet.mwalletcommons.data.config.YamlPropertySourceFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;


@EnableFeignClients
@EnableRedisRepositories
@EnableBinding({TransactionSource.class, TransactionSink.class, NotificationsSource.class, PaymentSink.class, TransactionCallbackSource.class})
@PropertySources({
        @PropertySource("classpath:application.yml"),
        @PropertySource("classpath:application-${spring.profiles.active}.yml"),
        @PropertySource(value = "classpath:stream.yml", factory = YamlPropertySourceFactory.class),
        @PropertySource(value = "classpath:stream-parking.yml", factory = YamlPropertySourceFactory.class)
})
@SpringBootApplication(scanBasePackages = "com.mwallet")
public class MWalletAccountsApplication {

    public static void main(String[] args) {
        SpringApplication.run(MWalletAccountsApplication.class, args);
    }

}
