package com.mwallet.mwalletaccounts.adapters.messaging;

import com.mwallet.mwalletcommons.parking.ParkingSource;
import com.mwallet.mwalletcommons.utils.AMQPUtils;
import com.mwallet.mwalletcommons.utils.MessageSender;
import com.mwallet.mwalletcommons.utils.MessageSender.DelayedMessage;
import com.mwallet.mwalletaccounts.application.ports.out.transactions.PublishTransactionOutputPort;
import com.mwallet.mwalletaccounts.domain.Account.AccountType;
import com.mwallet.mwalletaccounts.domain.Transaction;
import com.mwallet.mwalletaccounts.domain.TransactionCallback;
import com.mwallet.mwalletaccounts.infrastructure.stream.TransactionCallbackSource;
import com.mwallet.mwalletaccounts.infrastructure.stream.TransactionSource;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.rmi.UnexpectedException;

import static com.mwallet.mwalletcommons.utils.ObjectUtils.writeJson;
import static com.mwallet.mwalletaccounts.domain.Account.AccountType.JOINT_ACCOUNT;

/**
 * @author lawrence
 * created 26/01/2020 at 18:50
 **/
@Slf4j
@Service
@RequiredArgsConstructor
class PublishTransactionMessagingAdapter implements PublishTransactionOutputPort {

    private final TransactionSource transactionSource;
    private final TransactionCallbackSource transactionCallbackSource;
    private final MessageSender messageSender;
    private final ModelMapper modelMapper;

    /**
     * Transaction publish timeout in milliseconds
     */
    private final static long PUBLISH_TIMEOUT = 5000;

    @SneakyThrows
    public void publishTransaction(Transaction transaction) {
        boolean sent;
        switch (transaction.getTransactionType()) {
            case SEND_MONEY:
                sent = transactionSource.sendMoneyTransactionsOutput().send(AMQPUtils.buildMessageFrom(transaction), PUBLISH_TIMEOUT);
                break;
            case DEPOSIT:
                sent = transactionSource.depositMoneyTransactionsOutput().send(AMQPUtils.buildMessageFrom(transaction), PUBLISH_TIMEOUT);
                break;
            case WITHDRAW:
                sent = transactionSource.withdrawMoneyTransactionsOutput().send(AMQPUtils.buildMessageFrom(transaction), PUBLISH_TIMEOUT);
                break;
            default:
                throw new UnsupportedOperationException("COULD NOT GET MSG CHANNEL FOR TXN TYPE: " + transaction.getTransactionType());
        }
        if (!sent) throw new UnexpectedException("Transaction Publish Failure");
    }

    @Override
    @SneakyThrows
    public void publishDelayedTransaction(Transaction transaction, Long delay, Long retryCount) {
        String returnAddress;
        switch (transaction.getTransactionType()) {
            case SEND_MONEY:
                returnAddress = TransactionSource.SEND_MONEY_OUTPUT;
                break;
            case DEPOSIT:
                returnAddress = TransactionSource.DEPOSIT_MONEY_OUTPUT;
                break;
            case WITHDRAW:
                returnAddress = TransactionSource.WITHDRAW_MONEY_OUTPUT;
                break;
            default:
                throw new UnsupportedOperationException("COULD NOT GET RETURN ADDRESS FOR TXN TYPE: " + transaction.getTransactionType());
        }

        DelayedMessage<Transaction> delayedMessage = DelayedMessage.<Transaction>builder()
                .address(returnAddress)
                .delay(delay)
                .retryCount(retryCount)
                .payload(transaction)
                .build();

        boolean sent = messageSender.sendDelayed(delayedMessage, ParkingSource.PARKING_OUTPUT);
        if (!sent) throw new UnexpectedException("Transaction Publish Failure");
    }

    @Override
    public void publishTransactionCallback(Transaction transaction) {
        AccountType sourceAccountType = transaction.getSourceAccount().getAccountType();
        AccountType destinationAccountType = transaction.getDestinationAccount().getAccountType();

        if (sourceAccountType == JOINT_ACCOUNT || destinationAccountType == JOINT_ACCOUNT) {
            TransactionCallback transactionCallback = modelMapper.map(transaction, TransactionCallback.class);
            boolean sent = transactionCallbackSource.sendTransactionCallback().send(AMQPUtils.buildMessageFrom(transactionCallback), PUBLISH_TIMEOUT);
            log.debug("transactionCallback: {}", writeJson(transactionCallback));
            log.debug("transactionCallbackSent: {}", sent);
        }

    }


}
