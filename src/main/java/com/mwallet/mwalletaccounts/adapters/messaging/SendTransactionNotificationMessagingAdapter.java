package com.mwallet.mwalletaccounts.adapters.messaging;

import com.mwallet.mwalletcommons.notifications.*;
import com.mwallet.mwalletcommons.utils.AMQPUtils;
import com.mwallet.mwalletaccounts.application.ports.out.transactions.SendTransactionNotificationOutputPort;
import com.mwallet.mwalletaccounts.domain.Account;
import com.mwallet.mwalletaccounts.domain.Transaction;
import com.mwallet.mwalletaccounts.infrastructure.stream.NotificationsSource;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;

import static com.mwallet.mwalletcommons.utils.ObjectUtils.writeJson;

/**
 * @author lawrence
 * created 12/02/2020 at 22:40
 **/
@Slf4j
@Service
@RequiredArgsConstructor
class SendTransactionNotificationMessagingAdapter implements NotificationService<Transaction>, SendTransactionNotificationOutputPort {

    private final NotificationsSource notificationsSource;

    @Override
    public void sendNotification(
            @Valid Transaction transaction,
            @Valid @NotNull NotificationState notificationState,
            @Valid @NotNull NotificationType notificationType,
            @Valid @NotNull HashMap<Notification.Link, List<EgressType>> recipientEgressTypes) {
        Notification notification = Notification.builder()
                .recipientEgressTypes(recipientEgressTypes)
                .notificationType(notificationType)
                .timestamp(LocalDateTime.now())
                .customFields(new HashMap<>() {{
                    put("transactionRef", transaction.getTransactionRef());
                    put("amount", transaction.getAmount());
                    put("transactionCost", transaction.getTransactionCost());
                    put("transactionType", transaction.getTransactionType());
                    put("sourceAccount", transaction.getSourceAccount());
                    put("destinationAccount", transaction.getDestinationAccount());
                }})
                .build();

        log.debug("generatedNotification: {}", writeJson(notification));

        boolean sent = notificationsSource.notificationsOutput().send(AMQPUtils.buildMessageFrom(notification));
        log.info("transactionNotificationSent: {}", sent);

    }

    @Override
    public void send(
            @Valid @NotNull Transaction transaction,
            @Valid @NotNull NotificationType notificationType) {

        HashMap<Notification.Link, List<EgressType>> recipientsEgressTypes = new HashMap<>();

        log.info("=======================SendTransactionNotificationMessagingAdapter==========================");

        Account srcAccount = transaction.getSourceAccount();
        Account destAccount = transaction.getDestinationAccount();
        switch (transaction.getTransactionType()) {
            case DEPOSIT:
                recipientsEgressTypes.put(Notification.Link.builder().linkId(destAccount.getLinkId()).linkType(convertAccountTypeToRecipientType(destAccount.getAccountType())).build(), List.of(EgressType.SMS));
                break;
            case WITHDRAW:
                recipientsEgressTypes.put(Notification.Link.builder().linkId(srcAccount.getLinkId()).linkType(convertAccountTypeToRecipientType(srcAccount.getAccountType())).build(), List.of(EgressType.SMS));
                break;
            case SEND_MONEY:
                switch (notificationType) {
                    case TRANSACTION_SUCCESS:
                        recipientsEgressTypes.put(Notification.Link.builder().linkId(srcAccount.getLinkId()).linkType(convertAccountTypeToRecipientType(srcAccount.getAccountType())).build(), List.of(EgressType.SMS));
                        recipientsEgressTypes.put(Notification.Link.builder().linkId(destAccount.getLinkId()).linkType(convertAccountTypeToRecipientType(destAccount.getAccountType())).build(), List.of(EgressType.SMS));
                        break;
                    case TRANSACTION_FAIL:
                    case TRANSACTION_DUPLICATE:
                        recipientsEgressTypes.put(Notification.Link.builder().linkId(srcAccount.getLinkId()).linkType(convertAccountTypeToRecipientType(srcAccount.getAccountType())).build(), List.of(EgressType.SMS));
                        break;
                    default:
                        throw new UnsupportedOperationException("Unsupported notificationType: " + notificationType);
                }
                break;
            default:
                throw new UnsupportedOperationException("Unsupported transactionType: " + transaction.getTransactionType());

        }
        sendNotification(transaction, NotificationState.ALERT, notificationType, recipientsEgressTypes);
    }

    private Notification.LinkType convertAccountTypeToRecipientType(Account.AccountType accountType) {
        switch (accountType) {
            case USER_ACCOUNT:
            case JOINT_ACCOUNT:
                return Notification.LinkType.USER;
            case TRUST_ACCOUNT:
                return Notification.LinkType.TRUST;
            default:
                throw new UnsupportedOperationException("Unsupported accountType: " + accountType);
        }
    }
}


