package com.mwallet.mwalletaccounts.adapters.messaging;


import com.mwallet.mwalletaccounts.application.ports.in.transactions.DepositMoneyUseCase;
import com.mwallet.mwalletaccounts.application.ports.in.transactions.WithdrawMoneyUseCase;
import com.mwallet.mwalletaccounts.domain.Payment;
import com.mwallet.mwalletaccounts.domain.Payout.PayoutResponse;
import com.mwallet.mwalletaccounts.infrastructure.stream.PaymentSink;
import com.mwallet.mwalletcommons.utils.AMQPUtils;
import com.mwallet.mwalletcommons.utils.ObjectUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.stereotype.Service;

import java.util.Map;

import static com.mwallet.mwalletcommons.utils.ObjectUtils.*;


/**
 * @author lawrence
 * created 18/02/2020 at 23:25
 **/
@Slf4j
@Service
@RequiredArgsConstructor
class PaymentsResponseListener {

    private final DepositMoneyUseCase depositMoneyUseCase;
    private final WithdrawMoneyUseCase withdrawMoneyUseCase;

    @StreamListener(target = PaymentSink.PAYMENT_RESPONSE_INPUT)
    private void receivePaymentResponse(Payment.PaymentResponse paymentResponse, @Headers MessageHeaders messageHeaders, @Header(name = "x-death", required = false) Map<?, ?> death) {
        log.info("#########################  PAYMENT_RESPONSE_LISTENER  #######################");

        Long retryCount = messageHeaders.get("x-retries", Long.class);
        log.info("retryCount: {}, death: {}", retryCount, AMQPUtils.getDeadLetterCount(death));

        log.debug("receivedPaymentResponse: {}", writeJson(paymentResponse));

        try {
            depositMoneyUseCase.verifyDepositMoneyPayment(paymentResponse);
        } catch (Exception ex) {
            throw new AmqpRejectAndDontRequeueException(ex);
        }
        log.info("###################################################################################");
    }

    @StreamListener(target = PaymentSink.PAYOUT_RESPONSE_INPUT)
    private void receivePayoutResponse(PayoutResponse payoutResponse, @Headers MessageHeaders messageHeaders, @Header(name = "x-death", required = false) Map<?, ?> death) {
        log.info("#########################  PAYOUT_RESPONSE_LISTENER  #######################");

        Long retryCount = messageHeaders.get("x-retries", Long.class);
        log.info("retryCount: {}, death: {}", retryCount, AMQPUtils.getDeadLetterCount(death));

        log.debug("receivedPayoutResponse: {}", writeJson(payoutResponse));
        try {
            withdrawMoneyUseCase.verifyWithdrawMoneyPayout(payoutResponse);
        } catch (Exception ex) {
            throw new AmqpRejectAndDontRequeueException(ex);
        }
        log.info("###################################################################################");
    }
}
