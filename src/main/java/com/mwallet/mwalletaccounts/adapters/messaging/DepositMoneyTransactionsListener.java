package com.mwallet.mwalletaccounts.adapters.messaging;


import com.mwallet.mwalletaccounts.application.ports.in.transactions.DepositMoneyUseCase;
import com.mwallet.mwalletaccounts.application.ports.out.transactions.PublishTransactionOutputPort;
import com.mwallet.mwalletaccounts.domain.Transaction;
import com.mwallet.mwalletaccounts.infrastructure.stream.TransactionSink;
import com.mwallet.mwalletcommons.utils.AMQPUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.mwallet.mwalletaccounts.domain.Transaction.ConfirmationType.INTERNAL;

/**
 * @author lawrence
 * created 06/02/2020 at 11:20
 **/
@Slf4j
@Service
@RequiredArgsConstructor
class DepositMoneyTransactionsListener {

    private final DepositMoneyUseCase depositMoneyUseCase;
    private final PublishTransactionOutputPort publishTransactionOutputPort;

    private static Long MAX_RETRY_DELAY = TimeUnit.MINUTES.toMillis(30);

    @StreamListener(target = TransactionSink.DEPOSIT_MONEY_INPUT)
    private void receiveTransaction(Transaction transaction, @Headers MessageHeaders messageHeaders,@Header(name = "x-death", required = false) Map<?, ?> death) {
        log.info("#########################  DEPOSIT_MONEY_TRANSACTION_LISTENER  #######################");

        Long retryCount = messageHeaders.get("x-retries", Long.class);
        log.info("retryCount: {}, death: {}", retryCount, AMQPUtils.getDeadLetterCount(death));

        try {
            processDepositMoneyTransaction(transaction, retryCount);
        } catch (Exception ex) {
            throw new AmqpRejectAndDontRequeueException(ex);
        }
        log.info("###################################################################################");
    }

    /**
     * Process a deposit money transaction
     * @param transaction transaction to process
     * @param retryCount retry counter
     */
    private void processDepositMoneyTransaction(Transaction transaction, Long retryCount) {
        switch (transaction.getTransactionStatus()) {
            case ACCEPTED:
                depositMoneyUseCase.prepareDepositMoney(transaction);
                break;
            case PREPARED:
                /*IMPLICITLY, DEPOSIT transactions are INTERNALLY CONFIRMED*/
                assert (transaction.getConfirmationType() == INTERNAL) : "INTERNAL CONFIRMATION ONLY OVER MSG BROKER";
                //1. request payment
                boolean paymentRequestSuccessful = depositMoneyUseCase.requestDepositMoneyPayment(transaction);
                //2. if request is not successful retry via exponential back off
                if (!paymentRequestSuccessful) {
                    retryCount = retryCount == null ? 1 : retryCount;
                    long exponentialDelay = AMQPUtils.exponentialDelay(retryCount, MAX_RETRY_DELAY);
                    publishTransactionOutputPort.publishDelayedTransaction(transaction, exponentialDelay, retryCount);
                }
                break;
            case PAYMENT_VERIFIED:
                depositMoneyUseCase.confirmDepositMoney(transaction);
                break;
            case CONFIRMED:
                depositMoneyUseCase.completeDepositMoney(transaction);
                break;
            default:
                throw new UnsupportedOperationException("UNEXPECTED TRANSACTION STATE: " + transaction.getTransactionStatus());
        }
    }
}
