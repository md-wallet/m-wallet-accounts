package com.mwallet.mwalletaccounts.adapters.messaging;

import com.mwallet.mwalletcommons.utils.AMQPUtils;
import com.mwallet.mwalletaccounts.application.ports.in.transactions.SendMoneyUseCase;
import com.mwallet.mwalletaccounts.domain.Transaction;
import com.mwallet.mwalletaccounts.infrastructure.stream.TransactionSink;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Service;

import java.util.Map;

import static com.mwallet.mwalletaccounts.domain.Transaction.ConfirmationType.INTERNAL;

/**
 * @author lawrence
 * created 20/01/2020 at 02:56
 **/
@Slf4j
@Service
@RequiredArgsConstructor
class SendMoneyTransactionsListener {

    private final SendMoneyUseCase sendMoneyUseCase;

    @StreamListener(target = TransactionSink.SEND_MONEY_INPUT)
    private void receiveTransaction(Transaction transaction, @Header(name = "x-death", required = false) Map<?, ?> death) {
        log.info("#########################  SEND_MONEY_TRANSACTION_LISTENER  #######################");
        log.info("x-death: {}", AMQPUtils.getDeadLetterCount(death));

        try {
            processSendMoneyTransaction(transaction);
        } catch (Exception ex) {
            throw new AmqpRejectAndDontRequeueException(ex);
        }
        log.info("###################################################################################");
    }

    /**
     * Process sendMoney Transaction
     *
     * @param transaction transaction to process
     */
    private void processSendMoneyTransaction(Transaction transaction) {
        switch (transaction.getTransactionStatus()) {
            case ACCEPTED:
                sendMoneyUseCase.prepareSendMoney(transaction);
                break;
            case PREPARED:
                /*A prepared SEND_MONEY Transaction will only arrive on the listener for confirmation if and only if
                its an INTERNALLY confirmed transaction.*/
                assert (transaction.getConfirmationType() == INTERNAL) : "INTERNAL CONFIRMATION ONLY OVER MSG BROKER";
                sendMoneyUseCase.confirmSendMoney(transaction.getTransactionRef());
                break;
            case CONFIRMED:
                sendMoneyUseCase.completeSendMoney(transaction);
                break;
            default:
                throw new UnsupportedOperationException("UNEXPECTED TRANSACTION STATE: " + transaction.getTransactionStatus());
        }
    }


}
