package com.mwallet.mwalletaccounts.adapters.messaging;

import com.mwallet.mwalletcommons.utils.AMQPUtils;
import com.mwallet.mwalletaccounts.application.ports.in.transactions.WithdrawMoneyUseCase;
import com.mwallet.mwalletaccounts.application.ports.out.transactions.PublishTransactionOutputPort;
import com.mwallet.mwalletaccounts.domain.Transaction;
import com.mwallet.mwalletaccounts.infrastructure.stream.TransactionSink;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.mwallet.mwalletaccounts.domain.Transaction.ConfirmationType.INTERNAL;

/**
 * @author lawrence
 * created 12/02/2020 at 09:56
 **/
@Slf4j
@Service
@RequiredArgsConstructor
class WithdrawMoneyTransactionsListener {
    private final WithdrawMoneyUseCase withdrawMoneyUseCase;
    private final PublishTransactionOutputPort publishTransactionOutputPort;

    private static final Long MAX_RETRY_DELAY = TimeUnit.MINUTES.toMillis(30);

    @StreamListener(target = TransactionSink.WITHDRAW_MONEY_INPUT)
    private void receiveTransaction(Transaction transaction, @Headers MessageHeaders messageHeaders, @Header(name = "x-death", required = false) Map<?, ?> death) {
        log.info("#########################  WITHDRAW_MONEY_TRANSACTION_LISTENER  #######################");

        Long retryCount = messageHeaders.get("x-retries", Long.class);
        log.info("retryCount: {}, death: {}", retryCount, AMQPUtils.getDeadLetterCount(death));

        try {
            processWithdrawMoneyTransaction(transaction,retryCount);
        } catch (Exception ex) {
            throw new AmqpRejectAndDontRequeueException(ex);
        }
        log.info("###################################################################################");
    }

    /**
     * Process a withdraw money transaction
     * @param transaction transaction to process
     * @param retryCount retry counter
     */
    private void processWithdrawMoneyTransaction(Transaction transaction, Long retryCount){
        switch (transaction.getTransactionStatus()){
            case ACCEPTED:
                withdrawMoneyUseCase.prepareWithdrawMoney(transaction);
                break;
            case PREPARED:
                /*IMPLICITLY, WITHDRAW transactions are INTERNALLY CONFIRMED*/
                assert (transaction.getConfirmationType() == INTERNAL) : "INTERNAL CONFIRMATION ONLY OVER MSG BROKER";
                //1. request payout
                boolean payoutRequestSuccessful = withdrawMoneyUseCase.requestWithdrawMoneyPayout(transaction);
                //2. if request is not successful try via exponential back off
                if (!payoutRequestSuccessful) {
                    retryCount = retryCount == null ? 1 : retryCount;
                    long exponentialDelay = AMQPUtils.exponentialDelay(retryCount, MAX_RETRY_DELAY);
                    publishTransactionOutputPort.publishDelayedTransaction(transaction, exponentialDelay, retryCount);
                }
                break;
            case PAYOUT_VERIFIED:
                withdrawMoneyUseCase.confirmWithdrawMoney(transaction);
                break;
            case CONFIRMED:
                withdrawMoneyUseCase.completeWithdrawMoney(transaction);
                break;
            default:
                throw new UnsupportedOperationException("UNEXPECTED TRANSACTION STATE: " + transaction.getTransactionStatus());

        }
    }
}
