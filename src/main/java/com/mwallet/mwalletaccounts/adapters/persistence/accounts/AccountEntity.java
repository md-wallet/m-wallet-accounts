package com.mwallet.mwalletaccounts.adapters.persistence.accounts;

import com.mwallet.mwalletcommons.data.models.BaseEntity;
import com.mwallet.mwalletaccounts.domain.Account;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author lawrence
 * created 17/12/2019 at 10:43
 **/

@Entity
@Table(name = "accounts")
@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class AccountEntity extends BaseEntity {

    @Column(name = "account_id")
    private String accountId;

    @NotBlank
    @Column(name = "link_id")
    private String linkId;

    @NotNull
    @Column(name = "available_balance")
    private BigDecimal availableBalance;

    @NotNull
    @Column(name = "actual_balance")
    private BigDecimal actualBalance;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "account_status")
    private Account.AccountStatus accountStatus;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "account_type")
    private Account.AccountType accountType;

}
