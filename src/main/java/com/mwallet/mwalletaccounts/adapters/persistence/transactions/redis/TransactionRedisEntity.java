package com.mwallet.mwalletaccounts.adapters.persistence.transactions.redis;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mwallet.mwalletaccounts.domain.Transaction;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.TimeToLive;

import javax.validation.constraints.NotNull;
import java.util.concurrent.TimeUnit;

/**
 * @author lawrence
 * created 02/02/2020 at 11:15
 **/
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@RedisHash("TransactionRedisEntity")
@SuperBuilder(toBuilder = true)
class TransactionRedisEntity{

    @Id
    private String hash;

    @JsonProperty("TTL")
    @TimeToLive(unit = TimeUnit.SECONDS)
    private Long TTL;

    @NotNull
    private Transaction transaction;

}
