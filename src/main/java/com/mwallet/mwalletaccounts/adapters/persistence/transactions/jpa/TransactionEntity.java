package com.mwallet.mwalletaccounts.adapters.persistence.transactions.jpa;

import com.mwallet.mwalletcommons.data.models.BaseEntity;
import com.mwallet.mwalletaccounts.adapters.persistence.accounts.AccountEntity;
import com.mwallet.mwalletaccounts.domain.Transaction.ConfirmationType;
import com.mwallet.mwalletaccounts.domain.Transaction.PaymentMode;
import com.mwallet.mwalletaccounts.domain.Transaction.TransactionStatus;
import com.mwallet.mwalletaccounts.domain.Transaction.TransactionType;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

import static com.mwallet.mwalletaccounts.domain.Transaction.ConfirmationStatus;

/**
 * @author lawrence
 * created 17/12/2019 at 15:24
 **/

@Entity
@Table(name = "transactions")
@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class TransactionEntity extends BaseEntity {

    @NotNull
    @Column(name = "transaction_ref")
    private String transactionRef;

    @NotNull
    @Column(name = "amount")
    @DecimalMin(value = "0.0")
    private BigDecimal amount;

    @NotNull
    @Column(name = "transaction_cost")
    @DecimalMin(value = "0.0")
    private BigDecimal transactionCost;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "transaction_state")
    private TransactionStatus transactionStatus;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "payment_mode")
    private PaymentMode paymentMode;

    @NotBlank
    @Column(name = "billing_address")
    private String billingAddress;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "confirmation_type")
    private ConfirmationType confirmationType;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "confirmation_status")
    private ConfirmationStatus confirmationStatus;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "transaction_type")
    private TransactionType transactionType;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "src_account_id", referencedColumnName = "account_id", updatable = false)
    private AccountEntity sourceAccount;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "dest_account_id", referencedColumnName = "account_id", updatable = false)
    private AccountEntity destinationAccount;

    @NotBlank
    @Column(name = "transaction_desc")
    private String transactionDescription;

    @NotBlank
    @Column(name = "transaction_domain_type")
    private String transactionDomainType;
}
