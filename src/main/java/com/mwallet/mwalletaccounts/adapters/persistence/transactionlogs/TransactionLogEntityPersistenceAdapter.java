package com.mwallet.mwalletaccounts.adapters.persistence.transactionlogs;

import com.mwallet.mwalletcommons.annotations.PersistenceAdapter;
import com.mwallet.mwalletaccounts.adapters.persistence.transactions.jpa.TransactionEntity;
import com.mwallet.mwalletaccounts.adapters.persistence.transactions.jpa.TransactionEntityJpaRepository;
import com.mwallet.mwalletaccounts.application.ports.out.transactionlogs.LoadTransactionLogOutputPort;
import com.mwallet.mwalletaccounts.application.ports.out.transactionlogs.LogTransactionOutputPort;
import com.mwallet.mwalletaccounts.domain.Transaction;
import com.mwallet.mwalletaccounts.domain.TransactionLog;
import com.mwallet.mwalletcommons.data.services.Finder;
import com.mwallet.mwalletcommons.data.services.GenericPersistenceAdapter;
import com.mwallet.mwalletcommons.data.services.Nested;
import com.mwallet.mwalletcommons.exceptions.GenericHttpException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpStatus;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.lang.reflect.Type;
import java.util.List;
import java.util.function.Supplier;

import static com.mwallet.mwalletcommons.utils.ObjectUtils.writeJson;
import static com.mwallet.mwalletaccounts.application.ports.out.transactions.LoadTransactionOutputPort.TransactionNotFoundException;

/**
 * @author lawrence
 * created 22/01/2020 at 16:57
 **/

@Slf4j
@PersistenceAdapter
class TransactionLogEntityPersistenceAdapter extends GenericPersistenceAdapter<TransactionLog, TransactionLogEntity> implements LogTransactionOutputPort, LoadTransactionLogOutputPort {

    protected TransactionLogEntityPersistenceAdapter(ModelMapper modelMapper, JpaRepository<TransactionLogEntity, Long> jpaRepository) {
        super(modelMapper, jpaRepository);
    }

    @Override
    public TransactionLog logTransaction(@Valid Transaction transaction, @Valid @NotNull String message) {
        TransactionLog transactionLog = TransactionLog.builder()
                .transactionStatus(transaction.getTransactionStatus())
                .transaction(transaction)
                .srcAvailableBalance(transaction.getSourceAccount().getAvailableBalance())
                .srcActualBalance(transaction.getSourceAccount().getActualBalance())
                .destAvailableBalance(transaction.getDestinationAccount().getAvailableBalance())
                .destActualBalance(transaction.getDestinationAccount().getActualBalance())
                .confirmationStatus(transaction.getConfirmationStatus())
                .message(message)
                .build();
        return create(transactionLog);
    }

    @Override
    public List<TransactionLog> loadTransactionLogsByTransactionRef(@Valid @NotBlank String transactionRef) throws TransactionNotFoundException {
        return findListBy(Finder.by(Nested.as(Transaction::new, TransactionLog::setTransaction)
                .with(transactionRef, Transaction::setTransactionRef)));
    }

    @Override
    public <R extends GenericHttpException> Supplier<R> resourceNotFoundExceptionSupplier() {
        return () -> (R) new GenericHttpException("TransactionLog Not Found", HttpStatus.NOT_FOUND);
    }
}
