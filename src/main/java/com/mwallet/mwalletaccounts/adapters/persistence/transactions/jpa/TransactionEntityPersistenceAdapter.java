package com.mwallet.mwalletaccounts.adapters.persistence.transactions.jpa;

import ch.qos.logback.core.html.NOPThrowableRenderer;
import com.mwallet.mwalletaccounts.domain.Account;
import com.mwallet.mwalletcommons.annotations.PersistenceAdapter;
import com.mwallet.mwalletaccounts.adapters.persistence.accounts.AccountEntity;
import com.mwallet.mwalletaccounts.adapters.persistence.accounts.AccountEntityJpaRepository;
import com.mwallet.mwalletaccounts.application.ports.out.accounts.LoadAccountOutputPort.AccountNotFoundException;
import com.mwallet.mwalletaccounts.application.ports.out.transactions.LoadTransactionOutputPort;
import com.mwallet.mwalletaccounts.application.ports.out.transactions.SaveTransactionOutputPort;
import com.mwallet.mwalletaccounts.domain.Transaction;
import com.mwallet.mwalletcommons.data.models.PageableModel;
import com.mwallet.mwalletcommons.data.services.Finder;
import com.mwallet.mwalletcommons.data.services.GenericPersistenceAdapter;
import com.mwallet.mwalletcommons.data.services.Nested;
import com.mwallet.mwalletcommons.exceptions.GenericHttpException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mockito.internal.matchers.Find;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpStatus;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;
import java.util.function.Supplier;

import static com.mwallet.mwalletcommons.utils.ObjectUtils.writeJson;

/**
 * @author lawrence
 * created 18/12/2019 at 16:34
 **/
@Slf4j
@PersistenceAdapter
public class TransactionEntityPersistenceAdapter extends GenericPersistenceAdapter<Transaction, TransactionEntity> implements SaveTransactionOutputPort, LoadTransactionOutputPort {

    private final TransactionEntityJpaRepository transactionEntityJpaRepository;

    protected TransactionEntityPersistenceAdapter(ModelMapper modelMapper, TransactionEntityJpaRepository transactionEntityJpaRepository) {
        super(modelMapper, transactionEntityJpaRepository);
        this.transactionEntityJpaRepository = transactionEntityJpaRepository;
    }

    @Override
    public Transaction createTransaction(@Valid Transaction transaction) throws NonUniqueTransactionException {

        boolean exists = existsBy(Finder.by(transaction.getTransactionRef(), Transaction::setTransactionRef));
        if (exists)
            throw new NonUniqueTransactionException("NonUniqueTransaction, TransactionRef: " + transaction.getTransactionRef(), HttpStatus.CONFLICT);

        return create(transaction);
    }

    @Override
    public Transaction updateTransaction(@Valid Transaction transaction) {
        return update(transaction);
    }

    @Override
    public Page<Transaction> loadTransactionsByAccountId(
            @Valid @NotBlank String accountId,
            @Valid @NotNull Integer page,
            @Valid @NotNull Integer size
    ) {
        Pageable sortedByCreatedOnDesc = PageRequest.of(page, size, Sort.by("createdOn").descending());
        Page<TransactionEntity> transactionEntities = transactionEntityJpaRepository.findBySourceAccount_AccountIdOrDestinationAccount_AccountId(accountId, accountId, sortedByCreatedOnDesc);
        log.debug("loadedTransactionEntities: {}", transactionEntities.getContent().size());
        return transactionEntities.map(transactionEntity -> modelMapper.map(transactionEntity, Transaction.class));
    }

    @Override
    public Page<Transaction> loadTransactionsByAccountIdFilterByTime(
            @Valid @NotBlank String accountId,
            @Valid @NotNull LocalDateTime start,
            @Valid @NotNull LocalDateTime end,
            @Valid @NotNull Integer page,
            @Valid @NotNull Integer size) throws AccountNotFoundException {

        Pageable sortedByCreatedOnDesc = PageRequest.of(page, size, Sort.by("createdOn").descending());
        Page<TransactionEntity> transactionEntities = transactionEntityJpaRepository.findBySourceAccount_AccountIdOrDestinationAccount_AccountIdAndCreatedOnBetween(accountId, accountId, start, end, sortedByCreatedOnDesc);
        log.debug("loadedTransactionEntities: {}", transactionEntities.getContent().size());
        return transactionEntities.map(transactionEntity -> modelMapper.map(transactionEntity, Transaction.class));
    }

    @Override
    public Transaction loadTransactionByTransactionRef(@Valid @NotBlank String transactionRef) throws TransactionNotFoundException {
        return findBy(Finder.by(transactionRef, Transaction::setTransactionRef));
    }

    @Override
    public <R extends GenericHttpException> Supplier<R> resourceNotFoundExceptionSupplier() {
        return () -> (R) new TransactionNotFoundException("Transaction Not Found", HttpStatus.NOT_FOUND);
    }
}
