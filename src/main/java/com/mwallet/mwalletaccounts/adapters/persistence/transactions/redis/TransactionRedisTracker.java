package com.mwallet.mwalletaccounts.adapters.persistence.transactions.redis;

import com.mwallet.mwalletcommons.data.ids.IDGenerator;
import com.mwallet.mwalletcommons.utils.DateUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.support.atomic.RedisAtomicLong;
import org.springframework.stereotype.Service;

/**
 * @author lawrence
 * created 19/12/2019 at 14:10
 * <p>
 * This class implements a thread safe & distributed transaction counter
 **/
@Slf4j
@Service
class TransactionRedisTracker {

    @Getter
    private final RedisAtomicLong transactionCounter;
    @Getter
    private final RedisAtomicLong startTransactionCounterTimestamp;
    @Setter
    private Long resetIntervalSeconds;

    private static final String REFERENCE_ZONE_ID = "Africa/Nairobi";

    @Autowired
    TransactionRedisTracker(RedisConnectionFactory redisConnectionFactory) {
        this.resetIntervalSeconds = 1L;
        this.transactionCounter = new RedisAtomicLong("transactionCounter", redisConnectionFactory, 1);
        this.startTransactionCounterTimestamp = new RedisAtomicLong("startTransactionCounterTimestamp", redisConnectionFactory, getTimestampNow());
    }


    /**
     * Compute transaction counter which resets every second
     *
     * @return current epoch second + transaction counter in base36
     */
    public String getAndIncrementTransactionCounter() {

        log.debug("============= getTransactionCount =========");

        long currentTransactionCount = transactionCounter.get();
        long currentStartTimestamp = startTransactionCounterTimestamp.get();
        long timestampNow = getTimestampNow();

        if (isCounterStale(currentStartTimestamp, timestampNow)) {

            boolean counter_reset = transactionCounter.compareAndSet(currentTransactionCount, 1L);
            boolean start_count_timestamp_updated = startTransactionCounterTimestamp.compareAndSet(currentStartTimestamp, timestampNow);

            if (!(counter_reset && start_count_timestamp_updated)) this.getAndIncrementTransactionCounter();
        }

        long counter = transactionCounter.getAndIncrement();
        log.debug("counter: {}", counter);

        return IDGenerator.hashTransactionCounter(counter) + Long.toString(timestampNow, 36);
    }

    /**
     * Check whether the counter is stale by comparing the commence timestamp
     * to the current timestamp.
     *
     * @return true if the difference is bigger than the configured reset interval
     */
    private boolean isCounterStale(long startCounterTimestamp, long timestampNow) {
        long difference = timestampNow - startCounterTimestamp;

        boolean isCounterStale = difference >= resetIntervalSeconds;
        log.debug("difference: {}", difference);
        log.debug("isCounterStale: {}", isCounterStale);
        return isCounterStale;
    }

    /**
     * Get current timestamp
     *
     * @return computed local timestamp
     */
    private long getTimestampNow() {
        return DateUtils.getZonedDateTimeNow(REFERENCE_ZONE_ID)
                .toInstant()
                .getEpochSecond();
    }
}

