package com.mwallet.mwalletaccounts.adapters.persistence.accounts;

import com.mwallet.mwalletcommons.annotations.PersistenceAdapter;
import com.mwallet.mwalletcommons.data.ids.IDGenerator;
import com.mwallet.mwalletaccounts.application.ports.out.accounts.LoadAccountOutputPort;
import com.mwallet.mwalletaccounts.application.ports.out.accounts.SaveAccountOutputPort;
import com.mwallet.mwalletaccounts.domain.Account;
import com.mwallet.mwalletaccounts.domain.Account.AccountType;
import com.mwallet.mwalletcommons.data.services.Finder;
import com.mwallet.mwalletcommons.data.services.GenericPersistenceAdapter;
import com.mwallet.mwalletcommons.exceptions.GenericHttpException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import java.util.function.Supplier;

import static com.mwallet.mwalletcommons.utils.ObjectUtils.writeJson;

/**
 * @author lawrence
 * created 18/12/2019 at 09:24
 **/
@Slf4j
@PersistenceAdapter
public class AccountEntityPersistenceAdapter extends GenericPersistenceAdapter<Account, AccountEntity> implements SaveAccountOutputPort, LoadAccountOutputPort {

    protected AccountEntityPersistenceAdapter(ModelMapper modelMapper, JpaRepository<AccountEntity, Long> jpaRepository) {
        super(modelMapper, jpaRepository);
    }

    @Override
    @Transactional
    public Account createAccount(@Valid Account account) throws NonUniqueAccountException {
        if (existsByLinkIdAndAccountType(account.getLinkId(), account.getAccountType()))
            throw new NonUniqueAccountException("Duplicate account can not be created", HttpStatus.BAD_REQUEST);
        return create(account);
    }

    @Override
    public Account updateAccount(@Valid Account account) throws AccountNotFoundException {
        return update(account);
    }

    @Override
    public Account loadAccountById(@Valid @NotNull String accountId) throws AccountNotFoundException {
        return findBy(Finder.by(accountId, Account::setAccountId));
    }

    @Override
    public boolean existsByLinkIdAndAccountType(String linkId, AccountType accountType) {
        return existsBy(Finder.by(linkId, Account::setLinkId).and(accountType, Account::setAccountType));
    }

    @Override
    public <R extends GenericHttpException> Supplier<R> resourceNotFoundExceptionSupplier() {
        return () -> (R) new AccountNotFoundException("Account Not Found", HttpStatus.NOT_FOUND);
    }
}
