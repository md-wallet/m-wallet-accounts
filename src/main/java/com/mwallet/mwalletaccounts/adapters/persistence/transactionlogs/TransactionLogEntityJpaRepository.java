package com.mwallet.mwalletaccounts.adapters.persistence.transactionlogs;

import com.mwallet.mwalletaccounts.adapters.persistence.transactions.jpa.TransactionEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author lawrence
 * created 22/01/2020 at 16:51
 **/
public interface TransactionLogEntityJpaRepository extends JpaRepository<TransactionLogEntity,Long> {
    List<TransactionLogEntity> findAllByTransaction(TransactionEntity transactionEntity);
}
