package com.mwallet.mwalletaccounts.adapters.persistence.transactions.redis;

import com.mwallet.mwalletcommons.annotations.PersistenceAdapter;
import com.mwallet.mwalletaccounts.application.ports.out.transactions.TransactionCacheManagementOutputPort;
import com.mwallet.mwalletaccounts.domain.Transaction;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import static com.mwallet.mwalletcommons.utils.ObjectUtils.writeJson;
import static com.mwallet.mwalletaccounts.application.ports.out.transactions.LoadTransactionOutputPort.TransactionNotFoundException;

/**
 * @author lawrence
 * created 23/01/2020 at 12:34
 **/
@Slf4j
@PersistenceAdapter
@RequiredArgsConstructor
class TransactionCacheManagementAdapter implements TransactionCacheManagementOutputPort {

    private final TransactionRedisEntityRepository transactionRedisEntityRepository;
    private final ModelMapper modelMapper;

    @Value("60")
    private Long transactionTimeToLive;

    @Override
    public String saveTransaction(Transaction transaction) throws DuplicateTransactionException {
        log.debug("========= SaveTransactionCacheManagement ==========");

        String hash = generateTransactionHash(transaction);
        if (existsByHash(hash))
            throw new DuplicateTransactionException("Duplicate Transaction: " + hash, HttpStatus.CONFLICT);

        TransactionRedisEntity transactionRedisEntity = TransactionRedisEntity.builder()
                .hash(hash)
                .TTL(transactionTimeToLive)
                .transaction(transaction)
                .build();

        transactionRedisEntity = transactionRedisEntityRepository.save(transactionRedisEntity);

        log.debug("transactionRedisEntity: {}", writeJson(transactionRedisEntity));
        return hash;
    }

    @Override
    public boolean existsByHash(String hash) {
        log.debug("========= ExistsByHashTransactionCacheManagement ==========");
        log.debug("hash: {}", hash);
        return transactionRedisEntityRepository.findById(hash).isPresent();
    }

    @Override
    public Transaction getTransactionByHash(String hash) throws TransactionNotFoundException {
        log.debug("========= GetTransactionByHashTransactionCacheManagement ==========");
        log.debug("hash: {}", hash);
        TransactionRedisEntity transactionRedisEntity = transactionRedisEntityRepository.findById(hash)
                .orElseThrow(() -> new TransactionNotFoundException("Transaction Not Found: " + hash, HttpStatus.NOT_FOUND));
        return transactionRedisEntity.getTransaction();
    }

    @Override
    public void deleteByHash(@Valid @NotBlank String hash) {
        transactionRedisEntityRepository.deleteById(hash);
    }

    /**
     * Generate transaction hash from transaction fields that are unique for every transaction in a given window of time:
     * <ul>
     *     <li>sourceAccountId</li>
     *     <li>destinationAccountId</li>
     *     <li>amount</li>
     *     <li>transactionCost</li>
     *     <li>transactionType</li>
     *     <li>paymentMode</li>
     *     <li>confirmationType</li>
     * </ul>
     * This method uses the SHA256 hashing algorithm and returns the hash as hexadecimal encoded
     *
     * @param transaction transaction to generate hash for
     * @return generated sha256 hash
     */
    @SuppressWarnings("StringBufferReplaceableByString")
    private String generateTransactionHash(Transaction transaction) {
        String separator = "_";
        String toHash = new StringBuilder()
                .append(transaction.getSourceAccount().getAccountId())
                .append(separator)
                .append(transaction.getDestinationAccount().getAccountId())
                .append(separator)
                .append(transaction.getAmount())
                .append(separator)
                .append(transaction.getTransactionCost())
                .append(separator)
                .append(transaction.getTransactionType())
                .append(separator)
                .append(transaction.getPaymentMode())
                .append(separator)
                .append(transaction.getConfirmationType())
                .append(separator)
                .append(transaction.getBillingAddress())
                .append(separator)
                .append(transaction.getTransactionDomainType())
                .append(separator)
                .append(transaction.getTransactionDescription())
                .toString();
        return DigestUtils.sha256Hex(toHash);
    }
}
