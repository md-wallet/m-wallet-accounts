package com.mwallet.mwalletaccounts.adapters.persistence.transactions.redis;

import com.mwallet.mwalletaccounts.application.ports.out.transactions.GenerateTransactionRefOutputPort;
import com.mwallet.mwalletaccounts.domain.Transaction;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author lawrence
 * created 19/12/2019 at 15:57
 **/
@Slf4j
@Service
@RequiredArgsConstructor
class TransactionRefGeneratorAdapter implements GenerateTransactionRefOutputPort {
    private final TransactionRedisTracker transactionRedisTracker;

    @Override
    public String generateTransactionRef(Transaction transaction) {
        log.debug("=======GenerateTransactionRef===========");
        String transRef =
                generateTransactionRefHeader(transaction.getTransactionType()) + generateTransactionBody();

        transRef = transRef.toUpperCase();
        log.debug("transactionRef: {}", transRef);
        return transRef;
    }

    /**
     * Generate the first portion of the transactionRef which is MWallet
     * prefix M and the transactionType code
     *
     * @param transactionType transaction type
     * @return transactionId header
     */
    private String generateTransactionRefHeader(Transaction.TransactionType transactionType) {
        StringBuilder header = new StringBuilder();
        switch (transactionType) {
            case DEPOSIT:
                header.append("DP");
                break;
            case WITHDRAW:
                header.append("WD");
                break;
            case SEND_MONEY:
                header.append("SM");
                break;
            default:
                throw new UnsupportedOperationException("unsupported transactionType:" + transactionType);
        }
        return header.toString();
    }


    /**
     * Generate transactionRef footer which is current daily transaction counter
     *
     * @return transactionRef footer
     */
    private String generateTransactionBody() {
         return transactionRedisTracker.getAndIncrementTransactionCounter();
    }
}
