package com.mwallet.mwalletaccounts.adapters.persistence.transactions.jpa;

import com.mwallet.mwalletaccounts.adapters.persistence.accounts.AccountEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.Optional;

/**
 * @author lawrence
 * created 18/12/2019 at 16:31
 **/
public interface TransactionEntityJpaRepository extends JpaRepository<TransactionEntity,Long> {
    boolean existsByTransactionRef(String transactionRef);
    Optional<TransactionEntity> findByTransactionRef(String transactionRef);
    Page<TransactionEntity> findBySourceAccount_AccountIdOrDestinationAccount_AccountIdAndCreatedOnBetween(String sourceAccountId, String destinationAccountId, LocalDateTime start, LocalDateTime end, Pageable pageable);
    Page<TransactionEntity> findBySourceAccount_AccountIdOrDestinationAccount_AccountId(String sourceAccountId, String destinationAccountId,Pageable pageable);
    void deleteByTransactionRef(String transactionRef);
}
