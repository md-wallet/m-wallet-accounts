package com.mwallet.mwalletaccounts.adapters.persistence.accounts;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

import static com.mwallet.mwalletaccounts.domain.Account.*;

/**
 * @author lawrence
 * created 18/12/2019 at 09:25
 **/
public interface AccountEntityJpaRepository extends JpaRepository<AccountEntity,Long> {
    boolean existsByLinkIdAndAccountType(String linkId, AccountType accountType);
    Optional<AccountEntity> findByAccountId(String accountId);
}
