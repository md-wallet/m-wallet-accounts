package com.mwallet.mwalletaccounts.adapters.persistence.transactions.redis;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;



/**
 * @author lawrence
 * created 23/01/2020 at 12:58
 **/
@Repository
public interface TransactionRedisEntityRepository extends CrudRepository<TransactionRedisEntity,String> {

}
