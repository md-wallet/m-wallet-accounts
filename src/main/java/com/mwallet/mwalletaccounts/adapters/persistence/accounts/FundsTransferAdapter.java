package com.mwallet.mwalletaccounts.adapters.persistence.accounts;

import com.mwallet.mwalletcommons.annotations.PersistenceAdapter;
import com.mwallet.mwalletaccounts.application.ports.out.accounts.TransferFundsOutputPort;
import com.mwallet.mwalletaccounts.domain.Account;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.util.Pair;
import org.springframework.http.HttpStatus;

import java.math.BigDecimal;

import static com.mwallet.mwalletaccounts.application.ports.out.accounts.LoadAccountOutputPort.AccountNotFoundException;

/**
 * @author lawrence
 * created 19/01/2020 at 01:35
 **/

@Slf4j
@PersistenceAdapter
@RequiredArgsConstructor
class FundsTransferAdapter implements TransferFundsOutputPort {

    private final AccountEntityJpaRepository accountEntityJpaRepository;
    private final ModelMapper modelMapper;

    @Override
    public Pair<Account, Account> prepareTransfer(String srcAccountId, String destAccountId, BigDecimal amount, BigDecimal transactionCost)
            throws AccountNotFoundException {

        log.info("*************** FundsTransfer -----> prepareTransfer ***********");

        //1. setup
        Pair<AccountEntity, AccountEntity> accounts = loadAccounts(srcAccountId, destAccountId);
        AccountEntity sourceAccountEntity = accounts.getFirst();
        AccountEntity destinationAccountEntity = accounts.getSecond();
        BigDecimal totalAmount = amount.add(transactionCost);

        log.info("amount: {}, txnCost: {}", amount, transactionCost);

        //logic
        BigDecimal sourceAccountAvailableBalance = sourceAccountEntity.getAvailableBalance().subtract(totalAmount);
        BigDecimal destinationAccountActualBalance = destinationAccountEntity.getActualBalance().add(amount);

        sourceAccountEntity.setAvailableBalance(sourceAccountAvailableBalance);
        destinationAccountEntity.setActualBalance(destinationAccountActualBalance);


        //finalize
        return saveAccounts(sourceAccountEntity, destinationAccountEntity);
    }

    @Override
    public Pair<Account, Account> confirmTransfer(String srcAccountId, String destAccountId, BigDecimal amount, BigDecimal transactionCost)
            throws AccountNotFoundException {

        log.info("*************** FundsTransfer -----> confirmTransfer ***********");

        //setup
        Pair<AccountEntity, AccountEntity> accounts = loadAccounts(srcAccountId, destAccountId);
        AccountEntity sourceAccountEntity = accounts.getFirst();
        AccountEntity destinationAccountEntity = accounts.getSecond();
        BigDecimal totalAmount = amount.add(transactionCost);

        log.info("amount: {}, txnCost: {}", amount, transactionCost);

        //logic
        BigDecimal sourceAccountActualBalance = sourceAccountEntity.getActualBalance().subtract(totalAmount);
        BigDecimal destinationAccountAvailableBalance = destinationAccountEntity.getAvailableBalance().add(amount);

        sourceAccountEntity.setActualBalance(sourceAccountActualBalance);
        destinationAccountEntity.setAvailableBalance(destinationAccountAvailableBalance);

        //finalize
        return saveAccounts(sourceAccountEntity, destinationAccountEntity);
    }

    @Override
    public Pair<Account, Account> rollbackTransfer(String srcAccountId, String destAccountId, BigDecimal amount, BigDecimal transactionCost)
            throws AccountNotFoundException {

        log.info("*************** FundsTransfer -----> rollbackTransfer ***********");

        //setup
        Pair<AccountEntity, AccountEntity> accounts = loadAccounts(srcAccountId, destAccountId);
        AccountEntity sourceAccountEntity = accounts.getFirst();
        AccountEntity destinationAccountEntity = accounts.getSecond();
        BigDecimal totalAmount = amount.add(transactionCost);

        log.info("amount: {}, txnCost: {}", amount, transactionCost);

        //logic
        BigDecimal sourceAccountAvailableBalance = sourceAccountEntity.getAvailableBalance().add(totalAmount);
        BigDecimal destinationAccountActualBalance = destinationAccountEntity.getActualBalance().subtract(amount);

        sourceAccountEntity.setAvailableBalance(sourceAccountAvailableBalance);
        destinationAccountEntity.setActualBalance(destinationAccountActualBalance);

        //finalize
        return saveAccounts(sourceAccountEntity, destinationAccountEntity);
    }

    /**
     * Fetch latest version of source and destination accounts
     *
     * @param srcAccountId  source account Id
     * @param destAccountId destination account Id
     * @return pair of source and destination accounts
     */
    private Pair<AccountEntity, AccountEntity> loadAccounts(String srcAccountId, String destAccountId) {
        AccountEntity sourceAccountEntity = accountEntityJpaRepository
                .findByAccountId(srcAccountId)
                .orElseThrow(() -> new AccountNotFoundException("Account Not Found", HttpStatus.BAD_REQUEST));
        AccountEntity destinationAccountEntity = accountEntityJpaRepository.
                findByAccountId(destAccountId)
                .orElseThrow(() -> new AccountNotFoundException("Account Not Found", HttpStatus.BAD_REQUEST));

        log.info("src_before: [avail: {},actual: {}]",sourceAccountEntity.getAvailableBalance(),sourceAccountEntity.getActualBalance());
        log.info("dest_before: [avail: {},actual: {}]",destinationAccountEntity.getAvailableBalance(),destinationAccountEntity.getActualBalance());

        return Pair.of(sourceAccountEntity, destinationAccountEntity);
    }

    /**
     * Save source and destination accounts
     *
     * @param sourceAccountEntity      source account
     * @param destinationAccountEntity destination account
     * @return pair of source and destination accounts
     */
    private Pair<Account, Account> saveAccounts(AccountEntity sourceAccountEntity, AccountEntity destinationAccountEntity) {
        AccountEntity updatedSourceAccountEntity = accountEntityJpaRepository.save(sourceAccountEntity);
        AccountEntity updatedDestinationAccountEntity = accountEntityJpaRepository.save(destinationAccountEntity);

        Account mappedSourceAccount = modelMapper.map(updatedSourceAccountEntity, Account.class);
        Account mappedDestinationAccount = modelMapper.map(updatedDestinationAccountEntity, Account.class);

        log.info("src_after: [avail: {},actual: {}]",updatedSourceAccountEntity.getAvailableBalance(),updatedSourceAccountEntity.getActualBalance());
        log.info("dest_after: [avail: {},actual: {}]",updatedDestinationAccountEntity.getAvailableBalance(),updatedDestinationAccountEntity.getActualBalance());

        return Pair.of(mappedSourceAccount, mappedDestinationAccount);
    }
}
