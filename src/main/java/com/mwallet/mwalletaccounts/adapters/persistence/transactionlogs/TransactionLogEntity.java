package com.mwallet.mwalletaccounts.adapters.persistence.transactionlogs;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mwallet.mwalletcommons.data.models.BaseEntity;
import com.mwallet.mwalletaccounts.adapters.persistence.transactions.jpa.TransactionEntity;
import com.mwallet.mwalletaccounts.domain.Transaction;
import com.mwallet.mwalletaccounts.domain.Transaction.TransactionStatus;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author lawrence
 * created 22/01/2020 at 13:56
 **/
@Entity
@Table(name = "transaction_logs")
@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
class TransactionLogEntity extends BaseEntity {

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "transaction_state")
    private TransactionStatus transactionStatus;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "confirmation_status")
    private Transaction.ConfirmationStatus confirmationStatus;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "transaction_id",updatable = false)
    private TransactionEntity transaction;

    @NotNull
    @Column(name = "src_available_balance")
    private BigDecimal srcAvailableBalance;

    @NotNull
    @Column(name = "src_actual_balance")
    private BigDecimal srcActualBalance;

    @NotNull
    @Column(name = "dst_available_balance")
    private BigDecimal destAvailableBalance;

    @NotNull
    @Column(name = "dst_actual_balance")
    private BigDecimal destActualBalance;

    @NotNull
    @Column(name = "message")
    private String message;
}
