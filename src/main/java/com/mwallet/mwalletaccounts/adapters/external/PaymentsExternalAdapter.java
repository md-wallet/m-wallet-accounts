package com.mwallet.mwalletaccounts.adapters.external;

import com.mwallet.mwalletaccounts.application.ports.out.payments.PaymentsOutputPort;
import com.mwallet.mwalletaccounts.domain.Payment;
import com.mwallet.mwalletaccounts.domain.Payout;
import com.mwallet.mwalletaccounts.domain.Transaction;
import com.mwallet.mwalletaccounts.infrastructure.feign.PaymentsFeignClient;
import com.mwallet.mwalletaccounts.infrastructure.feign.PaymentsFeignClient.Models.PaymentRequestDTO;
import com.mwallet.mwalletaccounts.infrastructure.feign.PaymentsFeignClient.Models.PayoutRequestDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;

import static com.mwallet.mwalletcommons.utils.ObjectUtils.writeJson;


/**
 * @author lawrence
 * created 02/02/2020 at 10:43
 **/
@Slf4j
@Service
@Validated
@RequiredArgsConstructor
class PaymentsExternalAdapter implements PaymentsOutputPort {

    private final PaymentsFeignClient paymentsFeignClient;
    private final ModelMapper modelMapper;

    @Override
    public Payment.PaymentRequest requestPayment(@Valid Transaction transaction) {
        log.debug("==========PaymentsExternalAdapter============");
        Payment.PaymentRequest paymentRequest = Payment.PaymentRequest.builder()
                .transactionRef(transaction.getTransactionRef())
                .billingAddress(transaction.getBillingAddress())
                .paymentMode(transaction.getPaymentMode())
                .amount(transaction.getAmount())
                .paymentRequestStatus(Payment.PaymentRequest.PaymentRequestStatus.NONE)
                .transactionDescription(transaction.getTransactionDescription())
                .build();

        PaymentRequestDTO paymentRequestDTO = modelMapper.map(paymentRequest, PaymentRequestDTO.class);
        log.debug("paymentRequestDTO: {}", writeJson(paymentRequestDTO));

        Payment.PaymentRequest paymentResult = paymentsFeignClient.requestPayment(paymentRequestDTO);
        log.debug("paymentResult: {}", writeJson(paymentResult));

        return paymentResult;
    }

    @Override
    public Payout.PayoutRequest requestPayout(@Valid Transaction transaction) {
        log.debug("==========PaymentsExternalAdapter============");

        Payout.PayoutRequest payoutRequest = Payout.PayoutRequest.builder()
                .transactionRef(transaction.getTransactionRef())
                .billingAddress(transaction.getBillingAddress())
                .paymentMode(transaction.getPaymentMode())
                .amount(transaction.getAmount())
                .payoutRequestStatus(Payout.PayoutRequest.PayoutRequestStatus.NONE)
                .transactionDescription(transaction.getTransactionDescription())
                .build();

        PayoutRequestDTO payoutRequestDTO = modelMapper.map(payoutRequest, PayoutRequestDTO.class);
        log.debug("payoutRequestDTO: {}", writeJson(payoutRequestDTO));

        Payout.PayoutRequest payoutResult = paymentsFeignClient.requestPayout(payoutRequestDTO);
        log.debug("payoutResult: {}", writeJson(payoutResult));

        return payoutResult;
    }
}
