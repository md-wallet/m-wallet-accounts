package com.mwallet.mwalletaccounts.adapters.external;

import com.mwallet.mwalletaccounts.application.ports.out.payments.LoadPaymentCardOutputPort;
import com.mwallet.mwalletaccounts.domain.PaymentCard;
import com.mwallet.mwalletaccounts.infrastructure.feign.PaymentCardsFeignClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotBlank;
import java.util.Optional;

/**
 * @author lawrencemwaniki
 * created 18/05/2020 at 14:56
 **/

@Slf4j
@Service
@RequiredArgsConstructor
class PaymentCardsExternalAdapter implements LoadPaymentCardOutputPort {

    private final PaymentCardsFeignClient paymentCardsFeignClient;

    @Override
    public Optional<PaymentCard> getPaymentCard(@NotBlank String token) throws CouldNotLoadPaymentCardException {
        PaymentCard paymentCard = paymentCardsFeignClient.getCard(token);
        return Optional.ofNullable(paymentCard);
    }
}
