package com.mwallet.mwalletaccounts.adapters.web.dtos.transactions;

import com.mwallet.mwalletaccounts.domain.Transaction;
import com.mwallet.mwalletaccounts.domain.Transaction.PaymentMode;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import static com.mwallet.mwalletaccounts.domain.Payment.PaymentResponse;

/**
 * @author lawrence
 * created 06/02/2020 at 19:34
 **/
public class DepositMoneyDTOs {
    @Getter
    @Setter
    @SuperBuilder(toBuilder = true)
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ValidationRequestDTO {

        @ApiModelProperty(
                required = true,
                value = "destination accountId",
                example = "KFDK12")
        @NotBlank
        private String destinationAccountId;

        @ApiModelProperty(
                required = true,
                value = "amount to send",
                example = "1000")
        @NotNull
        @DecimalMin("0.00")
        private BigDecimal amount;


        @ApiModelProperty(
                required = true,
                value = "billing Address",
                example = "254712345678")
        @NotBlank
        private String billingAddress;

        @ApiModelProperty(
                required = true,
                value = "transaction Description",
                example = "Wallet deposit from 254712345678")
        @NotBlank
        private String transactionDescription;

        @ApiModelProperty(
                required = true,
                value = "payment mode",
                example = "MPESA")
        @NotNull
        private PaymentMode paymentMode;

        @ApiModelProperty(
                required = true,
                value = "domain type",
                example = "FUNDRAISER_DONATION")
        @NotBlank
        private String transactionDomainType;
    }


    @Getter
    @Setter
    @SuperBuilder(toBuilder = true)
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ValidationResponseDTO {
        @ApiModelProperty(
                required = true,
                value = "validated transaction hash",
                example = "98332dcf4d9d7c964e2d2438d183ca120fe44632beaf5fe2a5bacf5163ccae18")
        @NotBlank
        private String transactionHash;

        @ApiModelProperty(
                required = true,
                value = "transaction status",
                example = "VALIDATED")
        @NotNull
        private Transaction.TransactionStatus transactionStatus;
    }

    @Getter
    @Setter
    @SuperBuilder(toBuilder = true)
    @NoArgsConstructor
    @AllArgsConstructor
    public static class DepositMoneyResponseDTO {
        @ApiModelProperty(
                required = true,
                value = "transaction ref",
                example = "JFKNJNGFJ33HGH")
        @NotBlank
        private String transactionRef;

        @ApiModelProperty(
                required = true,
                value = "transaction status",
                example = "ACCEPTED")
        @NotNull
        private Transaction.TransactionStatus transactionStatus;
    }

    @Getter
    @Setter
    @SuperBuilder(toBuilder = true)
    @NoArgsConstructor
    @AllArgsConstructor
    public static class DepositMoneyPaymentVerificationDTO {
        @ApiModelProperty(
                required = true,
                value = "transaction ref",
                example = "JFKNJNGFJ33HGH")
        @NotBlank
        private String transactionRef;

        @ApiModelProperty(
                required = true,
                value = "created On",
                example = "2019-02-01T08:00:00")
        @NotNull
        @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
        private LocalDateTime timestamp;

        @ApiModelProperty(
                required = true,
                value = "payment status",
                example = "PAYMENT_RESPONSE_VERIFIED")
        @NotNull
        private PaymentResponse.PaymentResponseStatus paymentResponseStatus;

        @ApiModelProperty(
                required = true,
                value = "amount",
                example = "0.00")
        @NotNull
        @DecimalMin(value = "0.0")
        private BigDecimal amount;

        @ApiModelProperty(
                required = true,
                value = "paymentMode",
                example = "MPESA")
        @NotNull
        private PaymentMode paymentMode;

        @NotBlank
        @ApiModelProperty(
                required = true,
                value = "billingAddress",
                example = "254712345678")
        private String billingAddress;

        @ApiModelProperty(
                required = true,
                value = "narration",
                example = "TEST_NARRATION")
        @NotBlank
        @Builder.Default
        private String narration = "NONE";

    }
}
