package com.mwallet.mwalletaccounts.adapters.web.controllers;

import com.mwallet.mwalletaccounts.adapters.web.dtos.accounts.AccountInfoDTO;
import com.mwallet.mwalletaccounts.adapters.web.dtos.accounts.AccountStatusUpdateDTO;
import com.mwallet.mwalletaccounts.adapters.web.dtos.accounts.CreateAccountDTO;
import com.mwallet.mwalletaccounts.application.ports.in.accounts.CreateAccountUseCase;
import com.mwallet.mwalletaccounts.application.ports.in.accounts.GetAccountUseCase;
import com.mwallet.mwalletaccounts.application.ports.in.accounts.UpdateAccountStatusUseCase;
import com.mwallet.mwalletaccounts.domain.Account;
import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import static com.mwallet.mwalletcommons.utils.ObjectUtils.writeJson;


/**
 * @author lawrence
 * created 14/01/2020 at 21:43
 **/
@Slf4j
@RestController
@RequestMapping("/v1/management")
@RequiredArgsConstructor
class AccountManagementController {

    private final CreateAccountUseCase createAccountUseCase;
    private final GetAccountUseCase getAccountUseCase;
    private final UpdateAccountStatusUseCase updateAccountStatusUseCase;
    private final ModelMapper modelMapper;

    @PostMapping("/")
    @PreAuthorize("hasAuthority('SCOPE_m-wallet-accounts:write')")
    @ResponseStatus(HttpStatus.CREATED)
    public AccountInfoDTO createAccount(
            @ApiParam(value = "Create Account")
            @RequestBody @Valid CreateAccountDTO createAccountDTO
            ){
        log.info("============= CreateAccountWebAdapterLayer: {} =============",writeJson(createAccountDTO));

        Account account = Account.builder()
                .accountType(createAccountDTO.getAccountType().getAccountType())
                .linkId(createAccountDTO.getLinkId())
                .build();

        Account createdAccount = createAccountUseCase.createAccount(account);
        AccountInfoDTO accountInfoDTO = modelMapper.map(createdAccount,AccountInfoDTO.class);
        log.debug("accountInfoDTO: {}",writeJson(accountInfoDTO));
        return accountInfoDTO;
    }

    @GetMapping("/{accountId}")
    @PreAuthorize("hasAuthority('SCOPE_m-wallet-accounts:read')")
    public AccountInfoDTO getAccountInfo(
            @ApiParam(value = "accountId")
            @PathVariable @Valid @NotBlank String accountId){
        log.info("============= GetAccountInfoWebAdapterLayer: {} =============",accountId);

        Account foundAccount = getAccountUseCase.getAccountById(accountId);
        AccountInfoDTO accountInfoDTO = modelMapper.map(foundAccount,AccountInfoDTO.class);

        log.debug("accountInfoDTO: {}",writeJson(accountInfoDTO));
        return accountInfoDTO;
    }


    @PutMapping("/updateStatus")
    @PreAuthorize("hasAuthority('SCOPE_m-wallet-accounts:read') and hasAuthority('SCOPE_m-wallet-accounts:write')")
    public AccountInfoDTO updateAccountStatus(
            @ApiParam("Update Account status")
            @RequestBody @Valid @NotNull AccountStatusUpdateDTO accountStatusUpdateDTO){
        log.info("============= UpdateAccountStatusWebAdapterLayer =============");
        log.debug("updateAccountStatusDTO: {}",writeJson(accountStatusUpdateDTO));

        Account updatedAccount = updateAccountStatusUseCase.updateAccountStatus(accountStatusUpdateDTO.getAccountId(), accountStatusUpdateDTO.getAccountStatus());
        AccountInfoDTO accountInfoDTO = modelMapper.map(updatedAccount,AccountInfoDTO.class);

        log.debug("accountInfoDTO: {}",writeJson(accountInfoDTO));
        return accountInfoDTO;
    }


}
