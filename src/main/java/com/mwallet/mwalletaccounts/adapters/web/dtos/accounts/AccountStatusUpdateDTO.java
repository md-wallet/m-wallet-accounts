package com.mwallet.mwalletaccounts.adapters.web.dtos.accounts;

import com.mwallet.mwalletaccounts.domain.Account.AccountStatus;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotNull;

/**
 * @author lawrence
 * created 16/01/2020 at 11:18
 **/
@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class AccountStatusUpdateDTO {

    @ApiModelProperty(
            required = true,
            value = "accountId",
            example = "ABHBDH")
    @NotNull
    private String accountId;

    @ApiModelProperty(
            required = true,
            value = "account status",
            example = "ACTIVE")
    @NotNull
    private AccountStatus accountStatus;
}
