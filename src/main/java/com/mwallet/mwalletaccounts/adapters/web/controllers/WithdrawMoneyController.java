package com.mwallet.mwalletaccounts.adapters.web.controllers;

import com.mwallet.mwalletaccounts.adapters.web.dtos.transactions.WithdrawMoneyDTOs;
import com.mwallet.mwalletaccounts.application.ports.in.accounts.GetAccountUseCase;
import com.mwallet.mwalletaccounts.application.ports.in.transactions.WithdrawMoneyUseCase;
import com.mwallet.mwalletaccounts.domain.Account;
import com.mwallet.mwalletaccounts.domain.Transaction;
import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.util.Pair;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import static com.mwallet.mwalletcommons.utils.ObjectUtils.writeJson;
import static com.mwallet.mwalletaccounts.domain.Account.AccountType.TRUST_ACCOUNT;

/**
 * @author lawrence
 * created 12/02/2020 at 11:30
 **/

@Slf4j
@RestController
@RequestMapping("/v1/withdrawmoney")
@RequiredArgsConstructor
class WithdrawMoneyController {
    private final WithdrawMoneyUseCase withdrawMoneyUseCase;
    private final GetAccountUseCase getAccountUseCase;
    private final ModelMapper modelMapper;


    @PostMapping("/validate")
    @PreAuthorize("hasAuthority('SCOPE_m-wallet-accounts:write')")
    public WithdrawMoneyDTOs.ValidationResponseDTO validateWithdrawMoney(
            @ApiParam(value = "Withdraw Money Transaction validation info")
            @RequestBody @Valid WithdrawMoneyDTOs.ValidationRequestDTO validationRequestDTO
    ) {
        log.info("======= ValidateWithdrawMoneyWebAdapterLayer ============");
        log.debug("validationRequestDTO: {}", writeJson(validationRequestDTO));

        //1. fetch accounts
        Account sourceAccount = getAccountUseCase.getAccountById(validationRequestDTO.getSourceAccountId());
        Account destinationAccount = getAccountUseCase.getAccountById(TRUST_ACCOUNT.name());

        //2. build withdraw transaction
        Transaction transaction = Transaction.builder()
                .amount(validationRequestDTO.getAmount())
                .transactionCost(validationRequestDTO.getTransactionCost())
                .billingAddress(validationRequestDTO.getBillingAddress())
                .paymentMode(validationRequestDTO.getPaymentMode())
                .transactionDescription(validationRequestDTO.getTransactionDescription())
                .transactionDomainType(validationRequestDTO.getTransactionDomainType())
                .confirmationType(Transaction.ConfirmationType.INTERNAL)
                .sourceAccount(sourceAccount)
                .destinationAccount(destinationAccount)
                .transactionType(Transaction.TransactionType.WITHDRAW)
                .confirmationStatus(Transaction.ConfirmationStatus.NONE)
                .build();

        //3. validate transaction
        Pair<String, Transaction> validationResult = withdrawMoneyUseCase.validateWithdrawMoney(transaction);
        WithdrawMoneyDTOs.ValidationResponseDTO validationResponseDTO = WithdrawMoneyDTOs.ValidationResponseDTO.builder()
                .transactionHash(validationResult.getFirst())
                .transactionStatus(validationResult.getSecond().getTransactionStatus())
                .build();

        log.debug("validationResponseDTO: {}", writeJson(validationResponseDTO));
        return validationResponseDTO;
    }

    @PostMapping("/request/{transactionHash}")
    @PreAuthorize("hasAuthority('SCOPE_m-wallet-accounts:write')")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public WithdrawMoneyDTOs.WithdrawMoneyResponseDTO requestWithdrawMoney(
            @ApiParam(value = "Validated Transaction Hash")
            @PathVariable @Valid @NotBlank String transactionHash
    ) {
        log.info("======= RequestWithdrawMoneyWebAdapterLayer ============");
        log.debug("transactionHash: {}", transactionHash);

        Transaction acceptedTransaction = withdrawMoneyUseCase.requestWithdrawMoney(transactionHash);
        WithdrawMoneyDTOs.WithdrawMoneyResponseDTO withdrawMoneyResponseDTO = modelMapper.map(acceptedTransaction, WithdrawMoneyDTOs.WithdrawMoneyResponseDTO.class);

        log.debug("withdrawMoneyResponseDTO: {}", writeJson(withdrawMoneyResponseDTO));
        return withdrawMoneyResponseDTO;
    }
}
