package com.mwallet.mwalletaccounts.adapters.web.dtos.transactions;

import com.mwallet.mwalletaccounts.domain.Transaction.TransactionStatus;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

import static com.mwallet.mwalletaccounts.domain.Transaction.ConfirmationStatus;
import static com.mwallet.mwalletaccounts.domain.Transaction.ConfirmationType;

/**
 * @author lawrence
 * created 23/01/2020 at 08:54
 **/

public class SendMoneyDTOs {

    @Getter
    @Setter
    @SuperBuilder(toBuilder = true)
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ValidationRequestDTO {
        @ApiModelProperty(
                required = true,
                value = "source accountId",
                example = "QRBC12")
        @NotBlank
        private String sourceAccountId;

        @ApiModelProperty(
                required = true,
                value = "destination accountId",
                example = "KFDK12")
        @NotBlank
        private String destinationAccountId;

        @ApiModelProperty(
                required = true,
                value = "amount to send",
                example = "1000")
        @NotNull
        @DecimalMin("0.00")
        private BigDecimal amount;

        @ApiModelProperty(
                required = true,
                value = "transaction cost",
                example = "1000")
        @NotNull
        @DecimalMin("0.00")
        private BigDecimal transactionCost;

        @ApiModelProperty(
                required = true,
                value = "transaction confirmation type",
                example = "INTERNAL")
        @NotNull
        private ConfirmationType confirmationType;

        @ApiModelProperty(
                required = true,
                value = "transaction Description",
                example = "SendMoney to 254712345678")

        @NotBlank
        private String transactionDescription;

        @ApiModelProperty(
                required = true,
                value = "domain type",
                example = "FUNDRAISER_DONATION")
        @NotBlank
        private String transactionDomainType;
    }

    @Getter
    @Setter
    @SuperBuilder(toBuilder = true)
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ValidationResponseDTO {
        @ApiModelProperty(
                required = true,
                value = "validated transaction hash",
                example = "98332dcf4d9d7c964e2d2438d183ca120fe44632beaf5fe2a5bacf5163ccae18")
        @NotBlank
        private String transactionHash;

        @ApiModelProperty(
                required = true,
                value = "transaction status",
                example = "VALIDATED")
        @NotNull
        private TransactionStatus transactionStatus;
    }

    @Getter
    @Setter
    @SuperBuilder(toBuilder = true)
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SendMoneyResponseDTO {
        @ApiModelProperty(
                required = true,
                value = "transaction ref",
                example = "JFKNJNGFJ33HGH")
        @NotBlank
        private String transactionRef;

        @ApiModelProperty(
                required = true,
                value = "transaction status",
                example = "ACCEPTED")
        @NotNull
        private TransactionStatus transactionStatus;
    }

    @Getter
    @Setter
    @SuperBuilder(toBuilder = true)
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ConfirmationRequestDTO {
        @ApiModelProperty(
                required = true,
                value = "transaction ref",
                example = "JFKNJNGFJ33HGH")
        @NotBlank
        private String transactionRef;


        @ApiModelProperty(
                required = true,
                value = "confirmation status",
                example = "PROCEED")
        @NotNull
        private ConfirmationStatus confirmationStatus;


        @ApiModelProperty(
                required = true,
                value = "narration",
                example = "P2P Loan Offer Rejected")
        @NotBlank
        private String narration;
    }

}
