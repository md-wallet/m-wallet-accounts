package com.mwallet.mwalletaccounts.adapters.web.controllers;

import com.mwallet.mwalletaccounts.adapters.web.dtos.transactions.SendMoneyDTOs.ConfirmationRequestDTO;
import com.mwallet.mwalletaccounts.adapters.web.dtos.transactions.SendMoneyDTOs.SendMoneyResponseDTO;
import com.mwallet.mwalletaccounts.application.ports.in.accounts.GetAccountUseCase;
import com.mwallet.mwalletaccounts.application.ports.in.transactions.SendMoneyUseCase;
import com.mwallet.mwalletaccounts.domain.Account;
import com.mwallet.mwalletaccounts.domain.Transaction;
import com.mwallet.mwalletaccounts.domain.Transaction.ConfirmationStatus;
import com.mwallet.mwalletaccounts.domain.Transaction.TransactionStatus;
import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.util.Pair;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import static com.mwallet.mwalletcommons.utils.ObjectUtils.writeJson;
import static com.mwallet.mwalletaccounts.adapters.web.dtos.transactions.SendMoneyDTOs.ValidationRequestDTO;
import static com.mwallet.mwalletaccounts.adapters.web.dtos.transactions.SendMoneyDTOs.ValidationResponseDTO;
import static com.mwallet.mwalletaccounts.domain.Transaction.PaymentMode.WALLET;
import static com.mwallet.mwalletaccounts.domain.Transaction.TransactionType.SEND_MONEY;
import static com.mwallet.mwalletaccounts.domain.Transaction.builder;

/**
 * @author lawrence
 * created 23/01/2020 at 08:48
 **/

@Slf4j
@RestController
@RequestMapping("/v1/sendmoney")
@RequiredArgsConstructor
class SendMoneyController {

    private final GetAccountUseCase getAccountUseCase;
    private final SendMoneyUseCase sendMoneyUseCase;
    private final ModelMapper modelMapper;

    @PostMapping("/validate")
    @PreAuthorize("hasAuthority('SCOPE_m-wallet-accounts:write')")
    public ValidationResponseDTO validateSendMoney(
            @ApiParam(value = "Transaction validation info")
            @RequestBody @Valid ValidationRequestDTO sendMoneyValidationValidationRequestDTO
    ) {
        log.debug("sendMoneyValidationRequestDTO: {}", writeJson(sendMoneyValidationValidationRequestDTO));

        //fetch accounts
        Account sourceAccount = getAccountUseCase.getAccountById(sendMoneyValidationValidationRequestDTO.getSourceAccountId());
        Account destinationAccount = getAccountUseCase.getAccountById(sendMoneyValidationValidationRequestDTO.getDestinationAccountId());

        //build transaction
        Transaction transaction = builder()
                .amount(sendMoneyValidationValidationRequestDTO.getAmount())
                .transactionCost(sendMoneyValidationValidationRequestDTO.getTransactionCost())
                .confirmationType(sendMoneyValidationValidationRequestDTO.getConfirmationType())
                .transactionDescription(sendMoneyValidationValidationRequestDTO.getTransactionDescription())
                .transactionDomainType(sendMoneyValidationValidationRequestDTO.getTransactionDomainType())
                .sourceAccount(sourceAccount)
                .destinationAccount(destinationAccount)
                .paymentMode(WALLET)
                .transactionType(SEND_MONEY)
                .confirmationStatus(ConfirmationStatus.NONE)
                .transactionStatus(TransactionStatus.NONE)
                .billingAddress(sourceAccount.getAccountId())
                .build();

        //validate
        Pair<String, Transaction> validationResult = sendMoneyUseCase.validateSendMoney(transaction);
        ValidationResponseDTO sendMoneyValidationValidationResponseDTO = ValidationResponseDTO.builder()
                .transactionHash(validationResult.getFirst())
                .transactionStatus(validationResult.getSecond().getTransactionStatus())
                .build();

        log.debug("sendMoneyValidationResponseDTO: {}", writeJson(sendMoneyValidationValidationResponseDTO));
        return sendMoneyValidationValidationResponseDTO;
    }


    @PostMapping("/request/{transactionHash}")
    @PreAuthorize("hasAuthority('SCOPE_m-wallet-accounts:write')")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public SendMoneyResponseDTO requestSendMoney(
            @ApiParam(value = "Validated Transaction Hash")
            @PathVariable @Valid @NotBlank String transactionHash
    ) {
        log.debug("transactionHash: {}", transactionHash);

        //request
        Transaction acceptedTransaction = sendMoneyUseCase.requestSendMoney(transactionHash);
        SendMoneyResponseDTO sendMoneyResponseDTO = modelMapper.map(acceptedTransaction, SendMoneyResponseDTO.class);

        log.debug("sendMoneyResponseDTO: {}", writeJson(sendMoneyResponseDTO));
        return sendMoneyResponseDTO;
    }

    @PostMapping("/confirm")
    @PreAuthorize("hasAuthority('SCOPE_m-wallet-accounts:write')")
    public SendMoneyResponseDTO confirmSendMoney(
            @ApiParam(value = "Transaction confirmation info")
            @RequestBody @Valid ConfirmationRequestDTO confirmationRequestDTO
    ) {
        log.debug("confirmationRequestDTO: {}", writeJson(confirmationRequestDTO));

        //update narration in Confirmation Status and confirm transaction
        Transaction confirmedTransaction = sendMoneyUseCase.confirmSendMoney(
                confirmationRequestDTO.getTransactionRef(),
                confirmationRequestDTO.getConfirmationStatus(),
                confirmationRequestDTO.getNarration());

        SendMoneyResponseDTO sendMoneyResponseDTO = modelMapper.map(confirmedTransaction, SendMoneyResponseDTO.class);

        log.debug("sendMoneyResponseDTO: {}", writeJson(sendMoneyResponseDTO));
        return sendMoneyResponseDTO;
    }


}
