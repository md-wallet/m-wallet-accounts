package com.mwallet.mwalletaccounts.adapters.web.dtos.transactions;

import com.mwallet.mwalletaccounts.domain.Transaction;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author lawrence
 * created 26/01/2020 at 21:01
 **/
@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class TransactionLogDTO{

    @ApiModelProperty(
            required = true,
            value = "created on timestamp",
            example = "2020-08-12T08:00:00")
    @NotNull
    private String createdOn;

    @ApiModelProperty(
            required = true,
            value = "transaction ref number",
            example = "QRBC12BFDJHFD")
    @NotNull
    private String transactionRef;

    @ApiModelProperty(
            required = true,
            value = "transaction amount",
            example = "2000.54")
    @NotNull
    private BigDecimal amount;

    @ApiModelProperty(
            required = true,
            value = "transaction cost",
            example = "20.00")
    @NotNull
    private BigDecimal transactionCost;

    @ApiModelProperty(
            required = true,
            value = "transaction status",
            example = "VALIDATED")
    @NotNull
    private Transaction.TransactionStatus transactionStatus;

    @ApiModelProperty(
            required = true,
            value = "confirmation status",
            example = "ROLLBACK")
    @NotNull
    private Transaction.ConfirmationStatus confirmationStatus;

    @ApiModelProperty(
            required = true,
            value = "source account available balance",
            example = "1000.00")
    @NotNull
    @DecimalMin(value = "0.0")
    private BigDecimal srcAvailableBalance;

    @ApiModelProperty(
            required = true,
            value = "source account actual balance",
            example = "1000.00")
    @NotNull
    @DecimalMin(value = "0.0")
    private BigDecimal srcActualBalance;

    @ApiModelProperty(
            required = true,
            value = "destination account available balance",
            example = "1000.00")
    @NotNull
    @DecimalMin(value = "0.0")
    private BigDecimal destAvailableBalance;

    @ApiModelProperty(
            required = true,
            value = "destination account actual balance",
            example = "700.00")
    @NotNull
    @DecimalMin(value = "0.0")
    private BigDecimal destActualBalance;

    @ApiModelProperty(
            required = true,
            value = "log message",
            example = "SUCCESS")
    @NotNull
    private String message;
}
