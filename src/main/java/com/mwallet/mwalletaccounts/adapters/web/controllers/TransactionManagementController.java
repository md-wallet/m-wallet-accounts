package com.mwallet.mwalletaccounts.adapters.web.controllers;

import com.mwallet.mwalletcommons.data.models.PageableModel;
import com.mwallet.mwalletaccounts.adapters.web.dtos.transactions.TransactionInfoDTO;
import com.mwallet.mwalletaccounts.adapters.web.dtos.transactions.TransactionLogDTO;
import com.mwallet.mwalletaccounts.application.ports.in.transactions.GetTransactionLogsUseCase;
import com.mwallet.mwalletaccounts.application.ports.in.transactions.GetTransactionUseCase;
import com.mwallet.mwalletaccounts.domain.Transaction;
import com.mwallet.mwalletaccounts.domain.TransactionLog;
import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.data.domain.Page;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.util.List;

import static com.mwallet.mwalletcommons.utils.ObjectUtils.writeJson;

/**
 * @author lawrence
 * created 26/01/2020 at 18:44
 **/

@Slf4j
@RestController
@RequiredArgsConstructor
class TransactionManagementController {

    private final GetTransactionLogsUseCase getTransactionLogsUseCase;
    private final GetTransactionUseCase getTransactionUseCase;
    private final ModelMapper modelMapper;


    @GetMapping("/logs/{transactionRef}")
    @PreAuthorize("hasAuthority('SCOPE_m-wallet-accounts:read')")
    public List<TransactionLogDTO> getTransactionLogs(
            @ApiParam("accountId")
            @PathVariable @Valid @NotBlank String transactionRef
    ) {
        log.info("============= GetTransactionLogsWebAdapterLayer: {} =============", transactionRef);

        List<TransactionLog> transactionLogs = getTransactionLogsUseCase.getTransactionLogsByTransactionRef(transactionRef);
        Type listType = new TypeToken<List<TransactionLogDTO>>() {
        }.getType();
        return modelMapper.map(transactionLogs, listType);
    }

    @GetMapping("/{transactionRef}")
    @PreAuthorize("hasAuthority('SCOPE_m-wallet-accounts:read')")
    public TransactionInfoDTO getTransactionInfo(
            @ApiParam("transactionRef")
            @PathVariable @Valid @NotBlank String transactionRef
    ) {
        log.info("============= GetTransactionInfoWebAdapterLayer: {} =============", transactionRef);
        Transaction transaction = getTransactionUseCase.getTransactionByTransactionRef(transactionRef);
        TransactionInfoDTO transactionInfoDTO = modelMapper.map(transaction, TransactionInfoDTO.class);
        log.debug("transactionInfoDTO: {}", writeJson(transactionInfoDTO));
        return transactionInfoDTO;
    }

    @GetMapping("/account/{accountId}")
    @PreAuthorize("hasAuthority('SCOPE_m-wallet-accounts:read')")
    public PageableModel<TransactionInfoDTO> getTransactionsInfoByAccountId(
            @ApiParam(value = "accountId", required = true)
            @PathVariable @Valid @NotBlank String accountId,
            @ApiParam(value = "startDate", example = "2020-02-01T01:30:00")
            @RequestParam(required = false) @Valid @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime start,
            @ApiParam(value = "endDate", example = "2020-02-05T01:30:00")
            @RequestParam(required = false) @Valid @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime end,
            @ApiParam(value = "page to fetch", required = true)
            @RequestParam @Valid @NotNull Integer page,
            @ApiParam(value = "size of page", required = true)
            @RequestParam @Valid @NotNull Integer size
    ) {
        log.info("============= GetTransactionsWebAdapterLayer: {} =============", accountId);

        Page<Transaction> transactionPage;
        if (start == null || end == null)
            transactionPage = getTransactionUseCase.getTransactionsByAccountId(accountId, page, size);
        else
            transactionPage = getTransactionUseCase.getTransactionsByAccountIdFilteredByTime(accountId, start, end, page, size);

        Page<TransactionInfoDTO> transactionInfoDTOPage = transactionPage.map(transaction -> modelMapper.map(transaction, TransactionInfoDTO.class));
        return PageableModel.from(transactionInfoDTOPage);
    }
}
