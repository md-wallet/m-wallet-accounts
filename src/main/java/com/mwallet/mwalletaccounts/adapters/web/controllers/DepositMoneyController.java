package com.mwallet.mwalletaccounts.adapters.web.controllers;

import com.mwallet.mwalletaccounts.adapters.web.dtos.transactions.DepositMoneyDTOs;
import com.mwallet.mwalletaccounts.adapters.web.dtos.transactions.DepositMoneyDTOs.ValidationRequestDTO;
import com.mwallet.mwalletaccounts.application.ports.in.accounts.GetAccountUseCase;
import com.mwallet.mwalletaccounts.application.ports.in.transactions.DepositMoneyUseCase;
import com.mwallet.mwalletaccounts.domain.Account;
import com.mwallet.mwalletaccounts.domain.Transaction;
import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.util.Pair;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import static com.mwallet.mwalletcommons.utils.ObjectUtils.writeJson;
import static com.mwallet.mwalletaccounts.domain.Account.AccountType.TRUST_ACCOUNT;

/**
 * @author lawrence
 * created 06/02/2020 at 18:40
 **/

@Slf4j
@RestController
@RequestMapping("/v1/depositmoney")
@RequiredArgsConstructor
class DepositMoneyController {

    private final DepositMoneyUseCase depositMoneyUseCase;
    private final GetAccountUseCase getAccountUseCase;
    private final ModelMapper modelMapper;

    @PostMapping("/validate")
    @PreAuthorize("hasAuthority('SCOPE_m-wallet-accounts:write')")
    public DepositMoneyDTOs.ValidationResponseDTO validateDepositMoney(
            @ApiParam(value = "Deposit Money Transaction validation info")
            @RequestBody @Valid ValidationRequestDTO depositMoneyValidationRequestDTO
    ) {
        log.info("======= ValidateDepositMoneyWebAdapterLayer ============");
        log.debug("depositMoneyValidationRequestDTO: {}", writeJson(depositMoneyValidationRequestDTO));

        //1. fetch accounts
        Account sourceAccount = getAccountUseCase.getAccountById(TRUST_ACCOUNT.name());
        Account destinationAccount = getAccountUseCase.getAccountById(depositMoneyValidationRequestDTO.getDestinationAccountId());

        //2. build deposit transaction
        Transaction transaction = Transaction.builder()
                .amount(depositMoneyValidationRequestDTO.getAmount())
                .billingAddress(depositMoneyValidationRequestDTO.getBillingAddress())
                .transactionDescription(depositMoneyValidationRequestDTO.getTransactionDescription())
                .paymentMode(depositMoneyValidationRequestDTO.getPaymentMode())
                .transactionDomainType(depositMoneyValidationRequestDTO.getTransactionDomainType())
                .confirmationType(Transaction.ConfirmationType.INTERNAL)
                .sourceAccount(sourceAccount)
                .destinationAccount(destinationAccount)
                .transactionType(Transaction.TransactionType.DEPOSIT)
                .confirmationStatus(Transaction.ConfirmationStatus.NONE)
                .transactionStatus(Transaction.TransactionStatus.NONE)
                .build();

        //3. validate transaction
        Pair<String, Transaction> validationResult = depositMoneyUseCase.validateDepositMoney(transaction);
        DepositMoneyDTOs.ValidationResponseDTO depositMoneyValidationValidationResponseDTO = DepositMoneyDTOs.ValidationResponseDTO.builder()
                .transactionHash(validationResult.getFirst())
                .transactionStatus(validationResult.getSecond().getTransactionStatus())
                .build();

        log.debug("depositMoneyValidationResponseDTO: {}", writeJson(depositMoneyValidationValidationResponseDTO));
        return depositMoneyValidationValidationResponseDTO;
    }

    @PostMapping("/request/{transactionHash}")
    @PreAuthorize("hasAuthority('SCOPE_m-wallet-accounts:write')")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public DepositMoneyDTOs.DepositMoneyResponseDTO requestDepositMoney(
            @ApiParam(value = "Validated Transaction Hash")
            @PathVariable @Valid @NotBlank String transactionHash
    ) {
        log.info("======= RequestDepositMoneyWebAdapterLayer ============");
        log.debug("transactionHash: {}", transactionHash);

        Transaction acceptedTransaction = depositMoneyUseCase.requestDepositMoney(transactionHash);
        DepositMoneyDTOs.DepositMoneyResponseDTO depositMoneyResponseDTO = modelMapper.map(acceptedTransaction, DepositMoneyDTOs.DepositMoneyResponseDTO.class);

        log.debug("depositMoneyResponseDTO: {}", writeJson(depositMoneyResponseDTO));
        return depositMoneyResponseDTO;
    }

}
