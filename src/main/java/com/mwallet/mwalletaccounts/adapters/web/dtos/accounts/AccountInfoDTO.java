package com.mwallet.mwalletaccounts.adapters.web.dtos.accounts;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.mwallet.mwalletaccounts.domain.Account;
import com.mwallet.mwalletcommons.data.models.BaseDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author lawrence
 * created 14/01/2020 at 22:02
 **/
@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public
class AccountInfoDTO extends BaseDTO {

    @ApiModelProperty(
            required = true,
            value = "accountId",
            example = "ABCDBH")
    private String accountId;


    @ApiModelProperty(
            required = true,
            value = "account owner id",
            example = "QRBC12")
    @NotBlank
    private String linkId;

    @ApiModelProperty(
            required = true,
            value = "account status",
            example = "ACTIVE")
    @NotNull
    private Account.AccountStatus accountStatus;

    @ApiModelProperty(
            required = true,
            value = "account type",
            example = "USER_ACCOUNT")
    @NotNull
    private Account.AccountType accountType;

    @ApiModelProperty(
            required = true,
            value = "balance available for transactions",
            example = "50.00")
    @NotNull
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private BigDecimal availableBalance;

    @ApiModelProperty(
            required = true,
            value = "balance including frozen funds",
            example = "100.00")
    @NotNull
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private BigDecimal actualBalance;
}
