package com.mwallet.mwalletaccounts.adapters.web.dtos.transactions;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

import static com.mwallet.mwalletaccounts.domain.Transaction.*;

/**
 * @author lawrence
 * created 26/01/2020 at 22:27
 **/
@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class TransactionInfoDTO {

    @ApiModelProperty(
            required = true,
            value = "transaction ref number",
            example = "QRBC12BFDJHFD")
    @NotNull
    private String transactionRef;

    @ApiModelProperty(
            required = true,
            value = "created on timestamp",
            example = "2020-08-12T08:00:00")
    @NotNull
    private String createdOn;

    @ApiModelProperty(
            required = true,
            value = "created on timestamp",
            example = "2020-08-12T08:00:00")
    @NotNull
    private String updatedOn;

    @ApiModelProperty(
            required = true,
            value = "source account Id",
            example = "FNDJ")
    @NotNull
    private String sourceAccountId;

    @ApiModelProperty(
            required = true,
            value = "source account Id",
            example = "BHBD")
    @NotNull
    private String destinationAccountId;

    @ApiModelProperty(
            required = true,
            value = "transaction amount",
            example = "2000.54")
    @NotNull
    @JsonFormat(shape=JsonFormat.Shape.STRING)
    private BigDecimal amount;

    @ApiModelProperty(
            required = true,
            value = "transaction cost",
            example = "20.00")
    @NotNull
    @JsonFormat(shape=JsonFormat.Shape.STRING)
    private BigDecimal transactionCost;

    @ApiModelProperty(
            required = true,
            value = "transaction Type",
            example = "DEPOSIT")
    @NotNull
    private TransactionType transactionType;

    @ApiModelProperty(
            required = true,
            value = "transaction Type",
            example = "COMPLETED")
    @NotNull
    private TransactionStatus transactionStatus;

    @ApiModelProperty(
            required = true,
            value = "payment Mode",
            example = "MPESA")
    @NotNull
    private PaymentMode paymentMode;

    @ApiModelProperty(
            required = true,
            value = "confirmation Type",
            example = "INTERNAL")
    @NotNull
    private ConfirmationType confirmationType;

    @ApiModelProperty(
            required = true,
            value = "confirmation status",
            example = "PROCEED")
    @NotNull
    private ConfirmationStatus confirmationStatus;

    @ApiModelProperty(
            required = true,
            value = "transaction Type",
            example = "Transaction Description")
    @NotBlank
    private String transactionDescription;

    @ApiModelProperty(
            required = true,
            value = "domain type",
            example = "FUNDRAISER_DONATION")
    @NotBlank
    private String transactionDomainType;

}
