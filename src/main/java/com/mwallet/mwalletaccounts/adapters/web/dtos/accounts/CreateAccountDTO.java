package com.mwallet.mwalletaccounts.adapters.web.dtos.accounts;

import com.mwallet.mwalletaccounts.domain.Account;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author lawrence
 * created 18/01/2020 at 23:56
 **/
@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class CreateAccountDTO {

    @ApiModelProperty(
            required = true,
            value = "account owner id",
            example = "QRBC12")
    @NotBlank
    private String linkId;

    @ApiModelProperty(
            required = true,
            value = "account type",
            example = "USER_ACCOUNT")
    @NotNull
    private CreateAccountDTO.PublicAccountType accountType;


    @Getter
    @RequiredArgsConstructor
    public enum PublicAccountType {
        USER_ACCOUNT(Account.AccountType.USER_ACCOUNT),
        JOINT_ACCOUNT(Account.AccountType.JOINT_ACCOUNT);

        private final Account.AccountType accountType;
    }

}
