package com.mwallet.mwalletaccounts.application.ports.out.transactions;


import com.mwallet.mwalletaccounts.domain.Transaction;
import com.mwallet.mwalletcommons.annotations.OutputPort;

/**
 * @author lawrence
 * created 17/01/2020 at 14:49
 **/
@OutputPort
public interface PublishTransactionOutputPort {

    /**
     * Publish transaction to a message broker/event bus
     *
     * @param transaction transaction to publish
     */
    void publishTransaction(Transaction transaction);

    /**
     * Publish a delayed transaction to the parking queue
     * indicating how long to wait before the transaction is returned for processing
     * <br>
     * In addition, a retryCount is necessary if there is intention to keep track of how many
     * times a transaction has been delayed.
     *
     * @param transaction transaction to delay
     * @param delay       delay in ms
     * @param retryCount  retry counter
     */
    void publishDelayedTransaction(Transaction transaction, Long delay, Long retryCount);

    /**
     * Publish transaction callback to the initiator
     * @param transaction transaction
     */
    void publishTransactionCallback(Transaction transaction);
}
