package com.mwallet.mwalletaccounts.application.ports.out.payments;


import com.mwallet.mwalletaccounts.domain.PaymentCard;
import com.mwallet.mwalletcommons.annotations.OutputPort;
import com.mwallet.mwalletcommons.exceptions.GenericHttpException;
import org.springframework.http.HttpStatus;

import javax.validation.constraints.NotBlank;
import java.util.Optional;

/**
 * @author lawrencemwaniki
 * created 14/05/2020 at 16:50
 **/
@OutputPort
public interface LoadPaymentCardOutputPort {

    /**
     * Fetch {@link PaymentCard} by their {@link PaymentCard#token}
     *
     * @param token token
     * @return found card
     */
    Optional<PaymentCard> getPaymentCard(@NotBlank String token) throws CouldNotLoadPaymentCardException;

    class CouldNotLoadPaymentCardException extends GenericHttpException {
        public CouldNotLoadPaymentCardException() {
            super("Could Not Load Payment Card", HttpStatus.BAD_GATEWAY);
        }
    }
}
