package com.mwallet.mwalletaccounts.application.ports.out.transactionlogs;



import com.mwallet.mwalletaccounts.application.ports.out.transactions.LoadTransactionOutputPort;
import com.mwallet.mwalletaccounts.domain.TransactionLog;
import com.mwallet.mwalletcommons.annotations.OutputPort;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;

import static com.mwallet.mwalletaccounts.application.ports.out.transactions.LoadTransactionOutputPort.*;


/**
 * @author lawrence
 * created 22/01/2020 at 17:17
 **/
@OutputPort
public interface LoadTransactionLogOutputPort {
    /**
     * Load transaction logs for a specific transaction
     * @param transactionRef transaction ref number
     * @return list of transaction logs
     */
    List<TransactionLog> loadTransactionLogsByTransactionRef(@Valid @NotBlank String transactionRef) throws TransactionNotFoundException;
}
