package com.mwallet.mwalletaccounts.application.ports.in.transactions;


import com.mwallet.mwalletaccounts.domain.Account.AccountType;
import com.mwallet.mwalletaccounts.domain.Payout;
import com.mwallet.mwalletaccounts.domain.Transaction;
import com.mwallet.mwalletcommons.annotations.UseCase;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.data.util.Pair;

import javax.validation.Valid;
import java.util.Collections;
import java.util.List;

import static com.mwallet.mwalletaccounts.domain.Account.AccountType.*;
import static com.mwallet.mwalletaccounts.domain.Account.AccountType.TRUST_ACCOUNT;
import static com.mwallet.mwalletaccounts.domain.Account.AccountType.USER_ACCOUNT;
import static com.mwallet.mwalletaccounts.domain.Transaction.PaymentMode.MPESA;


/**
 * @author lawrence
 * created 11/02/2020 at 10:26
 **/
@UseCase
public interface WithdrawMoneyUseCase {

    /**
     * Check validity of transaction and cache the request
     * <br>
     * If valid, we generate a hash of the transaction and return
     * this to the caller.
     * <br>
     * If they intend to continue with the request they must come back
     * with this hash to ensure it is the same request.
     * <br>
     * The cached request will expire naturally if the caller
     * does not go ahead and request the funds transfer.
     * <br>
     * Transaction Status: {@link Transaction.TransactionStatus#VALIDATED}
     *
     * @param transaction transaction to check
     * @return pair of transaction hash and the transaction itself
     */
    Pair<String, Transaction> validateWithdrawMoney(@Valid Transaction transaction);


    /**
     * Request acceptance of withdraw money transaction.
     * <br>
     * The incoming hash will be used to retrieve the cached transaction.
     * <br>
     * A Transaction reference number is generated and the transaction is LIVE
     * <br>
     * Transaction Status: {@link Transaction.TransactionStatus#ACCEPTED}
     *
     * @param hash transaction hash provided during validation
     * @return Accepted Transaction
     */
    Transaction requestWithdrawMoney(@Valid String hash);

    /**
     * On successful acceptance of a transaction for processing,
     * Funds must be reserved on the available source account balance and realized
     * on the actual balance of the destination account.
     * <br>
     * In essence freezing this transaction amount from this account which
     * may be engaged in further transactions regardless of the time
     * duration of the current transaction or its success/failure.
     * <br>
     * Transaction Status: {@link Transaction.TransactionStatus#PREPARED}
     *
     * @param transaction transaction to confirm
     */
    void prepareWithdrawMoney(@Valid Transaction transaction);

    /**
     * After successful transaction preparation, an external request has to be made to
     * send funds to the customer.
     * <br>
     * Depending on the outcome of this request the transaction preparation may be rolled
     * back or the transaction flow allowed to move forward awaiting verification of receipt
     * of funds.
     * <br>
     * Transaction Status: {@link Transaction.TransactionStatus#PAYOUT_REQUESTED}
     *
     * @param transaction transaction to request payout for
     * @return true if the request is successful ie ACCEPTED or REJECTED, false if FAILED
     */
    boolean requestWithdrawMoneyPayout(@Valid Transaction transaction);

    /**
     * After successful payout request, asynchronously verify said payout request.
     * <br>
     * Depending on the outcome of the verification the transaction preparation may be rolled back
     * or the transaction flow allowed to move forward awaiting confirmation of the transaction
     * <br>
     * Transaction Status: {@link Transaction.TransactionStatus#PAYOUT_VERIFIED}
     *
     * @param payoutResponse verified payout
     */
    Transaction verifyWithdrawMoneyPayout(@Valid Payout.PayoutResponse payoutResponse);

    /**
     * The outcome of payout request or payout verification require transaction updates either available
     * balance updates or transaction rollbacks. This functionality is achieved here.
     * <br>
     * Transaction Status: {@link Transaction.TransactionStatus#CONFIRMED}
     *
     * @param transaction transaction to confirm
     * @return Confirmed Transaction
     */
    Transaction confirmWithdrawMoney(@Valid Transaction transaction);

    /**
     * On successful processing, complete transaction ie
     * send notifications to both parties <br>
     * Transaction Status: {@link Transaction.TransactionStatus#COMPLETED}
     *
     * @param transaction transaction to complete
     */
    void completeWithdrawMoney(@Valid Transaction transaction);

    /**
     * WithdrawMoney transaction constraints depending on the destination account
     */
    @Getter
    @RequiredArgsConstructor
    enum WithdrawMoneyConstraints{
        USER_WITHDRAW(100,70000),
        JOINT_WITHDRAW(100,70000);

        private final long minFundsWithdraw;
        private final long maxFundsWithdraw;

        public static final List<Transaction.PaymentMode> supportedPaymentModes = Collections
                .unmodifiableList(List.of(MPESA));

        public static final List<Pair<AccountType, AccountType>> supportedSrcDestAccountTypes = Collections
                .unmodifiableList(List.of(
                        Pair.of(USER_ACCOUNT, TRUST_ACCOUNT),
                        Pair.of(JOINT_ACCOUNT, TRUST_ACCOUNT)
                ));

        /**
         * Helper method to get the appropriate transaction details
         *
         * @param srcDestAccountTypes source destination account type pair
         * @return appropriate details
         */
        public static WithdrawMoneyConstraints getConstraints(Pair<AccountType, AccountType> srcDestAccountTypes){
            if (srcDestAccountTypes.equals(Pair.of(USER_ACCOUNT, TRUST_ACCOUNT)))
                return USER_WITHDRAW;
            else if (srcDestAccountTypes.equals(Pair.of(JOINT_ACCOUNT, TRUST_ACCOUNT)))
                return JOINT_WITHDRAW;
            else
                throw new UnsupportedOperationException("Unsupported Source and Destination Account Types: " + srcDestAccountTypes.toString());
        }
    }




}
