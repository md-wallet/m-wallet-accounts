package com.mwallet.mwalletaccounts.application.ports.out.accounts;


import com.mwallet.mwalletaccounts.domain.Account;
import com.mwallet.mwalletcommons.annotations.OutputPort;
import com.mwallet.mwalletcommons.exceptions.GenericHttpException;
import org.springframework.http.HttpStatus;

import javax.validation.Valid;

import static com.mwallet.mwalletaccounts.application.ports.out.accounts.LoadAccountOutputPort.*;

/**
 * @author lawrence
 * created 17/12/2019 at 22:45
 **/

@OutputPort
public interface SaveAccountOutputPort {

    /**
     * Create account
     * @param account account to create
     * @return created account
     */
    Account createAccount(@Valid Account account) throws NonUniqueAccountException;

    /**
     * Update account
     * @param account account to update
     * @return updated account
     */
    Account updateAccount(@Valid Account account) throws AccountNotFoundException;

    class NonUniqueAccountException extends GenericHttpException {
        public NonUniqueAccountException(String message, HttpStatus httpStatus) {
            super(message, httpStatus);
        }
    }
}
