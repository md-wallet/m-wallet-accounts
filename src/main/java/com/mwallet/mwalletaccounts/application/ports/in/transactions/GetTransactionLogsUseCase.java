package com.mwallet.mwalletaccounts.application.ports.in.transactions;



import com.mwallet.mwalletaccounts.domain.TransactionLog;
import com.mwallet.mwalletcommons.annotations.UseCase;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @author lawrence
 * created 26/01/2020 at 18:47
 **/
@UseCase
public interface GetTransactionLogsUseCase {

    /**
     * Fetch list of transaction logs
     * @param transactionRef transaction ref
     * @return list of transaction logs
     */
    List<TransactionLog> getTransactionLogsByTransactionRef(@Valid @NotBlank String transactionRef);
}
