package com.mwallet.mwalletaccounts.application.ports.in.transactions;


import com.mwallet.mwalletaccounts.domain.Account;
import com.mwallet.mwalletaccounts.domain.Payment;
import com.mwallet.mwalletaccounts.domain.Transaction;
import com.mwallet.mwalletcommons.annotations.UseCase;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.data.util.Pair;

import javax.validation.Valid;
import java.util.Collections;
import java.util.List;

import static com.mwallet.mwalletaccounts.domain.Account.AccountType.*;
import static com.mwallet.mwalletaccounts.domain.Transaction.PaymentMode.CARD;
import static com.mwallet.mwalletaccounts.domain.Transaction.PaymentMode.MPESA;


/**
 * @author lawrence
 * created 01/02/2020 at 13:08
 **/
@UseCase
public interface DepositMoneyUseCase {

    /**
     * Check validity of transaction and cache the request
     * <br>
     * If valid, we generate a hash of the transaction and return
     * this to the caller.
     * <br>
     * If they intend to continue with the request they must come back
     * with this hash to ensure it is the same request.
     * <br>
     * The cached request will expire naturally if the caller
     * does not go ahead and request the funds transfer.
     * <br>
     * Transaction Status: {@link Transaction.TransactionStatus#VALIDATED}
     *
     * @param transaction transaction to check
     * @return pair of transaction hash and the transaction itself
     */
    Pair<String, Transaction> validateDepositMoney(@Valid Transaction transaction);

    /**
     * Request acceptance of deposit money transaction.
     * <br>
     * The incoming hash will be used to retrieve the cached transaction.
     * <br>
     * A Transaction reference number is generated and the transaction is LIVE
     * <br>
     * Transaction Status: {@link Transaction.TransactionStatus#ACCEPTED}
     *
     * @param hash transaction hash provided during validation
     * @return Accepted Transaction
     */
    Transaction requestDepositMoney(@Valid String hash);

    /**
     * On successful acceptance of a transaction for processing,
     * Funds must be reserved on the available source account balance and realized
     * on the actual balance of the destination account.
     * <br>
     * In essence freezing this transaction amount from this account which
     * may be engaged in further transactions regardless of the time
     * duration of the current transaction or its success/failure.
     * <br>
     * Transaction Status: {@link Transaction.TransactionStatus#PREPARED}
     *
     * @param transaction transaction to confirm
     */
    void prepareDepositMoney(@Valid Transaction transaction);


    /**
     * After successful transaction preparation, an external request has to be made to
     * collect funds from the customer.
     * <br>
     * Depending on the outcome of this request the transaction preparation may be rolled
     * back or the transaction flow allowed to move forward awaiting verification of receipt
     * of funds.
     * <br>
     * Transaction Status: {@link Transaction.TransactionStatus#PAYMENT_REQUESTED}
     *
     * @param transaction transaction to request payment for
     * @return true if the request is successful ie ACCEPTED or REJECTED, false if FAILED
     */
    boolean requestDepositMoneyPayment(@Valid Transaction transaction);

    /**
     * After successful payment request, asynchronously verify said payment request.
     * <br>
     * Depending on the outcome of the verification the transaction preparation may be rolled back
     * or the transaction flow allowed to move forward awaiting confirmation of the transaction
     * <br>
     * Transaction Status: {@link Transaction.TransactionStatus#PAYMENT_VERIFIED}
     *
     * @param paymentResponse verified payment
     */
    Transaction verifyDepositMoneyPayment(@Valid Payment.PaymentResponse paymentResponse);

    /**
     * The outcome of payment request or payment verification require transaction updates either available
     * balance updates or transaction rollbacks. This functionality is achieved here.
     * <br>
     * Transaction Status: {@link Transaction.TransactionStatus#CONFIRMED}
     *
     * @param transaction transaction to confirm
     * @return Confirmed Transaction
     */
    Transaction confirmDepositMoney(@Valid Transaction transaction);

    /**
     * On successful processing, complete transaction ie
     * send notifications to both parties <br>
     * Transaction Status: {@link Transaction.TransactionStatus#COMPLETED}
     *
     * @param transaction transaction to complete
     */
    void completeDepositMoney(@Valid Transaction transaction);


    /**
     * DepositMoney transaction constraints depending on the source account
     */
    @Getter
    @RequiredArgsConstructor
    enum DepositMoneyConstraints {
        USER_DEPOSIT(10, 70000),
        JOINT_DEPOSIT(10, 70000);

        private final long minFundsDeposit;
        private final long maxFundsDeposit;

        public static final List<Transaction.PaymentMode> supportedPaymentModes = Collections
                .unmodifiableList(List.of(MPESA, CARD));

        public static final List<Pair<Account.AccountType, Account.AccountType>> supportedSrcDestAccountTypes = Collections
                .unmodifiableList(List.of(
                        Pair.of(TRUST_ACCOUNT, USER_ACCOUNT),
                        Pair.of(TRUST_ACCOUNT, JOINT_ACCOUNT)
                ));

        /**
         * Helper method to get the appropriate transaction details
         *
         * @param srcDestAccountTypes source destination account type pair
         * @return appropriate details
         */
        public static DepositMoneyConstraints getConstraints(Pair<Account.AccountType, Account.AccountType> srcDestAccountTypes) {
            if (srcDestAccountTypes.equals(Pair.of(TRUST_ACCOUNT, USER_ACCOUNT)))
                return USER_DEPOSIT;
            else if (srcDestAccountTypes.equals(Pair.of(TRUST_ACCOUNT, JOINT_ACCOUNT)))
                return JOINT_DEPOSIT;
            else
                throw new UnsupportedOperationException("Unsupported Source and Destination Account Types: " + srcDestAccountTypes.toString());
        }
    }
}
