package com.mwallet.mwalletaccounts.application.ports.out.transactionlogs;



import com.mwallet.mwalletaccounts.domain.Transaction;
import com.mwallet.mwalletaccounts.domain.TransactionLog;
import com.mwallet.mwalletcommons.annotations.OutputPort;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author lawrence
 * created 22/01/2020 at 16:54
 **/
@OutputPort
public interface LogTransactionOutputPort {

    /**
     * Create transactionLog from transaction
     * @param transaction transaction to log
     * @param message message to attach to log
     */
    TransactionLog logTransaction(@Valid Transaction transaction, @Valid @NotNull String message);
}
