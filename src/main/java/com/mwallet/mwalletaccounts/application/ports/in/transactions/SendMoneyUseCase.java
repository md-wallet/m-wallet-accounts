package com.mwallet.mwalletaccounts.application.ports.in.transactions;


import com.mwallet.mwalletaccounts.domain.Transaction;
import com.mwallet.mwalletcommons.annotations.UseCase;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.data.util.Pair;
import org.springframework.http.HttpStatus;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.List;

import static com.mwallet.mwalletaccounts.domain.Account.*;
import static com.mwallet.mwalletaccounts.domain.Account.AccountType.*;
import static com.mwallet.mwalletaccounts.domain.Transaction.*;
import static com.mwallet.mwalletaccounts.domain.Transaction.PaymentMode.WALLET;
import static com.mwallet.mwalletaccounts.domain.ValidateTransaction.*;


/**
 * @author lawrence
 * created 17/01/2020 at 07:58
 **/
@UseCase
public interface SendMoneyUseCase {


    /**
     * Check validity of transaction and cache the request
     * <br>
     * If valid, we generate a hash of the transaction and return
     * this to the caller.
     * <br>
     * If they intend to continue with the request they must come back
     * with this hash to ensure it is the same request.
     * <br>
     * The cached request will expire naturally if the caller
     * does not go ahead and request the funds transfer.
     * <br>
     * Transaction Status: {@link TransactionStatus#VALIDATED}
     *
     * @param transaction transaction to check
     * @return pair of transaction hash and the transaction itself
     */
    Pair<String, Transaction> validateSendMoney(@Valid Transaction transaction);

    /**
     * Request acceptance of send money transaction.
     * <br>
     * The incoming hash will be used to retrieve the cached transaction.
     * <br>
     * A Transaction reference number is generated and the transaction is LIVE
     * <br>
     * Transaction Status: {@link TransactionStatus#ACCEPTED}
     *
     * @param hash transaction hash provided during validation
     * @return Accepted Transaction
     */
    Transaction requestSendMoney(@Valid @NotBlank String hash);

    /**
     * On successful acceptance of a transaction for processing,
     * Funds must be reserved on the available source account balance and realized
     * on the actual balance of the destination account.
     * <br>
     * In essence freezing this transaction amount from this account which
     * may be in engaged in further transactions regardless of the time
     * duration of the current transaction or its success/failure.
     * <br>
     * Transaction Status: {@link TransactionStatus#PREPARED}
     *
     * @param transaction transaction to confirm
     */
    void prepareSendMoney(@Valid Transaction transaction);

    /**
     * On successful preparation of a transaction, the intention is to collect
     * funds from the actual balance of the source account and reflect this on
     * the available destination account.
     * <br>
     * To support the possibility that freezing funds may not always require
     * immediate collection of said funds, Transactions with confirmation type
     * {@link ConfirmationType#EXTERNAL} will require external trigger of this
     * functionality.
     * <br>
     * Transaction Status: {@link TransactionStatus#CONFIRMED}
     *
     * @param transactionRef     ref number of transaction to confirm
     * @param confirmationStatus confirmation status to use for the transaction
     * @param narration associated narration
     * @return Confirmed Transaction
     */
    Transaction confirmSendMoney(@Valid @NotBlank String transactionRef, @Valid @NotNull ConfirmationStatus confirmationStatus, @Valid @NotBlank String narration);

    /**
     * In this case confirmation is triggered with implicit PROCEED status
     *
     * @param transactionRef ref number of transaction to confirm
     * @return Confirmed Transaction
     * @see SendMoneyUseCase#confirmSendMoney(String, ConfirmationStatus, String)
     */
    default Transaction confirmSendMoney(@Valid @NotBlank String transactionRef) {
        return confirmSendMoney(transactionRef, ConfirmationStatus.PROCEED,"SUCCESS_IMPLICIT");
    }

    /**
     * On successful processing, complete transaction ie
     * send notifications to both parties <br>
     * Transaction Status: {@link TransactionStatus#COMPLETED}
     *
     * @param transaction transaction to complete
     */
    void completeSendMoney(@Valid Transaction transaction);


    /**
     * SendMoney transaction constraints depending on the source
     * and destination accounts
     */
    @Getter
    @RequiredArgsConstructor
    enum SendMoneyConstraints {

        USER_TO_JOINT(10, 200000),
        JOINT_TO_JOINT(1000, 500000),
        JOINT_TO_USER(10, 200000);

        private final long minFundsTransferable;
        private final long maxFundsTransferable;

        public static final List<PaymentMode> supportedPaymentModes = Collections
                .unmodifiableList(List.of(WALLET));

        public static final List<Pair<AccountType, AccountType>> supportedSrcDestAccountTypes = Collections
                .unmodifiableList(List.of(
                        Pair.of(USER_ACCOUNT, JOINT_ACCOUNT),
                        Pair.of(JOINT_ACCOUNT, USER_ACCOUNT),
                        Pair.of(JOINT_ACCOUNT, JOINT_ACCOUNT)
                ));

        /**
         * Helper method to get the appropriate transaction details
         *
         * @param srcDestAccountTypes source destination account type pair
         * @return appropriate details
         */
        public static SendMoneyConstraints getConstraints(Pair<AccountType, AccountType> srcDestAccountTypes) {

            if (srcDestAccountTypes.equals(Pair.of(USER_ACCOUNT, JOINT_ACCOUNT)))
                return SendMoneyConstraints.USER_TO_JOINT;
            else if (srcDestAccountTypes.equals(Pair.of(JOINT_ACCOUNT, USER_ACCOUNT)))
                return SendMoneyConstraints.JOINT_TO_USER;
            else if (srcDestAccountTypes.equals(Pair.of(JOINT_ACCOUNT, JOINT_ACCOUNT)))
                return SendMoneyConstraints.JOINT_TO_JOINT;
            else {
                throw new InvalidTransactionException("Unsupported Source and Destination Account Types: " + srcDestAccountTypes.toString(), HttpStatus.UNPROCESSABLE_ENTITY);
            }
        }
    }
}
