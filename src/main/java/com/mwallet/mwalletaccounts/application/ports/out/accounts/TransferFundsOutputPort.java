package com.mwallet.mwalletaccounts.application.ports.out.accounts;


import com.mwallet.mwalletaccounts.domain.Account;
import com.mwallet.mwalletcommons.annotations.OutputPort;
import org.springframework.data.util.Pair;

import java.math.BigDecimal;

import static com.mwallet.mwalletaccounts.application.ports.out.accounts.LoadAccountOutputPort.*;


/**
 * @author lawrence
 * created 19/01/2020 at 00:58
 **/
@OutputPort
public interface TransferFundsOutputPort {
    /**
     * This is a two stage process with transactional support in case of rollbacks: <br>
     * 1. Withdraw Funds from the available balance of source account to freeze this amount <br>
     * 2. Deposit Funds to the actual balance of the destination account
     *
     * @param srcAccountId    source account Id
     * @param destAccountId   destination account Id
     * @param amount          amount to prepare
     * @param transactionCost transaction cost
     * @return source, destination accounts latest models
     * @throws AccountNotFoundException when given an invalid accountId
     */
    Pair<Account, Account> prepareTransfer(String srcAccountId, String destAccountId, BigDecimal amount, BigDecimal transactionCost) throws AccountNotFoundException;

    /**
     * This is a two stage process with transactional support in case of rollbacks:
     * 1. Withdraw Funds from the actual balance of source account
     * 2. Deposit Funds to the available balance of the destination account
     *
     * @param srcAccountId    source account Id
     * @param destAccountId   destination account Id
     * @param amount          amount to confirm
     * @param transactionCost transaction cost
     * @return source, destination accounts latest models
     * @throws AccountNotFoundException when given an invalid accountId
     */
    Pair<Account, Account> confirmTransfer(String srcAccountId, String destAccountId, BigDecimal amount, BigDecimal transactionCost) throws AccountNotFoundException;

    /**
     * Rollback the action of {@link TransferFundsOutputPort#prepareTransfer(String, String, BigDecimal, BigDecimal)}
     * <br>
     * 1. Restore Funds to the available balance of source account
     * 2. Remove Funds from actual balance of destination account
     *
     * @param srcAccountId    source account Id
     * @param destAccountId   destination account Id
     * @param amount          amount to rollback
     * @param transactionCost transaction cost
     * @return source, destination accounts latest models
     * @throws AccountNotFoundException when given an invalid accountId
     */
    Pair<Account, Account> rollbackTransfer(String srcAccountId, String destAccountId, BigDecimal amount, BigDecimal transactionCost) throws AccountNotFoundException;
}
