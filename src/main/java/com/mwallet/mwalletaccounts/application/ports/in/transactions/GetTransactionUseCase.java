package com.mwallet.mwalletaccounts.application.ports.in.transactions;


import com.mwallet.mwalletaccounts.domain.Transaction;
import com.mwallet.mwalletcommons.annotations.UseCase;
import org.springframework.data.domain.Page;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * @author lawrence
 * created 27/01/2020 at 00:16
 **/
@UseCase
public interface GetTransactionUseCase {
    /**
     * Load transaction by transaction ref
     *
     * @param transactionRef transaction ref
     * @return found transaction
     */
    Transaction getTransactionByTransactionRef(@Valid @NotBlank String transactionRef);

    /**
     * Load transactions by account id
     *
     * @param accountId account id
     * @param page      page to fetch
     * @param size      size of page
     * @return found transactions
     */
    Page<Transaction> getTransactionsByAccountId(
            @Valid @NotBlank String accountId,
            @Valid @NotNull Integer page,
            @Valid @NotNull Integer size
    );

    /**
     * Load transactions by account id and time
     *
     * @param accountId account id
     * @param page      page to fetch
     * @param size      size of page
     * @return found transactions
     */
    Page<Transaction> getTransactionsByAccountIdFilteredByTime(
            @Valid @NotBlank String accountId,
            @Valid @NotNull LocalDateTime start,
            @Valid @NotNull LocalDateTime end,
            @Valid @NotNull Integer page,
            @Valid @NotNull Integer size);
}
