package com.mwallet.mwalletaccounts.application.ports.out.transactions;

import com.mwallet.mwalletaccounts.domain.Transaction;
import com.mwallet.mwalletcommons.annotations.OutputPort;

/**
 * @author lawrence
 * created 19/12/2019 at 15:45
 **/
@OutputPort
public interface GenerateTransactionRefOutputPort {
    /**
     * Generate unique transaction reference id
     * @param transaction transaction to generate ref for
     * @return generated reference Id
     */
    String generateTransactionRef(Transaction transaction);
}
