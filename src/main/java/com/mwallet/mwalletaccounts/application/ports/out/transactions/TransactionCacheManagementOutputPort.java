package com.mwallet.mwalletaccounts.application.ports.out.transactions;


import com.mwallet.mwalletaccounts.domain.Transaction;
import com.mwallet.mwalletcommons.annotations.OutputPort;
import com.mwallet.mwalletcommons.exceptions.GenericHttpException;
import org.springframework.http.HttpStatus;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import static com.mwallet.mwalletaccounts.application.ports.out.transactions.LoadTransactionOutputPort.*;


/**
 * @author lawrence
 * created 23/01/2020 at 11:43
 **/
@OutputPort
public interface TransactionCacheManagementOutputPort {

    /**
     * Save transaction to cache
     * @param transaction transaction to save
     * @return saved transaction hash
     */
    String saveTransaction(@Valid Transaction transaction) throws DuplicateTransactionException;

    /**
     * Check if transaction exists by hash
     * @param hash hash to check
     * @return true if exists false otherwise
     */
    boolean existsByHash(@Valid @NotBlank String hash);

    /**
     * Get transaction by hash
     * @param hash transaction hash
     * @return found transaction
     */
    Transaction getTransactionByHash(@Valid @NotBlank String hash) throws TransactionNotFoundException;

    /**
     * Remove transaction from cache
     * @param hash transaction hash
     */
    void deleteByHash(@Valid @NotBlank String hash);

    class DuplicateTransactionException extends GenericHttpException {
        public DuplicateTransactionException(String message, HttpStatus httpStatus) {
            super(message, httpStatus);
        }
    }

}
