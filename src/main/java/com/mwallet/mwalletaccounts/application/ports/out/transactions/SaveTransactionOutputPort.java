package com.mwallet.mwalletaccounts.application.ports.out.transactions;


import com.mwallet.mwalletaccounts.domain.Transaction;
import com.mwallet.mwalletcommons.annotations.OutputPort;
import com.mwallet.mwalletcommons.exceptions.GenericHttpException;
import org.springframework.http.HttpStatus;

import javax.validation.Valid;

/**
 * @author lawrence
 * created 18/12/2019 at 16:37
 **/
@OutputPort
public interface SaveTransactionOutputPort {
    /**
     * Create transaction
     * @param transaction transaction to create
     * @return created transaction
     */
    Transaction createTransaction(@Valid Transaction transaction) throws NonUniqueTransactionException;

    /**
     * Update existing transaction
     * @param transaction transaction to update
     * @return updated transaction
     */
    Transaction updateTransaction(@Valid Transaction transaction);

    class NonUniqueTransactionException extends GenericHttpException {
        public NonUniqueTransactionException(String message, HttpStatus httpStatus) {
            super(message, httpStatus);
        }
    }
}
