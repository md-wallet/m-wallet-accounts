package com.mwallet.mwalletaccounts.application.ports.in.accounts;



import com.mwallet.mwalletaccounts.domain.Account;
import com.mwallet.mwalletcommons.annotations.InputPort;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author lawrence
 * created 14/01/2020 at 21:56
 **/
@InputPort
public interface GetAccountUseCase {
    /**
     * Get account by its unique id
     * @param accountId account id
     * @return found account
     */
    Account getAccountById(@Valid @NotNull String accountId);
}
