package com.mwallet.mwalletaccounts.application.ports.in.accounts;


import com.mwallet.mwalletaccounts.domain.Account;
import com.mwallet.mwalletcommons.annotations.InputPort;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import static com.mwallet.mwalletaccounts.domain.Account.*;


/**
 * @author lawrence
 * created 14/01/2020 at 16:41
 **/
@InputPort
public interface UpdateAccountStatusUseCase {

    /**
     * Change account status to desired state
     * @param accountStatus new state
     */
    Account updateAccountStatus(@NotNull String accountId, @Valid @NotNull AccountStatus accountStatus);

}
