package com.mwallet.mwalletaccounts.application.ports.out.accounts;


import com.mwallet.mwalletaccounts.domain.Account;
import com.mwallet.mwalletaccounts.domain.Account.AccountType;
import com.mwallet.mwalletcommons.annotations.OutputPort;
import com.mwallet.mwalletcommons.exceptions.GenericHttpException;
import org.springframework.http.HttpStatus;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;



/**
 * @author lawrence
 * created 18/12/2019 at 09:16
 **/
@OutputPort
public interface LoadAccountOutputPort {
    /**
     * Find account by account id
     * @param accountId account id
     * @return found account
     */
    Account loadAccountById(@Valid @NotNull String accountId) throws AccountNotFoundException;

    /**
     * Check if account exists by the given linkId and accountType
     * @param linkId link id
     * @param accountType account Type
     * @return true if exists false otherwise
     */
    boolean existsByLinkIdAndAccountType(@Valid @NotEmpty String linkId, @Valid @NotNull AccountType accountType);

    class AccountNotFoundException extends GenericHttpException {
        public AccountNotFoundException(String message, HttpStatus httpStatus) {
            super(message, httpStatus);
        }
    }
}
