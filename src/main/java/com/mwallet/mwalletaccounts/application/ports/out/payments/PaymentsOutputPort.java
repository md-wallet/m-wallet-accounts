package com.mwallet.mwalletaccounts.application.ports.out.payments;



import com.mwallet.mwalletaccounts.domain.Payment;
import com.mwallet.mwalletaccounts.domain.Payout;
import com.mwallet.mwalletaccounts.domain.Transaction;
import com.mwallet.mwalletcommons.annotations.OutputPort;

import javax.validation.Valid;

import static com.mwallet.mwalletaccounts.domain.Payment.*;
import static com.mwallet.mwalletaccounts.domain.Payout.*;

/**
 * @author lawrence
 * created 02/02/2020 at 10:18
 **/
@OutputPort
public interface PaymentsOutputPort {

    /**
     * Request payment
     * @param transaction to request payment for
     * @return requested payment
     */
    PaymentRequest requestPayment(@Valid Transaction transaction);

    /**
     * Request payout
     * @param transaction transaction to request payout for
     * @return requested payout
     */
    PayoutRequest requestPayout(@Valid Transaction transaction);

}
