package com.mwallet.mwalletaccounts.application.ports.in.accounts;


import com.mwallet.mwalletaccounts.domain.Account;
import com.mwallet.mwalletcommons.annotations.InputPort;
import com.mwallet.mwalletcommons.exceptions.GenericHttpException;
import org.springframework.http.HttpStatus;

import javax.validation.Valid;

/**
 * @author lawrence
 * created 14/01/2020 at 15:23
 **/
@InputPort
public interface CreateAccountUseCase {

    /**
     * Create a new account if no account exists with the given linkId and AccountType
     * @param account account to create
     * @throws AccountAlreadyExistsException on event that the account is not unique
     */
    Account createAccount(@Valid Account account) throws AccountAlreadyExistsException;

    class AccountAlreadyExistsException extends GenericHttpException {
        public AccountAlreadyExistsException(String message, HttpStatus httpStatus) {
            super(message, httpStatus);
        }
    }
}
