package com.mwallet.mwalletaccounts.application.ports.out.transactions;


import com.mwallet.mwalletaccounts.domain.Transaction;
import com.mwallet.mwalletcommons.annotations.OutputPort;
import com.mwallet.mwalletcommons.notifications.NotificationType;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author lawrence
 * created 12/02/2020 at 22:36
 **/
@OutputPort
public interface SendTransactionNotificationOutputPort {

    /**
     * Send transaction notification
     * @param transaction transaction to send notification for
     */
    void send(
            @Valid @NotNull Transaction transaction,
            @Valid @NotNull NotificationType notificationType);
}
