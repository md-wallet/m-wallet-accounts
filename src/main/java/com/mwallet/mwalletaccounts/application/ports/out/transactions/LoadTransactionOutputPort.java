package com.mwallet.mwalletaccounts.application.ports.out.transactions;


import com.mwallet.mwalletaccounts.application.ports.out.accounts.LoadAccountOutputPort;
import com.mwallet.mwalletaccounts.domain.Transaction;
import com.mwallet.mwalletcommons.annotations.OutputPort;
import com.mwallet.mwalletcommons.exceptions.GenericHttpException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

import static com.mwallet.mwalletaccounts.application.ports.out.accounts.LoadAccountOutputPort.*;

/**
 * @author lawrence
 * created 18/12/2019 at 16:44
 **/
@OutputPort
public interface LoadTransactionOutputPort {
    /**
     * Find transaction by transaction ref
     *
     * @param transactionRef unique ref
     * @return found transaction
     * @throws TransactionNotFoundException if transaction does not exist
     */
    Transaction loadTransactionByTransactionRef(@Valid @NotBlank String transactionRef) throws TransactionNotFoundException;

    /**
     * Get all transactions associated with an account ordered by time descending
     *
     * @param accountId id of account to find transactions for
     * @return list of found transactions
     */
    Page<Transaction> loadTransactionsByAccountId(
            @Valid @NotBlank String accountId,
            @Valid @NotNull Integer page,
            @Valid @NotNull Integer size);

    /**
     * Get all transactions associated with an account in a specific time window
     *
     * @param accountId id of account to find transactions for
     * @return list of found transactions
     */
    Page<Transaction> loadTransactionsByAccountIdFilterByTime(
            @Valid @NotBlank String accountId,
            @Valid @NotNull LocalDateTime start,
            @Valid @NotNull LocalDateTime end,
            @Valid @NotNull Integer page, @Valid @NotNull Integer size) throws AccountNotFoundException;

    class TransactionNotFoundException extends GenericHttpException {
        public TransactionNotFoundException(String message, HttpStatus httpStatus) {
            super(message, httpStatus);
        }
    }
}
