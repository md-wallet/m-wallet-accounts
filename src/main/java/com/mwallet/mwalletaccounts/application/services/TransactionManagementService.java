package com.mwallet.mwalletaccounts.application.services;


import com.mwallet.mwalletaccounts.application.ports.in.transactions.GetTransactionLogsUseCase;
import com.mwallet.mwalletaccounts.application.ports.in.transactions.GetTransactionUseCase;
import com.mwallet.mwalletaccounts.application.ports.out.transactionlogs.LoadTransactionLogOutputPort;
import com.mwallet.mwalletaccounts.application.ports.out.transactions.LoadTransactionOutputPort;
import com.mwallet.mwalletaccounts.domain.Transaction;
import com.mwallet.mwalletaccounts.domain.TransactionLog;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author lawrence
 * created 26/01/2020 at 20:37
 **/
@Service
@RequiredArgsConstructor
class TransactionManagementService implements GetTransactionLogsUseCase, GetTransactionUseCase {
    private final LoadTransactionOutputPort loadTransactionOutputPort;
    private final LoadTransactionLogOutputPort loadTransactionLogOutputPort;

    @Override
    public List<TransactionLog> getTransactionLogsByTransactionRef(@Valid @NotBlank String transactionRef) {
        return loadTransactionLogOutputPort.loadTransactionLogsByTransactionRef(transactionRef);
    }

    @Override
    public Transaction getTransactionByTransactionRef(@Valid @NotBlank String transactionRef) {
        return loadTransactionOutputPort.loadTransactionByTransactionRef(transactionRef);
    }

    @Override
    public Page<Transaction> getTransactionsByAccountId(
            @Valid @NotBlank String accountId,
            @Valid @NotNull Integer page,
            @Valid @NotNull Integer size) {
        return loadTransactionOutputPort.loadTransactionsByAccountId(accountId, page, size);
    }

    @Override
    public Page<Transaction> getTransactionsByAccountIdFilteredByTime(
            @Valid @NotBlank String accountId,
            @Valid @NotNull LocalDateTime start,
            @Valid @NotNull LocalDateTime end,
            @Valid @NotNull Integer page,
            @Valid @NotNull Integer size) {
        return loadTransactionOutputPort.loadTransactionsByAccountIdFilterByTime(accountId, start, end, page, size);
    }


}
