package com.mwallet.mwalletaccounts.application.services;


import com.mwallet.mwalletaccounts.application.ports.in.transactions.SendMoneyUseCase;
import com.mwallet.mwalletaccounts.application.ports.out.accounts.LoadAccountOutputPort;
import com.mwallet.mwalletaccounts.application.ports.out.accounts.TransferFundsOutputPort;
import com.mwallet.mwalletaccounts.application.ports.out.transactionlogs.LogTransactionOutputPort;
import com.mwallet.mwalletaccounts.application.ports.out.transactions.*;
import com.mwallet.mwalletaccounts.domain.Transaction;
import com.mwallet.mwalletaccounts.domain.Transaction.TransactionType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

import static com.mwallet.mwalletaccounts.application.ports.in.transactions.SendMoneyUseCase.SendMoneyConstraints.getConstraints;
import static com.mwallet.mwalletaccounts.application.ports.out.transactions.TransactionCacheManagementOutputPort.*;
import static com.mwallet.mwalletaccounts.domain.Transaction.*;
import static com.mwallet.mwalletaccounts.domain.Transaction.ConfirmationStatus.*;
import static com.mwallet.mwalletaccounts.domain.Transaction.TransactionStatus.*;
import static com.mwallet.mwalletaccounts.domain.Transaction.TransactionType.*;
import static com.mwallet.mwalletcommons.notifications.NotificationType.TRANSACTION_DUPLICATE;


/**
 * @author lawrence
 * created 17/01/2020 at 08:08
 **/
@Slf4j
@Service
@Validated
@Transactional(rollbackFor = Exception.class, noRollbackFor = DuplicateTransactionException.class)
class SendMoneyService extends AbstractTransactionManager implements SendMoneyUseCase {

    private final TransactionCacheManagementOutputPort transactionCacheManagementOutputPort;
    private final LoadAccountOutputPort loadAccountOutputPort;
    private final LoadTransactionOutputPort loadTransactionOutputPort;
    private final SendTransactionNotificationOutputPort sendTransactionNotificationOutputPort;

    @Autowired
    public SendMoneyService(
            SaveTransactionOutputPort saveTransactionOutputPort, TransferFundsOutputPort transferFundsOutputPort,
            LogTransactionOutputPort logTransactionOutputPort, LoadTransactionOutputPort loadTransactionOutputPort,
            GenerateTransactionRefOutputPort generateTransactionRefOutputPort, PublishTransactionOutputPort publishTransactionOutputPort,
            LoadAccountOutputPort loadAccountOutputPort, TransactionCacheManagementOutputPort transactionCacheManagementOutputPort,
            SendTransactionNotificationOutputPort sendTransactionNotificationOutputPort) {
        super(
                saveTransactionOutputPort, transferFundsOutputPort, logTransactionOutputPort,
                loadTransactionOutputPort, generateTransactionRefOutputPort, publishTransactionOutputPort,
                sendTransactionNotificationOutputPort
        );

        this.transactionCacheManagementOutputPort = transactionCacheManagementOutputPort;
        this.loadAccountOutputPort = loadAccountOutputPort;
        this.loadTransactionOutputPort = loadTransactionOutputPort;
        this.sendTransactionNotificationOutputPort = sendTransactionNotificationOutputPort;
    }


    @Override
    public Pair<String, Transaction> validateSendMoney(@Valid Transaction transaction) {
        log.info("###################### START: ValidateSendMoney ########################");
        log.info("=======================================================================");
        log.info("incomingTransactionStatus: {}", transaction.getTransactionStatus());

        //1. ensure we are processing the correct transaction type
        checkTransactionType(transaction, SEND_MONEY);
        //2. ensure that we are generating a hash for a completely new transaction
        checkTransactionStatus(transaction, TransactionStatus.NONE);
        //3. global transaction validation
        validateTransaction(transaction);

        //4. store the transaction temporarily in a cache layer and return the corresponding hash
        String hash;
        try {
            hash = transactionCacheManagementOutputPort.saveTransaction(transaction);
        } catch (DuplicateTransactionException de) {
            sendTransactionNotificationOutputPort.send(transaction, TRANSACTION_DUPLICATE);
            throw de;
        }

        log.info("✅✅SendMoneyTransaction validated with hash: {}✅✅", hash);
        log.info("validatedTransactionStatus: {}", transaction.getTransactionStatus());
        log.info("========================================================================");
        log.info("####################### END: ValidateSendMoney #########################");
        return Pair.of(hash, transaction);
    }

    @Override
    public Transaction requestSendMoney(@Valid @NotBlank String hash) {
        log.info("###################### START: RequestSendMoney ########################");
        log.info("=======================================================================");
        log.info("transactionHash: {}", hash);

        //1. fetch transaction from cache layer using the given hash
        Transaction cachedTransactionRequest = transactionCacheManagementOutputPort.getTransactionByHash(hash);
        log.info("cachedTransactionStatus: {}", cachedTransactionRequest.getTransactionStatus());

        //2. update accounts to latest state before re-validation
        Transaction transaction = cachedTransactionRequest.toBuilder()
                .sourceAccount(loadAccountOutputPort.loadAccountById(cachedTransactionRequest.getSourceAccount().getAccountId()))
                .destinationAccount(loadAccountOutputPort.loadAccountById(cachedTransactionRequest.getDestinationAccount().getAccountId()))
                .build();

        //3. ensure we are processing the correct transaction type
        checkTransactionType(transaction, SEND_MONEY);
        //4. ensure that we are starting the transaction for a VALIDATED transaction
        checkTransactionStatus(transaction, VALIDATED);
        /*5. being asynchronous we must ensure the transaction is still valid
        long after the request was deemed valid by #validateTransaction and a hash issued*/
        validateTransaction(transaction);
        //6. start transaction and return the transaction with REF_NUMBER
        Transaction startedTransaction = startTransaction(transaction);
        //7. remove the cached transaction from cache
        transactionCacheManagementOutputPort.deleteByHash(hash);

        log.info("✅✅SendMoneyTransaction started with ref: {}✅✅", startedTransaction.getTransactionRef());
        log.info("startedTransactionStatus: {}", startedTransaction.getTransactionStatus());
        log.info("========================================================================");
        log.info("######################## END: RequestSendMoney #########################");
        return startedTransaction;
    }

    @Override
    public void prepareSendMoney(@Valid Transaction transaction) {
        log.info("####################### START: PrepareSendMoney ########################");
        log.info("=======================================================================");
        log.info("Freeze (amount+cost) from source account until transaction succeeds/fails");
        log.info("transactionRef: {}", transaction.getTransactionRef());
        log.info("incomingTransactionStatus: {}", transaction.getTransactionStatus());


        //1. ensure we are processing the correct transaction type
        checkTransactionType(transaction, SEND_MONEY);
        //2. ensure that we are preparing an ACCEPTED transaction
        checkTransactionStatus(transaction, ACCEPTED);
        //3. ensure the transaction status is in sync with the saved transaction
        transaction = synchronizeTransaction(transaction);
        //4. start transaction preparation
        Transaction preparedTransaction = prepareTransaction(transaction);

        log.info("✅✅SendMoneyTransaction prepared with ref: {}✅✅", preparedTransaction.getTransactionRef());
        log.info("preparedTransactionStatus: {}", preparedTransaction.getTransactionStatus());
        log.info("========================================================================");
        log.info("####################### END: PrepareSendMoney ##########################");
    }

    @Override
    public Transaction confirmSendMoney(
            @Valid @NotBlank String transactionRef,
            @Valid @NotNull ConfirmationStatus confirmationStatus,
            @Valid @NotBlank String narration) {
        log.info("####################### START: ConfirmSendMoney ########################");
        log.info("=======================================================================");
        log.info("Transfer (amount+cost) from source account and avail it on destination");

        //1. load transaction by ref
        Transaction transaction = loadTransactionOutputPort.loadTransactionByTransactionRef(transactionRef);
        log.info("transactionRef: {}", transaction.getTransactionRef());
        log.info("confirmationStatus: {}", confirmationStatus);
        log.info("narration: {}", narration);
        log.info("incomingTransactionStatus: {}", transaction.getTransactionStatus());

        //2. ensure we are processing the correct transaction type
        checkTransactionType(transaction, SEND_MONEY);
        //3. ensure that we are confirming a PREPARED transaction
        checkTransactionStatus(transaction, PREPARED);
        //4. confirm or rollback transaction
        Transaction savedTransaction;
        switch (confirmationStatus) {
            case PROCEED:
                savedTransaction = confirmTransaction(transaction, narration);
                log.info("✅✅SendMoneyTransaction confirmed with ref: {}✅✅", transaction.getTransactionRef());
                break;
            case ROLLBACK:
                savedTransaction = rollbackTransaction(transaction, narration);
                log.info("⚠️⚠️SendMoneyTransaction rolledBack with ref: {}⚠️⚠️", transaction.getTransactionRef());
                break;
            case NONE:
            default:
                throw new UnsupportedOperationException("Unsupported Transaction Confirmation Status: " + confirmationStatus);
        }


        log.info("savedTransactionStatus: {}", savedTransaction.getTransactionStatus());
        log.info("========================================================================");
        log.info("####################### END: ConfirmSendMoney ##########################");

        return savedTransaction;
    }

    @Override
    public void completeSendMoney(@Valid Transaction transaction) {
        log.info("####################### START: CompleteSendMoney ########################");
        log.info("=======================================================================");
        log.info("transactionRef: {}", transaction.getTransactionRef());
        log.info("incomingTransactionStatus: {}", transaction.getTransactionStatus());

        //1. ensure the transaction status is in sync with the saved transaction
        transaction = synchronizeTransaction(transaction);
        //2. ensure we are processing the correct transaction type
        checkTransactionType(transaction, SEND_MONEY);
        //3. ensure that we are completing a CONFIRMED transaction
        checkTransactionStatus(transaction, CONFIRMED);
        //4. complete transaction
        Transaction completedTransaction = completeTransaction(transaction);

        log.info("✅✅SendMoneyTransaction completed with ref: {}✅✅", transaction.getTransactionRef());
        log.info("completedTransactionStatus: {}", completedTransaction.getTransactionStatus());
        log.info("========================================================================");
        log.info("####################### END: CompleteSendMoney ##########################");
    }


    /**
     * {@inheritDoc}
     * Do {@link TransactionType#SEND_MONEY} transaction specific checks then call super
     *
     * @param transaction transaction to validate
     * @throws InvalidTransactionException if transaction is invalid
     */
    @Override
    protected void validateTransaction(@Valid Transaction transaction) throws InvalidTransactionException {

        //1. fetch constraints
        SendMoneyConstraints constraints = getConstraints(
                Pair.of(transaction.getSourceAccount().getAccountType(),
                        transaction.getDestinationAccount().getAccountType()
                )
        );

        //2. do sendMoney checks
        checkTransactionType(transaction, SEND_MONEY);
        checkSupportedPaymentModes(transaction, SendMoneyConstraints.supportedPaymentModes);
        checkSupportedAccountTypes(transaction, SendMoneyConstraints.supportedSrcDestAccountTypes);
        checkTransactionAmountInRange(
                transaction,
                BigDecimal.valueOf(constraints.getMinFundsTransferable()),
                BigDecimal.valueOf(constraints.getMaxFundsTransferable())
        );

        //3. do global transaction checks
        super.validateTransaction(transaction);
    }

}
