package com.mwallet.mwalletaccounts.application.services;


import com.mwallet.mwalletaccounts.application.ports.out.accounts.TransferFundsOutputPort;
import com.mwallet.mwalletaccounts.application.ports.out.transactionlogs.LogTransactionOutputPort;
import com.mwallet.mwalletaccounts.application.ports.out.transactions.*;
import com.mwallet.mwalletaccounts.domain.Account;
import com.mwallet.mwalletaccounts.domain.Transaction;
import com.mwallet.mwalletaccounts.domain.ValidateTransaction;
import com.mwallet.mwalletcommons.notifications.NotificationType;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.math.BigDecimal;

import static com.mwallet.mwalletaccounts.domain.Transaction.*;
import static com.mwallet.mwalletaccounts.domain.Transaction.ConfirmationStatus.*;
import static com.mwallet.mwalletaccounts.domain.Transaction.TransactionStatus.*;
import static com.mwallet.mwalletcommons.notifications.NotificationType.*;
import static com.mwallet.mwalletcommons.notifications.NotificationType.TRANSACTION_FAIL;


/**
 * @author lawrence
 * created 17/01/2020 at 08:00
 * <p>
 * NOTE: CLASSES EXTENDING THIS CLASS MUST USE SPRING TRANSACTIONS
 **/

@Slf4j
@Service
@RequiredArgsConstructor
public abstract class AbstractTransactionManager extends ValidateTransaction {

    private final SaveTransactionOutputPort saveTransactionOutputPort;
    private final TransferFundsOutputPort transferFundsOutputPort;
    private final LogTransactionOutputPort logTransactionOutputPort;
    private final LoadTransactionOutputPort loadTransactionOutputPort;
    private final GenerateTransactionRefOutputPort generateTransactionRefOutputPort;
    private final PublishTransactionOutputPort publishTransactionOutputPort;
    private final SendTransactionNotificationOutputPort sendTransactionNotificationOutputPort;

    /**
     * Validate the transaction details
     *
     * @param transaction transaction to validate
     * @throws InvalidTransactionException if transaction is invalid
     */
    protected void validateTransaction(@Valid Transaction transaction) throws InvalidTransactionException {

        checkCyclicTransaction(transaction);
        checkAccountsStatus(transaction);
        checkFundsAvailability(transaction);

        transaction.setTransactionStatus(VALIDATED);
    }

    /**
     * Execute transaction commence strategy <br>
     * Transaction Status: ACCEPTED
     *
     * @param transaction transaction to validate
     * @return started Transaction
     */
    protected Transaction startTransaction(@Valid Transaction transaction) {

        //generate ref number and accept transaction
        String transactionRef = generateTransactionRefOutputPort.generateTransactionRef(transaction);
        transaction.setTransactionRef(transactionRef);
        transaction.setTransactionStatus(ACCEPTED);

        //save transaction
        Transaction savedTransaction = saveTransactionOutputPort.createTransaction(transaction);

        //save transaction log
        logTransactionOutputPort.logTransaction(savedTransaction, "STARTED TXN: " + transactionRef);

        //publish transaction
        publishTransactionOutputPort.publishTransaction(savedTransaction);

        return savedTransaction;
    }

    /**
     * Execute transaction preparation strategy <br>
     * Transaction Status: PREPARED
     *
     * @param transaction transaction to prepare
     * @return prepared transaction
     */
    protected Transaction prepareTransaction(@Valid Transaction transaction) {

        //1. update accounts positions
        Pair<Account, Account> updatedAccounts = transferFundsOutputPort.prepareTransfer(
                transaction.getSourceAccount().getAccountId(),
                transaction.getDestinationAccount().getAccountId(),
                transaction.getAmount(), transaction.getTransactionCost()
        );

        //2. update account positions and mark transaction prepared
        transaction.setSourceAccount(updatedAccounts.getFirst());
        transaction.setDestinationAccount(updatedAccounts.getSecond());
        transaction.setTransactionStatus(PREPARED);

        //3. save transaction
        Transaction savedTransaction = saveTransactionOutputPort.updateTransaction(transaction);

        //4. save transaction log
        BigDecimal totalAmount = savedTransaction.getAmount().add(savedTransaction.getTransactionCost());
        String message = "ACCOUNTS: -SRC_AVAIL(" + totalAmount + "), " + "+DST_ACTUAL(" + savedTransaction.getAmount() + ")";
        logTransactionOutputPort.logTransaction(savedTransaction, message);
        log.info("transactionLog: {}", savedTransaction.getTransactionStatus() + ":: " + message);

        //5. publish transaction if we dont need external confirmation of funds movement
        if (savedTransaction.getConfirmationType() == ConfirmationType.INTERNAL) {
            publishTransactionOutputPort.publishTransaction(savedTransaction);
            log.info("published INTERNALLY confirmed transaction: {}", transaction.getTransactionRef());
        } else
            log.info("saved EXTERNALLY confirmed transaction awaiting confirmation/verification: {}", savedTransaction.getTransactionRef());

        return savedTransaction;
    }

    /**
     * Execute transaction processing strategy <br>
     * Transaction Status: PROCESSED
     *
     * @param transaction transaction to process
     */
    protected Transaction confirmTransaction(@Valid Transaction transaction, String narration) {

        //1. update accounts positions
        Pair<Account, Account> updatedAccounts = transferFundsOutputPort.confirmTransfer(
                transaction.getSourceAccount().getAccountId(),
                transaction.getDestinationAccount().getAccountId(),
                transaction.getAmount(), transaction.getTransactionCost()
        );

        //2. update account positions and mark transaction confirmed
        transaction.setSourceAccount(updatedAccounts.getFirst());
        transaction.setDestinationAccount(updatedAccounts.getSecond());
        transaction.setTransactionStatus(CONFIRMED);
        transaction.setConfirmationStatus(PROCEED);

        //3. save transaction
        Transaction savedTransaction = saveTransactionOutputPort.updateTransaction(transaction);

        //4. log transaction
        BigDecimal totalAmount = savedTransaction.getAmount().add(savedTransaction.getTransactionCost());
        String message = "ACCOUNTS: -SRC_ACTUAL(" + totalAmount + "), " + "+DST_AVAIL(" + savedTransaction.getAmount() + "), CONFIRMATION_NARRATION: " + narration;
        logTransactionOutputPort.logTransaction(savedTransaction, message);
        log.info("transactionLog: {}", savedTransaction.getTransactionStatus() + ":: " + message);

        //5. publish transaction
        publishTransactionOutputPort.publishTransaction(savedTransaction);
        log.info("published CONFIRMED transaction: {}", savedTransaction.getTransactionRef());

        return savedTransaction;
    }

    /**
     * Execute transaction rollback strategy
     * <br>
     * A transaction can only be rolled back if its in a PREPARED state
     *
     * @param transaction transaction to rollback
     */
    protected Transaction rollbackTransaction(@Valid Transaction transaction, String narration) {

        //1. update accounts positions
        Pair<Account, Account> updatedAccounts = transferFundsOutputPort.rollbackTransfer(
                transaction.getSourceAccount().getAccountId(),
                transaction.getDestinationAccount().getAccountId(),
                transaction.getAmount(), transaction.getTransactionCost()
        );

        //2. update account positions and mark transaction failed
        transaction.setSourceAccount(updatedAccounts.getFirst());
        transaction.setDestinationAccount(updatedAccounts.getSecond());
        transaction.setTransactionStatus(FAILED);
        transaction.setConfirmationStatus(ROLLBACK);

        //3. save transaction
        Transaction savedTransaction = saveTransactionOutputPort.updateTransaction(transaction);

        //4. log transaction
        BigDecimal totalAmount = savedTransaction.getAmount().add(savedTransaction.getTransactionCost());
        String message = "ACCOUNTS: +SRC_AVAIL(" + totalAmount + "), " + "-DST_ACTUAL(" + savedTransaction.getAmount() + "), ROLLBACK_NARRATION: " + narration;
        logTransactionOutputPort.logTransaction(savedTransaction, message);
        log.info("transactionLog: {}", savedTransaction.getTransactionStatus() + ":: " + message);

        //5. callback
        publishTransactionOutputPort.publishTransactionCallback(transaction);

        //6. notification
        sendTransactionNotificationOutputPort.send(transaction, TRANSACTION_FAIL);
        return savedTransaction;
    }

    /**
     * Execute transaction completion strategy
     * Transaction Status: COMPLETED
     *
     * @param transaction transaction to complete
     * @return completed transaction
     */
    protected Transaction completeTransaction(@Valid Transaction transaction) {

        //1. mark transaction complete
        transaction.setTransactionStatus(COMPLETED);

        //2. save transaction
        Transaction savedTransaction = saveTransactionOutputPort.updateTransaction(transaction);

        //3. log transaction
        String message = "COMPLETED TXN: " + transaction.getTransactionRef();
        logTransactionOutputPort.logTransaction(savedTransaction, message);
        log.info("transactionLog: {}", message);

        //4. callback
        publishTransactionOutputPort.publishTransactionCallback(transaction);

        //5. notification
        sendTransactionNotificationOutputPort.send(transaction, TRANSACTION_SUCCESS);
        return savedTransaction;
    }

    /**
     * Synchronize a stale transaction before any processing.
     * Ensure the transaction status matches the saved transaction.
     *
     * @param receivedTransaction transaction received from a source that is not the db eg broker,cache
     * @return synchronized transaction
     */
    protected Transaction synchronizeTransaction(Transaction receivedTransaction) {

        //1. fetch transaction
        Transaction loadedTransaction = loadTransactionOutputPort.loadTransactionByTransactionRef(receivedTransaction.getTransactionRef());

        log.info("synchronizeTransaction: [expectedStatus: {}, actualStatus: {}]", receivedTransaction.getTransactionStatus(), loadedTransaction.getTransactionStatus());

        //2. transaction status must be the same on both sides
        assert (receivedTransaction.getTransactionStatus() == loadedTransaction.getTransactionStatus()) : "INCONSISTENT TRANSACTION STATES: " + receivedTransaction.getTransactionStatus() + "(stale) | " + loadedTransaction.getTransactionStatus() + "(current)";

        //3. return transaction with latest account & txn positions
        return loadedTransaction;
    }

}
