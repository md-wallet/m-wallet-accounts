package com.mwallet.mwalletaccounts.application.services;


import com.mwallet.mwalletaccounts.application.ports.in.transactions.DepositMoneyUseCase;
import com.mwallet.mwalletaccounts.application.ports.out.accounts.LoadAccountOutputPort;
import com.mwallet.mwalletaccounts.application.ports.out.accounts.TransferFundsOutputPort;
import com.mwallet.mwalletaccounts.application.ports.out.payments.LoadPaymentCardOutputPort;
import com.mwallet.mwalletaccounts.application.ports.out.payments.PaymentsOutputPort;
import com.mwallet.mwalletaccounts.application.ports.out.transactionlogs.LogTransactionOutputPort;
import com.mwallet.mwalletaccounts.application.ports.out.transactions.*;
import com.mwallet.mwalletaccounts.domain.Payment;
import com.mwallet.mwalletaccounts.domain.PaymentCard;
import com.mwallet.mwalletaccounts.domain.Transaction;
import com.mwallet.mwalletaccounts.domain.Transaction.PaymentMode;
import com.mwallet.mwalletaccounts.domain.Transaction.TransactionType;
import com.mwallet.mwalletcommons.utils.ComparableUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import java.math.BigDecimal;

import static com.mwallet.mwalletaccounts.application.ports.out.transactions.TransactionCacheManagementOutputPort.*;
import static com.mwallet.mwalletaccounts.domain.Transaction.ConfirmationStatus.*;
import static com.mwallet.mwalletaccounts.domain.Transaction.PaymentMode.*;
import static com.mwallet.mwalletaccounts.domain.Transaction.TransactionStatus.*;
import static com.mwallet.mwalletaccounts.domain.Transaction.TransactionStatus.CONFIRMED;
import static com.mwallet.mwalletaccounts.domain.Transaction.TransactionStatus.NONE;
import static com.mwallet.mwalletaccounts.domain.Transaction.TransactionType.*;
import static com.mwallet.mwalletcommons.notifications.NotificationType.TRANSACTION_DUPLICATE;


/**
 * @author lawrence
 * created 01/02/2020 at 22:23
 **/
@Slf4j
@Service
@Transactional(rollbackFor = Exception.class, noRollbackFor = DuplicateTransactionException.class)
class DepositMoneyService extends AbstractTransactionManager implements DepositMoneyUseCase {

    private final TransactionCacheManagementOutputPort transactionCacheManagementOutputPort;
    private final LoadAccountOutputPort loadAccountOutputPort;
    private final SaveTransactionOutputPort saveTransactionOutputPort;
    private final LoadTransactionOutputPort loadTransactionOutputPort;
    private final LogTransactionOutputPort logTransactionOutputPort;
    private final PublishTransactionOutputPort publishTransactionOutputPort;
    private final PaymentsOutputPort paymentsOutputPort;
    private final SendTransactionNotificationOutputPort sendTransactionNotificationOutputPort;
    private final LoadPaymentCardOutputPort loadPaymentCardOutputPort;

    public DepositMoneyService(
            SaveTransactionOutputPort saveTransactionOutputPort, TransferFundsOutputPort transferFundsOutputPort,
            LogTransactionOutputPort logTransactionOutputPort, LoadTransactionOutputPort loadTransactionOutputPort,
            GenerateTransactionRefOutputPort generateTransactionRefOutputPort, PublishTransactionOutputPort publishTransactionOutputPort,
            TransactionCacheManagementOutputPort transactionCacheManagementOutputPort, LoadAccountOutputPort loadAccountOutputPort,
            PaymentsOutputPort paymentsOutputPort, SendTransactionNotificationOutputPort sendTransactionNotificationOutputPort, LoadPaymentCardOutputPort loadPaymentCardOutputPort) {
        super(
                saveTransactionOutputPort, transferFundsOutputPort, logTransactionOutputPort,
                loadTransactionOutputPort, generateTransactionRefOutputPort, publishTransactionOutputPort,
                sendTransactionNotificationOutputPort
        );

        this.transactionCacheManagementOutputPort = transactionCacheManagementOutputPort;
        this.loadAccountOutputPort = loadAccountOutputPort;
        this.saveTransactionOutputPort = saveTransactionOutputPort;
        this.loadTransactionOutputPort = loadTransactionOutputPort;
        this.logTransactionOutputPort = logTransactionOutputPort;
        this.publishTransactionOutputPort = publishTransactionOutputPort;
        this.paymentsOutputPort = paymentsOutputPort;
        this.sendTransactionNotificationOutputPort = sendTransactionNotificationOutputPort;
        this.loadPaymentCardOutputPort = loadPaymentCardOutputPort;
    }

    @Override
    public Pair<String, Transaction> validateDepositMoney(@Valid Transaction transaction) {
        log.info("###################### START: ValidateDepositMoney ########################");
        log.info("===========================================================================");
        log.info("incomingTransactionStatus: {}", transaction.getTransactionStatus());

        //1. ensure that we are generating a hash for a completely new transaction
        checkTransactionStatus(transaction, NONE);
        //2. execute global transaction validation
        validateTransaction(transaction);
        //3. store the transaction temporarily in a cache layer and return the corresponding hash
        String hash;
        try {
            hash = transactionCacheManagementOutputPort.saveTransaction(transaction);
        } catch (DuplicateTransactionException de) {
            sendTransactionNotificationOutputPort.send(transaction, TRANSACTION_DUPLICATE);
            throw de;
        }
        log.info("✅✅DepositMoneyTransaction validated with hash: {}✅✅", hash);
        log.info("validatedTransactionStatus: {}", transaction.getTransactionStatus());
        log.info("========================================================================");
        log.info("####################### END: ValidateDepositMoney #########################");
        return Pair.of(hash, transaction);
    }

    @Override
    public Transaction requestDepositMoney(@Valid String hash) {
        log.info("###################### START: RequestDepositMoney ########################");
        log.info("=======================================================================");
        log.info("transactionHash: {}", hash);

        //1. fetch transaction from cache layer using the given hash
        Transaction cachedTransactionRequest = transactionCacheManagementOutputPort.getTransactionByHash(hash);
        log.info("cachedTransactionStatus: {}", cachedTransactionRequest.getTransactionStatus());
        //2. refresh the account details
        Transaction transaction = cachedTransactionRequest.toBuilder()
                .sourceAccount(loadAccountOutputPort.loadAccountById(cachedTransactionRequest.getSourceAccount().getAccountId()))
                .destinationAccount(loadAccountOutputPort.loadAccountById(cachedTransactionRequest.getDestinationAccount().getAccountId()))
                .build();
        //3. ensure that we are starting the transaction for a VALIDATED transaction
        checkTransactionStatus(transaction, VALIDATED);
        /*4. being asynchronous we must ensure the transaction is still valid
        long after the request was deemed valid by #validateTransaction and a hash issued*/
        validateTransaction(transaction);
        //5. start transaction and return the transaction with REF_NUMBER
        Transaction startedTransaction = startTransaction(transaction);
        //6. remove the cached transaction from cache
        transactionCacheManagementOutputPort.deleteByHash(hash);

        log.info("✅✅DepositMoneyTransaction started with ref: {}✅✅", startedTransaction.getTransactionRef());
        log.info("startedTransactionStatus: {}", startedTransaction.getTransactionStatus());
        log.info("========================================================================");
        log.info("######################## END: RequestDepositMoney #########################");
        return startedTransaction;
    }

    @Override
    public void prepareDepositMoney(@Valid Transaction transaction) {
        log.info("####################### START: PrepareDepositMoney ########################");
        log.info("===========================================================================");
        log.info("Freeze (amount+cost) from source account until transaction succeeds/fails");
        log.info("transactionRef: {}", transaction.getTransactionRef());
        log.info("incomingTransactionStatus: {}", transaction.getTransactionStatus());

        //1. ensure we are processing the correct transaction type
        checkTransactionType(transaction, DEPOSIT);
        //2. ensure that we are preparing an ACCEPTED transaction
        checkTransactionStatus(transaction, ACCEPTED);
        //3. ensure the transaction status is in sync with the saved transaction
        transaction = synchronizeTransaction(transaction);
        //4. start transaction preparation
        Transaction preparedTransaction = prepareTransaction(transaction);

        log.info("✅✅DepositMoneyTransaction prepared with ref: {}✅✅", preparedTransaction.getTransactionRef());
        log.info("preparedTransactionStatus: {}", preparedTransaction.getTransactionStatus());
        log.info("========================================================================");
        log.info("####################### END: PrepareDepositMoney ##########################");
    }

    @Override
    public boolean requestDepositMoneyPayment(@Valid Transaction transaction) {
        log.info("####################### START: RequestDepositMoneyPayment ########################");
        log.info("===================================================================================");
        log.info("Request payments-service to acquire funds from client");
        log.info("transactionRef: {}", transaction.getTransactionRef());
        log.info("paymentMode: {}", transaction.getPaymentMode());
        log.info("billingAddress: {}", transaction.getBillingAddress());

        //1. ensure we are processing the correct transaction type
        checkTransactionType(transaction, DEPOSIT);
        //2. ensure that we are preparing a PREPARED transaction
        checkTransactionStatus(transaction, PREPARED);
        //3. ensure the transaction status is in sync with the saved transaction
        transaction = synchronizeTransaction(transaction);
        //4. request payment
        Payment.PaymentRequest paymentRequest = paymentsOutputPort.requestPayment(transaction);
        //5. update transaction status
        transaction.setTransactionStatus(PAYMENT_REQUESTED);
        //6. save transaction
        Transaction savedTransaction = saveTransactionOutputPort.updateTransaction(transaction);

        log.info("paymentRequestStatus: {}", paymentRequest.getPaymentRequestStatus());
        log.info("updatedTransactionStatus: {}", savedTransaction.getTransactionStatus());

        //7. log transaction
        logTransactionOutputPort.logTransaction(savedTransaction, paymentRequest.getNarration());
        log.info("transactionLog: {}", paymentRequest.getNarration());

        //8. publish or rollback depending on outcome
        boolean isPaymentRequestSuccessful;
        switch (paymentRequest.getPaymentRequestStatus()) {
            case PAYMENT_REQUEST_ACCEPTED:
                //9. confirm request went well and wait for async payment verification
                isPaymentRequestSuccessful = true;
                break;
            case PAYMENT_REQUEST_REJECTED:
                //9. confirm request went well but for some reason it was rejected so we need to rollback
                rollbackTransaction(savedTransaction, paymentRequest.getNarration());
                isPaymentRequestSuccessful = true;
                break;
            case PAYMENT_REQUEST_FAILED:
                //9. reset transaction status to PREPARED and return false indicating a payment request was not successful and should be retried
                savedTransaction.setTransactionStatus(PREPARED);
                savedTransaction = saveTransactionOutputPort.updateTransaction(savedTransaction);

                log.info("updatedTransactionStatus: {}", savedTransaction.getTransactionStatus());

                //10. log that we have returned the transaction to a prepared state for another request
                String message = "RESET_TRANSACTION TO PREPARED STATE & RETRY PAYMENT_REQUEST";
                logTransactionOutputPort.logTransaction(savedTransaction, message);

                log.info("transactionLog: {}", message);
                isPaymentRequestSuccessful = false;
                break;
            default:
                throw new UnsupportedOperationException("Unsupported PaymentRequest Status: " + paymentRequest.getPaymentRequestStatus());
        }
        log.info("isPaymentRequestSuccessful: {}", isPaymentRequestSuccessful);
        if (isPaymentRequestSuccessful)
            log.info("✅✅DepositMoneyTransaction paymentRequested SUCCESSFUL✅✅");
        else
            log.info("⚠️⚠️DepositMoneyTransaction paymentRequested FAILED⚠️⚠️");


        log.info("savedTransactionStatus: {}", savedTransaction.getTransactionStatus());
        log.info("========================================================================");
        log.info("####################### END: RequestDepositMoneyPayment ##########################");

        return isPaymentRequestSuccessful;
    }

    @Override
    public Transaction verifyDepositMoneyPayment(@Valid Payment.PaymentResponse paymentResponse) {
        log.info("####################### START: VerifyDepositMoneyPayment ########################");
        log.info("===================================================================================");
        log.info("payments-service confirm funds receipt from client");
        log.info("paymentMode: {}", paymentResponse.getPaymentMode());
        log.info("billingAddress: {}", paymentResponse.getBillingAddress());

        //1. fetch transaction by transactionRef
        Transaction transaction = loadTransactionOutputPort.loadTransactionByTransactionRef(paymentResponse.getTransactionRef());
        log.info("transactionRef: {}", transaction.getTransactionRef());

        //2. ensure we are processing the correct transaction type
        checkTransactionType(transaction, DEPOSIT);
        //3. ensure that we are verifying a PAYMENT_REQUESTED transaction
        checkTransactionStatus(transaction, PAYMENT_REQUESTED);
        //4. check payment response
        checkPaymentResponse(paymentResponse, transaction);
        //5. mark transaction status as PAYMENT_VERIFIED
        transaction.setTransactionStatus(PAYMENT_VERIFIED);
        //6. update confirmation status
        Transaction.ConfirmationStatus confirmationStatus;
        switch (paymentResponse.getPaymentResponseStatus()) {
            case PAYMENT_RESPONSE_VERIFIED:
                confirmationStatus = PROCEED;
                break;
            case PAYMENT_RESPONSE_FAILED:
                confirmationStatus = ROLLBACK;
                break;
            default:
                throw new UnsupportedOperationException("Unsupported PaymentResponse Status: " + paymentResponse.getPaymentResponseStatus());
        }
        transaction.setConfirmationStatus(confirmationStatus);
        //6. save transaction
        Transaction savedTransaction = saveTransactionOutputPort.updateTransaction(transaction);

        //7. log transaction
        logTransactionOutputPort.logTransaction(savedTransaction, paymentResponse.getNarration());
        log.info("transactionLog: {}", paymentResponse.getNarration());
        //8. publish transaction
        publishTransactionOutputPort.publishTransaction(savedTransaction);

        log.info("✅✅DepositMoneyTransaction verified✅✅");
        log.info("confirmationStatus: {}", confirmationStatus);
        log.info("savedTransactionStatus: {}", savedTransaction.getTransactionStatus());
        log.info("========================================================================");
        log.info("####################### END: VerifyDepositMoneyPayment ##########################");

        return savedTransaction;
    }

    @Override
    public Transaction confirmDepositMoney(@Valid Transaction transaction) {
        log.info("####################### START: ConfirmDepositMoney ########################");
        log.info("=======================================================================");
        log.info("Transfer amount from source account and avail it on destination");
        log.info("transactionRef: {}", transaction.getTransactionRef());
        log.info("confirmationStatus: {}", transaction.getConfirmationStatus());
        log.info("incomingTransactionStatus: {}", transaction.getTransactionStatus());

        //1. ensure we are processing the correct transaction type
        checkTransactionType(transaction, DEPOSIT);
        //2. ensure that we are preparing a payment verified transaction
        checkTransactionStatus(transaction, PAYMENT_VERIFIED);
        //3. ensure the transaction status is in sync with the saved transaction
        transaction = synchronizeTransaction(transaction);

        //4. confirm or rollback the transaction
        Transaction savedTransaction;
        switch (transaction.getConfirmationStatus()) {
            case PROCEED:
                savedTransaction = confirmTransaction(transaction, "IMPLICIT_PROCEED");
                break;
            case ROLLBACK:
                savedTransaction = rollbackTransaction(transaction, "IMPLICIT_ROLLBACK");
                break;
            case NONE:
            default:
                throw new UnsupportedOperationException("Unsupported Transaction Confirmation Status: " + transaction.getConfirmationStatus());
        }

        log.info("✅✅DepositMoneyTransaction confirmed✅✅");
        log.info("savedTransactionStatus: {}", savedTransaction.getTransactionStatus());
        log.info("========================================================================");
        log.info("####################### END: ConfirmDepositMoney ##########################");

        return savedTransaction;
    }

    @Override
    public void completeDepositMoney(@Valid Transaction transaction) {
        log.info("####################### START: CompleteDepositMoney ########################");
        log.info("============================================================================");
        log.info("transactionRef: {}", transaction.getTransactionRef());
        log.info("incomingTransactionStatus: {}", transaction.getTransactionStatus());

        //1. ensure we are processing the correct transaction type
        checkTransactionType(transaction, DEPOSIT);
        //2. ensure that we are completing a CONFIRMED transaction
        checkTransactionStatus(transaction, CONFIRMED);
        //3. ensure the transaction status is in sync with the saved transaction
        transaction = synchronizeTransaction(transaction);
        //4. complete transaction
        Transaction completedTransaction = completeTransaction(transaction);

        log.info("✅✅DepositMoneyTransaction completed with ref: {}✅✅", transaction.getTransactionRef());
        log.info("completedTransactionStatus: {}", completedTransaction.getTransactionStatus());
        log.info("============================================================================");
        log.info("####################### END: CompleteDepositMoney ##########################");
    }

    /**
     * {@inheritDoc}
     * Do {@link TransactionType#DEPOSIT} transaction specific checks then call super
     *
     * @param transaction transaction to validate
     * @throws InvalidTransactionException if transaction is invalid
     */
    @Override
    protected void validateTransaction(@Valid Transaction transaction) throws InvalidTransactionException {

        //1. fetch constraints
        DepositMoneyConstraints constraints = DepositMoneyConstraints.getConstraints(
                Pair.of(transaction.getSourceAccount().getAccountType(),
                        transaction.getDestinationAccount().getAccountType()
                )
        );

        //2. do depositMoney checks
        checkTransactionType(transaction, DEPOSIT);
        checkSupportedPaymentModes(transaction, DepositMoneyConstraints.supportedPaymentModes);
        checkSupportedAccountTypes(transaction, DepositMoneyConstraints.supportedSrcDestAccountTypes);
        checkTransactionAmountInRange(
                transaction,
                BigDecimal.valueOf(constraints.getMinFundsDeposit()),
                BigDecimal.valueOf(constraints.getMaxFundsDeposit())
        );
        assert (ComparableUtils.isEqualTo(transaction.getTransactionCost(), BigDecimal.ZERO)) : "DEPOSIT TRANSACTIONS SHOULD NOT HAVE A TXN COST";

        //3. check card if one is in use
        if (transaction.getPaymentMode() == CARD) {
            PaymentCard paymentCard = loadPaymentCardOutputPort.getPaymentCard(transaction.getBillingAddress()).orElse(null);
            checkPaymentCard(paymentCard);
        }

        //4. do global transaction checks
        super.validateTransaction(transaction);
    }


}
