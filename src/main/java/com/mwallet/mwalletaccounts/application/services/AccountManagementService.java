package com.mwallet.mwalletaccounts.application.services;


import com.mwallet.mwalletaccounts.application.ports.in.accounts.CreateAccountUseCase;
import com.mwallet.mwalletaccounts.application.ports.in.accounts.GetAccountUseCase;
import com.mwallet.mwalletaccounts.application.ports.in.accounts.UpdateAccountStatusUseCase;
import com.mwallet.mwalletaccounts.application.ports.out.accounts.LoadAccountOutputPort;
import com.mwallet.mwalletaccounts.application.ports.out.accounts.SaveAccountOutputPort;
import com.mwallet.mwalletaccounts.domain.Account;
import com.mwallet.mwalletcommons.data.ids.IDGenerator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import static com.mwallet.mwalletaccounts.application.ports.out.accounts.LoadAccountOutputPort.*;
import static com.mwallet.mwalletcommons.utils.ObjectUtils.writeJson;


/**
 * @author lawrence
 * created 18/12/2019 at 09:22
 **/
@Slf4j
@Service
@RequiredArgsConstructor
class AccountManagementService implements CreateAccountUseCase, GetAccountUseCase, UpdateAccountStatusUseCase {

    private final LoadAccountOutputPort loadAccountOutputPort;
    private final SaveAccountOutputPort saveAccountOutputPort;

    @Override
    public Account createAccount(@Valid Account account) throws AccountAlreadyExistsException {
        log.info("========== Register Account: {} ==========", writeJson(account));

        if (loadAccountOutputPort.existsByLinkIdAndAccountType(account.getLinkId(), account.getAccountType()))
            throw new AccountAlreadyExistsException("Account already registered", HttpStatus.BAD_REQUEST);

        Account createdAccount = saveAccountOutputPort.createAccount(account);
        String accountId = IDGenerator.generateAccountId(createdAccount.getId());
        createdAccount.setAccountId(accountId);
        return saveAccountOutputPort.updateAccount(createdAccount);
    }

    @Override
    public Account updateAccountStatus(@NotNull String accountId, @Valid @NotNull Account.AccountStatus accountStatus) {
        log.info("========== Update Account Status: {}:{} ==========", accountId, accountStatus);

        Account account = loadAccountOutputPort.loadAccountById(accountId);
        account.setAccountStatus(accountStatus);
        return saveAccountOutputPort.updateAccount(account);
    }

    @Override
    public Account getAccountById(@Valid @NotNull String accountId) throws AccountNotFoundException {
        return loadAccountOutputPort.loadAccountById(accountId);
    }
}
