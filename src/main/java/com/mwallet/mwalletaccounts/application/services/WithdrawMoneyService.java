package com.mwallet.mwalletaccounts.application.services;


import com.mwallet.mwalletaccounts.application.ports.in.transactions.WithdrawMoneyUseCase;
import com.mwallet.mwalletaccounts.application.ports.out.accounts.LoadAccountOutputPort;
import com.mwallet.mwalletaccounts.application.ports.out.accounts.TransferFundsOutputPort;
import com.mwallet.mwalletaccounts.application.ports.out.payments.PaymentsOutputPort;
import com.mwallet.mwalletaccounts.application.ports.out.transactionlogs.LogTransactionOutputPort;
import com.mwallet.mwalletaccounts.application.ports.out.transactions.*;
import com.mwallet.mwalletaccounts.domain.Payout;
import com.mwallet.mwalletaccounts.domain.Transaction;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.math.BigDecimal;


import static com.mwallet.mwalletaccounts.domain.Transaction.ConfirmationStatus.PROCEED;
import static com.mwallet.mwalletaccounts.domain.Transaction.ConfirmationStatus.ROLLBACK;
import static com.mwallet.mwalletaccounts.domain.Transaction.TransactionStatus.*;
import static com.mwallet.mwalletaccounts.domain.Transaction.TransactionType.WITHDRAW;
import static com.mwallet.mwalletaccounts.application.ports.out.transactions.TransactionCacheManagementOutputPort.*;
import static com.mwallet.mwalletcommons.notifications.NotificationType.TRANSACTION_DUPLICATE;

/**
 * @author lawrence
 * created 11/02/2020 at 11:47
 **/
@Slf4j
@Service
@Validated
@Transactional(rollbackFor = Exception.class,noRollbackFor= DuplicateTransactionException.class)
class WithdrawMoneyService extends AbstractTransactionManager implements WithdrawMoneyUseCase {

    private final TransactionCacheManagementOutputPort transactionCacheManagementOutputPort;
    private final LoadAccountOutputPort loadAccountOutputPort;
    private final PaymentsOutputPort paymentsOutputPort;
    private final SaveTransactionOutputPort saveTransactionOutputPort;
    private final LogTransactionOutputPort logTransactionOutputPort;
    private final LoadTransactionOutputPort loadTransactionOutputPort;
    private final PublishTransactionOutputPort publishTransactionOutputPort;
    private final SendTransactionNotificationOutputPort sendTransactionNotificationOutputPort;

    public WithdrawMoneyService(
            SaveTransactionOutputPort saveTransactionOutputPort, TransferFundsOutputPort transferFundsOutputPort,
            LogTransactionOutputPort logTransactionOutputPort, LoadTransactionOutputPort loadTransactionOutputPort,
            GenerateTransactionRefOutputPort generateTransactionRefOutputPort, PublishTransactionOutputPort publishTransactionOutputPort,
            TransactionCacheManagementOutputPort transactionCacheManagementOutputPort, LoadAccountOutputPort loadAccountOutputPort,
            PaymentsOutputPort paymentsOutputPort,
            SendTransactionNotificationOutputPort sendTransactionNotificationOutputPort) {
        super(
                saveTransactionOutputPort, transferFundsOutputPort, logTransactionOutputPort,
                loadTransactionOutputPort, generateTransactionRefOutputPort, publishTransactionOutputPort,
                sendTransactionNotificationOutputPort
        );
        this.transactionCacheManagementOutputPort = transactionCacheManagementOutputPort;
        this.loadAccountOutputPort = loadAccountOutputPort;
        this.paymentsOutputPort = paymentsOutputPort;
        this.saveTransactionOutputPort = saveTransactionOutputPort;
        this.logTransactionOutputPort = logTransactionOutputPort;
        this.loadTransactionOutputPort = loadTransactionOutputPort;
        this.publishTransactionOutputPort = publishTransactionOutputPort;
        this.sendTransactionNotificationOutputPort = sendTransactionNotificationOutputPort;
    }

    @Override
    public Pair<String, Transaction> validateWithdrawMoney(@Valid Transaction transaction) {
        log.info("###################### START: ValidateWithdrawMoney ########################");
        log.info("=============================================================================");
        log.info("incomingTransactionStatus: {}", transaction.getTransactionStatus());

        //1. ensure we are processing the correct transaction type
        checkTransactionType(transaction, WITHDRAW);
        //2. ensure that we are generating a hash for a completely new transaction
        checkTransactionStatus(transaction, NONE);
        //3. execute global transaction validation
        validateTransaction(transaction);
        //4. store the transaction temporarily in a cache layer and return the corresponding hash
        String hash;
        try {
            hash = transactionCacheManagementOutputPort.saveTransaction(transaction);
        } catch (DuplicateTransactionException de) {
            sendTransactionNotificationOutputPort.send(transaction, TRANSACTION_DUPLICATE);
            throw de;
        }

        log.info("✅✅WithdrawMoneyTransaction validated with hash: {}✅✅", hash);
        log.info("validatedTransactionStatus: {}", transaction.getTransactionStatus());
        log.info("========================================================================");
        log.info("####################### END: ValidateWithdrawMoney #########################");
        return Pair.of(hash, transaction);
    }

    @Override
    public Transaction requestWithdrawMoney(@Valid String hash) {
        log.info("###################### START: RequestWithdrawMoney ########################");
        log.info("=======================================================================");
        log.info("transactionHash: {}", hash);

        //1. fetch transaction from cache layer using the given hash
        Transaction cachedTransactionRequest = transactionCacheManagementOutputPort.getTransactionByHash(hash);
        log.info("cachedTransactionStatus: {}", cachedTransactionRequest.getTransactionStatus());
        //2. refresh the account details
        Transaction transaction = cachedTransactionRequest.toBuilder()
                .sourceAccount(loadAccountOutputPort.loadAccountById(cachedTransactionRequest.getSourceAccount().getAccountId()))
                .destinationAccount(loadAccountOutputPort.loadAccountById(cachedTransactionRequest.getDestinationAccount().getAccountId()))
                .build();
        //3. ensure we are processing the correct transaction type
        checkTransactionType(transaction, WITHDRAW);
        //4. ensure that we are starting the transaction for a VALIDATED transaction
        checkTransactionStatus(transaction, VALIDATED);
        /*5. being asynchronous we must ensure the transaction is still valid
        long after the request was deemed valid by #validateTransaction and a hash issued*/
        validateTransaction(transaction);
        //6. start transaction and return the transaction with REF_NUMBER
        Transaction startedTransaction = startTransaction(transaction);
        //7. remove the cached transaction from cache
        transactionCacheManagementOutputPort.deleteByHash(hash);

        log.info("✅✅WithdrawMoneyTransaction started with ref: {}✅✅", startedTransaction.getTransactionRef());
        log.info("startedTransactionStatus: {}", startedTransaction.getTransactionStatus());
        log.info("========================================================================");
        log.info("######################## END: RequestWithdrawMoney #########################");
        return startedTransaction;
    }

    @Override
    public void prepareWithdrawMoney(@Valid Transaction transaction) {
        log.info("####################### START: PrepareWithdrawMoney ########################");
        log.info("===========================================================================");
        log.info("Freeze (amount+cost) from source account until transaction succeeds/fails");
        log.info("transactionRef: {}", transaction.getTransactionRef());
        log.info("incomingTransactionStatus: {}", transaction.getTransactionStatus());

        //1. ensure we are processing the correct transaction type
        checkTransactionType(transaction, WITHDRAW);
        //2. ensure that we are preparing an ACCEPTED transaction
        checkTransactionStatus(transaction, ACCEPTED);
        //3. ensure the transaction status is in sync with the saved transaction
        transaction = synchronizeTransaction(transaction);
        //4. start transaction preparation
        Transaction preparedTransaction = prepareTransaction(transaction);

        log.info("✅✅WithdrawMoneyTransaction prepared with ref: {}✅✅", preparedTransaction.getTransactionRef());
        log.info("preparedTransactionStatus: {}", preparedTransaction.getTransactionStatus());
        log.info("========================================================================");
        log.info("####################### END: PrepareWithdrawMoney ##########################");
    }

    @Override
    public boolean requestWithdrawMoneyPayout(@Valid Transaction transaction) {
        log.info("####################### START: RequestWithdrawMoneyPayout ########################");
        log.info("===================================================================================");
        log.info("Request payments-service to send funds to client");
        log.info("transactionRef: {}", transaction.getTransactionRef());
        log.info("paymentMode: {}", transaction.getPaymentMode());
        log.info("billingAddress: {}", transaction.getBillingAddress());

        //1. ensure we are processing the correct transaction type
        checkTransactionType(transaction, WITHDRAW);
        //2. ensure that we are preparing a PREPARED transaction
        checkTransactionStatus(transaction, PREPARED);
        //3. ensure the transaction status is in sync with the saved transaction
        transaction = synchronizeTransaction(transaction);
        //4 request payout
        Payout.PayoutRequest payoutRequest = paymentsOutputPort.requestPayout(transaction);
        //5. update transaction status
        transaction.setTransactionStatus(PAYOUT_REQUESTED);
        //6. save transaction
        Transaction savedTransaction = saveTransactionOutputPort.updateTransaction(transaction);

        log.info("payoutRequestStatus: {}", payoutRequest.getPayoutRequestStatus());
        log.info("updatedTransactionStatus: {}", savedTransaction.getTransactionStatus());

        //7. log transaction
        logTransactionOutputPort.logTransaction(savedTransaction, payoutRequest.getNarration());
        log.info("transactionLog: {}", payoutRequest.getNarration());

        //8. publish or rollback depending on outcome
        boolean isPayoutRequestSuccessful;
        switch (payoutRequest.getPayoutRequestStatus()) {
            case PAYOUT_REQUEST_ACCEPTED:
                //9. confirm request went well and wait for async payout verification
                isPayoutRequestSuccessful = true;
                break;
            case PAYOUT_REQUEST_REJECTED:
                //9. confirm request went well but for some reason it was rejected so we need to rollback
                rollbackTransaction(savedTransaction, payoutRequest.getNarration());
                isPayoutRequestSuccessful = true;
                break;
            case PAYOUT_REQUEST_FAILED:
                //9. reset transaction status to PREPARED and return false indicating a payout request was not successful and should be retried
                savedTransaction.setTransactionStatus(PREPARED);
                savedTransaction = saveTransactionOutputPort.updateTransaction(savedTransaction);

                log.info("updatedTransactionStatus: {}", savedTransaction.getTransactionStatus());

                //10. log that we have returned the transaction to a prepared state for another request
                String message = "RESET_TRANSACTION TO PREPARED STATE & RETRY PAYOUT_REQUEST";
                logTransactionOutputPort.logTransaction(savedTransaction, message);
                log.info("transactionLog: {}", message);
                isPayoutRequestSuccessful = false;
                break;
            default:
                throw new UnsupportedOperationException("Unsupported PayoutRequest Status: " + payoutRequest.getPayoutRequestStatus());
        }
        log.info("isPayoutRequestSuccessful: {}", isPayoutRequestSuccessful);
        if (isPayoutRequestSuccessful)
            log.info("✅✅WithdrawMoneyTransaction payoutRequested SUCCESSFUL✅✅");
        else
            log.info("⚠️⚠️WithdrawMoneyTransaction payoutRequested FAILED⚠️⚠️");


        log.info("savedTransactionStatus: {}", savedTransaction.getTransactionStatus());
        log.info("========================================================================");
        log.info("####################### END: RequestWithdrawMoneyPayout ##########################");

        return isPayoutRequestSuccessful;
    }

    @Override
    public Transaction verifyWithdrawMoneyPayout(Payout.@Valid PayoutResponse payoutResponse) {
        log.info("####################### START: VerifyWithdrawMoneyPayout ########################");
        log.info("===================================================================================");
        log.info("payments-service confirm funds receipt by client");
        log.info("paymentMode: {}", payoutResponse.getPaymentMode());
        log.info("billingAddress: {}", payoutResponse.getBillingAddress());

        //1. fetch transaction by transactionRef
        Transaction transaction = loadTransactionOutputPort.loadTransactionByTransactionRef(payoutResponse.getTransactionRef());
        log.info("transactionRef: {}", transaction.getTransactionRef());

        //2. ensure we are processing the correct transaction type
        checkTransactionType(transaction, WITHDRAW);
        //3. ensure that we are verifying a PAYOUT_REQUESTED transaction
        checkTransactionStatus(transaction, PAYOUT_REQUESTED);
        //4. check payout response
        checkPayoutResponse(payoutResponse, transaction);
        //5. mark transaction state as PAYOUT_VERIFIED
        transaction.setTransactionStatus(PAYOUT_VERIFIED);
        //6. update confirmation status
        Transaction.ConfirmationStatus confirmationStatus;
        switch (payoutResponse.getPayoutResponseStatus()) {
            case PAYOUT_RESPONSE_VERIFIED:
                confirmationStatus = PROCEED;
                break;
            case PAYOUT_RESPONSE_FAILED:
                confirmationStatus = ROLLBACK;
                break;
            default:
                throw new UnsupportedOperationException("Unsupported PayoutResponse Status: " + payoutResponse.getPayoutResponseStatus());
        }
        transaction.setConfirmationStatus(confirmationStatus);
        //6. save transaction
        Transaction savedTransaction = saveTransactionOutputPort.updateTransaction(transaction);
        //7. log transaction
        logTransactionOutputPort.logTransaction(savedTransaction, payoutResponse.getNarration());
        log.info("transactionLog: {}", payoutResponse.getNarration());
        //8. publish transaction
        publishTransactionOutputPort.publishTransaction(savedTransaction);

        log.info("✅✅WithdrawMoneyTransaction verified✅✅");
        log.info("confirmationStatus: {}", confirmationStatus);
        log.info("savedTransactionStatus: {}", savedTransaction.getTransactionStatus());
        log.info("========================================================================");
        log.info("####################### END: VerifyWithdrawMoneyPayout ##########################");

        return savedTransaction;
    }

    @Override
    public Transaction confirmWithdrawMoney(@Valid Transaction transaction) {
        log.info("####################### START: ConfirmWithdrawMoney ########################");
        log.info("=======================================================================");
        log.info("Transfer amount from source account and avail it on destination");
        log.info("transactionRef: {}", transaction.getTransactionRef());
        log.info("confirmationStatus: {}", transaction.getConfirmationStatus());
        log.info("incomingTransactionStatus: {}", transaction.getTransactionStatus());

        //1. ensure we are processing the correct transaction type
        checkTransactionType(transaction, WITHDRAW);
        //2. ensure that we are preparing a payout verified transaction
        checkTransactionStatus(transaction, PAYOUT_VERIFIED);
        //3. ensure the transaction status is in sync with the saved transaction
        transaction = synchronizeTransaction(transaction);

        //4. confirm or rollback the transaction
        Transaction savedTransaction;
        switch (transaction.getConfirmationStatus()) {
            case PROCEED:
                savedTransaction = confirmTransaction(transaction, "IMPLICIT_PROCEED");
                break;
            case ROLLBACK:
                savedTransaction = rollbackTransaction(transaction, "IMPLICIT_ROLLBACK");
                break;
            case NONE:
            default:
                throw new UnsupportedOperationException("Unsupported Transaction Confirmation Status: " + transaction.getConfirmationStatus());
        }

        log.info("✅✅WithdrawMoneyTransaction confirmed✅✅");
        log.info("savedTransactionStatus: {}", savedTransaction.getTransactionStatus());
        log.info("========================================================================");
        log.info("####################### END: ConfirmWithdrawMoney ##########################");

        return savedTransaction;
    }

    @Override
    public void completeWithdrawMoney(@Valid Transaction transaction) {
        log.info("####################### START: CompleteWithdrawMoney ########################");
        log.info("============================================================================");
        log.info("transactionRef: {}", transaction.getTransactionRef());
        log.info("incomingTransactionStatus: {}", transaction.getTransactionStatus());

        //1. ensure we are processing the correct transaction type
        checkTransactionType(transaction, WITHDRAW);
        //2. ensure that we are completing a CONFIRMED transaction
        checkTransactionStatus(transaction, CONFIRMED);
        //3. ensure the transaction status is in sync with the saved transaction
        transaction = synchronizeTransaction(transaction);
        //4. complete transaction
        Transaction completedTransaction = completeTransaction(transaction);

        log.info("✅✅WithdrawMoneyTransaction completed with ref: {}✅✅", transaction.getTransactionRef());
        log.info("completedTransactionStatus: {}", completedTransaction.getTransactionStatus());
        log.info("============================================================================");
        log.info("####################### END: CompleteWithdrawMoney ##########################");
    }

    @Override
    protected void validateTransaction(@Valid Transaction transaction) throws InvalidTransactionException {
        //1. fetch constraints
        WithdrawMoneyConstraints constraints = WithdrawMoneyConstraints.getConstraints(
                Pair.of(transaction.getSourceAccount().getAccountType(),
                        transaction.getDestinationAccount().getAccountType()
                )
        );

        //2. do withdrawMoney checks
        checkTransactionType(transaction, WITHDRAW);
        checkSupportedPaymentModes(transaction, WithdrawMoneyConstraints.supportedPaymentModes);
        checkSupportedAccountTypes(transaction, WithdrawMoneyConstraints.supportedSrcDestAccountTypes);
        checkTransactionAmountInRange(
                transaction,
                BigDecimal.valueOf(constraints.getMinFundsWithdraw()),
                BigDecimal.valueOf(constraints.getMaxFundsWithdraw())
        );

        //3. do global transaction checks
        super.validateTransaction(transaction);
    }
}
