# MWallet Accounts

![pipeline](https://gitlab.com/md-wallet/m-wallet-accounts/badges/master/pipeline.svg)

MWallet Accounts, is the core of m-wallet and is responsible for validating and executing funds
transfers and general book keeping on the platform.

## Summary
* **Transaction Types:** `DEPOSIT, WITHDRAW, SEND_MONEY`
     
* **PaymentModes:** `WALLET, MPESA, CARD, BANK`

## Transaction Flow

Deposit Transaction Flow is given as a sample flow:

* ##### `Pair<String, Transaction> validateDepositMoney(@Valid Transaction transaction)`

    Validate deposit transaction, compute a hash of the validated transaction and cache this in a key value store (redis)

* ##### `Transaction requestDepositMoney(@Valid String hash)`
    The requesting client/service after confirmation of transaction details submits the earlier generated hash to begin the transaction     

* ##### `void prepareDepositMoney(@Valid Transaction transaction)`         
    Reserve Funds on the source account available balance and realize them on the destination actual balance     

* ##### `boolean requestDepositMoneyPayment(@Valid Transaction transaction)`
    Call external service (m-wallet-payments) to collect funds from the user depending on the `PaymentMode`. Depending
    on the outcome of this request, the transaction preparation above maybe rolled back or the transaction
    moved on awaiting verification of receipt of funds.

* ##### `Transaction verifyDepositMoneyPayment(@Valid PaymentResponse paymentResponse)`
    Asynchronously respond to the PaymentResponse, which may be positive or negative. If negative, `ROLLBACK` the preparation,
    positive the transaction is moved on for confirmation.
 
* ##### `Transaction confirmDepositMoney(@Valid Transaction transaction)`
    Reserved Funds on the source account are now subtracted from the actual balance and added to the destination available balance

* ##### `void completeDepositMoney(@Valid Transaction transaction)`
    Send notifications to concerned parties and execute callbacks if any.


## Note

This service is still in development with a number of `TODOs`:
* Oauth2 auth-server (m-waller-auth)
* Dedicated payments service (m-wallet-payments)
* Swagger Documentation for all endpoints
* Deployment
    
## Copyright

* Copyright (C) Lawrence Mwaniki - All Rights Reserved
 * Unauthorized copying of this project, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Lawrence Mwaniki <kilailawrence@gmail.com>, May 2020
 

   
       